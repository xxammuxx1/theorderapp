<?php

/* default/template/checkout/cart.twig */
class __TwigTemplate_651bef4c6d12415998c3e55b7c08321d77f2cb514eab79fae04c43f55f96c74b extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div id=\"checkout-cart\" class=\"container\">
  <ul class=\"breadcrumb\">
    ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 5
            echo "      <li class=\"breadcrumb-item\"><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", []);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", []);
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "  </ul>
  ";
        // line 8
        if (($context["attention"] ?? null)) {
            // line 9
            echo "    <div class=\"alert alert-info\"><i class=\"fas fa-info-circle\"></i> ";
            echo ($context["attention"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
  ";
        }
        // line 13
        echo "  ";
        if (($context["success"] ?? null)) {
            // line 14
            echo "    <div class=\"alert alert-success alert-dismissible\"><i class=\"fas fa-check-circle\"></i> ";
            echo ($context["success"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
  ";
        }
        // line 18
        echo "  ";
        if (($context["error_warning"] ?? null)) {
            // line 19
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fas fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
  ";
        }
        // line 23
        echo "  <div class=\"row\">";
        echo ($context["column_left"] ?? null);
        echo "
    <div id=\"content\" class=\"col\">";
        // line 24
        echo ($context["content_top"] ?? null);
        echo "
      <h1>";
        // line 25
        echo ($context["heading_title"] ?? null);
        echo "
        ";
        // line 26
        if (($context["weight"] ?? null)) {
            // line 27
            echo "          &nbsp;(";
            echo ($context["weight"] ?? null);
            echo ")
        ";
        }
        // line 28
        echo " </h1>
      <form action=\"";
        // line 29
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\">
        <div class=\"table-responsive\">
          <table class=\"table table-bordered\">
            <thead>
              <tr>
                <td class=\"text-center\">";
        // line 34
        echo ($context["column_image"] ?? null);
        echo "</td>
                <td class=\"text-left\">";
        // line 35
        echo ($context["column_name"] ?? null);
        echo "</td>
                <td class=\"text-left\">";
        // line 36
        echo ($context["column_model"] ?? null);
        echo "</td>
                <td class=\"text-left\">";
        // line 37
        echo ($context["column_quantity"] ?? null);
        echo "</td>
                <td class=\"text-right\">";
        // line 38
        echo ($context["column_price"] ?? null);
        echo "</td>
                <td class=\"text-right\">";
        // line 39
        echo ($context["column_total"] ?? null);
        echo "</td>
              </tr>
            </thead>
            <tbody>
              ";
        // line 43
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 44
            echo "                <tr>
                  <td class=\"text-center\">";
            // line 45
            if (twig_get_attribute($this->env, $this->source, $context["product"], "thumb", [])) {
                echo " <a href=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", []);
                echo "\"><img src=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "thumb", []);
                echo "\" alt=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", []);
                echo "\" title=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", []);
                echo "\" class=\"img-thumbnail\"/></a> ";
            }
            echo "</td>
                  <td class=\"text-left\"><a href=\"";
            // line 46
            echo twig_get_attribute($this->env, $this->source, $context["product"], "href", []);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "name", []);
            echo "</a> ";
            if ( !twig_get_attribute($this->env, $this->source, $context["product"], "stock", [])) {
                echo " <span class=\"text-danger\">***</span> ";
            }
            // line 47
            echo "                    ";
            if (twig_get_attribute($this->env, $this->source, $context["product"], "option", [])) {
                // line 48
                echo "                      ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["product"], "option", []));
                foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                    // line 49
                    echo "                        <br/>
                        <small>";
                    // line 50
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", []);
                    echo ": ";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "value", []);
                    echo "</small>
                      ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 52
                echo "                    ";
            }
            // line 53
            echo "                    ";
            if (twig_get_attribute($this->env, $this->source, $context["product"], "reward", [])) {
                // line 54
                echo "                      <br/>
                      <small>";
                // line 55
                echo twig_get_attribute($this->env, $this->source, $context["product"], "reward", []);
                echo "</small>
                    ";
            }
            // line 57
            echo "                    ";
            if (twig_get_attribute($this->env, $this->source, $context["product"], "recurring", [])) {
                // line 58
                echo "                      <br/>
                      <span class=\"label label-info\">";
                // line 59
                echo ($context["text_recurring_item"] ?? null);
                echo "</span>
                      <small>";
                // line 60
                echo twig_get_attribute($this->env, $this->source, $context["product"], "recurring", []);
                echo "</small>
                    ";
            }
            // line 61
            echo "</td>
                  <td class=\"text-left\">";
            // line 62
            echo twig_get_attribute($this->env, $this->source, $context["product"], "model", []);
            echo "</td>
                  <td class=\"text-left\">
                    <div class=\"input-group\">
                      <input type=\"text\" name=\"quantity[";
            // line 65
            echo twig_get_attribute($this->env, $this->source, $context["product"], "cart_id", []);
            echo "]\" value=\"";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "quantity", []);
            echo "\" size=\"1\" class=\"form-control\">
                      <div class=\"input-group-append\">
                        <button type=\"submit\" data-toggle=\"tooltip\" title=\"";
            // line 67
            echo ($context["button_update"] ?? null);
            echo "\" class=\"btn btn-primary\"><i class=\"fas fa-sync-alt\"></i></button>
                        <button type=\"button\" data-toggle=\"tooltip\" title=\"";
            // line 68
            echo ($context["button_remove"] ?? null);
            echo "\" class=\"btn btn-danger\" onclick=\"cart.remove('";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "cart_id", []);
            echo "');\"><i class=\"fas fa-times-circle\"></i></button>
                      </div>
                    </div>
                  </td>
                  <td class=\"text-right\">";
            // line 72
            echo twig_get_attribute($this->env, $this->source, $context["product"], "price", []);
            echo "</td>
                  <td class=\"text-right\">";
            // line 73
            echo twig_get_attribute($this->env, $this->source, $context["product"], "total", []);
            echo "</td>
                </tr>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 76
        echo "              ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["vouchers"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["voucher"]) {
            // line 77
            echo "                <tr>
                  <td></td>
                  <td class=\"text-left\">";
            // line 79
            echo twig_get_attribute($this->env, $this->source, $context["voucher"], "description", []);
            echo "</td>
                  <td class=\"text-left\"></td>
                  <td class=\"text-left\">
                    <div class=\"input-group\">
                      <input type=\"text\" name=\"\" value=\"1\" size=\"1\" disabled=\"disabled\" class=\"form-control\"/>
                      <div class=\"input-group-append\">
                        <button type=\"button\" data-toggle=\"tooltip\" title=\"";
            // line 85
            echo ($context["button_remove"] ?? null);
            echo "\" class=\"btn btn-danger\" onclick=\"voucher.remove('";
            echo twig_get_attribute($this->env, $this->source, $context["voucher"], "key", []);
            echo "');\"><i class=\"fas fa-times-circle\"></i></button>
                      </div>
                    </div>
                  </td>
                  <td class=\"text-right\">";
            // line 89
            echo twig_get_attribute($this->env, $this->source, $context["voucher"], "amount", []);
            echo "</td>
                  <td class=\"text-right\">";
            // line 90
            echo twig_get_attribute($this->env, $this->source, $context["voucher"], "amount", []);
            echo "</td>
                </tr>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['voucher'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 93
        echo "            </tbody>
          </table>
        </div>
      </form>
      ";
        // line 97
        if (($context["modules"] ?? null)) {
            // line 98
            echo "        <h2>";
            echo ($context["text_next"] ?? null);
            echo "</h2>
        <p>";
            // line 99
            echo ($context["text_next_choice"] ?? null);
            echo "</p>
        <div id=\"accordion\">
          ";
            // line 101
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["modules"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                // line 102
                echo "            ";
                echo $context["module"];
                echo "
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 104
            echo "        </div>
      ";
        }
        // line 106
        echo "      <br/>
      <div class=\"row\">
        <div class=\"col-md-4 offset-md-8\">
          <table class=\"table table-bordered\">
            ";
        // line 110
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["totals"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["total"]) {
            // line 111
            echo "              <tr>
                <td class=\"text-right\"><strong>";
            // line 112
            echo twig_get_attribute($this->env, $this->source, $context["total"], "title", []);
            echo ":</strong></td>
                <td class=\"text-right\">";
            // line 113
            echo twig_get_attribute($this->env, $this->source, $context["total"], "text", []);
            echo "</td>
              </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['total'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 116
        echo "          </table>
        </div>
      </div>
      <div class=\"d-inline-block pt-2 pd-2 w-100\">
        <div class=\"float-left\"><a href=\"";
        // line 120
        echo ($context["continue"] ?? null);
        echo "\" class=\"btn btn-light\">";
        echo ($context["button_shopping"] ?? null);
        echo "</a></div>
        <div class=\"float-right\"><a href=\"";
        // line 121
        echo ($context["checkout"] ?? null);
        echo "\" class=\"btn btn-primary\">";
        echo ($context["button_checkout"] ?? null);
        echo "</a></div>
      </div>
      ";
        // line 123
        echo ($context["content_bottom"] ?? null);
        echo "</div>
    ";
        // line 124
        echo ($context["column_right"] ?? null);
        echo "</div>
</div>
";
        // line 126
        echo ($context["footer"] ?? null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "default/template/checkout/cart.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  388 => 126,  383 => 124,  379 => 123,  372 => 121,  366 => 120,  360 => 116,  351 => 113,  347 => 112,  344 => 111,  340 => 110,  334 => 106,  330 => 104,  321 => 102,  317 => 101,  312 => 99,  307 => 98,  305 => 97,  299 => 93,  290 => 90,  286 => 89,  277 => 85,  268 => 79,  264 => 77,  259 => 76,  250 => 73,  246 => 72,  237 => 68,  233 => 67,  226 => 65,  220 => 62,  217 => 61,  212 => 60,  208 => 59,  205 => 58,  202 => 57,  197 => 55,  194 => 54,  191 => 53,  188 => 52,  178 => 50,  175 => 49,  170 => 48,  167 => 47,  159 => 46,  145 => 45,  142 => 44,  138 => 43,  131 => 39,  127 => 38,  123 => 37,  119 => 36,  115 => 35,  111 => 34,  103 => 29,  100 => 28,  94 => 27,  92 => 26,  88 => 25,  84 => 24,  79 => 23,  71 => 19,  68 => 18,  60 => 14,  57 => 13,  49 => 9,  47 => 8,  44 => 7,  33 => 5,  29 => 4,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/template/checkout/cart.twig", "");
    }
}
