<?php
// Heading
$_['heading_title'] = 'Use Gift Certificate';

// Text
$_['text_voucher']  = 'Gift Certificate (%s)';
$_['text_success']  = 'Success: Your gift certificate discount has been applied!';

// Entry
$_['entry_voucher'] = 'Enter your gift certificate code here';

// Error
$_['error_voucher'] = 'Gift Certificate is either invalid or the balance has been used up!';
$_['error_empty']   = 'Please enter a gift certificate code!';