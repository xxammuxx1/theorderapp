<?php
// Heading
$_['heading_title']        = 'Your order has been placed!';

// Text
$_['text_basket']          = 'Shopping Cart';
$_['text_checkout']        = 'Checkout';
$_['text_success']         = 'Success';
$_['text_customer']        = '<p>Your order has been successfully placed!</p><p>Order ID: #%s <a href="%s"> View Order </a>.</p><p>Thanks for shopping with us online!</p>';
$_['text_guest']           = '<p>Your order has been successfully placed!</p><p>Please direct any questions you have to the <a href="%s">store owner</a>.</p><p>Thanks for shopping with us online!</p>';