<?php
// Text
$_['text_subject']        = '%s - Thank you for registering';
$_['text_welcome']        = 'Welcome and thank you for registering at %s!';
$_['text_login']          = 'Your account has now been created and you can log in by using your email address and password by visiting our website or at the following URL:';
$_['text_approval']       = 'Your account must be approved before you can login. Once approved you can log in by using your email address and password by visiting our website or at the following URL:';
$_['text_service']        = 'Upon logging in, you will be able to access other services including reviewing past orders, downloading your invoices and editing your account details.';
$_['text_thanks']         = 'Thanks,';
$_['text_new_customer']   = 'New customer';
$_['text_signup']         = 'A new customer has signed up:';
$_['text_customer_group'] = 'Customer Group:';
$_['text_firstname']      = 'First Name:';
$_['text_lastname']       = 'Last Name:';
$_['text_email']          = 'E-Mail:';
$_['text_telephone']      = 'Phone Number:';

$_['button_login']           = 'Login';
$_['text_subject_verify']        = '%s - Please verify your acount';