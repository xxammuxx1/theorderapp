<?php
// Text
$_['text_success']     = 'Success: Your gift voucher discount has been applied!';
$_['text_cart']        = 'Shopping cart updated!';
$_['text_for']         = '%s Gift Certificate for %s';

// Error
$_['error_permission'] = 'You do not have permission to access the API!';
$_['error_voucher']    = 'Gift Voucher is either invalid or the balance has been used up!';
$_['error_to_name']    = 'Recipient\'s Name must be between 1 and 64 characters!';
$_['error_from_name']  = 'Your Name must be between 1 and 64 characters!';
$_['error_email']      = 'E-Mail Address does not appear to be valid!';
$_['error_theme']      = 'You must select a theme!';
$_['error_amount']     = 'Amount must be between %s and %s!';