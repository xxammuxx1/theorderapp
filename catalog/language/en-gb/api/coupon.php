<?php
// Text
$_['text_success']     = 'Success: Your coupon discount has been applied!';

// Error
$_['error_permission'] = 'You do not have permission to access the API!';
$_['error_coupon']     = 'Coupon is either invalid, expired or reached it\'s usage limit!';