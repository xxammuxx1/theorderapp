<?php
// Heading
$_['heading_title']        = 'Create Account';

// Text
$_['text_account']         = 'Account';
$_['text_register']        = 'Register';
$_['text_account_already'] = 'Already have an account? <a href="%s">Sign in</a>.';
$_['text_your_details']    = 'Your Personal Details';
$_['text_newsletter']      = 'Newsletter';
$_['text_your_password']   = 'Your Password';
$_['text_agree']           = 'I have read and agree to the <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_customer_group'] = 'Customer Group';
$_['entry_firstname']      = 'Your Name';
$_['entry_lastname']       = 'Last Name';
$_['entry_email']          = 'E-Mail';
$_['entry_telephone']      = 'Mobile number';
$_['entry_newsletter']     = 'Subscribe';
$_['entry_password']       = 'Password';
$_['entry_confirm']        = 'Password Confirm';

// Error
$_['error_exists']         = 'E-Mail Address is already registered!';

$_['error_telephone_exists']         = 'Mobile number is already registered!';

$_['error_firstname']      = 'Name must be between 1 and 32 characters!';
$_['error_lastname']       = 'Last Name must be between 1 and 32 characters!';
$_['error_email']          = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']      = 'Mobile number should be a valid Indian number of 10 digits!';
$_['error_custom_field']   = '%s required!';
$_['error_password']       = 'Password must be between 4 and 20 characters!';
$_['error_confirm']        = 'Password confirmation does not match password!';
$_['error_agree']          = 'You must agree to the %s!';