<?php
// Heading
$_['heading_title']                = 'Account Login';

// Text
$_['text_account']                 = 'Account';
$_['text_login']                   = 'Sign in';
$_['text_new_customer']            = 'New User';
$_['text_register']                = 'Create Account';
$_['text_register_account']        = 'To shop faster, be up to date on an order\'s status, and keep track of the orders you have previously made.';
$_['text_returning_customer']      = 'Existing User';
$_['text_i_am_returning_customer'] = 'I am a returning customer';
$_['text_forgotten']               = 'Forgotten Password';

// Entry
$_['entry_email']                  = 'Email or mobile number';
$_['entry_password']               = 'Password';

// Error
$_['error_login']                  = 'No match for E-Mail Address and/or mobile number and/or Password.';
$_['error_attempts']               = 'Your account has exceeded allowed number of login attempts. Please try again in 1 hour.';
$_['error_approved']               = 'Please verify your Email %s before you can login. <a href="%s">Send Verification link</a>';