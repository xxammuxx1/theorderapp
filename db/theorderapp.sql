-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3308
-- Generation Time: Mar 28, 2020 at 03:38 PM
-- Server version: 8.0.18
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `theorderapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `oc_address`
--

DROP TABLE IF EXISTS `oc_address`;
CREATE TABLE IF NOT EXISTS `oc_address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `company` varchar(60) NOT NULL,
  `address_1` varchar(128) NOT NULL,
  `address_2` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `custom_field` text NOT NULL,
  PRIMARY KEY (`address_id`),
  KEY `customer_id` (`customer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_address`
--

INSERT INTO `oc_address` (`address_id`, `customer_id`, `firstname`, `lastname`, `company`, `address_1`, `address_2`, `city`, `postcode`, `country_id`, `zone_id`, `custom_field`) VALUES
(1, 1, 'Amit', 'Chaurasia', 'None', 'Balaji Delux mens Hostel (Sai Serenity)', 'Opp. Gruhasiri Pride, Gachibowli', 'Hyderabad', '500032', 99, 4231, ''),
(2, 8, 'Abhishek', 'Chaurasia', '', 'Amar Jyoti Road, Garulpar, Deoria', '', 'Deoria', '274001', 99, 1505, ''),
(3, 9, 'Amit', 'Chaurasia', 'None', 'Balaji Delux mens Hostel (Sai Serenity)', 'Opp. Gruhasiri Pride, Gachibowli', 'Hyderabad', '500032', 99, 4231, ''),
(9, 7, 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 99, 1505, ''),
(8, 7, 'Abhishek', '', '', 'near vijay takij', 'amar jyoti road', 'Deoria', '274001', 99, 1505, ''),
(10, 10, 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 99, 1505, ''),
(11, 12, 'Amit', 'Chaurasia', '', 'Balaji Delux mens Hostel (Sai Serenity)', 'Opp. Gruhasiri Pride, Gachibowli', 'Deoria', '274001', 99, 1505, '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_api`
--

DROP TABLE IF EXISTS `oc_api`;
CREATE TABLE IF NOT EXISTS `oc_api` (
  `api_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `key` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`api_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_api`
--

INSERT INTO `oc_api` (`api_id`, `username`, `key`, `status`, `date_added`, `date_modified`) VALUES
(1, 'Default', 'a2a6d3183b173af0cb4808e95188337fca2e9eb037a7437d2dae09f4d2702f3eae0b277c4569cf98aca529a711209fa3fbee123051e255c3333491819d7ced76aaf87bb675d16a09b3d5bb35a2e4bc55001243c7199e7369caf89fbc78b2de0647b6ed20efddd1fc32c110cc6dfba950c49e15b2196b7ddfcf5f1eae0dee8cc0', 1, '2020-02-08 10:11:41', '2020-03-01 11:31:45'),
(2, 'apiuser_view', 'MaL24PsGhicqLVS55KmIueazviWTwP21t0lN05h116AwgrPuqtE5eJsg4iwszJp0g8CtH4yWb3Z24iw2qsq5rpAEZqpaffar5gi0Ica0ppooZDBDrxRpth8DXFMbsoog3OHWbRvOltoPCIlzbkoSjYLUgma3FR1bGieYa47gOe5AlHFS0WLqz8HsgRKsjjWKHkhWlblQvFYos0kpDisTALgoVI0FAxGuAkvo3wESTplfwvLEhJwT708EgGx85GX0', 1, '2020-03-01 11:33:06', '2020-03-01 11:33:06');

-- --------------------------------------------------------

--
-- Table structure for table `oc_api_ip`
--

DROP TABLE IF EXISTS `oc_api_ip`;
CREATE TABLE IF NOT EXISTS `oc_api_ip` (
  `api_ip_id` int(11) NOT NULL AUTO_INCREMENT,
  `api_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  PRIMARY KEY (`api_ip_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_api_ip`
--

INSERT INTO `oc_api_ip` (`api_ip_id`, `api_id`, `ip`) VALUES
(2, 1, '::1'),
(3, 2, '::1');

-- --------------------------------------------------------

--
-- Table structure for table `oc_api_session`
--

DROP TABLE IF EXISTS `oc_api_session`;
CREATE TABLE IF NOT EXISTS `oc_api_session` (
  `api_session_id` int(11) NOT NULL AUTO_INCREMENT,
  `api_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`api_session_id`)
) ENGINE=MyISAM AUTO_INCREMENT=80 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_api_session`
--

INSERT INTO `oc_api_session` (`api_session_id`, `api_id`, `session_id`, `ip`, `date_added`, `date_modified`) VALUES
(71, 1, '07428a019e183e6763011d9427', '::1', '2020-03-21 22:56:09', '2020-03-21 22:58:18'),
(70, 1, '336ed186396d50bc554bb78f2b', '::1', '2020-03-21 22:53:38', '2020-03-21 22:53:38'),
(69, 1, '499ae23badd5c40dbd175e1ce1', '::1', '2020-03-21 22:51:17', '2020-03-21 22:55:18'),
(67, 1, '51bb2257bd0db68309dd022b54', '::1', '2020-03-21 22:48:45', '2020-03-21 22:48:45'),
(68, 1, 'de96a4fb36a549efca87f23016', '::1', '2020-03-21 22:49:03', '2020-03-21 22:51:00'),
(72, 1, 'd72d2cf2bc01a61c2ba5b41be1', '::1', '2020-03-21 22:58:18', '2020-03-21 22:58:28'),
(73, 1, 'c3b0790c5efbeb6e2b51de0a20', '::1', '2020-03-21 22:58:28', '2020-03-21 22:58:28'),
(74, 1, '180caf4b4107082f736d56d707', '::1', '2020-03-21 23:00:45', '2020-03-21 23:00:45'),
(75, 1, '6c6f2390fcd9ec280675c4e020', '::1', '2020-03-21 23:01:00', '2020-03-21 23:01:00'),
(76, 1, '1633b0bb5f1e0a54bf1c155504', '::1', '2020-03-21 23:01:04', '2020-03-21 23:01:04'),
(77, 1, 'f392a78e40297cc5a5e591ac35', '::1', '2020-03-21 23:01:10', '2020-03-21 23:02:36'),
(78, 1, '45c8e5b87fd5ed766e78670c0e', '::1', '2020-03-21 23:02:43', '2020-03-21 23:03:15'),
(79, 1, '72d0a1fe62d59e55ce12f01274', '::1', '2020-03-21 23:03:15', '2020-03-21 23:03:15');

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute`
--

DROP TABLE IF EXISTS `oc_attribute`;
CREATE TABLE IF NOT EXISTS `oc_attribute` (
  `attribute_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`attribute_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_attribute`
--

INSERT INTO `oc_attribute` (`attribute_id`, `attribute_group_id`, `sort_order`) VALUES
(1, 6, 1),
(2, 6, 5),
(3, 6, 3),
(4, 3, 1),
(5, 3, 2),
(6, 3, 3),
(7, 3, 4),
(8, 3, 5),
(9, 3, 6),
(10, 3, 7),
(11, 3, 8);

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute_description`
--

DROP TABLE IF EXISTS `oc_attribute_description`;
CREATE TABLE IF NOT EXISTS `oc_attribute_description` (
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`attribute_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_attribute_description`
--

INSERT INTO `oc_attribute_description` (`attribute_id`, `language_id`, `name`) VALUES
(1, 1, 'Description'),
(2, 1, 'No. of Cores'),
(4, 1, 'test 1'),
(5, 1, 'test 2'),
(6, 1, 'test 3'),
(7, 1, 'test 4'),
(8, 1, 'test 5'),
(9, 1, 'test 6'),
(10, 1, 'test 7'),
(11, 1, 'test 8'),
(3, 1, 'Clockspeed');

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute_group`
--

DROP TABLE IF EXISTS `oc_attribute_group`;
CREATE TABLE IF NOT EXISTS `oc_attribute_group` (
  `attribute_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`attribute_group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_attribute_group`
--

INSERT INTO `oc_attribute_group` (`attribute_group_id`, `sort_order`) VALUES
(3, 2),
(4, 1),
(5, 3),
(6, 4);

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute_group_description`
--

DROP TABLE IF EXISTS `oc_attribute_group_description`;
CREATE TABLE IF NOT EXISTS `oc_attribute_group_description` (
  `attribute_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`attribute_group_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_attribute_group_description`
--

INSERT INTO `oc_attribute_group_description` (`attribute_group_id`, `language_id`, `name`) VALUES
(3, 1, 'Memory'),
(4, 1, 'Technical'),
(5, 1, 'Motherboard'),
(6, 1, 'Processor');

-- --------------------------------------------------------

--
-- Table structure for table `oc_banner`
--

DROP TABLE IF EXISTS `oc_banner`;
CREATE TABLE IF NOT EXISTS `oc_banner` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`banner_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_banner`
--

INSERT INTO `oc_banner` (`banner_id`, `name`, `status`) VALUES
(6, 'Popular Categories', 1),
(7, 'Home Page Slideshow', 1),
(8, 'Manufacturers', 0),
(9, 'Custom Popular Categories', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_banner_image`
--

DROP TABLE IF EXISTS `oc_banner_image`;
CREATE TABLE IF NOT EXISTS `oc_banner_image` (
  `banner_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`banner_image_id`)
) ENGINE=MyISAM AUTO_INCREMENT=197 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_banner_image`
--

INSERT INTO `oc_banner_image` (`banner_image_id`, `banner_id`, `language_id`, `title`, `link`, `image`, `sort_order`) VALUES
(132, 8, 1, 'Dell', '', 'catalog/demo/manufacturer/dell.png', 0),
(158, 6, 1, 'Mushroom', 'index.php?route=product/product&amp;path=57&amp;product_id=45', 'catalog/grocery/vegetagles/fresh-button-mushroom-spawn-500x500.jpg', 0),
(157, 6, 1, 'Basmati Rice', 'index.php?route=product/product&amp;path=57&amp;product_id=44', 'catalog/grocery/daawat_pulav_basmati_rice_5kg.jpg', 0),
(156, 6, 1, 'Apple', 'index.php?route=product/manufacturer/info&amp;manufacturer_id=7', 'catalog/grocery/fruits/apples.jpg', 0),
(187, 7, 1, 'Offers', 'index.php?route=product/category&amp;language=en-gb&amp;path=71', 'catalog/Sale.jpg', 1),
(186, 7, 1, 'Oil &amp; Ghee', 'index.php?route=product/product&amp;path=57&amp;product_id=28', 'catalog/slider-img2-1170x542.jpg', 3),
(185, 7, 1, 'Fresh Grocery', 'index.php?route=product/product&amp;path=57&amp;product_id=49', 'catalog/slider-img1-1170x542.jpg', 2),
(155, 6, 1, 'Bread', 'index.php?route=product/product&amp;path=57&amp;product_id=46', 'catalog/grocery/milk-breads/4881035-bread-01.jpg', 0),
(154, 6, 1, 'Banana', 'index.php?route=product/product&amp;path=57&amp;product_id=49', 'catalog/grocery/fruits/ioana-cristiana-0WW38q7lGZA-unsplash.jpg', 0),
(153, 6, 1, 'Grapes', 'index.php?route=product/product&amp;path=57&amp;product_id=49', 'catalog/grocery/fruits/grapes.jpg', 0),
(195, 9, 1, 'Dry Fruits', 'index.php?route=product/category&amp;language=en-gb&amp;path=69', 'catalog/grocery/dry-fruits/cashew-kaaju.jpg', 5),
(194, 9, 1, 'Salt &amp; Sugar', 'index.php?route=product/category&amp;language=en-gb&amp;path=65', 'catalog/grocery/sugar-salt-jaggery/AASHIRVAD-SALT-1-KG.jpg', 4),
(193, 9, 1, 'Vegetables', 'index.php?route=product/category&amp;language=en-gb&amp;path=62', 'catalog/grocery/vegetagles/water-fruits-veggies-ftr.jpg', 3),
(192, 9, 1, 'Dal &amp; Pulses', 'index.php?route=product/category&amp;language=en-gb&amp;path=63', 'catalog/grocery/moong-dal.jpg', 2),
(191, 9, 1, 'Fresh Fruits', 'index.php?route=product/category&amp;language=en-gb&amp;path=61', 'catalog/grocery/fruits/apples.jpg', 1),
(190, 9, 1, 'Offers', 'index.php?route=product/category&amp;language=en-gb&amp;path=71', 'catalog/Sale.jpg', 8),
(189, 9, 1, 'Oil &amp; Ghee', 'index.php?route=product/category&amp;language=en-gb&amp;path=25', 'catalog/grocery/oil-and-ghee/41fAkhF7UtL.jpg', 7),
(188, 7, 1, 'Paneer', 'index.php?route=product/product&amp;path=57&amp;product_id=29', 'catalog/grocery/paneer.jpg', 4),
(196, 9, 1, 'Masala &amp; Spices', 'index.php?route=product/category&amp;language=en-gb&amp;path=64', 'catalog/grocery/masala-spices/gol-marich-masala.jpeg', 6);

-- --------------------------------------------------------

--
-- Table structure for table `oc_cart`
--

DROP TABLE IF EXISTS `oc_cart`;
CREATE TABLE IF NOT EXISTS `oc_cart` (
  `cart_id` int(11) NOT NULL AUTO_INCREMENT,
  `api_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `option` text NOT NULL,
  `quantity` int(5) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`cart_id`),
  KEY `cart_id` (`api_id`,`customer_id`,`session_id`,`product_id`,`recurring_id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_cart`
--

INSERT INTO `oc_cart` (`cart_id`, `api_id`, `customer_id`, `session_id`, `product_id`, `recurring_id`, `option`, `quantity`, `date_added`) VALUES
(21, 0, 1, 'b82bf9dba33e19fb43d7fa7430', 44, 0, '[]', 2, '2020-02-22 20:30:15'),
(28, 0, 1, 'b82bf9dba33e19fb43d7fa7430', 50, 0, '[]', 1, '2020-02-29 19:12:32'),
(29, 0, 1, 'b82bf9dba33e19fb43d7fa7430', 43, 0, '[]', 1, '2020-02-29 19:12:54'),
(30, 0, 1, 'b82bf9dba33e19fb43d7fa7430', 45, 0, '[]', 1, '2020-02-29 19:13:06'),
(31, 0, 1, 'b82bf9dba33e19fb43d7fa7430', 46, 0, '[]', 1, '2020-02-29 19:13:09'),
(42, 0, 9, '75c9eb5ef5622d5a02efc03436', 31, 0, '[]', 1, '2020-02-29 22:56:03'),
(44, 0, 9, '75c9eb5ef5622d5a02efc03436', 51, 0, '[]', 2, '2020-02-29 22:58:32'),
(49, 0, 9, '75c9eb5ef5622d5a02efc03436', 43, 0, '[]', 1, '2020-02-29 23:20:37'),
(50, 0, 9, '75c9eb5ef5622d5a02efc03436', 53, 0, '[]', 1, '2020-03-01 13:12:35'),
(71, 0, 7, '15e2c2674f2b288c190ec36311', 60, 0, '[]', 1, '2020-03-08 11:45:31'),
(72, 0, 7, '15e2c2674f2b288c190ec36311', 62, 0, '[]', 7, '2020-03-08 11:45:51'),
(75, 0, 7, '15e2c2674f2b288c190ec36311', 64, 0, '[]', 1, '2020-03-13 05:35:42'),
(90, 0, 10, '653ef7bb85404d987367261cda', 29, 0, '[]', 2, '2020-03-19 14:21:12');

-- --------------------------------------------------------

--
-- Table structure for table `oc_category`
--

DROP TABLE IF EXISTS `oc_category`;
CREATE TABLE IF NOT EXISTS `oc_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`category_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category`
--

INSERT INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(25, 'catalog/grocery/oil-and-ghee/225x265_Products_0011_Kachi-ghani-new-1ltr-pet.png', 0, 1, 1, 3, 1, '2009-01-31 01:04:25', '2020-02-15 16:00:02'),
(18, 'catalog/demo/hp_2.jpg', 0, 1, 0, 2, 1, '2009-01-05 21:49:15', '2020-02-15 14:55:08'),
(28, 'catalog/grocery/oil-and-ghee/02_0019_Soya-1-Litre-Pet.png', 25, 0, 0, 1, 1, '2009-02-02 13:11:12', '2020-02-24 01:39:51'),
(63, 'catalog/grocery/channa-dal-1-kg.jpg', 0, 1, 1, 0, 1, '2020-02-15 16:50:08', '2020-02-15 16:50:08'),
(29, '', 25, 0, 0, 1, 1, '2009-02-02 13:11:37', '2020-02-15 16:07:59'),
(60, 'catalog/grocery/birthday-cakes/white-round-cake-topped-with-yellow-slice-fruit-140831.jpg', 0, 1, 1, 3, 1, '2020-02-15 16:32:46', '2020-02-15 17:26:43'),
(33, '', 0, 1, 1, 6, 1, '2009-02-03 14:17:55', '2020-02-15 15:51:49'),
(34, 'catalog/demo/ipod_touch_4.jpg', 0, 1, 4, 7, 1, '2009-02-03 14:18:11', '2020-02-15 14:46:30'),
(39, '', 34, 0, 0, 0, 1, '2010-09-18 14:04:17', '2020-02-15 16:07:21'),
(61, 'catalog/grocery/fruits/fruits.jpg', 0, 1, 1, 0, 1, '2020-02-15 16:44:54', '2020-02-15 16:44:54'),
(45, 'catalog/grocery/milk-breads/4881035-bread-01.jpg', 18, 0, 0, 0, 1, '2010-09-24 18:29:16', '2020-02-24 01:37:50'),
(46, 'catalog/grocery/milk-breads/milk_PNG12734.png', 18, 0, 0, 0, 1, '2010-09-24 18:29:31', '2020-02-24 01:36:44'),
(62, 'catalog/grocery/vegetagles/water-fruits-veggies-ftr.jpg', 0, 1, 1, 0, 1, '2020-02-15 16:47:21', '2020-02-15 16:47:21'),
(50, 'catalog/grocery/eggs-meat-fish/egg-tray.jpg', 34, 0, 0, 0, 1, '2010-11-07 11:14:23', '2020-02-15 16:06:39'),
(52, '', 34, 0, 0, 0, 1, '2010-11-07 11:16:09', '2020-02-15 16:06:57'),
(70, '', 0, 0, 1, 0, 1, '2020-03-01 13:12:25', '2020-03-01 13:12:25'),
(64, 'catalog/grocery/masala-spices/walnut-bow-plates-pepper-wallpaper-preview.jpg', 0, 1, 1, 0, 1, '2020-02-15 16:52:16', '2020-02-15 16:52:16'),
(65, 'catalog/grocery/sugar-salt-jaggery/AASHIRVAD-SALT-1-KG.jpg', 0, 1, 1, 0, 1, '2020-02-15 16:55:41', '2020-02-15 16:55:41'),
(66, 'catalog/grocery/fruits/ioana-cristiana-0WW38q7lGZA-unsplash.jpg', 61, 0, 1, 0, 1, '2020-02-15 17:30:11', '2020-02-15 17:30:11'),
(67, 'catalog/grocery/fruits/apples.jpg', 61, 0, 1, 0, 1, '2020-02-15 17:33:24', '2020-02-15 17:33:24'),
(68, 'catalog/grocery/fruits/grapes.jpg', 61, 0, 1, 0, 1, '2020-02-15 17:37:33', '2020-03-28 11:42:51'),
(69, '', 0, 1, 5, 5, 1, '2020-02-22 22:35:29', '2020-02-22 22:35:29'),
(71, 'catalog/latest-banners/sale_teal.png', 0, 0, 1, 1, 1, '2020-03-21 22:38:08', '2020-03-28 11:41:44');

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_description`
--

DROP TABLE IF EXISTS `oc_category_description`;
CREATE TABLE IF NOT EXISTS `oc_category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`category_id`,`language_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category_description`
--

INSERT INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(28, 1, 'Edible Oil', '&lt;p&gt;Edible Oil&lt;/p&gt;\r\n', 'Edible Oil', 'Edible Oil', 'cooking-oil, edible-oil'),
(33, 1, 'Sauce, Jam, &amp; Pickles', '&lt;p&gt;Sauce, Jam, &amp;amp; Pickles&lt;/p&gt;\r\n', 'Sauce, Jam, &amp; Pickles', 'Sauce, Jam, &amp; Pickles', 'Sauce, Jam, &amp; Pickles'),
(29, 1, 'Ghee', '&lt;p&gt;Ghee&lt;/p&gt;\r\n', 'Ghee', 'Ghee', 'Ghee'),
(25, 1, 'Oil &amp; Ghee', '&lt;p&gt;Oil &amp;amp; Ghee&lt;/p&gt;\r\n', 'Oil &amp; Ghee', 'Oil &amp; Ghee', 'Oil &amp; Ghee'),
(60, 1, 'Birthday Cakes', '&lt;p&gt;Birthday Cakes&lt;/p&gt;\r\n', 'Birthday Cakes', 'Birthday Cakes', 'Birthday Cakes'),
(61, 1, 'Fruits', '&lt;p&gt;Fruits&lt;/p&gt;\r\n', 'Fruits', 'Fruits', 'Fruits'),
(39, 1, 'Meat', '', 'test 6', '', ''),
(62, 1, 'Vegetables', '&lt;p&gt;Vegetables&lt;/p&gt;\r\n', 'Vegetables', 'Vegetables', 'Vegetables'),
(34, 1, 'Eggs, Meat &amp; Fish', '&lt;p&gt;Eggs, Meat &amp;amp; Fish&lt;/p&gt;\r\n', 'Eggs, Meat &amp; Fish', 'Order Online Eggs, Meat, Fish', 'Eggs, Meat, Fish'),
(66, 1, 'Banana', '&lt;p&gt;Banana&lt;/p&gt;\r\n', 'Banana', 'Banana', 'Banana'),
(67, 1, 'Apples', '&lt;p&gt;Apples&lt;/p&gt;\r\n', 'Apples', 'Apples', 'Apples'),
(68, 1, 'Grapes', '&lt;p&gt;Grapes&lt;/p&gt;\r\n', 'Grapes', 'Grapes', 'Grapes'),
(18, 1, 'Milk &amp; Bread', '&lt;p&gt;Milk &amp;amp; Bread&lt;/p&gt;\r\n', 'Milk &amp; Bread', 'Order Online Milk &amp; Bread', 'Milk &amp; Bread'),
(63, 1, 'Dal &amp; Pulses', '&lt;p&gt;Dal &amp;amp; Pulses&lt;/p&gt;\r\n', 'Dal &amp; Pulses', 'Dal &amp; Pulses', 'Dal &amp; Pulses'),
(64, 1, 'Masala &amp; Spices', '&lt;p&gt;Masala &amp;amp; Spices&lt;/p&gt;\r\n', 'Masala &amp; Spices', 'Masala &amp; Spices', 'Masala &amp; Spices'),
(65, 1, 'Salt, Sugar, &amp; Jaggery', '&lt;p&gt;Salt, Sugar, &amp;amp; Jaggery&lt;/p&gt;\r\n', 'Salt, Sugar, &amp; Jaggery', 'Salt, Sugar, &amp; Jaggery', 'Salt, Sugar, &amp; Jaggery'),
(45, 1, 'White Bread', '&lt;p&gt;White Bread&lt;/p&gt;\r\n', 'White Bread', 'White Bread', 'White Bread'),
(46, 1, 'Milk', '&lt;p&gt;Milk&lt;/p&gt;\r\n', 'Milk', 'Milk', 'Milk'),
(50, 1, 'Eggs', '&lt;p&gt;Eggs&lt;/p&gt;\r\n', 'Eggs', 'Eggs', 'Eggs'),
(52, 1, 'Fish', '', 'test 20', '', ''),
(70, 1, 'Other Items', '&lt;p&gt;Other Items&lt;/p&gt;\r\n', 'Other Items', 'Other Items', 'other-items'),
(69, 1, 'Dry Fruits', '&lt;p&gt;Dry Fruits&lt;/p&gt;\r\n', 'Dry Fruits', 'Dry Fruits', 'dry-fruits'),
(71, 1, 'Offers', '&lt;p&gt;All offers, discounts&lt;/p&gt;\r\n', 'Offers', 'Offers, Discounts', 'offers');

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_filter`
--

DROP TABLE IF EXISTS `oc_category_filter`;
CREATE TABLE IF NOT EXISTS `oc_category_filter` (
  `category_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`filter_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_path`
--

DROP TABLE IF EXISTS `oc_category_path`;
CREATE TABLE IF NOT EXISTS `oc_category_path` (
  `category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`path_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category_path`
--

INSERT INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(25, 25, 0),
(28, 25, 0),
(28, 28, 1),
(29, 25, 0),
(29, 29, 1),
(65, 65, 0),
(64, 64, 0),
(63, 63, 0),
(62, 62, 0),
(61, 61, 0),
(60, 60, 0),
(18, 18, 0),
(45, 18, 0),
(45, 45, 1),
(46, 18, 0),
(46, 46, 1),
(33, 33, 0),
(34, 34, 0),
(66, 66, 1),
(66, 61, 0),
(68, 68, 1),
(68, 61, 0),
(39, 34, 0),
(39, 39, 1),
(50, 34, 0),
(50, 50, 1),
(52, 34, 0),
(52, 52, 1),
(70, 70, 0),
(67, 67, 1),
(67, 61, 0),
(69, 69, 0),
(71, 71, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_to_layout`
--

DROP TABLE IF EXISTS `oc_category_to_layout`;
CREATE TABLE IF NOT EXISTS `oc_category_to_layout` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category_to_layout`
--

INSERT INTO `oc_category_to_layout` (`category_id`, `store_id`, `layout_id`) VALUES
(34, 0, 0),
(34, 1, 0),
(18, 0, 0),
(18, 1, 0),
(33, 0, 0),
(33, 1, 0),
(25, 0, 0),
(25, 1, 0),
(50, 0, 0),
(50, 1, 0),
(52, 0, 0),
(52, 1, 0),
(39, 0, 0),
(39, 1, 0),
(29, 0, 0),
(29, 1, 0),
(60, 1, 0),
(60, 0, 0),
(61, 0, 0),
(61, 1, 0),
(62, 0, 0),
(62, 1, 0),
(63, 0, 0),
(63, 1, 0),
(64, 0, 0),
(64, 1, 0),
(65, 0, 0),
(65, 1, 0),
(66, 0, 0),
(66, 1, 0),
(67, 0, 0),
(67, 1, 0),
(68, 2, 0),
(68, 0, 0),
(69, 0, 0),
(69, 1, 0),
(46, 0, 0),
(46, 1, 0),
(45, 0, 0),
(45, 1, 0),
(28, 0, 0),
(28, 1, 0),
(70, 0, 0),
(70, 1, 0),
(71, 0, 15),
(71, 2, 15),
(71, 1, 15),
(68, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_to_store`
--

DROP TABLE IF EXISTS `oc_category_to_store`;
CREATE TABLE IF NOT EXISTS `oc_category_to_store` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category_to_store`
--

INSERT INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(18, 0),
(18, 1),
(25, 0),
(28, 0),
(28, 1),
(29, 0),
(33, 0),
(34, 0),
(39, 0),
(45, 0),
(45, 1),
(46, 0),
(46, 1),
(50, 0),
(52, 0),
(60, 0),
(60, 1),
(61, 0),
(62, 0),
(62, 1),
(63, 0),
(63, 1),
(64, 0),
(64, 1),
(65, 0),
(65, 1),
(66, 0),
(66, 1),
(67, 0),
(67, 1),
(68, 0),
(68, 1),
(69, 0),
(69, 1),
(70, 0),
(71, 0),
(71, 1),
(71, 2);

-- --------------------------------------------------------

--
-- Table structure for table `oc_country`
--

DROP TABLE IF EXISTS `oc_country`;
CREATE TABLE IF NOT EXISTS `oc_country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `iso_code_2` varchar(2) NOT NULL,
  `iso_code_3` varchar(3) NOT NULL,
  `address_format` text NOT NULL,
  `postcode_required` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`country_id`)
) ENGINE=MyISAM AUTO_INCREMENT=258 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_country`
--

INSERT INTO `oc_country` (`country_id`, `name`, `iso_code_2`, `iso_code_3`, `address_format`, `postcode_required`, `status`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', '', 0, 1),
(2, 'Albania', 'AL', 'ALB', '', 0, 1),
(3, 'Algeria', 'DZ', 'DZA', '', 0, 1),
(4, 'American Samoa', 'AS', 'ASM', '', 0, 1),
(5, 'Andorra', 'AD', 'AND', '', 0, 1),
(6, 'Angola', 'AO', 'AGO', '', 0, 1),
(7, 'Anguilla', 'AI', 'AIA', '', 0, 1),
(8, 'Antarctica', 'AQ', 'ATA', '', 0, 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', '', 0, 1),
(10, 'Argentina', 'AR', 'ARG', '', 0, 1),
(11, 'Armenia', 'AM', 'ARM', '', 0, 1),
(12, 'Aruba', 'AW', 'ABW', '', 0, 1),
(13, 'Australia', 'AU', 'AUS', '', 0, 1),
(14, 'Austria', 'AT', 'AUT', '', 0, 1),
(15, 'Azerbaijan', 'AZ', 'AZE', '', 0, 1),
(16, 'Bahamas', 'BS', 'BHS', '', 0, 1),
(17, 'Bahrain', 'BH', 'BHR', '', 0, 1),
(18, 'Bangladesh', 'BD', 'BGD', '', 0, 1),
(19, 'Barbados', 'BB', 'BRB', '', 0, 1),
(20, 'Belarus', 'BY', 'BLR', '', 0, 1),
(21, 'Belgium', 'BE', 'BEL', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 0, 1),
(22, 'Belize', 'BZ', 'BLZ', '', 0, 1),
(23, 'Benin', 'BJ', 'BEN', '', 0, 1),
(24, 'Bermuda', 'BM', 'BMU', '', 0, 1),
(25, 'Bhutan', 'BT', 'BTN', '', 0, 1),
(26, 'Bolivia', 'BO', 'BOL', '', 0, 1),
(27, 'Bosnia and Herzegovina', 'BA', 'BIH', '', 0, 1),
(28, 'Botswana', 'BW', 'BWA', '', 0, 1),
(29, 'Bouvet Island', 'BV', 'BVT', '', 0, 1),
(30, 'Brazil', 'BR', 'BRA', '', 0, 1),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', '', 0, 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', '', 0, 1),
(33, 'Bulgaria', 'BG', 'BGR', '', 0, 1),
(34, 'Burkina Faso', 'BF', 'BFA', '', 0, 1),
(35, 'Burundi', 'BI', 'BDI', '', 0, 1),
(36, 'Cambodia', 'KH', 'KHM', '', 0, 1),
(37, 'Cameroon', 'CM', 'CMR', '', 0, 1),
(38, 'Canada', 'CA', 'CAN', '', 0, 1),
(39, 'Cape Verde', 'CV', 'CPV', '', 0, 1),
(40, 'Cayman Islands', 'KY', 'CYM', '', 0, 1),
(41, 'Central African Republic', 'CF', 'CAF', '', 0, 1),
(42, 'Chad', 'TD', 'TCD', '', 0, 1),
(43, 'Chile', 'CL', 'CHL', '', 0, 1),
(44, 'China', 'CN', 'CHN', '', 0, 1),
(45, 'Christmas Island', 'CX', 'CXR', '', 0, 1),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', '', 0, 1),
(47, 'Colombia', 'CO', 'COL', '', 0, 1),
(48, 'Comoros', 'KM', 'COM', '', 0, 1),
(49, 'Congo', 'CG', 'COG', '', 0, 1),
(50, 'Cook Islands', 'CK', 'COK', '', 0, 1),
(51, 'Costa Rica', 'CR', 'CRI', '', 0, 1),
(52, 'Cote D\'Ivoire', 'CI', 'CIV', '', 0, 1),
(53, 'Croatia', 'HR', 'HRV', '', 0, 1),
(54, 'Cuba', 'CU', 'CUB', '', 0, 1),
(55, 'Cyprus', 'CY', 'CYP', '', 0, 1),
(56, 'Czech Republic', 'CZ', 'CZE', '', 0, 1),
(57, 'Denmark', 'DK', 'DNK', '', 0, 1),
(58, 'Djibouti', 'DJ', 'DJI', '', 0, 1),
(59, 'Dominica', 'DM', 'DMA', '', 0, 1),
(60, 'Dominican Republic', 'DO', 'DOM', '', 0, 1),
(61, 'East Timor', 'TL', 'TLS', '', 0, 1),
(62, 'Ecuador', 'EC', 'ECU', '', 0, 1),
(63, 'Egypt', 'EG', 'EGY', '', 0, 1),
(64, 'El Salvador', 'SV', 'SLV', '', 0, 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', '', 0, 1),
(66, 'Eritrea', 'ER', 'ERI', '', 0, 1),
(67, 'Estonia', 'EE', 'EST', '', 0, 1),
(68, 'Ethiopia', 'ET', 'ETH', '', 0, 1),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', '', 0, 1),
(70, 'Faroe Islands', 'FO', 'FRO', '', 0, 1),
(71, 'Fiji', 'FJ', 'FJI', '', 0, 1),
(72, 'Finland', 'FI', 'FIN', '', 0, 1),
(74, 'France, Metropolitan', 'FR', 'FRA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(75, 'French Guiana', 'GF', 'GUF', '', 0, 1),
(76, 'French Polynesia', 'PF', 'PYF', '', 0, 1),
(77, 'French Southern Territories', 'TF', 'ATF', '', 0, 1),
(78, 'Gabon', 'GA', 'GAB', '', 0, 1),
(79, 'Gambia', 'GM', 'GMB', '', 0, 1),
(80, 'Georgia', 'GE', 'GEO', '', 0, 1),
(81, 'Germany', 'DE', 'DEU', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(82, 'Ghana', 'GH', 'GHA', '', 0, 1),
(83, 'Gibraltar', 'GI', 'GIB', '', 0, 1),
(84, 'Greece', 'GR', 'GRC', '', 0, 1),
(85, 'Greenland', 'GL', 'GRL', '', 0, 1),
(86, 'Grenada', 'GD', 'GRD', '', 0, 1),
(87, 'Guadeloupe', 'GP', 'GLP', '', 0, 1),
(88, 'Guam', 'GU', 'GUM', '', 0, 1),
(89, 'Guatemala', 'GT', 'GTM', '', 0, 1),
(90, 'Guinea', 'GN', 'GIN', '', 0, 1),
(91, 'Guinea-Bissau', 'GW', 'GNB', '', 0, 1),
(92, 'Guyana', 'GY', 'GUY', '', 0, 1),
(93, 'Haiti', 'HT', 'HTI', '', 0, 1),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', '', 0, 1),
(95, 'Honduras', 'HN', 'HND', '', 0, 1),
(96, 'Hong Kong', 'HK', 'HKG', '', 0, 1),
(97, 'Hungary', 'HU', 'HUN', '', 0, 1),
(98, 'Iceland', 'IS', 'ISL', '', 0, 1),
(99, 'India', 'IN', 'IND', '', 0, 1),
(100, 'Indonesia', 'ID', 'IDN', '', 0, 1),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', '', 0, 1),
(102, 'Iraq', 'IQ', 'IRQ', '', 0, 1),
(103, 'Ireland', 'IE', 'IRL', '', 0, 1),
(104, 'Israel', 'IL', 'ISR', '', 0, 1),
(105, 'Italy', 'IT', 'ITA', '', 0, 1),
(106, 'Jamaica', 'JM', 'JAM', '', 0, 1),
(107, 'Japan', 'JP', 'JPN', '', 0, 1),
(108, 'Jordan', 'JO', 'JOR', '', 0, 1),
(109, 'Kazakhstan', 'KZ', 'KAZ', '', 0, 1),
(110, 'Kenya', 'KE', 'KEN', '', 0, 1),
(111, 'Kiribati', 'KI', 'KIR', '', 0, 1),
(112, 'North Korea', 'KP', 'PRK', '', 0, 1),
(113, 'South Korea', 'KR', 'KOR', '', 0, 1),
(114, 'Kuwait', 'KW', 'KWT', '', 0, 1),
(115, 'Kyrgyzstan', 'KG', 'KGZ', '', 0, 1),
(116, 'Lao People\'s Democratic Republic', 'LA', 'LAO', '', 0, 1),
(117, 'Latvia', 'LV', 'LVA', '', 0, 1),
(118, 'Lebanon', 'LB', 'LBN', '', 0, 1),
(119, 'Lesotho', 'LS', 'LSO', '', 0, 1),
(120, 'Liberia', 'LR', 'LBR', '', 0, 1),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', '', 0, 1),
(122, 'Liechtenstein', 'LI', 'LIE', '', 0, 1),
(123, 'Lithuania', 'LT', 'LTU', '', 0, 1),
(124, 'Luxembourg', 'LU', 'LUX', '', 0, 1),
(125, 'Macau', 'MO', 'MAC', '', 0, 1),
(126, 'FYROM', 'MK', 'MKD', '', 0, 1),
(127, 'Madagascar', 'MG', 'MDG', '', 0, 1),
(128, 'Malawi', 'MW', 'MWI', '', 0, 1),
(129, 'Malaysia', 'MY', 'MYS', '', 0, 1),
(130, 'Maldives', 'MV', 'MDV', '', 0, 1),
(131, 'Mali', 'ML', 'MLI', '', 0, 1),
(132, 'Malta', 'MT', 'MLT', '', 0, 1),
(133, 'Marshall Islands', 'MH', 'MHL', '', 0, 1),
(134, 'Martinique', 'MQ', 'MTQ', '', 0, 1),
(135, 'Mauritania', 'MR', 'MRT', '', 0, 1),
(136, 'Mauritius', 'MU', 'MUS', '', 0, 1),
(137, 'Mayotte', 'YT', 'MYT', '', 0, 1),
(138, 'Mexico', 'MX', 'MEX', '', 0, 1),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', '', 0, 1),
(140, 'Moldova, Republic of', 'MD', 'MDA', '', 0, 1),
(141, 'Monaco', 'MC', 'MCO', '', 0, 1),
(142, 'Mongolia', 'MN', 'MNG', '', 0, 1),
(143, 'Montserrat', 'MS', 'MSR', '', 0, 1),
(144, 'Morocco', 'MA', 'MAR', '', 0, 1),
(145, 'Mozambique', 'MZ', 'MOZ', '', 0, 1),
(146, 'Myanmar', 'MM', 'MMR', '', 0, 1),
(147, 'Namibia', 'NA', 'NAM', '', 0, 1),
(148, 'Nauru', 'NR', 'NRU', '', 0, 1),
(149, 'Nepal', 'NP', 'NPL', '', 0, 1),
(150, 'Netherlands', 'NL', 'NLD', '', 0, 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', '', 0, 1),
(152, 'New Caledonia', 'NC', 'NCL', '', 0, 1),
(153, 'New Zealand', 'NZ', 'NZL', '', 0, 1),
(154, 'Nicaragua', 'NI', 'NIC', '', 0, 1),
(155, 'Niger', 'NE', 'NER', '', 0, 1),
(156, 'Nigeria', 'NG', 'NGA', '', 0, 1),
(157, 'Niue', 'NU', 'NIU', '', 0, 1),
(158, 'Norfolk Island', 'NF', 'NFK', '', 0, 1),
(159, 'Northern Mariana Islands', 'MP', 'MNP', '', 0, 1),
(160, 'Norway', 'NO', 'NOR', '', 0, 1),
(161, 'Oman', 'OM', 'OMN', '', 0, 1),
(162, 'Pakistan', 'PK', 'PAK', '', 0, 1),
(163, 'Palau', 'PW', 'PLW', '', 0, 1),
(164, 'Panama', 'PA', 'PAN', '', 0, 1),
(165, 'Papua New Guinea', 'PG', 'PNG', '', 0, 1),
(166, 'Paraguay', 'PY', 'PRY', '', 0, 1),
(167, 'Peru', 'PE', 'PER', '', 0, 1),
(168, 'Philippines', 'PH', 'PHL', '', 0, 1),
(169, 'Pitcairn', 'PN', 'PCN', '', 0, 1),
(170, 'Poland', 'PL', 'POL', '', 0, 1),
(171, 'Portugal', 'PT', 'PRT', '', 0, 1),
(172, 'Puerto Rico', 'PR', 'PRI', '', 0, 1),
(173, 'Qatar', 'QA', 'QAT', '', 0, 1),
(174, 'Reunion', 'RE', 'REU', '', 0, 1),
(175, 'România', 'RO', 'ROM', '', 0, 1),
(176, 'Russian Federation', 'RU', 'RUS', '', 0, 1),
(177, 'Rwanda', 'RW', 'RWA', '', 0, 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', '', 0, 1),
(179, 'Saint Lucia', 'LC', 'LCA', '', 0, 1),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '', 0, 1),
(181, 'Samoa', 'WS', 'WSM', '', 0, 1),
(182, 'San Marino', 'SM', 'SMR', '', 0, 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', '', 0, 1),
(184, 'Saudi Arabia', 'SA', 'SAU', '', 0, 1),
(185, 'Senegal', 'SN', 'SEN', '', 0, 1),
(186, 'Seychelles', 'SC', 'SYC', '', 0, 1),
(187, 'Sierra Leone', 'SL', 'SLE', '', 0, 1),
(188, 'Singapore', 'SG', 'SGP', '', 0, 1),
(189, 'Slovak Republic', 'SK', 'SVK', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city} {postcode}\r\n{zone}\r\n{country}', 0, 1),
(190, 'Slovenia', 'SI', 'SVN', '', 0, 1),
(191, 'Solomon Islands', 'SB', 'SLB', '', 0, 1),
(192, 'Somalia', 'SO', 'SOM', '', 0, 1),
(193, 'South Africa', 'ZA', 'ZAF', '', 0, 1),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS', 'SGS', '', 0, 1),
(195, 'Spain', 'ES', 'ESP', '', 0, 1),
(196, 'Sri Lanka', 'LK', 'LKA', '', 0, 1),
(197, 'St. Helena', 'SH', 'SHN', '', 0, 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', '', 0, 1),
(199, 'Sudan', 'SD', 'SDN', '', 0, 1),
(200, 'Suriname', 'SR', 'SUR', '', 0, 1),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', '', 0, 1),
(202, 'Swaziland', 'SZ', 'SWZ', '', 0, 1),
(203, 'Sweden', 'SE', 'SWE', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(204, 'Switzerland', 'CH', 'CHE', '', 0, 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', '', 0, 1),
(206, 'Taiwan', 'TW', 'TWN', '', 0, 1),
(207, 'Tajikistan', 'TJ', 'TJK', '', 0, 1),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', '', 0, 1),
(209, 'Thailand', 'TH', 'THA', '', 0, 1),
(210, 'Togo', 'TG', 'TGO', '', 0, 1),
(211, 'Tokelau', 'TK', 'TKL', '', 0, 1),
(212, 'Tonga', 'TO', 'TON', '', 0, 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', '', 0, 1),
(214, 'Tunisia', 'TN', 'TUN', '', 0, 1),
(215, 'Turkey', 'TR', 'TUR', '', 0, 1),
(216, 'Turkmenistan', 'TM', 'TKM', '', 0, 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', '', 0, 1),
(218, 'Tuvalu', 'TV', 'TUV', '', 0, 1),
(219, 'Uganda', 'UG', 'UGA', '', 0, 1),
(220, 'Ukraine', 'UA', 'UKR', '', 0, 1),
(221, 'United Arab Emirates', 'AE', 'ARE', '', 0, 1),
(222, 'United Kingdom', 'GB', 'GBR', '', 1, 1),
(223, 'United States', 'US', 'USA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}', 0, 1),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', '', 0, 1),
(225, 'Uruguay', 'UY', 'URY', '', 0, 1),
(226, 'Uzbekistan', 'UZ', 'UZB', '', 0, 1),
(227, 'Vanuatu', 'VU', 'VUT', '', 0, 1),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', '', 0, 1),
(229, 'Venezuela', 'VE', 'VEN', '', 0, 1),
(230, 'Viet Nam', 'VN', 'VNM', '', 0, 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', '', 0, 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', '', 0, 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', '', 0, 1),
(234, 'Western Sahara', 'EH', 'ESH', '', 0, 1),
(235, 'Yemen', 'YE', 'YEM', '', 0, 1),
(237, 'Democratic Republic of Congo', 'CD', 'COD', '', 0, 1),
(238, 'Zambia', 'ZM', 'ZMB', '', 0, 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', '', 0, 1),
(242, 'Montenegro', 'ME', 'MNE', '', 0, 1),
(243, 'Serbia', 'RS', 'SRB', '', 0, 1),
(244, 'Aaland Islands', 'AX', 'ALA', '', 0, 1),
(245, 'Bonaire, Sint Eustatius and Saba', 'BQ', 'BES', '', 0, 1),
(246, 'Curacao', 'CW', 'CUW', '', 0, 1),
(247, 'Palestinian Territory, Occupied', 'PS', 'PSE', '', 0, 1),
(248, 'South Sudan', 'SS', 'SSD', '', 0, 1),
(249, 'St. Barthelemy', 'BL', 'BLM', '', 0, 1),
(250, 'St. Martin (French part)', 'MF', 'MAF', '', 0, 1),
(251, 'Canary Islands', 'IC', 'ICA', '', 0, 1),
(252, 'Ascension Island (British)', 'AC', 'ASC', '', 0, 1),
(253, 'Kosovo, Republic of', 'XK', 'UNK', '', 0, 1),
(254, 'Isle of Man', 'IM', 'IMN', '', 0, 1),
(255, 'Tristan da Cunha', 'TA', 'SHN', '', 0, 1),
(256, 'Guernsey', 'GG', 'GGY', '', 0, 1),
(257, 'Jersey', 'JE', 'JEY', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon`
--

DROP TABLE IF EXISTS `oc_coupon`;
CREATE TABLE IF NOT EXISTS `oc_coupon` (
  `coupon_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `code` varchar(20) NOT NULL,
  `type` char(1) NOT NULL,
  `discount` decimal(15,4) NOT NULL,
  `logged` tinyint(1) NOT NULL,
  `shipping` tinyint(1) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `uses_total` int(11) NOT NULL,
  `uses_customer` varchar(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `coupon_text` varchar(2555) DEFAULT NULL,
  PRIMARY KEY (`coupon_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_coupon`
--

INSERT INTO `oc_coupon` (`coupon_id`, `name`, `code`, `type`, `discount`, `logged`, `shipping`, `total`, `date_start`, `date_end`, `uses_total`, `uses_customer`, `status`, `date_added`, `coupon_text`) VALUES
(4, '-10% Discount', 'FLAT10', 'P', '10.0000', 0, 0, '799.0000', '2014-01-01', '2020-05-23', 10, '10', 1, '2009-01-27 13:55:03', 'Flat 10% off on Order Total of more than 799.'),
(5, 'Free Shipping', 'FREESHIP', 'P', '0.0000', 0, 1, '1000.0000', '2020-01-29', '2020-04-14', 2, '2', 0, '2009-03-14 21:13:53', 'Get Free Shipping on Order Total of more than 1000. '),
(6, '-15.00 Discount', 'FLAT15', 'P', '15.0000', 0, 0, '2000.0000', '2014-01-01', '2020-05-16', 100000, '10000', 1, '2009-03-14 21:15:18', 'Flat 15% off on Order Total of more than 2000.');

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon_category`
--

DROP TABLE IF EXISTS `oc_coupon_category`;
CREATE TABLE IF NOT EXISTS `oc_coupon_category` (
  `coupon_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`coupon_id`,`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon_history`
--

DROP TABLE IF EXISTS `oc_coupon_history`;
CREATE TABLE IF NOT EXISTS `oc_coupon_history` (
  `coupon_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`coupon_history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon_product`
--

DROP TABLE IF EXISTS `oc_coupon_product`;
CREATE TABLE IF NOT EXISTS `oc_coupon_product` (
  `coupon_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`coupon_product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_cron`
--

DROP TABLE IF EXISTS `oc_cron`;
CREATE TABLE IF NOT EXISTS `oc_cron` (
  `cron_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(64) NOT NULL,
  `cycle` varchar(12) NOT NULL,
  `action` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`cron_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_cron`
--

INSERT INTO `oc_cron` (`cron_id`, `code`, `cycle`, `action`, `status`, `date_added`, `date_modified`) VALUES
(1, 'currency', 'day', 'cron/currency', 1, '2014-09-25 14:40:00', '2014-09-25 14:40:00'),
(2, 'gdpr', 'day', 'cron/gdpr', 1, '2014-09-25 14:40:00', '2014-09-25 14:40:00');

-- --------------------------------------------------------

--
-- Table structure for table `oc_currency`
--

DROP TABLE IF EXISTS `oc_currency`;
CREATE TABLE IF NOT EXISTS `oc_currency` (
  `currency_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` double(15,8) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`currency_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_currency`
--

INSERT INTO `oc_currency` (`currency_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `status`, `date_modified`) VALUES
(1, 'Pound Sterling', 'GBP', '£', '', '2', 0.61250001, 0, '2020-02-15 18:20:23'),
(2, 'US Dollar', 'USD', '$', '', '2', 1.00000000, 0, '2020-02-15 18:20:35'),
(3, 'Euro', 'EUR', '', '€', '2', 0.78460002, 0, '2020-02-15 18:20:58'),
(4, 'Hong Kong Dollar', 'HKD', 'HK$', '', '2', 7.82224000, 0, '2018-02-16 12:00:00'),
(5, 'Indian Rupee', 'INR', '₹', '', '2', 1.00000000, 1, '2020-02-08 05:01:52'),
(6, 'Russian Ruble', 'RUB', '₽', '', '2', 56.40360000, 0, '2018-02-16 12:00:00'),
(7, 'Chinese Yuan Renminbi', 'CNY', '¥', '', '2', 6.34510000, 0, '2018-02-16 12:00:00'),
(8, 'Australian Dollar', 'AUD', '$', '', '2', 1.26544000, 0, '2018-02-16 12:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer`
--

DROP TABLE IF EXISTS `oc_customer`;
CREATE TABLE IF NOT EXISTS `oc_customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_group_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `cart` text NOT NULL,
  `wishlist` text NOT NULL,
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `address_id` int(11) NOT NULL DEFAULT '0',
  `custom_field` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `safe` tinyint(1) NOT NULL,
  `token` text NOT NULL,
  `code` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_customer`
--

INSERT INTO `oc_customer` (`customer_id`, `customer_group_id`, `store_id`, `language_id`, `firstname`, `lastname`, `email`, `telephone`, `fax`, `password`, `salt`, `cart`, `wishlist`, `newsletter`, `address_id`, `custom_field`, `ip`, `status`, `safe`, `token`, `code`, `date_added`) VALUES
(11, 1, 0, 1, 'Abhishek', '', 'c.haurasiaabhi09@gmail.com', '8217763387', '', '$2y$10$OVmsJ8YsHtqQdsO59vcyQebq3Dkg4.cQwGHfjDukztaZNYLjyg4ni', '', '', '', 0, 0, '', '::1', 1, 0, '', '', '2020-03-20 13:21:12'),
(10, 3, 0, 1, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '123', '', '$2y$10$6fsz5ite0iybWCz/JUA5BObRfOMNT43TBZUn5qCMV8Shobdc4mdq.', '', '', '', 0, 10, '', '::1', 1, 0, '', '3119248bf15e71a1732745270d18b110202cd096', '2020-03-16 11:45:34'),
(12, 1, 0, 1, 'Amit', 'Chaurasia', 'xxammuxx@gmail.com', '8019773696', '', '$2y$10$D/xT5b.tKBOQzY0Ii3cTd.SbQarp/.kXTSJgnb9jF95/i.11hsHte', '', '', '', 0, 11, '{\"10\":\"\"}', '::1', 1, 0, '17a0f9addbeee80b0662ad9fc', '', '2020-03-21 21:23:12');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_activity`
--

DROP TABLE IF EXISTS `oc_customer_activity`;
CREATE TABLE IF NOT EXISTS `oc_customer_activity` (
  `customer_activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `data` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_activity_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_affiliate`
--

DROP TABLE IF EXISTS `oc_customer_affiliate`;
CREATE TABLE IF NOT EXISTS `oc_customer_affiliate` (
  `customer_id` int(11) NOT NULL,
  `company` varchar(40) NOT NULL,
  `website` varchar(255) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `commission` decimal(4,2) NOT NULL DEFAULT '0.00',
  `tax` varchar(64) NOT NULL,
  `payment` varchar(6) NOT NULL,
  `cheque` varchar(100) NOT NULL,
  `paypal` varchar(64) NOT NULL,
  `bank_name` varchar(64) NOT NULL,
  `bank_branch_number` varchar(64) NOT NULL,
  `bank_swift_code` varchar(64) NOT NULL,
  `bank_account_name` varchar(64) NOT NULL,
  `bank_account_number` varchar(64) NOT NULL,
  `custom_field` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_customer_affiliate`
--

INSERT INTO `oc_customer_affiliate` (`customer_id`, `company`, `website`, `tracking`, `commission`, `tax`, `payment`, `cheque`, `paypal`, `bank_name`, `bank_branch_number`, `bank_swift_code`, `bank_account_name`, `bank_account_number`, `custom_field`, `status`, `date_added`) VALUES
(1, 'None', '', 'cfebd68c3a', '1.00', '', 'bank', '', '', '', '', '', 'Tanay Deep', '234509879849038', '[]', 1, '2020-03-01 11:49:46');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_affiliate_report`
--

DROP TABLE IF EXISTS `oc_customer_affiliate_report`;
CREATE TABLE IF NOT EXISTS `oc_customer_affiliate_report` (
  `customer_affiliate_report_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `country` varchar(2) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_affiliate_report_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_approval`
--

DROP TABLE IF EXISTS `oc_customer_approval`;
CREATE TABLE IF NOT EXISTS `oc_customer_approval` (
  `customer_approval_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `type` varchar(9) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_approval_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_group`
--

DROP TABLE IF EXISTS `oc_customer_group`;
CREATE TABLE IF NOT EXISTS `oc_customer_group` (
  `customer_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `approval` int(1) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`customer_group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_customer_group`
--

INSERT INTO `oc_customer_group` (`customer_group_id`, `approval`, `sort_order`) VALUES
(1, 0, 1),
(2, 1, 1),
(3, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_group_description`
--

DROP TABLE IF EXISTS `oc_customer_group_description`;
CREATE TABLE IF NOT EXISTS `oc_customer_group_description` (
  `customer_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`customer_group_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_customer_group_description`
--

INSERT INTO `oc_customer_group_description` (`customer_group_id`, `language_id`, `name`, `description`) VALUES
(1, 1, 'Default', 'test'),
(2, 1, 'Facebook Users', 'Accounts created using Facebook Login'),
(3, 1, 'Google Users', 'Accounts created using Google Login');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_history`
--

DROP TABLE IF EXISTS `oc_customer_history`;
CREATE TABLE IF NOT EXISTS `oc_customer_history` (
  `customer_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_ip`
--

DROP TABLE IF EXISTS `oc_customer_ip`;
CREATE TABLE IF NOT EXISTS `oc_customer_ip` (
  `customer_ip_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `country` varchar(2) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_ip_id`),
  KEY `ip` (`ip`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_customer_ip`
--

INSERT INTO `oc_customer_ip` (`customer_ip_id`, `customer_id`, `store_id`, `ip`, `country`, `date_added`) VALUES
(1, 1, 0, '::1', '', '2020-02-15 14:57:39'),
(2, 1, 0, '::1', '', '2020-02-22 15:32:01'),
(3, 1, 0, '::1', '', '2020-02-22 20:20:13'),
(4, 1, 0, '::1', '', '2020-02-23 15:45:53'),
(5, 2, 0, '::1', '', '2020-02-27 11:50:43'),
(6, 8, 0, '::1', '', '2020-02-27 15:26:57'),
(7, 8, 0, '::1', '', '2020-02-28 12:11:29'),
(8, 1, 0, '::1', '', '2020-02-29 21:23:16'),
(9, 8, 0, '::1', '', '2020-03-07 16:28:57'),
(10, 8, 0, '::1', '', '2020-03-07 16:30:14'),
(11, 4, 0, '::1', '', '2020-03-10 16:30:43'),
(12, 11, 0, '::1', '', '2020-03-20 13:31:08'),
(13, 11, 0, '::1', '', '2020-03-20 13:32:18'),
(14, 12, 0, '::1', '', '2020-03-21 22:12:56');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_login`
--

DROP TABLE IF EXISTS `oc_customer_login`;
CREATE TABLE IF NOT EXISTS `oc_customer_login` (
  `customer_login_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(96) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `total` int(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`customer_login_id`),
  KEY `email` (`email`),
  KEY `ip` (`ip`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_customer_login`
--

INSERT INTO `oc_customer_login` (`customer_login_id`, `email`, `ip`, `total`, `date_added`, `date_modified`) VALUES
(1, 'amit', '::1', 2, '2020-02-22 15:31:56', '2020-02-29 21:23:04'),
(3, 'tesla', '::1', 3, '2020-02-29 14:37:55', '2020-02-29 15:13:25'),
(4, 'admin', '::1', 1, '2020-02-29 21:22:50', '2020-02-29 21:22:50'),
(5, '8019773695', '::1', 3, '2020-03-07 16:21:25', '2020-03-07 16:27:00'),
(10, 'root', '::1', 1, '2020-03-20 12:54:53', '2020-03-20 12:54:53'),
(11, '123', '::1', 1, '2020-03-20 13:20:08', '2020-03-20 13:20:08'),
(12, 'chaurasiaabhi09@gmail.com', '::1', 1, '2020-03-20 13:20:33', '2020-03-20 13:20:33'),
(15, '8019773694', '::1', 1, '2020-03-28 19:13:10', '2020-03-28 19:13:10');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_online`
--

DROP TABLE IF EXISTS `oc_customer_online`;
CREATE TABLE IF NOT EXISTS `oc_customer_online` (
  `ip` varchar(40) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `referer` text NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_customer_online`
--

INSERT INTO `oc_customer_online` (`ip`, `customer_id`, `url`, `referer`, `date_added`) VALUES
('::1', 0, 'http://localhost/theorderapp/index.php?route=product/product&amp;language=en-gb&amp;path=71&amp;product_id=33', 'http://localhost/theorderapp/index.php?route=product/category&amp;language=en-gb&amp;path=71', '2020-03-28 20:00:45');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_reward`
--

DROP TABLE IF EXISTS `oc_customer_reward`;
CREATE TABLE IF NOT EXISTS `oc_customer_reward` (
  `customer_reward_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `points` int(8) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_reward_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_customer_reward`
--

INSERT INTO `oc_customer_reward` (`customer_reward_id`, `customer_id`, `order_id`, `description`, `points`, `date_added`) VALUES
(3, 7, 25, 'Order ID: 25', 10, '2020-03-08 11:44:12'),
(4, 10, 0, 'Signup Reward', 5, '2020-03-16 11:45:34'),
(7, 10, 33, 'Order ID: #33', -4, '2020-03-16 14:38:33'),
(8, 11, 0, 'Signup Reward', 5, '2020-03-20 13:21:12'),
(9, 12, 0, 'Signup Reward', 5, '2020-03-21 21:23:12'),
(14, 12, 79, 'Order ID: 79', 10, '2020-03-21 22:54:32'),
(12, 12, 79, 'Order ID: #79', 400, '2020-03-21 22:50:17'),
(13, 12, 79, 'Order ID: 79', 10, '2020-03-21 22:51:00');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_search`
--

DROP TABLE IF EXISTS `oc_customer_search`;
CREATE TABLE IF NOT EXISTS `oc_customer_search` (
  `customer_search_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category` tinyint(1) NOT NULL,
  `description` tinyint(1) NOT NULL,
  `products` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_search_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_customer_search`
--

INSERT INTO `oc_customer_search` (`customer_search_id`, `store_id`, `language_id`, `customer_id`, `keyword`, `category_id`, `sub_category`, `description`, `products`, `ip`, `date_added`) VALUES
(1, 0, 1, 7, 'Daawat Basamati Rice', 0, 0, 0, 1, '::1', '2020-03-08 10:42:40'),
(2, 0, 1, 7, 'Daawat Basamati Rice', 0, 0, 0, 1, '::1', '2020-03-08 11:35:55'),
(3, 0, 1, 7, 'Daawat Basamati Rice', 0, 0, 0, 1, '::1', '2020-03-08 11:37:28'),
(4, 0, 1, 7, 'Amul Ghee', 0, 0, 0, 2, '::1', '2020-03-08 11:38:17'),
(5, 0, 1, 7, 'Amul Ghee', 0, 0, 0, 2, '::1', '2020-03-08 11:38:44'),
(6, 0, 1, 10, 'Amul Ghee', 0, 0, 0, 2, '::1', '2020-03-16 15:41:32'),
(7, 0, 1, 0, 'sev', 0, 0, 0, 1, '::1', '2020-03-28 15:30:26');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_transaction`
--

DROP TABLE IF EXISTS `oc_customer_transaction`;
CREATE TABLE IF NOT EXISTS `oc_customer_transaction` (
  `customer_transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_transaction_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_wishlist`
--

DROP TABLE IF EXISTS `oc_customer_wishlist`;
CREATE TABLE IF NOT EXISTS `oc_customer_wishlist` (
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_id`,`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_customer_wishlist`
--

INSERT INTO `oc_customer_wishlist` (`customer_id`, `product_id`, `date_added`) VALUES
(9, 44, '2020-02-29 21:50:02'),
(7, 29, '2020-03-07 13:23:07');

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field`
--

DROP TABLE IF EXISTS `oc_custom_field`;
CREATE TABLE IF NOT EXISTS `oc_custom_field` (
  `custom_field_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `value` text NOT NULL,
  `validation` varchar(255) NOT NULL,
  `location` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`custom_field_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_custom_field`
--

INSERT INTO `oc_custom_field` (`custom_field_id`, `type`, `value`, `validation`, `location`, `status`, `sort_order`) VALUES
(1, 'select', '', '', 'account', 0, 1),
(2, 'radio', '', '', 'account', 0, 2),
(3, 'checkbox', '', '', 'account', 0, 3),
(4, 'text', '', '', 'account', 0, 4),
(5, 'textarea', '', '', 'account', 0, 5),
(6, 'file', '', '', 'account', 0, 6),
(7, 'date', '', '', 'account', 0, 7),
(8, 'time', '', '', 'account', 0, 8),
(9, 'datetime', '', '', 'account', 0, 9),
(10, 'datetime', '', '', 'account', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_customer_group`
--

DROP TABLE IF EXISTS `oc_custom_field_customer_group`;
CREATE TABLE IF NOT EXISTS `oc_custom_field_customer_group` (
  `custom_field_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL,
  PRIMARY KEY (`custom_field_id`,`customer_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_custom_field_customer_group`
--

INSERT INTO `oc_custom_field_customer_group` (`custom_field_id`, `customer_group_id`, `required`) VALUES
(1, 1, 1),
(2, 1, 1),
(3, 1, 1),
(4, 1, 1),
(5, 1, 1),
(6, 1, 1),
(7, 1, 1),
(8, 1, 1),
(9, 1, 1),
(10, 1, 0),
(10, 2, 0),
(10, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_description`
--

DROP TABLE IF EXISTS `oc_custom_field_description`;
CREATE TABLE IF NOT EXISTS `oc_custom_field_description` (
  `custom_field_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`custom_field_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_custom_field_description`
--

INSERT INTO `oc_custom_field_description` (`custom_field_id`, `language_id`, `name`) VALUES
(1, 1, 'Select'),
(2, 1, 'Radio'),
(3, 1, 'Checkbox'),
(4, 1, 'Text'),
(5, 1, 'Textarea'),
(6, 1, 'File'),
(7, 1, 'Date'),
(8, 1, 'Time'),
(9, 1, 'Date &amp; Time'),
(10, 1, 'Last Logged In');

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_value`
--

DROP TABLE IF EXISTS `oc_custom_field_value`;
CREATE TABLE IF NOT EXISTS `oc_custom_field_value` (
  `custom_field_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `custom_field_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`custom_field_value_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_custom_field_value`
--

INSERT INTO `oc_custom_field_value` (`custom_field_value_id`, `custom_field_id`, `sort_order`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 2, 1),
(5, 2, 2),
(6, 2, 3),
(7, 3, 1),
(8, 3, 2),
(9, 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_value_description`
--

DROP TABLE IF EXISTS `oc_custom_field_value_description`;
CREATE TABLE IF NOT EXISTS `oc_custom_field_value_description` (
  `custom_field_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`custom_field_value_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_custom_field_value_description`
--

INSERT INTO `oc_custom_field_value_description` (`custom_field_value_id`, `language_id`, `custom_field_id`, `name`) VALUES
(1, 1, 1, 'Test 1'),
(2, 1, 1, 'test 2'),
(3, 1, 1, 'Test 3'),
(4, 1, 2, 'Test 1'),
(5, 1, 2, 'Test 2'),
(6, 1, 2, 'Test 3'),
(7, 1, 3, 'Test 1'),
(8, 1, 3, 'Test 2'),
(9, 1, 3, 'Test 3');

-- --------------------------------------------------------

--
-- Table structure for table `oc_download`
--

DROP TABLE IF EXISTS `oc_download`;
CREATE TABLE IF NOT EXISTS `oc_download` (
  `download_id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(160) NOT NULL,
  `mask` varchar(128) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`download_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_download_description`
--

DROP TABLE IF EXISTS `oc_download_description`;
CREATE TABLE IF NOT EXISTS `oc_download_description` (
  `download_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`download_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_download_report`
--

DROP TABLE IF EXISTS `oc_download_report`;
CREATE TABLE IF NOT EXISTS `oc_download_report` (
  `download_report_id` int(11) NOT NULL AUTO_INCREMENT,
  `download_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `country` varchar(2) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`download_report_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_event`
--

DROP TABLE IF EXISTS `oc_event`;
CREATE TABLE IF NOT EXISTS `oc_event` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(64) NOT NULL,
  `trigger` text NOT NULL,
  `action` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `sort_order` int(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`event_id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_event`
--

INSERT INTO `oc_event` (`event_id`, `code`, `trigger`, `action`, `status`, `sort_order`) VALUES
(1, 'activity_customer_add', 'catalog/model/account/customer/addCustomer/after', 'event/activity/addCustomer', 1, 1),
(2, 'activity_customer_edit', 'catalog/model/account/customer/editCustomer/after', 'event/activity/editCustomer', 1, 1),
(3, 'activity_customer_password', 'catalog/model/account/customer/editPassword/after', 'event/activity/editPassword', 1, 1),
(4, 'activity_customer_forgotten', 'catalog/model/account/customer/editCode/after', 'event/activity/forgotten', 1, 1),
(5, 'activity_transaction', 'catalog/model/account/customer/addTransaction/after', 'event/activity/addTransaction', 1, 1),
(6, 'activity_customer_login', 'catalog/model/account/customer/deleteLoginAttempts/after', 'event/activity/login', 1, 1),
(7, 'activity_address_add', 'catalog/model/account/address/addAddress/after', 'event/activity/addAddress', 1, 1),
(8, 'activity_address_edit', 'catalog/model/account/address/editAddress/after', 'event/activity/editAddress', 1, 1),
(9, 'activity_address_delete', 'catalog/model/account/address/deleteAddress/after', 'event/activity/deleteAddress', 1, 1),
(10, 'activity_affiliate_add', 'catalog/model/account/affiliate/addAffiliate/after', 'event/activity/addAffiliate', 1, 1),
(11, 'activity_affiliate_edit', 'catalog/model/account/affiliate/editAffiliate/after', 'event/activity/editAffiliate', 1, 1),
(12, 'activity_order_add', 'catalog/model/checkout/order/addOrderHistory/before', 'event/activity/addOrderHistory', 1, 1),
(13, 'activity_return_add', 'catalog/model/account/return/addReturn/after', 'event/activity/addReturn', 1, 1),
(14, 'mail_transaction', 'catalog/model/account/customer/addTransaction/after', 'mail/transaction', 1, 1),
(15, 'mail_forgotten', 'catalog/model/account/customer/editCode/after', 'mail/forgotten', 1, 1),
(16, 'mail_customer_add', 'catalog/model/account/customer/addCustomer/after', 'mail/register', 1, 1),
(17, 'mail_customer_alert', 'catalog/model/account/customer/addCustomer/after', 'mail/register/alert', 1, 1),
(18, 'mail_affiliate_add', 'catalog/model/account/affiliate/addAffiliate/after', 'mail/affiliate', 1, 1),
(19, 'mail_affiliate_alert', 'catalog/model/account/affiliate/addAffiliate/after', 'mail/affiliate/alert', 1, 1),
(20, 'mail_voucher', 'catalog/model/checkout/order/addOrderHistory/after', 'extension/total/voucher/send', 1, 1),
(21, 'mail_order_add', 'catalog/model/checkout/order/addOrderHistory/before', 'mail/order', 1, 1),
(22, 'mail_order_alert', 'catalog/model/checkout/order/addOrderHistory/before', 'mail/order/alert', 1, 1),
(23, 'mail_gdpr', 'catalog/model/account/gdpr/addGdpr/after', 'mail/gdpr', 1, 1),
(24, 'mail_review', 'catalog/model/catalog/review/addReview/after', 'mail/review', 1, 1),
(25, 'statistics_review_add', 'catalog/model/catalog/review/addReview/after', 'event/statistics/addReview', 1, 1),
(26, 'statistics_return_add', 'catalog/model/account/return/addReturn/after', 'event/statistics/addReturn', 1, 1),
(27, 'statistics_order_history', 'catalog/model/checkout/order/addOrderHistory/before', 'event/statistics/addOrderHistory', 1, 1),
(28, 'admin_mail_affiliate_approve', 'admin/model/customer/customer_approval/approveAffiliate/after', 'mail/affiliate/approve', 1, 1),
(29, 'admin_mail_affiliate_deny', 'admin/model/customer/customer_approval/denyAffiliate/after', 'mail/affiliate/deny', 1, 1),
(30, 'admin_mail_customer_approve', 'admin/model/customer/customer_approval/approveCustomer/after', 'mail/customer/approve', 1, 1),
(31, 'admin_mail_customer_deny', 'admin/model/customer/customer_approval/denyCustomer/after', 'mail/customer/deny', 1, 1),
(32, 'admin_mail_reward', 'admin/model/customer/customer/addReward/after', 'mail/reward', 1, 1),
(33, 'admin_mail_transaction', 'admin/model/customer/customer/addTransaction/after', 'mail/transaction', 1, 1),
(34, 'admin_mail_return', 'admin/model/sale/return/addReturnHistory/after', 'mail/return', 1, 1),
(35, 'admin_mail_forgotten', 'admin/model/user/user/editCode/after', 'mail/forgotten', 1, 1),
(36, 'admin_currency_add', 'admin/model/localisation/currency/addCurrency/after', 'event/currency', 1, 1),
(37, 'admin_currency_edit', 'admin/model/localisation/currency/editCurrency/after', 'event/currency', 1, 1),
(38, 'admin_setting', 'admin/model/setting/setting/editSetting/after', 'event/currency', 1, 1),
(39, 'admin_mail_gdpr', 'admin/model/customer/gdpr/editStatus/after', 'mail/gdpr', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_extension`
--

DROP TABLE IF EXISTS `oc_extension`;
CREATE TABLE IF NOT EXISTS `oc_extension` (
  `extension_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL,
  PRIMARY KEY (`extension_id`)
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_extension`
--

INSERT INTO `oc_extension` (`extension_id`, `type`, `code`) VALUES
(1, 'payment', 'cod'),
(2, 'total', 'shipping'),
(3, 'total', 'sub_total'),
(5, 'total', 'total'),
(6, 'module', 'banner'),
(7, 'module', 'carousel'),
(8, 'total', 'credit'),
(9, 'shipping', 'flat'),
(10, 'total', 'handling'),
(11, 'total', 'low_order_fee'),
(12, 'total', 'coupon'),
(13, 'module', 'category'),
(14, 'module', 'account'),
(15, 'total', 'reward'),
(17, 'payment', 'free_checkout'),
(18, 'module', 'featured'),
(19, 'module', 'slideshow'),
(20, 'theme', 'default'),
(21, 'dashboard', 'activity'),
(22, 'dashboard', 'sale'),
(23, 'dashboard', 'recent'),
(24, 'dashboard', 'order'),
(25, 'dashboard', 'online'),
(26, 'dashboard', 'map'),
(27, 'dashboard', 'customer'),
(28, 'dashboard', 'chart'),
(29, 'report', 'sale_coupon'),
(31, 'report', 'customer_search'),
(32, 'report', 'customer_transaction'),
(33, 'report', 'product_purchased'),
(34, 'report', 'product_viewed'),
(35, 'report', 'sale_return'),
(36, 'report', 'sale_order'),
(37, 'report', 'sale_shipping'),
(38, 'report', 'sale_tax'),
(39, 'report', 'customer_activity'),
(40, 'report', 'customer_order'),
(41, 'report', 'customer_reward'),
(42, 'currency', 'fixer'),
(43, 'theme', 'grocery_theme'),
(45, 'theme', 'mintleaf'),
(46, 'module', 'topcategory'),
(47, 'module', 'bestseller'),
(48, 'module', 'latest'),
(49, 'module', 'store'),
(50, 'module', 'special'),
(51, 'module', 'bannergrid'),
(52, 'payment', 'razorpay'),
(53, 'shipping', 'free'),
(54, 'shipping', 'pickup'),
(55, 'captcha', 'basic'),
(56, 'module', 'filter'),
(57, 'fraud', 'ip'),
(58, 'module', 'html'),
(59, 'module', 'tawkto');

-- --------------------------------------------------------

--
-- Table structure for table `oc_extension_install`
--

DROP TABLE IF EXISTS `oc_extension_install`;
CREATE TABLE IF NOT EXISTS `oc_extension_install` (
  `extension_install_id` int(11) NOT NULL AUTO_INCREMENT,
  `extension_id` int(11) NOT NULL,
  `extension_download_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`extension_install_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_extension_install`
--

INSERT INTO `oc_extension_install` (`extension_install_id`, `extension_id`, `extension_download_id`, `filename`, `date_added`) VALUES
(5, 0, 0, 'mintleaf.ocmod.zip', '2020-02-19 14:51:45'),
(6, 0, 0, 'mintleaf.ocmod.zip', '2020-02-19 14:52:59');

-- --------------------------------------------------------

--
-- Table structure for table `oc_extension_path`
--

DROP TABLE IF EXISTS `oc_extension_path`;
CREATE TABLE IF NOT EXISTS `oc_extension_path` (
  `extension_path_id` int(11) NOT NULL AUTO_INCREMENT,
  `extension_install_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`extension_path_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter`
--

DROP TABLE IF EXISTS `oc_filter`;
CREATE TABLE IF NOT EXISTS `oc_filter` (
  `filter_id` int(11) NOT NULL AUTO_INCREMENT,
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`filter_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter_description`
--

DROP TABLE IF EXISTS `oc_filter_description`;
CREATE TABLE IF NOT EXISTS `oc_filter_description` (
  `filter_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`filter_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter_group`
--

DROP TABLE IF EXISTS `oc_filter_group`;
CREATE TABLE IF NOT EXISTS `oc_filter_group` (
  `filter_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`filter_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter_group_description`
--

DROP TABLE IF EXISTS `oc_filter_group_description`;
CREATE TABLE IF NOT EXISTS `oc_filter_group_description` (
  `filter_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`filter_group_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_fraud_ip`
--

DROP TABLE IF EXISTS `oc_fraud_ip`;
CREATE TABLE IF NOT EXISTS `oc_fraud_ip` (
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_gdpr`
--

DROP TABLE IF EXISTS `oc_gdpr`;
CREATE TABLE IF NOT EXISTS `oc_gdpr` (
  `gdpr_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `code` varchar(40) NOT NULL,
  `email` varchar(96) NOT NULL,
  `action` varchar(6) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`gdpr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_geo_zone`
--

DROP TABLE IF EXISTS `oc_geo_zone`;
CREATE TABLE IF NOT EXISTS `oc_geo_zone` (
  `geo_zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`geo_zone_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_geo_zone`
--

INSERT INTO `oc_geo_zone` (`geo_zone_id`, `name`, `description`, `date_added`, `date_modified`) VALUES
(3, 'UK VAT Zone', 'UK VAT', '2009-01-06 23:26:25', '2010-02-26 22:33:24'),
(4, 'UK Shipping', 'UK Shipping Zones', '2009-06-23 01:14:53', '2010-12-15 15:18:13'),
(5, 'Deoria, U.P., India', 'Deoria, U.P., India', '2020-02-15 18:23:16', '2020-02-15 18:23:44');

-- --------------------------------------------------------

--
-- Table structure for table `oc_information`
--

DROP TABLE IF EXISTS `oc_information`;
CREATE TABLE IF NOT EXISTS `oc_information` (
  `information_id` int(11) NOT NULL AUTO_INCREMENT,
  `bottom` int(1) NOT NULL DEFAULT '0',
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`information_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_information`
--

INSERT INTO `oc_information` (`information_id`, `bottom`, `sort_order`, `status`) VALUES
(1, 1, 5, 1),
(2, 1, 1, 1),
(3, 1, 4, 1),
(4, 1, 2, 1),
(5, 1, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_information_description`
--

DROP TABLE IF EXISTS `oc_information_description`;
CREATE TABLE IF NOT EXISTS `oc_information_description` (
  `information_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` mediumtext NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`information_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_information_description`
--

INSERT INTO `oc_information_description` (`information_id`, `language_id`, `title`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, 1, 'About Us', '&lt;p&gt;We are based out of Deoria, U.P., 274001.&lt;/p&gt;\r\n', 'About Us', 'About Us', 'about-us'),
(2, 1, 'Terms &amp; Conditions', '&lt;p&gt;Thank you for using KiranaKorner! These Terms of Service (“Terms”) govern your use of the KiranaKorner services, including KiranaKorner’s website, KiranaKorner’s mobile applications, and any websites (or portions thereof) or mobile applications that are operated by KiranaKorner (collectively, the “Services”), and are entered into by you and Virtual Supermarkets Limited, trading as KiranaKorner.&lt;/p&gt;\r\n\r\n&lt;p&gt;By using the Services, you agree to be bound by these Terms and acknowledge and agree to the collection, use and disclosure of your personal information in accordance with KiranaKorner’s Privacy Policy.&lt;/p&gt;\r\n\r\n&lt;p&gt;The Services comprise a platform that presents you with a set of one or more retailer virtual storefronts from which you can select goods for picking, packing, and delivery by individual personal shoppers (“Personal Shoppers”) to your location or, if available, for you to pick up in-store. In some cases, picking, packing, or delivery services may be performed by third parties including a retailer or third party logistics provider (collectively, “Third Party Providers”).&lt;/p&gt;\r\n\r\n&lt;p&gt;Delivery may be conducted by each individual Personal Shopper’s select method of transportation. You acknowledge that transportation or logistics services are provided by third party independent contractors who are not employed by KiranaKorner.&lt;/p&gt;\r\n\r\n&lt;p&gt;When you use the Services to place an order for products, you authorize the purchase and delivery of those products from the retailers you select. Unless otherwise specified, you acknowledge and agree that KiranaKorner and the Personal Shopper are acting as your agents in picking, packing, and/or delivery of goods purchased by you and are not the seller of the goods to you. You agree that your purchase is being made from the retailer you have selected, that retailer is the merchant of record, and that title to any goods passes to you when they are purchased at the applicable retailer’s store. You agree that KiranaKorner or the applicable retailer will obtain a credit card authorization for your credit card on file with KiranaKorner to cover the cost of the goods you have purchased from the retailer and any separate KiranaKorner fees, and your card will be charged for the goods purchased by you and any applicable fees. Your card may be temporarily authorized for an amount greater than the total amount of the purchase appearing in the original check out. This higher authorized amount is a temporary authorization charge on your order, to deal with situations where your total purchase amount turns out to be higher than the original amount due to special requests, added items, replacement items or weight adjustments.&lt;/p&gt;\r\n\r\n&lt;p&gt;You also acknowledge and agree that, except as expressly provided for otherwise in these Terms or a separate agreement between you and KiranaKorner, KiranaKorner does not form any employment or agency relationship with you and does not hold title to any goods that you order through the Services.&lt;/p&gt;\r\n\r\n&lt;p&gt;Unless otherwise indicated, all prices and other amounts are in the currency of the jurisdiction where the delivery takes place.&lt;/p&gt;\r\n\r\n&lt;p&gt;Occasionally there may be information on the Services that contains typographical errors, inaccuracies, or omissions that may relate to pricing, product descriptions, promotions offers, and availability. KiranaKorner reserves the right to correct any errors, inaccuracies or omissions and to change or update information or refuse or cancel orders if any information on the sites is inaccurate at any time without prior notice (including after you have submitted your order and/or your credit card has been charged).&lt;/p&gt;\r\n\r\n&lt;p&gt;1. Your Use of the Services&lt;/p&gt;\r\n\r\n&lt;p&gt;KiranaKorner grants you a limited, non-exclusive, non-transferable, and revocable license to use the Services for their intended purposes subject to your compliance with these Terms and KiranaKorner’s policies. You may not copy, modify, distribute, sell, or lease any part of the Services. Unless such restriction is prohibited by law or you have KiranaKorner’s written permission, you may not reverse engineer or attempt to extract the source code of the Services. You may only access the Services through the interfaces that KiranaKorner provides for that purpose (for example, you may not “scrape” the Services through automated means or “frame” any part of the Services), and you may not interfere or attempt to disrupt the Services.&lt;/p&gt;\r\n\r\n&lt;p&gt;Some parts of the Services may allow you to upload or submit content (such as text, images, video, recipes, lists, links, and other materials). You retain all rights in any content that you upload or submit, and are solely responsible for that content. You grant KiranaKorner a non-exclusive, royalty-free, worldwide, transferable, sub-licenseable license to use, store, publicly display, publicly perform, reproduce, modify, create derivative works from, and distribute any such content for the purposes of operating, providing, and improving the Services. KiranaKorner may, in its sole discretion, remove or take down any content that you upload or submit to the Services for any reason, including violation of these Terms or any other policies.&lt;/p&gt;\r\n\r\n&lt;p&gt;You may have the option of accessing the Services through downloadable software and this software may update itself automatically on your device. Some software or portions of software, in the Services may be governed by open source licenses. In that case, KiranaKorner will make such licenses available to you and, in the case of conflict between such a license and these Terms, the open source license will control but only with respect to the software, or portion of the software, to which it applies.&lt;/p&gt;\r\n\r\n&lt;p&gt;If you are using KiranaKorner on behalf of a business or other entity, you represent and warrant that you have the necessary authority to bind that business or entity to these Terms and that you are agreeing to these Terms on behalf of that business or entity.&lt;/p&gt;\r\n\r\n&lt;p&gt;In order to use the Services, you may need to create a user account. You agree that you are responsible for all conduct and transactions that take place on or using your account and that you will take precautions to keep your password and other account information secure. You also agree that you will comply with all applicable laws when accessing or using the Services and you will respect those who you encounter in your use of the Services, including Personal Shoppers and individuals who support KiranaKorner’s Help Center. KiranaKorner reserves the right to decline orders, refuse partial or full delivery, terminate accounts, and/or cancel orders at any time in its sole discretion.&lt;/p&gt;\r\n\r\n&lt;p&gt;We’re constantly modifying and improving the Services. KiranaKorner may introduce new features, change existing features, or remove features from the Services at any time and without notice. If you provide KiranaKorner with any feedback on or comments regarding the Services, you grant KiranaKorner the right to use such feedback or comments for any purpose without restriction or payment to you.&lt;/p&gt;\r\n\r\n&lt;p&gt;If you have any requests for order cancellations, refunds, or returns, please visit your account to initiate such requests or review our Help Center articles for our policies regarding the same.&lt;/p&gt;\r\n\r\n&lt;p&gt;2. KiranaKorner Communications&lt;/p&gt;\r\n\r\n&lt;p&gt;By creating an KiranaKorner user account, you agree to accept and receive communications from KiranaKorner or Personal Shoppers, including via e-mail, text message, calls, and push notifications to the cellular telephone number you provided to KiranaKorner. You understand and agree that you may receive communications generated by automatic telephone dialing systems and/or which will deliver prerecorded messages sent by or on behalf of KiranaKorner, its affiliated companies and/or Personal Shoppers, including but not limited to communications concerning orders placed through your account on the Services. Message and data rates may apply. If you do not wish to receive promotional emails, text messages, or other communications, you may opt out of such communications at any time in Your Account Settings. You may also opt-out of receiving text messages from KiranaKorner by replying “STOP” from the mobile device receiving the messages.&lt;/p&gt;\r\n\r\n&lt;p&gt;3. KiranaKorner Coupons&lt;/p&gt;\r\n\r\n&lt;p&gt;KiranaKorner Coupons are manufacturer’s coupons that are automatically applied to qualifying products upon purchase to help users save money on the products they love. Coupons are available for a limited time only and may be subject to certain restrictions. Coupons are subject to change, cancellation, or expiration at any time. If you do not purchase the qualifying items added to your cart while the Coupon is still in effect, the Coupon’s offer will not apply. Coupons apply only to qualifying items displaying the offer and may not be combined with other promotional offers or mail-in rebates. KiranaKorner is not a retailer or seller. Coupons are issued and paid by the manufacturer or retailer of the advertised product. The user is required to pay any applicable sales tax related to use of the Coupon. When Coupons are redeemed, sales tax may be charged on the undiscounted original price of the product(s). Coupons may not be sold, copied, modified, or transferred. A Coupon has no cash value. Coupons are good while supplies last. Void where restricted or prohibited by law.&lt;/p&gt;\r\n\r\n&lt;p&gt;4. Transactions involving Alcohol&lt;/p&gt;\r\n\r\n&lt;p&gt;You may have the option to order delivery of alcohol products in some locations and from certain retailers. You agree that you will comply with all applicable laws and not cause KiranaKorner, your Personal Shopper, or any retailer to contravene any applicable laws. You agree that you are of legal drinking age for purchasing, possessing, and consuming alcohol (i.e., 18 years of age or older in Kenya). If you order alcohol products, you understand and acknowledge that neither KiranaKorner nor a Personal Shopper can accept your order of alcohol products, and the order will only be delivered if the retailer accepts your order. You agree that, upon delivery of alcohol products, the recipient will provide valid government-issued identification proving their age to the Personal Shopper delivering the alcohol products and that the recipient will not be intoxicated when receiving delivery of such products. You agree that if any applicable legal requirements for the delivery of alcohol are not met, KiranaKorner reserves the right to cancel the alcohol-related portion of your order.&lt;/p&gt;\r\n\r\n&lt;p&gt;5. Third-party Products and Content&lt;/p&gt;\r\n\r\n&lt;p&gt;You agree that KiranaKorner does not assume responsibility for any products, content, services, websites, advertisements, offers, or information that is provided by third parties and made available through the Services. If you purchase, use, or access any such products, content, services, advertisements, offers, or information through the Services, you agree that you do so at your own risk and that KiranaKorner will have no liability based on such purchase, use, or access.&lt;/p&gt;\r\n\r\n&lt;p&gt;6. SERVICE PROVIDED AS-IS AND RELEASE OF CLAIMS&lt;/p&gt;\r\n\r\n&lt;p&gt;The Services are provided &quot;AS IS&quot; AND &quot;AS AVAILABLE.&quot; to the maximum extent permitted by applicable laws, KiranaKorner disclaims all representations, conditions, and warranties, express, legal, implied, or statutory, including the implied warranties or conditions of merchantability, quality, fitness for a particular purpose, durability, title, and non-infringement. In addition, to the maximum extent permitted by applicable laws, KiranaKorner makes no representation, warranty, conditions, or guarantee regarding the reliability, timeliness, quality, suitability, or availability of the services, any services provided by Personal Shoppers or Third Party providers, or goods requested through the use of the services from retailers, or that the services will be uninterrupted or error-free. KiranaKorner does not guarantee the quality, suitability, safety or ability of Personal Shoppers, Third Party providers, or retailers. You agree that the entire risk arising out of your use of the services, any services provided by Personal Shoppers or Third Party providers, or any products requested by you or delivered to you, remains solely with you.&lt;/p&gt;\r\n\r\n&lt;p&gt;To the maximum extent permitted by applicable laws, you agree that neither KiranaKorner nor its affiliates, retail partners, licensors, or suppliers is responsible for the fitness or conduct of any Personal Shopper or Third Party provider. Neither KiranaKorner nor its affiliates, retail partners, licensors, or suppliers will be liable for any claim, injury or damage arising in connection with the acts or omissions of any Personal Shopper or Third Party provider.&lt;/p&gt;\r\n\r\n&lt;p&gt;If you have a dispute with one or more Personal Shoppers or Third Party Providers, you agree to release KiranaKorner (including KiranaKorner’s affiliates, and each of their respective officers, directors, employees, agents, shareholders, retail partners, licensors, and suppliers) from any claims, demands and damages of every kind and nature, known and unknown, suspected and unsuspected, disclosed and undisclosed, arising out of or in any way connected to such disputes.&lt;/p&gt;\r\n\r\n&lt;p&gt;7. LIMITATION OF LIABILITY&lt;/p&gt;\r\n\r\n&lt;p&gt;This provision applies to the maximum extent permitted by applicable laws.&lt;/p&gt;\r\n\r\n&lt;p&gt;In no event shall KiranaKorner (including its affiliates, and each of their respective officers, directors, employees, agents, shareholders, retail partners, licensors, and suppliers) be liable to you for any incidental, special, punitive, consequential, or indirect damages (including, but not limited to, damages, for deletion, corruption, loss of data, loss of programs, failure to store any information or other content maintained or transmitted by the services, service interruptions, or for the cost of procurement of substitute services) arising out of or in connection with the services, or these terms, however arising including negligence, even if KiranaKorner or KiranaKorner’s agents or representatives know or have been advised of the possibility of such damages.&lt;/p&gt;\r\n\r\n&lt;p&gt;In no event shall KiranaKorner (including its affiliates, and each of their respective officers, directors, employees, agents, shareholders, retail partners, licensors, and suppliers) be liable for any indirect, special, punitive, incidental, exemplary and/or consequential damages (including, but not limited to physical damages, bodily injury, death and/or emotional distress and discomfort) arising out of your use of the services, any services provided by Personal Shoppers or Third Party providers, or any products requested by you or delivered to you, even if KiranaKorner or KiranaKorner’s agents or representatives know or have been advised of the possibility of such damages.&lt;/p&gt;\r\n\r\n&lt;p&gt;KiranaKorner, its affiliates, retail partners, licensors, suppliers and distributors will not be liable for aggregate liability for all claims relating to the services, any services provided by Personal Shoppers or Third Party providers, or any products requested by you or delivered to you for more than the greater of KSh 1,000 or the amounts paid by you to KiranaKorner for those specific services.&lt;/p&gt;\r\n\r\n&lt;p&gt;8. Indemnification&lt;/p&gt;\r\n\r\n&lt;p&gt;You agree to defend, indemnify and hold harmless KiranaKorner and its officers, directors, employees, agents, shareholders, affiliates, and retail partners (each, an &quot;Indemnified Party&quot;) from and against any losses, claims, actions, costs, damages, penalties, fines and expenses, including without limitation attorneys’ and experts’ fees and expenses, that may be incurred by an Indemnified Party arising out of, relating to or resulting from your unauthorized use of the Services or from any breach by you of these Terms, including without limitation any actual or alleged violation of any law, rule or regulation.&lt;/p&gt;\r\n\r\n&lt;p&gt;9. Disputes &amp;amp; Arbitration&lt;/p&gt;\r\n\r\n&lt;p&gt;If you have a dispute with KiranaKorner arising out of your use of the Services, this Section 9 applies. You agree to contact KiranaKorner first and attempt to work out any such dispute amicably.&lt;/p&gt;\r\n\r\n&lt;p&gt;Mandatory Arbitration: If we’re unable to work out a solution amicably, both you and KiranaKorner agree to resolve any disputes arising out of your use of the Services or these Terms through binding arbitration.&lt;/p&gt;\r\n\r\n&lt;p&gt;Class Action Waiver: To the extent permissible by law, all claims must be brought in the parties’ individual capacity, and not as a plaintiff or class member in any purported class, collective action, or non-PAGA representative proceeding (collectively “Class Action Waiver”). The arbitrator may not consolidate more than one person’s claims or engage in any class arbitration. You agree that, by entering into these terms, you and KiranaKorner are each waiving the right to a trial by jury or to participate in a class action.&lt;/p&gt;\r\n\r\n&lt;p&gt;In any lawsuit in which (1) the complaint is filed as a class action, collective action or non-PAGA representative action; and (2) the civil court of competent jurisdiction in which the complaint was filed finds the Class Action Waiver is unenforceable (and such finding is confirmed by appellate review if review is sought), the Class Action Waiver shall be severable from this Agreement and in such instances, the class action, collective action and/or non-PAGA representative action must be litigated in a civil court of competent jurisdiction and not as a class, collective or non-PAGA representative arbitration.&lt;/p&gt;\r\n\r\n&lt;p&gt;PAGA waiver: To the extent permissible by law, there will be no right or authority for any dispute to be brought, heard, or arbitrated on a group basis or in any action in which a party seeks to represent other individual(s) in a private attorney general action (PAGA Waiver). PAGA claims may only be arbitrated on an individual basis.&lt;/p&gt;\r\n\r\n&lt;p&gt;In any lawsuit in which (1) the complaint is filed as a private attorney general action seeking to represent any individual(s) other than the named plaintiff; and (2) the civil court of competent jurisdiction in which the complaint was filed finds the PAGA Waiver is unenforceable (and such finding is confirmed by appellate review if review is sought), the PAGA Waiver shall be severable from this Agreement and in such instances, the private attorney general action must be litigated in a civil court of competent jurisdiction and not as a private attorney general arbitration.&lt;/p&gt;\r\n\r\n&lt;p&gt;Notwithstanding any other clause contained in this Agreement, any claim that all or part of the Class Action Waiver or PAGA Waiver is invalid, unenforceable, unconscionable, void or voidable may be determined only by a court of competent jurisdiction and not by an arbitrator. The Class Action Waiver and PAGA Waiver shall be severable when a dispute is filed as an individual action and severance is necessary to ensure that the individual action proceeds in arbitration.&lt;/p&gt;\r\n\r\n&lt;p&gt;The arbitration will be held in the country where the Services are requested by you.&lt;/p&gt;\r\n\r\n&lt;p&gt;Arbitration Fees: Payment of all arbitration fees and each party will be responsible for their own fees.&lt;/p&gt;\r\n\r\n&lt;p&gt;10. Termination&lt;/p&gt;\r\n\r\n&lt;p&gt;You can stop using the Services at any time and without notice to us. Similarly, KiranaKorner may terminate access to the Services to you or any other users or stop offering the all or part of the Services at any time without notice.&lt;/p&gt;\r\n\r\n&lt;p&gt;11. Controlling Law&lt;/p&gt;\r\n\r\n&lt;p&gt;To the extent permitted by applicable law, these Terms will be governed by the laws of the Country where the Services are requested or rendered to be provided without respect to its conflicts of laws principles. To the extent permitted by applicable law, any claims arising out of or relating to these Terms or use of the Services that are not subject to Section 9 (Disputes &amp;amp; Arbitration) of these Terms shall be brought exclusively in the Country where the Services are requested or rendered and you and KiranaKorner consent to the personal jurisdiction of those courts.&lt;/p&gt;\r\n\r\n&lt;p&gt;12. Entire Agreement &amp;amp;Severability&lt;/p&gt;\r\n\r\n&lt;p&gt;These Terms, subject to any amendments, modifications, or additional agreements you enter into with KiranaKorner, shall constitute the entire agreement between you and KiranaKorner with respect to the Services and any use of the Services. If any provision of these Terms is found to be invalid by a court competent jurisdiction, that provision only will be limited to the minimum extent necessary and the remaining provisions will remain in full force and effect.&lt;/p&gt;\r\n\r\n&lt;p&gt;13. No Waiver&lt;/p&gt;\r\n\r\n&lt;p&gt;KiranaKorner’s failure to monitor or enforce a provision of these Terms does not constitute a waiver of its right to do so in the future with respect to that provision, any other provision, or these Terms as a whole.&lt;/p&gt;\r\n\r\n&lt;p&gt;14. Assignment&lt;/p&gt;\r\n\r\n&lt;p&gt;You may not assign any of your rights, licenses, or obligations under these Terms. Any such attempt at assignment by you shall be void. KiranaKorner may assign its rights, licenses, and obligations under these Terms without limitation.&lt;/p&gt;\r\n\r\n&lt;p&gt;15. Changes to the Terms&lt;/p&gt;\r\n\r\n&lt;p&gt;We may make changes to these Terms from time to time. When KiranaKorner does so, KiranaKorner will post the most current version of the Terms on KiranaKorner’s website and, if a revision to the Terms is material, KiranaKorner will notify you of the new Terms (for example, by email or a notification on the Services). Changes to these terms will not apply retroactively. If you do not agree to the modified terms, you should discontinue your use of the Services.&lt;/p&gt;\r\n\r\n&lt;p&gt;16. Copyright Policy&lt;/p&gt;\r\n\r\n&lt;p&gt;KiranaKorner respects the intellectual property rights of others and has implemented a copyright policy in accordance with relevant laws. KiranaKorner will respond to valid notices of copyright infringement and reserves the right to terminate any users, at KiranaKorner’s sole discretion and without notice, who repeatedly infringe copyrights or other intellectual property rights.&lt;/p&gt;\r\n\r\n&lt;p&gt;If you believe any content posted or made available on the Services constitutes infringement of your copyright rights, you may send a written notice of infringement to KiranaKorner’s designated Copyright Agent using the contact information listed below. In your notice, please specify the nature of the copyright infringement and include the following information: (a) an electronic or physical signature of the owner of the copyright in question or a person authorized to act on behalf of the owner of the copyright; (b) a description of the claimed infringing material as well as identification of the claimed infringing material, including the location of such material on the Services (e.g., the URL of the claimed infringing material if applicable or other means by which KiranaKorner may locate the material); (c) complete contact information, including the name of the owner of the copyright and your name, title, address, telephone number, and email address; (d) a statement that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law; and (e) a statement, made under penalty of perjury, that the information provided in your notice is accurate and that you are the copyright owner or authorized to act on behalf of the owner.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;TheOrderApp&lt;/strong&gt;&lt;br /&gt;\r\nAmar Jyoti Road, Garulpar&lt;br /&gt;\r\nDeoria - India&lt;br /&gt;\r\nxxammuxx@gmail.com&lt;br /&gt;\r\n+91 8217763387&lt;/p&gt;\r\n\r\n&lt;p&gt;17. Contact Information&lt;/p&gt;\r\n\r\n&lt;p&gt;If you have any questions, complaints or comments about the Services contact KiranaKorner at:&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;TheOrderApp&lt;/strong&gt;&lt;br /&gt;\r\nAmar Jyoti Road, Garulpar&lt;br /&gt;\r\nDeoria - India&lt;br /&gt;\r\nxxammuxx@gmail.com&lt;br /&gt;\r\n+91 8217763387&lt;/p&gt;\r\n\r\n&lt;p&gt;For customer service inquiries visit KiranaKorner’s&amp;nbsp;&lt;a href=&quot;https://www.KiranaKorner.com/admin/help&quot;&gt;Help Center&lt;/a&gt;&lt;/p&gt;\r\n', 'Terms &amp; Conditions', '', ''),
(3, 1, 'Privacy Policy', '&lt;p&gt;Thank you for using the KiranaKorner Services! We are committed to providing you the best online shopping and delivery experience possible. This Privacy Policy explains what information we (Virtual Supermarkets Limited trading as KiranaKorner) collect, how that information is used, under what circumstances we share information, and the choices you can make about that information. This Privacy Policy applies whether you access the KiranaKorner Services (as defined in the Terms of Service) through a browser, a mobile application, or any other method.&lt;/p&gt;\r\n\r\n&lt;p&gt;Information we collect&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;1. Information you provide to us or allow others to provide to us&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;At various points in the KiranaKorner experience, you may provide us with information about yourself. For example, when you create an account through the Services, you provide us with personal information like your name, email address, and zip or postal code. And if you place an order through the Services, we collect information including your address, phone number, birth date (for alcohol orders, where available), credit card information, and the details of your order. Your account information may be updated or corrected by accessing your account settings.&lt;/p&gt;\r\n\r\n&lt;p&gt;If you log into the Services through a third-party service, both we and that third-party may receive some information about you and your use of the services. For example, if you choose to log into the Services with your Facebook account, we may receive information from Facebook, such as your name, e-mail address, public profile information, and information about your contacts. We may also offer social sharing tools (such as the Facebook “Like” button) that let you share actions on the Services with other websites and vice versa. In those cases, the providers of those tools may receive information about you when you use those tools. You should check the privacy policies of these third party services and your settings there for more information.&lt;/p&gt;\r\n\r\n&lt;p&gt;If you wish to invite your friends and contacts to use the Services, we will give you the option of either entering in their contact information manually or importing it from your address books on other third party services. In both cases, we will store this information for the sole purposes of allowing you to send your friends referral offers, for determining whether your friends use the Services after a referral is sent and to remind your friends of the referral sent on your behalf.&lt;/p&gt;\r\n\r\n&lt;p&gt;Our partners may let us collect information about use of their sites/apps or share such information with us. For example, if you use a KiranaKorner button or widget on another site or app, we may receive information about your use of that button or widget and the third-party site/app.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;2. Technical information about usage of the Services&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;When you use the Services, either through a browser or mobile app, we automatically receive some technical information about the hardware and software that is being used.&lt;/p&gt;\r\n\r\n&lt;p&gt;Cookies, Pixels, and Other Tracking Technologies: We, our partners, our advertisers, and third party advertising networks use various technologies to collect information, including but not limited to cookies, pixels, scripts, and device identifiers. Cookies are small text files that are sent by your computer when you access our services through a browser. We, our partners, our advertisers, and third party advertising networks may use session cookies (which expire when you close your browser), persistent cookies (which expire when you choose to clear them from your browser), pixels, scripts, and other identifiers to collect information from your browser or device that helps us do things such as understand how you use our services and other services; personalize your experience; measure, manage, and display advertising on the Services or on other services; understand your usage of the Services and other services in order to serve customized ads and remember that you are logged into the Services. By using your browser settings, you may block cookies or adjust settings for notifications when a cookie is set.&lt;/p&gt;\r\n\r\n&lt;p&gt;We employ some third-party services to help us understand the usage of the Services and the performance of advertising, and these third-parties may also deploy cookies, pixels, or other identifiers on the Services or collect information through our mobile applications. For example, we use Google Analytics to understand, in a non-personal way, how users interact with various portions of the Services -- you can learn more about information that Google may collect here.&lt;/p&gt;\r\n\r\n&lt;p&gt;Log information: When you use the Services, our servers will record information about your usage of the Services and information that is sent by your browser or device. Log information can include things like the IP address of your device, information about the browser, operating system and/or app you are using, unique device identifiers, pages that you navigate to and links that you click, searches that you run on the Services, and other ways you interact with the Services. If you are logged into the Services, this information is stored with your account information.&lt;/p&gt;\r\n\r\n&lt;p&gt;Interest-Based or Online Behavioral Advertising: KiranaKorner may use third-party advertising companies to serve interest-based advertisements to you. These companies compile information from various online sources (including mobile-enabled browsers and applications) to match you with ads that will be the most relevant, interesting, and timely for you. If you would like to opt-out of interest-based advertising, please visit http://optout.networkadvertising.org/#/ . Please note that you will be opted out of all interest-based advertising from all business members of the Network Advertising Initiative for that specific browser on that specific device. If you opt-out, you may continue to see KiranaKorner’s or our partners’ online advertisements; however, these ads will not be as relevant to you.&lt;/p&gt;\r\n\r\n&lt;p&gt;How we use your information&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;We may use the information we collect for various purposes, including to:&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li&gt;Provide the Services to you, improve the quality of the service, and develop new products and services&lt;/li&gt;\r\n	&lt;li&gt;Allow your Personal Shopper (which means those that shop for and/or deliver the order for you, including our retail partner and their employees/agents where applicable or our third-party providers) to choose your items at a retailer site, deliver your items to you, and/or call or text you with any updates or issues&lt;/li&gt;\r\n	&lt;li&gt;Charge you for the purchase and delivery costs through one or more payment processing partners&lt;/li&gt;\r\n	&lt;li&gt;Offer you customized content (including advertising, coupons, and promotions)&lt;/li&gt;\r\n	&lt;li&gt;Understand how users interact with the Services (including advertising both on and off the Services) as a whole and to test new features or changes in our features&lt;/li&gt;\r\n	&lt;li&gt;Provide customer service, respond to your communications and requests, and contact you about your use of the Services&lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;p&gt;You can opt-out of receiving promotional communications from KiranaKorner by using the settings on the Account Info page or by using the unsubscribe mechanism included in the message, where applicable.&lt;/p&gt;\r\n\r\n&lt;p&gt;What we share&lt;/p&gt;\r\n\r\n&lt;p&gt;The Services comprise a platform that presents you with a set of one or more retailer virtual storefronts from which you can select goods for picking, packing, and delivery by individual Personal Shopper(s) to your location or, if available, for you to pick up in-store. In order to make this work, we need to share information about you and your order with the other parties who help enable the service. This includes, for example, the Personal Shopper(s) who pick and deliver your order, the payment processing partner(s) that we use to validate and charge your credit card and/or mobile money, and the retail partner(s) from whom you are purchasing goods. To be clear, only our payment processing partner(s) receive credit card and mobile money information.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;We also share information under the following principles:&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li&gt;With your consent or at your direction — We will share your information with entities outside of the Services when we have your consent to do so or it is done at your direction. For example:\r\n	&lt;ul&gt;\r\n		&lt;li&gt;If you share a recipe publicly on the Services, it is viewable by anyone along with your first name and last name.&lt;/li&gt;\r\n		&lt;li&gt;If you invite friends to use the Services through our referral program, we will share some information with the friends you invite like your name and picture.&lt;/li&gt;\r\n	&lt;/ul&gt;\r\n	&lt;/li&gt;\r\n	&lt;li&gt;For external processing or service provision — We sometimes employ third parties to process information on our behalf or to provide certain services (such as delivery services, advertising services, or information to better tailor our services to you). For the purposes of this processing or provision of services, we may share your information with them under appropriate confidentiality provisions.&lt;/li&gt;\r\n	&lt;li&gt;For legal purposes — We may share your information when we believe that the disclosure is reasonably necessary to (a) comply with applicable laws, regulations, legal process, or requests from law enforcement or regulatory authorities, (b) prevent, detect, or otherwise handle fraud, security, or technical issues, and (c) protect the safety, rights, or property of any person, the public, or KiranaKorner.&lt;/li&gt;\r\n	&lt;li&gt;On a non-personal or aggregate basis — We share information on both a non-personally identifying basis (including, but not limited to, order and delivery details but not including credit card information) or an aggregate basis.&lt;/li&gt;\r\n	&lt;li&gt;To enable purchase of alcohol — When you buy alcohol using the Services, we may be required by law to share certain information with the retailer who makes the sale. This information could include, among other things, the names and addresses of the purchaser and recipient, government issued ID information, the quantity, brand, price, proof, and volume of alcohol purchased, and a recipient signature.&lt;/li&gt;\r\n	&lt;li&gt;For business purposes — We may share your information in connection with, or during negotiations of, any merger, sale of company assets, financing or acquisition of all or a portion of our business by another company. We may also share your information between and among KiranaKorner, and its current and future partners, affiliates, subsidiaries, and other companies under common control and ownership.&lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;p&gt;Changes to this Policy&lt;/p&gt;\r\n\r\n&lt;p&gt;This policy may change from time to time and any revised Privacy Policy will be posted at this page, so we encourage you to review it regularly.&lt;/p&gt;\r\n\r\n&lt;p&gt;You may request access to or correction of your personal information, or withdraw consent to our collection, use or disclosure of your personal information, by writing to our Trust &amp;amp; Safety Manager at the address or email below. These rights are subject to applicable contractual and legal restrictions and reasonable notice We may take reasonable steps to verify your identity before honoring any such requests.&lt;/p&gt;\r\n\r\n&lt;p&gt;Contact Information&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Questions or comments about our Privacy Policy? Contact Us at:&lt;/strong&gt;&lt;br /&gt;\r\n&lt;strong&gt;TheOrderApp&lt;/strong&gt;&lt;br /&gt;\r\nAmar Jyoti Road, Garulpar&lt;br /&gt;\r\nDeoria - India&lt;br /&gt;\r\nxxammuxx@gmail.com&lt;br /&gt;\r\n+91 8217763387&lt;br /&gt;\r\nFor customer service inquiries visit our&amp;nbsp;&lt;a href=&quot;https://demo.KiranaKorner.com/admin/help&quot;&gt;Help Center&lt;/a&gt;&lt;/p&gt;\r\n', 'Privacy Policy', 'Meta Tag Description\r\n', ''),
(5, 1, 'Returns &amp; Refund', '&lt;p&gt;&lt;strong&gt;Returns Policy&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;We accept returns of most products* for exchange or refund, less original shipping charge (if applicable), within 3 days of the delivery date. Items must be in new, unaltered, and unused condition**. Original sales receipt must accompany your return. Refunds can only be credited to the original payment method.&lt;br /&gt;\r\n&lt;br /&gt;\r\nWe\'ll pay the return shipping costs if the return is a result of our error (you received an incorrect or defective item).&lt;br /&gt;\r\n&lt;br /&gt;\r\nIf you need to return an item, please &lt;a href=&quot;http://localhost/theorderapp/index.php?route=information/contact&amp;amp;language=en-gb&quot; target=&quot;_blank&quot;&gt;Contact Us&lt;/a&gt; with your order number and details about the product you would like to return. We will respond quickly with instructions for how to return items from your order.&lt;/p&gt;\r\n\r\n&lt;p&gt;Return will be processed only if:&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li&gt;it is determined that the product was not damaged while in your possession;&lt;/li&gt;\r\n	&lt;li&gt;the product is not different from what was shipped to you;&lt;/li&gt;\r\n	&lt;li&gt;the product is returned in original condition (with brand’s/manufacturer\'s box, MRP tag intact, user manual, warranty card and all the accessories therein).&lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;p&gt;Products marked as &quot;non-returnable&quot; on the product detail page cannot be returned.&lt;/p&gt;\r\n\r\n&lt;p&gt;*Unless the product’s item description notes that it cannot be returned or stipulates a different return policy time period&lt;br /&gt;\r\n**New, Unaltered, and Unused Condition means an item that is returned without showing signs of wear or damage. For returns that show signs of use or are missing accessories, a 20% restocking fee will be charged.&lt;/p&gt;\r\n', 'Returns &amp; Refund', 'Returns &amp; Refund Policy', 'refund-policy, return-policy'),
(4, 1, 'Shipping &amp; Delivery', '&lt;p&gt;Currently, we only deliver only in and around Deoria, U.P., 274001.&lt;/p&gt;\r\n\r\n&lt;p&gt;Shipping, delivery costs will be displayed either on the individual item details page or on the checkout page.&lt;/p&gt;\r\n\r\n&lt;p&gt;The shipping charge will be FREE if your Amazon&amp;nbsp;order values Rs. 500 or more.&lt;/p&gt;\r\n\r\n&lt;p&gt;The minimum cart amount should be Rs. 350 for successful checkout.&lt;/p&gt;\r\n\r\n&lt;p&gt;The shipping speeds and charges for&amp;nbsp;items that are&amp;nbsp;shipped directly by the seller may vary per seller and are displayed before placing the order.&lt;/p&gt;\r\n\r\n&lt;p&gt;In addition to the above, Pay On Delivery (POD) may not be available for certain items and pincodes. Please check for POD availability at the time of checkout.&lt;/p&gt;\r\n\r\n&lt;p&gt;If there\'s no physical address to deliver your order, you may select a pick-up store (if any) in your vicinity to complete the order.&lt;/p&gt;\r\n', 'Shipping &amp; Delivery', 'Shipping &amp; Delivery', 'shipping-policy,delivery-policy');

-- --------------------------------------------------------

--
-- Table structure for table `oc_information_to_layout`
--

DROP TABLE IF EXISTS `oc_information_to_layout`;
CREATE TABLE IF NOT EXISTS `oc_information_to_layout` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  PRIMARY KEY (`information_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_information_to_layout`
--

INSERT INTO `oc_information_to_layout` (`information_id`, `store_id`, `layout_id`) VALUES
(3, 0, 0),
(2, 0, 0),
(5, 0, 0),
(5, 1, 0),
(1, 0, 0),
(1, 1, 0),
(4, 1, 0),
(4, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_information_to_store`
--

DROP TABLE IF EXISTS `oc_information_to_store`;
CREATE TABLE IF NOT EXISTS `oc_information_to_store` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`information_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_information_to_store`
--

INSERT INTO `oc_information_to_store` (`information_id`, `store_id`) VALUES
(1, 0),
(1, 1),
(2, 0),
(3, 0),
(4, 0),
(4, 1),
(5, 0),
(5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_language`
--

DROP TABLE IF EXISTS `oc_language`;
CREATE TABLE IF NOT EXISTS `oc_language` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(64) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`language_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_language`
--

INSERT INTO `oc_language` (`language_id`, `name`, `code`, `locale`, `image`, `sort_order`, `status`) VALUES
(1, 'English', 'en-gb', 'en-gb,en', 'gb.png', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_layout`
--

DROP TABLE IF EXISTS `oc_layout`;
CREATE TABLE IF NOT EXISTS `oc_layout` (
  `layout_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`layout_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_layout`
--

INSERT INTO `oc_layout` (`layout_id`, `name`) VALUES
(1, 'Home'),
(2, 'Product'),
(3, 'Category'),
(4, 'Default'),
(5, 'Manufacturer'),
(6, 'Account'),
(7, 'Checkout'),
(8, 'Contact'),
(9, 'Sitemap'),
(10, 'Affiliate'),
(11, 'Information'),
(12, 'Compare'),
(13, 'Search'),
(14, 'Credits'),
(15, 'Wide (No left and right columns)');

-- --------------------------------------------------------

--
-- Table structure for table `oc_layout_module`
--

DROP TABLE IF EXISTS `oc_layout_module`;
CREATE TABLE IF NOT EXISTS `oc_layout_module` (
  `layout_module_id` int(11) NOT NULL AUTO_INCREMENT,
  `layout_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `position` varchar(14) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`layout_module_id`)
) ENGINE=MyISAM AUTO_INCREMENT=145 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_layout_module`
--

INSERT INTO `oc_layout_module` (`layout_module_id`, `layout_id`, `code`, `position`, `sort_order`) VALUES
(2, 4, '0', 'content_top', 0),
(3, 4, '0', 'content_top', 1),
(20, 5, '0', 'column_left', 2),
(69, 10, 'account', 'column_right', 1),
(68, 6, 'account', 'column_right', 1),
(143, 1, 'category', 'content_top', 5),
(142, 1, 'carousel.29', 'content_top', 4),
(141, 1, 'latest.35', 'content_top', 3),
(72, 3, 'category', 'column_left', 1),
(73, 3, 'banner.30', 'column_left', 2),
(140, 1, 'featured.34', 'content_top', 2),
(110, 6, 'tawkto', 'content_bottom', 999),
(111, 10, 'tawkto', 'content_bottom', 999),
(112, 3, 'tawkto', 'content_bottom', 999),
(113, 7, 'tawkto', 'content_bottom', 999),
(114, 12, 'tawkto', 'content_bottom', 999),
(115, 8, 'tawkto', 'content_bottom', 999),
(116, 14, 'tawkto', 'content_bottom', 999),
(117, 4, 'tawkto', 'content_bottom', 999),
(119, 11, 'tawkto', 'content_bottom', 999),
(120, 5, 'tawkto', 'content_bottom', 999),
(121, 2, 'tawkto', 'content_bottom', 999),
(122, 13, 'tawkto', 'content_bottom', 999),
(123, 9, 'tawkto', 'content_bottom', 999),
(139, 1, 'topcategory.40', 'content_top', 1),
(138, 1, 'slideshow.27', 'content_top', 0),
(144, 1, 'tawkto', 'content_bottom', 999);

-- --------------------------------------------------------

--
-- Table structure for table `oc_layout_route`
--

DROP TABLE IF EXISTS `oc_layout_route`;
CREATE TABLE IF NOT EXISTS `oc_layout_route` (
  `layout_route_id` int(11) NOT NULL AUTO_INCREMENT,
  `layout_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL,
  PRIMARY KEY (`layout_route_id`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_layout_route`
--

INSERT INTO `oc_layout_route` (`layout_route_id`, `layout_id`, `store_id`, `route`) VALUES
(1, 6, 0, 'account/%'),
(2, 6, 0, 'information/gdpr'),
(3, 10, 0, 'affiliate/%'),
(4, 3, 0, 'product/category'),
(6, 2, 0, 'product/product'),
(7, 11, 0, 'information/information'),
(8, 7, 0, 'checkout/%'),
(9, 8, 0, 'information/contact'),
(10, 9, 0, 'information/sitemap'),
(11, 4, 0, ''),
(12, 5, 0, 'product/manufacturer'),
(13, 12, 0, 'product/compare'),
(14, 13, 0, 'product/search'),
(15, 6, 1, 'account/%'),
(16, 6, 1, 'information/gdpr'),
(17, 10, 1, 'affiliate/%'),
(18, 3, 1, 'product/category'),
(20, 2, 1, 'product/product'),
(21, 11, 1, 'information/information'),
(22, 7, 1, 'checkout/%'),
(23, 8, 1, 'information/contact'),
(24, 9, 1, 'information/sitemap'),
(25, 4, 1, ''),
(26, 5, 1, 'product/manufacturer'),
(27, 12, 1, 'product/compare'),
(28, 13, 1, 'product/search'),
(29, 14, 0, 'credits'),
(67, 1, 2, 'common/home'),
(66, 1, 0, 'common/home'),
(44, 6, 2, 'account/%'),
(45, 6, 2, 'information/gdpr'),
(46, 10, 2, 'affiliate/%'),
(47, 3, 2, 'product/category'),
(48, 2, 2, 'product/product'),
(49, 11, 2, 'information/information'),
(50, 7, 2, 'checkout/%'),
(51, 8, 2, 'information/contact'),
(52, 9, 2, 'information/sitemap'),
(53, 4, 2, ''),
(54, 5, 2, 'product/manufacturer'),
(55, 12, 2, 'product/compare'),
(56, 13, 2, 'product/search'),
(57, 14, 2, 'credits'),
(65, 1, 1, 'common/home');

-- --------------------------------------------------------

--
-- Table structure for table `oc_length_class`
--

DROP TABLE IF EXISTS `oc_length_class`;
CREATE TABLE IF NOT EXISTS `oc_length_class` (
  `length_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `value` decimal(15,8) NOT NULL,
  PRIMARY KEY (`length_class_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_length_class`
--

INSERT INTO `oc_length_class` (`length_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '10.00000000'),
(3, '0.39370000');

-- --------------------------------------------------------

--
-- Table structure for table `oc_length_class_description`
--

DROP TABLE IF EXISTS `oc_length_class_description`;
CREATE TABLE IF NOT EXISTS `oc_length_class_description` (
  `length_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL,
  PRIMARY KEY (`length_class_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_length_class_description`
--

INSERT INTO `oc_length_class_description` (`length_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Centimeter', 'cm'),
(2, 1, 'Millimeter', 'mm'),
(3, 1, 'Inch', 'in');

-- --------------------------------------------------------

--
-- Table structure for table `oc_location`
--

DROP TABLE IF EXISTS `oc_location`;
CREATE TABLE IF NOT EXISTS `oc_location` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `address` text NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `geocode` varchar(32) NOT NULL,
  `image` varchar(255) NOT NULL,
  `open` text NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`location_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_location`
--

INSERT INTO `oc_location` (`location_id`, `name`, `address`, `telephone`, `fax`, `geocode`, `image`, `open`, `comment`) VALUES
(1, 'Kirana Corner', 'Amar Jyoti Road, Deoria', '8019773695', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_manufacturer`
--

DROP TABLE IF EXISTS `oc_manufacturer`;
CREATE TABLE IF NOT EXISTS `oc_manufacturer` (
  `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`manufacturer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_manufacturer`
--

INSERT INTO `oc_manufacturer` (`manufacturer_id`, `name`, `image`, `sort_order`) VALUES
(5, 'Fortune', 'catalog/demo/htc_logo.jpg', 0),
(6, 'Palm', 'catalog/demo/palm_logo.jpg', 0),
(7, 'Patanjali', 'catalog/demo/hp_logo.jpg', 0),
(8, 'Aashirwad', 'catalog/demo/apple_logo.jpg', 0),
(9, 'Amul', 'catalog/demo/canon_logo.jpg', 0),
(10, 'Nestle', 'catalog/demo/sony_logo.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_manufacturer_to_store`
--

DROP TABLE IF EXISTS `oc_manufacturer_to_store`;
CREATE TABLE IF NOT EXISTS `oc_manufacturer_to_store` (
  `manufacturer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`manufacturer_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_manufacturer_to_store`
--

INSERT INTO `oc_manufacturer_to_store` (`manufacturer_id`, `store_id`) VALUES
(5, 0),
(5, 1),
(6, 0),
(7, 0),
(7, 1),
(8, 0),
(8, 1),
(9, 0),
(9, 1),
(10, 0),
(10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_marketing`
--

DROP TABLE IF EXISTS `oc_marketing`;
CREATE TABLE IF NOT EXISTS `oc_marketing` (
  `marketing_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `code` varchar(64) NOT NULL,
  `clicks` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`marketing_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_marketing_report`
--

DROP TABLE IF EXISTS `oc_marketing_report`;
CREATE TABLE IF NOT EXISTS `oc_marketing_report` (
  `marketing_report_id` int(11) NOT NULL AUTO_INCREMENT,
  `marketing_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `country` varchar(2) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`marketing_report_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_modification`
--

DROP TABLE IF EXISTS `oc_modification`;
CREATE TABLE IF NOT EXISTS `oc_modification` (
  `modification_id` int(11) NOT NULL AUTO_INCREMENT,
  `extension_install_id` int(11) NOT NULL,
  `extension_download_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(64) NOT NULL,
  `author` varchar(64) NOT NULL,
  `version` varchar(32) NOT NULL,
  `link` varchar(255) NOT NULL,
  `xml` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`modification_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_module`
--

DROP TABLE IF EXISTS `oc_module`;
CREATE TABLE IF NOT EXISTS `oc_module` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `code` varchar(32) NOT NULL,
  `setting` text NOT NULL,
  PRIMARY KEY (`module_id`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_module`
--

INSERT INTO `oc_module` (`module_id`, `name`, `code`, `setting`) VALUES
(30, 'Category', 'banner', '{\"name\":\"Category\",\"banner_id\":\"6\",\"width\":\"250\",\"height\":\"160\",\"status\":\"1\"}'),
(29, 'Popular Categories', 'carousel', '{\"name\":\"Popular Categories\",\"banner_id\":\"6\",\"width\":\"130\",\"height\":\"100\",\"status\":\"0\"}'),
(27, 'Home Page', 'slideshow', '{\"name\":\"Home Page\",\"banner_id\":\"7\",\"width\":\"1140\",\"height\":\"380\",\"status\":\"1\"}'),
(31, 'Banner 1', 'banner', '{\"name\":\"Banner 1\",\"banner_id\":\"6\",\"width\":\"350\",\"height\":\"160\",\"status\":\"1\"}'),
(38, 'Delivery Notes', 'html', '{\"name\":\"Delivery Notes\",\"module_description\":{\"1\":{\"title\":\"Delivery Notes\",\"description\":\"&lt;p&gt;Orders delivered only in Deoria.&lt;\\/p&gt;\\r\\n\"}},\"status\":\"1\"}'),
(33, 'Best Seller', 'bestseller', '{\"name\":\"Best Seller\",\"limit\":\"5\",\"width\":\"200\",\"height\":\"200\",\"status\":\"1\"}'),
(34, 'Latest', 'featured', '{\"name\":\"Latest\",\"product_name\":\"\",\"product\":[\"40\",\"30\",\"45\",\"65\",\"41\",\"44\"],\"limit\":\"8\",\"width\":\"250\",\"height\":\"250\",\"status\":\"1\"}'),
(35, 'Latest', 'latest', '{\"name\":\"Latest\",\"limit\":\"6\",\"width\":\"200\",\"height\":\"200\",\"status\":\"1\"}'),
(36, 'Top Categories', 'bannergrid', '{\"name\":\"Top Categories\",\"banner_id\":\"6\",\"width\":\"200\",\"height\":\"200\",\"grid\":\"6\",\"status\":\"0\"}'),
(37, 'Popular Categories', 'topcategory', '{\"name\":\"Popular Categories\",\"banner_id\":\"7\",\"width\":\"400\",\"height\":\"180\",\"status\":\"1\"}'),
(39, 'Grocery Categories', 'carousel', '{\"name\":\"Grocery Categories\",\"banner_id\":\"9\",\"width\":\"180\",\"height\":\"120\",\"status\":\"1\"}'),
(40, 'Grocery Categories', 'topcategory', '{\"name\":\"Grocery Categories\",\"banner_id\":\"9\",\"width\":\"100\",\"height\":\"100\",\"status\":\"1\"}'),
(41, 'Offers and Discounts', 'special', '{\"name\":\"Offers and Discounts\",\"limit\":\"5\",\"width\":\"200\",\"height\":\"200\",\"status\":\"1\"}');

-- --------------------------------------------------------

--
-- Table structure for table `oc_option`
--

DROP TABLE IF EXISTS `oc_option`;
CREATE TABLE IF NOT EXISTS `oc_option` (
  `option_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`option_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option`
--

INSERT INTO `oc_option` (`option_id`, `type`, `sort_order`) VALUES
(1, 'radio', 1),
(2, 'checkbox', 2),
(4, 'text', 3),
(5, 'select', 4),
(6, 'textarea', 5),
(7, 'file', 6),
(8, 'date', 7),
(9, 'time', 8),
(10, 'datetime', 9),
(11, 'select', 10),
(12, 'date', 11);

-- --------------------------------------------------------

--
-- Table structure for table `oc_option_description`
--

DROP TABLE IF EXISTS `oc_option_description`;
CREATE TABLE IF NOT EXISTS `oc_option_description` (
  `option_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`option_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option_description`
--

INSERT INTO `oc_option_description` (`option_id`, `language_id`, `name`) VALUES
(1, 1, 'Radio'),
(2, 1, 'Checkbox'),
(4, 1, 'Text'),
(6, 1, 'Textarea'),
(8, 1, 'Date'),
(7, 1, 'File'),
(5, 1, 'Select'),
(9, 1, 'Time'),
(10, 1, 'Date &amp; Time'),
(12, 1, 'Delivery Date'),
(11, 1, 'Size');

-- --------------------------------------------------------

--
-- Table structure for table `oc_option_value`
--

DROP TABLE IF EXISTS `oc_option_value`;
CREATE TABLE IF NOT EXISTS `oc_option_value` (
  `option_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`option_value_id`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option_value`
--

INSERT INTO `oc_option_value` (`option_value_id`, `option_id`, `image`, `sort_order`) VALUES
(43, 1, '', 3),
(32, 1, '', 1),
(45, 2, '', 4),
(44, 2, '', 3),
(42, 5, '', 4),
(41, 5, '', 3),
(39, 5, '', 1),
(40, 5, '', 2),
(31, 1, '', 2),
(23, 2, '', 1),
(24, 2, '', 2),
(46, 11, '', 1),
(47, 11, '', 2),
(48, 11, '', 3);

-- --------------------------------------------------------

--
-- Table structure for table `oc_option_value_description`
--

DROP TABLE IF EXISTS `oc_option_value_description`;
CREATE TABLE IF NOT EXISTS `oc_option_value_description` (
  `option_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`option_value_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option_value_description`
--

INSERT INTO `oc_option_value_description` (`option_value_id`, `language_id`, `option_id`, `name`) VALUES
(43, 1, 1, 'Large'),
(32, 1, 1, 'Small'),
(45, 1, 2, 'Checkbox 4'),
(44, 1, 2, 'Checkbox 3'),
(31, 1, 1, 'Medium'),
(42, 1, 5, 'Yellow'),
(41, 1, 5, 'Green'),
(39, 1, 5, 'Red'),
(40, 1, 5, 'Blue'),
(23, 1, 2, 'Checkbox 1'),
(24, 1, 2, 'Checkbox 2'),
(48, 1, 11, 'Large'),
(47, 1, 11, 'Medium'),
(46, 1, 11, 'Small');

-- --------------------------------------------------------

--
-- Table structure for table `oc_order`
--

DROP TABLE IF EXISTS `oc_order`;
CREATE TABLE IF NOT EXISTS `oc_order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_no` int(11) NOT NULL DEFAULT '0',
  `invoice_prefix` varchar(26) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `store_name` varchar(64) NOT NULL,
  `store_url` varchar(255) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `custom_field` text NOT NULL,
  `payment_firstname` varchar(32) NOT NULL,
  `payment_lastname` varchar(32) NOT NULL,
  `payment_company` varchar(60) NOT NULL,
  `payment_address_1` varchar(128) NOT NULL,
  `payment_address_2` varchar(128) NOT NULL,
  `payment_city` varchar(128) NOT NULL,
  `payment_postcode` varchar(10) NOT NULL,
  `payment_country` varchar(128) NOT NULL,
  `payment_country_id` int(11) NOT NULL,
  `payment_zone` varchar(128) NOT NULL,
  `payment_zone_id` int(11) NOT NULL,
  `payment_address_format` text NOT NULL,
  `payment_custom_field` text NOT NULL,
  `payment_method` varchar(128) NOT NULL,
  `payment_code` varchar(128) NOT NULL,
  `shipping_firstname` varchar(32) NOT NULL,
  `shipping_lastname` varchar(32) NOT NULL,
  `shipping_company` varchar(60) NOT NULL,
  `shipping_address_1` varchar(128) NOT NULL,
  `shipping_address_2` varchar(128) NOT NULL,
  `shipping_city` varchar(128) NOT NULL,
  `shipping_postcode` varchar(10) NOT NULL,
  `shipping_country` varchar(128) NOT NULL,
  `shipping_country_id` int(11) NOT NULL,
  `shipping_zone` varchar(128) NOT NULL,
  `shipping_zone_id` int(11) NOT NULL,
  `shipping_address_format` text NOT NULL,
  `shipping_custom_field` text NOT NULL,
  `shipping_method` varchar(128) NOT NULL,
  `shipping_code` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `order_status_id` int(11) NOT NULL DEFAULT '0',
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(15,4) NOT NULL,
  `marketing_id` int(11) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `language_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `currency_value` decimal(15,8) NOT NULL DEFAULT '1.00000000',
  `ip` varchar(40) NOT NULL,
  `forwarded_ip` varchar(40) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `accept_language` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=MyISAM AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_order`
--

INSERT INTO `oc_order` (`order_id`, `invoice_no`, `invoice_prefix`, `store_id`, `store_name`, `store_url`, `customer_id`, `customer_group_id`, `firstname`, `lastname`, `email`, `telephone`, `fax`, `custom_field`, `payment_firstname`, `payment_lastname`, `payment_company`, `payment_address_1`, `payment_address_2`, `payment_city`, `payment_postcode`, `payment_country`, `payment_country_id`, `payment_zone`, `payment_zone_id`, `payment_address_format`, `payment_custom_field`, `payment_method`, `payment_code`, `shipping_firstname`, `shipping_lastname`, `shipping_company`, `shipping_address_1`, `shipping_address_2`, `shipping_city`, `shipping_postcode`, `shipping_country`, `shipping_country_id`, `shipping_zone`, `shipping_zone_id`, `shipping_address_format`, `shipping_custom_field`, `shipping_method`, `shipping_code`, `comment`, `total`, `order_status_id`, `affiliate_id`, `commission`, `marketing_id`, `tracking`, `language_id`, `currency_id`, `currency_code`, `currency_value`, `ip`, `forwarded_ip`, `user_agent`, `accept_language`, `date_added`, `date_modified`) VALUES
(1, 0, 'INV-2020-00', 0, 'Grocery Saver', 'http://localhost/theorderapp/', 0, 1, 'Abhishek', 'Chaurasia', 'chaurasiaabhi09@gmail.com', '08217763387', '', '[]', 'Abhishek', 'Chaurasia', '', 'Amar Jyoti Road, Garulpar, Deoria', 'Amar Jyoti Road, Garulpar, Deoria', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', 'Chaurasia', '', 'Amar Jyoti Road, Garulpar, Deoria', 'Amar Jyoti Road, Garulpar, Deoria', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '284.9900', 2, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-02-22 14:11:51', '2020-02-22 14:12:47'),
(2, 1, 'INV-2020-00', 0, 'Grocery Saver', 'http://localhost/theorderapp/', 1, 1, 'Amit', 'Chaurasia', 'xxammuxx@gmail.com', '8019773695', '', '[]', 'Amit', 'Chaurasia', 'None', 'Balaji Delux mens Hostel (Sai Serenity)', 'Opp. Gruhasiri Pride, Gachibowli', 'Hyderabad', '500032', 'India', 99, 'Telangana', 4231, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Amit', 'Chaurasia', 'None', 'Balaji Delux mens Hostel (Sai Serenity)', 'Opp. Gruhasiri Pride, Gachibowli', 'Hyderabad', '500032', 'India', 99, 'Telangana', 4231, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '125.0000', 3, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8', '2020-02-22 15:32:32', '2020-02-22 19:26:06'),
(3, 0, 'INV-2020-00', 0, 'Grocery Saver', 'http://localhost/theorderapp/', 8, 1, 'Abhishek', 'Chaurasia', 'chaurasiaab.hi09@gmail.com', '5465465534', '', '', 'Abhishek', 'Chaurasia', '', 'Amar Jyoti Road, Garulpar, Deoria', '', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', 'Chaurasia', '', 'Amar Jyoti Road, Garulpar, Deoria', '', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '559.9800', 5, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-02-27 15:51:34', '2020-02-27 15:58:51'),
(4, 0, 'INV-2020-00', 0, 'Grocery Saver', 'http://localhost/theorderapp/', 8, 1, 'Abhishek', 'Chaurasia', 'chaurasiaab.hi09@gmail.com', '5465465534', '', '', 'Abhishek', 'Chaurasia', '', 'Amar Jyoti Road, Garulpar, Deoria', '', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', 'Chaurasia', '', 'Amar Jyoti Road, Garulpar, Deoria', '', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '2879.9000', 7, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-02-28 12:31:45', '2020-02-28 13:07:36'),
(5, 0, 'INV-2020-00', 0, 'Grocery Saver', 'http://localhost/theorderapp/', 9, 1, 'Amit', 'Chaurasia', 'xxammuxx@gmail.com', '+918019773695', '', '{\"10\":\"\"}', 'Amit', 'Chaurasia', 'None', 'None', 'None', 'Deoria', '243', 'India', 99, 'Mizoram', 1496, '', '[]', 'Cash On Delivery', 'cod', 'Amit', 'Chaurasia', 'None', 'None', 'None', 'Deoria', '243', 'India', 99, 'Mizoram', 1496, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '2000.0000', 7, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/82.0.4073.0 Safari/537.36', 'en-US,en;q=0.9', '2020-02-29 22:03:46', '2020-02-29 22:29:03'),
(6, 0, 'INV-2020-00', 0, 'Grocery Saver', 'http://localhost/theorderapp/', 9, 1, 'Amit', 'Chaurasia', 'xxammuxx@gmail.com', '9703465708', '', '', 'Amit', 'Chaurasia', 'None', 'Balaji Delux mens Hostel (Sai Serenity)', 'Opp. Gruhasiri Pride, Gachibowli', 'Hyderabad', '500032', 'India', 99, 'Telangana', 4231, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Amit', 'Chaurasia', 'None', 'Balaji Delux mens Hostel (Sai Serenity)', 'Opp. Gruhasiri Pride, Gachibowli', 'Hyderabad', '500032', 'India', 99, 'Telangana', 4231, '', '[]', 'Flat Shipping Rate', 'flat.flat', 'Please deliver free.', '2730.0000', 7, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8', '2020-02-29 22:33:54', '2020-02-29 23:21:21'),
(7, 0, 'INV-2020-00', 0, 'Grocery Saver', 'http://localhost/theorderapp/', 9, 1, 'Amit', 'Chaurasia', 'xxammuxx@gmail.com', '9703465708', '', '{\"10\":\"\"}', 'Amit', 'Chaurasia', 'None', 'Balaji Delux mens Hostel (Sai Serenity)', 'Opp. Gruhasiri Pride, Gachibowli', 'Hyderabad', '500032', 'India', 99, 'Telangana', 4231, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Amit', 'Chaurasia', 'None', 'Balaji Delux mens Hostel (Sai Serenity)', 'Opp. Gruhasiri Pride, Gachibowli', 'Hyderabad', '500032', 'India', 99, 'Telangana', 4231, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '350.0000', 7, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8', '2020-02-29 22:48:56', '2020-02-29 23:04:09'),
(8, 0, 'INV-2020-00', 0, 'Grocery Saver', 'http://localhost/theorderapp/', 9, 1, 'Amit', 'Chaurasia', 'xxammuxx@gmail.com', '9703465708', '', '{\"10\":\"\"}', 'Amit', 'Chaurasia', 'None', 'Balaji Delux mens Hostel (Sai Serenity)', 'Opp. Gruhasiri Pride, Gachibowli', 'Hyderabad', '500032', 'India', 99, 'Telangana', 4231, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Amit', 'Chaurasia', 'None', 'Balaji Delux mens Hostel (Sai Serenity)', 'Opp. Gruhasiri Pride, Gachibowli', 'Hyderabad', '500032', 'India', 99, 'Telangana', 4231, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '540.0000', 5, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8', '2020-02-29 22:59:20', '2020-02-29 23:01:01'),
(9, 0, 'INV-2020-00', 0, 'Grocery Saver', 'http://localhost/theorderapp/', 7, 1, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', '', 'tyty', 'Deoria', '274001', 'India', 99, 'Jowzjan', 12, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', '', '', '', 'tyty', 'Deoria', '274001', 'India', 99, 'Jowzjan', 12, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-05 12:32:25', '2020-03-05 12:32:25'),
(10, 0, 'INV-2020-00', 0, 'Grocery Saver', 'http://localhost/theorderapp/', 7, 1, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', '', 'test address', 'Deoria', '274001', 'India', 99, 'Jowzjan', 12, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', '', '', '', 'test address', 'Deoria', '274001', 'India', 99, 'Jowzjan', 12, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-05 13:22:31', '2020-03-05 13:22:31'),
(11, 0, 'INV-2020-00', 0, 'Grocery Saver', 'http://localhost/theorderapp/', 7, 1, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', '', 'test address', 'Deoria', '274001', 'India', 99, 'Jowzjan', 12, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', '', '', '', 'test address', 'Deoria', '274001', 'India', 99, 'Jowzjan', 12, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-05 13:43:40', '2020-03-05 13:43:40'),
(12, 0, 'INV-2020-00', 0, 'Grocery Saver', 'http://localhost/theorderapp/', 7, 1, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', '', 'test address', 'Deoria', '274001', 'India', 99, 'Jowzjan', 12, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', '', '', '', 'test address', 'Deoria', '274001', 'India', 99, 'Jowzjan', 12, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-05 13:44:08', '2020-03-05 13:44:08'),
(13, 0, 'INV-2020-00', 0, 'Grocery Saver', 'http://localhost/theorderapp/', 7, 1, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '594.9800', 7, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-05 14:08:19', '2020-03-05 18:26:14'),
(14, 0, 'INV-2020-00', 0, 'Grocery Saver', 'http://localhost/theorderapp/', 7, 1, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'near vijay takij', 'amar jyoti road', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', '', '', 'near vijay takij', 'amar jyoti road', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '559.9800', 17, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-05 14:14:28', '2020-03-05 18:17:04'),
(15, 0, 'INV-2020-00', 0, 'Grocery Saver', 'http://localhost/theorderapp/', 7, 1, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '359.9900', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-06 13:51:43', '2020-03-06 13:51:43'),
(16, 0, 'INV-2020-00', 0, 'Grocery Saver', 'http://localhost/theorderapp/', 7, 1, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '359.9900', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-06 13:54:09', '2020-03-06 13:54:09'),
(17, 0, 'INV-2020-00', 0, 'Grocery Saver', 'http://localhost/theorderapp/', 7, 1, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '359.9900', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-06 13:55:55', '2020-03-06 13:55:55'),
(18, 0, 'INV-2020-00', 0, 'Grocery Saver', 'http://localhost/theorderapp/', 7, 1, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '364.9900', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-06 13:59:49', '2020-03-06 13:59:49'),
(19, 0, 'INV-2020-00', 0, 'Grocery Saver', 'http://localhost/theorderapp/', 7, 1, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '3164.9000', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-07 13:15:46', '2020-03-07 13:15:46'),
(20, 0, 'INV-2020-00', 0, 'Grocery Saver', 'http://localhost/theorderapp/', 7, 1, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '3164.9000', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-07 13:16:43', '2020-03-07 13:16:43'),
(21, 0, 'INV-2020-00', 0, 'Grocery Saver', 'http://localhost/theorderapp/', 7, 1, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '3164.9000', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-07 13:17:01', '2020-03-07 13:17:01'),
(22, 0, 'INV-2020-00', 0, 'Grocery Saver', 'http://localhost/theorderapp/', 7, 1, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '3164.9000', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-07 13:17:05', '2020-03-07 13:17:05'),
(23, 0, 'INV-2020-00', 0, 'Grocery Saver', 'http://localhost/theorderapp/', 7, 1, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '3164.9000', 17, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-07 13:17:26', '2020-03-07 13:36:48'),
(24, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 7, 1, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '3119.8900', 17, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-07 15:18:46', '2020-03-08 11:38:33'),
(25, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 7, 1, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '664.9800', 5, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-08 11:42:30', '2020-03-08 11:44:12'),
(26, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 7, 1, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '340.0000', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-08 11:46:45', '2020-03-08 11:46:45'),
(27, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 7, 1, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '635.0000', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-13 05:36:00', '2020-03-13 05:36:00'),
(28, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 7, 1, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', '', 'qwerty', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '635.0000', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-13 05:36:10', '2020-03-13 05:36:10'),
(29, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '562.9800', 2, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-16 12:27:06', '2020-03-16 13:10:55'),
(30, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '561.9800', 1, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-16 13:16:58', '2020-03-16 13:18:24'),
(31, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '556.9800', 1, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-16 13:22:55', '2020-03-16 13:23:32'),
(32, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '556.9800', 1, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-16 13:27:13', '2020-03-16 13:27:29'),
(33, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '350.9900', 1, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-16 14:38:07', '2020-03-16 14:38:33'),
(34, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 13:57:18', '2020-03-17 13:57:18'),
(35, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 14:30:10', '2020-03-17 14:30:10'),
(36, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 14:41:24', '2020-03-17 14:41:24'),
(37, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 14:42:16', '2020-03-17 14:42:16'),
(38, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 14:45:05', '2020-03-17 14:45:05'),
(39, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 14:45:38', '2020-03-17 14:45:38'),
(40, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 14:46:33', '2020-03-17 14:46:33'),
(41, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 14:56:11', '2020-03-17 14:56:11'),
(42, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 14:57:28', '2020-03-17 14:57:28'),
(43, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 14:58:21', '2020-03-17 14:58:21'),
(44, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 14:58:50', '2020-03-17 14:58:50'),
(45, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 14:59:25', '2020-03-17 14:59:25'),
(46, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 15:00:11', '2020-03-17 15:00:11'),
(47, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 15:00:59', '2020-03-17 15:00:59'),
(48, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 15:04:31', '2020-03-17 15:04:31'),
(49, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 15:06:29', '2020-03-17 15:06:29'),
(50, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 15:07:16', '2020-03-17 15:07:16'),
(51, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 15:07:59', '2020-03-17 15:07:59'),
(52, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 15:08:49', '2020-03-17 15:08:49'),
(53, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 15:11:35', '2020-03-17 15:11:35'),
(54, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 15:13:09', '2020-03-17 15:13:09'),
(55, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 15:14:15', '2020-03-17 15:14:15'),
(56, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 15:15:35', '2020-03-17 15:15:35'),
(57, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 15:18:35', '2020-03-17 15:18:35'),
(58, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 15:19:23', '2020-03-17 15:19:23'),
(59, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 15:19:35', '2020-03-17 15:19:35'),
(60, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 15:20:09', '2020-03-17 15:20:09'),
(61, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 15:20:25', '2020-03-17 15:20:25'),
(62, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 15:20:36', '2020-03-17 15:20:36'),
(63, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 15:20:51', '2020-03-17 15:20:51'),
(64, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 15:21:03', '2020-03-17 15:21:03'),
(65, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 15:21:58', '2020-03-17 15:21:58'),
(66, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Pickup From Store', 'pickup.pickup', '', '594.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 15:24:13', '2020-03-17 15:24:13');
INSERT INTO `oc_order` (`order_id`, `invoice_no`, `invoice_prefix`, `store_id`, `store_name`, `store_url`, `customer_id`, `customer_group_id`, `firstname`, `lastname`, `email`, `telephone`, `fax`, `custom_field`, `payment_firstname`, `payment_lastname`, `payment_company`, `payment_address_1`, `payment_address_2`, `payment_city`, `payment_postcode`, `payment_country`, `payment_country_id`, `payment_zone`, `payment_zone_id`, `payment_address_format`, `payment_custom_field`, `payment_method`, `payment_code`, `shipping_firstname`, `shipping_lastname`, `shipping_company`, `shipping_address_1`, `shipping_address_2`, `shipping_city`, `shipping_postcode`, `shipping_country`, `shipping_country_id`, `shipping_zone`, `shipping_zone_id`, `shipping_address_format`, `shipping_custom_field`, `shipping_method`, `shipping_code`, `comment`, `total`, `order_status_id`, `affiliate_id`, `commission`, `marketing_id`, `tracking`, `language_id`, `currency_id`, `currency_code`, `currency_value`, `ip`, `forwarded_ip`, `user_agent`, `accept_language`, `date_added`, `date_modified`) VALUES
(68, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '404.9900', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 16:21:14', '2020-03-17 16:21:14'),
(69, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '404.9900', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 16:21:57', '2020-03-17 16:21:57'),
(70, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '404.9900', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 16:23:22', '2020-03-17 16:23:22'),
(71, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '404.9900', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 17:47:42', '2020-03-17 17:47:42'),
(72, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '404.9900', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 17:48:36', '2020-03-17 17:48:36'),
(73, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Credit Card / Debit Card / Net Banking (Razorpay)', 'razorpay', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '404.9900', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-17 17:48:51', '2020-03-17 17:48:51'),
(74, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '684.9800', 0, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-19 12:26:06', '2020-03-19 12:26:06'),
(75, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '684.9800', 1, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-19 13:06:08', '2020-03-19 13:06:50'),
(76, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '564.9800', 1, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-19 13:23:58', '2020-03-19 13:24:17'),
(77, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '564.9800', 1, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-19 13:24:46', '2020-03-19 13:25:02'),
(78, 0, 'INV-2020-00', 0, 'KiranaKorner', 'http://localhost/theorderapp/', 10, 3, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '', '', '', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Cash On Delivery', 'cod', 'Abhishek', '', '', 'dth', 'acfdf', 'Deoria', '274001', 'India', 99, 'Uttar Pradesh', 1505, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '404.9900', 1, 0, '0.0000', 0, '', 1, 5, 'INR', '1.00000000', '::1', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'en-US,en;q=0.9,hi;q=0.8,pt-BR;q=0.7,pt;q=0.6', '2020-03-19 14:20:10', '2020-03-19 14:20:14');

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_history`
--

DROP TABLE IF EXISTS `oc_order_history`;
CREATE TABLE IF NOT EXISTS `oc_order_history` (
  `order_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`order_history_id`)
) ENGINE=MyISAM AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_order_history`
--

INSERT INTO `oc_order_history` (`order_history_id`, `order_id`, `order_status_id`, `notify`, `comment`, `date_added`) VALUES
(1, 1, 2, 1, 'Payment Successful. Razorpay Payment Id:pay_EJj7Khspb1TieH', '2020-02-22 14:12:47'),
(2, 2, 2, 1, 'Payment Successful. Razorpay Payment Id:pay_EJkUHj8Jk0dJVP', '2020-02-22 15:33:15'),
(3, 2, 0, 0, '', '2020-02-22 19:26:02'),
(4, 2, 3, 0, '', '2020-02-22 19:26:06'),
(5, 3, 1, 0, '', '2020-02-27 15:51:48'),
(6, 3, 5, 0, '', '2020-02-27 15:58:51'),
(7, 4, 1, 0, '', '2020-02-28 12:32:03'),
(8, 4, 7, 1, 'Order ID #4 Cancelled', '2020-02-28 13:07:36'),
(9, 5, 1, 0, '', '2020-02-29 22:04:05'),
(10, 5, 0, 0, '', '2020-02-29 22:27:40'),
(11, 5, 2, 0, '', '2020-02-29 22:27:49'),
(12, 5, 0, 0, '', '2020-02-29 22:28:19'),
(13, 5, 2, 0, '', '2020-02-29 22:28:27'),
(14, 5, 7, 1, 'Order ID #5 Cancelled', '2020-02-29 22:29:03'),
(15, 6, 2, 1, 'Payment Successful. Razorpay Payment Id:pay_EMdPSCx2w8GfXN', '2020-02-29 22:34:57'),
(16, 7, 2, 1, 'Payment Successful. Razorpay Payment Id:pay_EMdffs6Afb4Cjw', '2020-02-29 22:50:16'),
(17, 8, 2, 1, 'Payment Successful. Razorpay Payment Id:pay_EMdpxBhjHFTFcn', '2020-02-29 23:00:00'),
(18, 8, 0, 0, '', '2020-02-29 23:00:49'),
(19, 8, 5, 0, '', '2020-02-29 23:01:01'),
(20, 7, 0, 0, '', '2020-02-29 23:03:36'),
(21, 7, 3, 0, '', '2020-02-29 23:03:43'),
(22, 7, 7, 1, 'Order ID #7 Cancelled', '2020-02-29 23:04:09'),
(23, 6, 7, 1, 'Order ID #6 Cancelled', '2020-02-29 23:21:21'),
(24, 13, 1, 0, '', '2020-03-05 14:08:37'),
(25, 14, 1, 0, '', '2020-03-05 14:14:45'),
(26, 14, 17, 0, 'Shop doesn;t delivers in selected timeslot', '2020-03-05 18:17:04'),
(27, 13, 7, 1, 'gdfhrtfhyhyt', '2020-03-05 18:26:14'),
(28, 23, 1, 0, '', '2020-03-07 13:18:04'),
(29, 23, 17, 1, '', '2020-03-07 13:36:48'),
(30, 24, 1, 0, '', '2020-03-07 15:19:10'),
(31, 24, 5, 0, '', '2020-03-08 11:38:33'),
(32, 25, 1, 0, '', '2020-03-08 11:42:46'),
(33, 25, 5, 0, '', '2020-03-08 11:44:12'),
(34, 29, 1, 0, '', '2020-03-16 12:27:30'),
(35, 29, 2, 0, 'accepted order', '2020-03-16 13:10:55'),
(36, 30, 1, 0, '', '2020-03-16 13:18:24'),
(37, 31, 1, 0, '', '2020-03-16 13:23:32'),
(38, 32, 1, 0, '', '2020-03-16 13:27:29'),
(39, 33, 1, 0, '', '2020-03-16 14:38:33'),
(41, 75, 1, 0, '', '2020-03-19 13:06:50'),
(42, 76, 1, 0, '', '2020-03-19 13:24:04'),
(43, 76, 1, 0, '', '2020-03-19 13:24:17'),
(44, 77, 1, 0, '', '2020-03-19 13:25:02'),
(45, 78, 1, 0, '', '2020-03-19 14:20:14');

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_option`
--

DROP TABLE IF EXISTS `oc_order_option`;
CREATE TABLE IF NOT EXISTS `oc_order_option` (
  `order_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_option_value_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(32) NOT NULL,
  PRIMARY KEY (`order_option_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_order_option`
--

INSERT INTO `oc_order_option` (`order_option_id`, `order_id`, `order_product_id`, `product_option_id`, `product_option_value_id`, `name`, `value`, `type`) VALUES
(2, 2, 3, 225, 0, 'Delivery Date', '2020-02-16', 'date'),
(4, 7, 20, 226, 16, 'Select', 'Blue', 'select'),
(5, 68, 148, 225, 0, 'Delivery Date', '2020-02-16', 'date'),
(6, 69, 150, 225, 0, 'Delivery Date', '2020-02-16', 'date'),
(7, 70, 152, 225, 0, 'Delivery Date', '2020-02-16', 'date'),
(8, 71, 154, 225, 0, 'Delivery Date', '2020-02-16', 'date'),
(9, 72, 156, 225, 0, 'Delivery Date', '2020-02-16', 'date'),
(10, 73, 158, 225, 0, 'Delivery Date', '2020-02-16', 'date'),
(11, 74, 160, 225, 0, 'Delivery Date', '2020-02-16', 'date'),
(12, 75, 162, 225, 0, 'Delivery Date', '2020-02-16', 'date'),
(13, 78, 166, 225, 0, 'Delivery Date', '2020-02-16', 'date');

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_product`
--

DROP TABLE IF EXISTS `oc_order_product`;
CREATE TABLE IF NOT EXISTS `oc_order_product` (
  `order_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `master_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `reward` int(8) NOT NULL,
  `return_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`order_product_id`),
  KEY `order_id` (`order_id`)
) ENGINE=MyISAM AUTO_INCREMENT=175 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_order_product`
--

INSERT INTO `oc_order_product` (`order_product_id`, `order_id`, `product_id`, `master_id`, `name`, `model`, `quantity`, `price`, `total`, `tax`, `reward`, `return_id`) VALUES
(1, 1, 29, 0, 'Palm Treo Pro', 'Product 2', 1, '279.9900', '279.9900', '0.0000', 0, NULL),
(3, 2, 47, 0, 'Moong Dal', '1 kg Pouch', 1, '120.0000', '120.0000', '0.0000', 0, NULL),
(4, 3, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, ''),
(5, 4, 29, 0, 'Paneer', 'Product 2', 10, '279.9900', '2799.9000', '0.0000', 0, NULL),
(6, 4, 31, 0, 'Kabuli Channa', '1 kg Pouch', 1, '80.0000', '80.0000', '0.0000', 0, '3'),
(9, 5, 44, 0, 'Daawat Basamati Rice', '5 Kg', 2, '1000.0000', '2000.0000', '0.0000', 1400, NULL),
(10, 6, 43, 0, 'Ketchup Tomato', '200 ML', 1, '500.0000', '500.0000', '0.0000', 600, '5'),
(11, 6, 46, 0, 'White Bread', 'White Bread', 2, '1000.0000', '2000.0000', '0.0000', 0, NULL),
(12, 6, 51, 0, 'Chhuara (Dry Dates)', '250g', 1, '230.0000', '230.0000', '0.0000', 0, NULL),
(13, 6, 52, 0, 'Kaju (Cashews)', '250g', 1, '0.0000', '0.0000', '0.0000', 0, NULL),
(21, 7, 41, 0, 'Brown Eggs', 'Pack of 6', 1, '336.5000', '336.5000', '0.0000', 0, NULL),
(20, 7, 30, 0, 'Aashirwad Salt', '1 kg Pouch', 1, '13.5000', '13.5000', '0.0000', 200, NULL),
(19, 8, 51, 0, 'Chhuara (Dry Dates)', '250g', 2, '230.0000', '460.0000', '0.0000', 0, NULL),
(18, 8, 31, 0, 'Kabuli Channa', '1 kg Pouch', 1, '80.0000', '80.0000', '0.0000', 0, NULL),
(22, 9, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(23, 9, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(24, 10, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(25, 10, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(26, 11, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(27, 11, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(28, 12, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(29, 12, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(30, 13, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(31, 13, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(32, 14, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(33, 15, 29, 0, 'Paneer', 'Product 2', 1, '279.9900', '279.9900', '0.0000', 0, NULL),
(34, 15, 31, 0, 'Kabuli Channa', '1 kg Pouch', 1, '80.0000', '80.0000', '0.0000', 0, NULL),
(35, 16, 29, 0, 'Paneer', 'Product 2', 1, '279.9900', '279.9900', '0.0000', 0, NULL),
(36, 16, 31, 0, 'Kabuli Channa', '1 kg Pouch', 1, '80.0000', '80.0000', '0.0000', 0, NULL),
(37, 17, 29, 0, 'Paneer', 'Product 2', 1, '279.9900', '279.9900', '0.0000', 0, NULL),
(38, 17, 31, 0, 'Kabuli Channa', '1 kg Pouch', 1, '80.0000', '80.0000', '0.0000', 0, NULL),
(39, 18, 29, 0, 'Paneer', 'Product 2', 1, '279.9900', '279.9900', '0.0000', 0, NULL),
(40, 18, 31, 0, 'Kabuli Channa', '1 kg Pouch', 1, '80.0000', '80.0000', '0.0000', 0, NULL),
(41, 19, 29, 0, 'Paneer', 'Product 2', 10, '279.9900', '2799.9000', '0.0000', 0, NULL),
(42, 19, 31, 0, 'Kabuli Channa', '1 kg Pouch', 2, '80.0000', '160.0000', '0.0000', 0, NULL),
(43, 19, 33, 0, 'Amul Ghee', 'Product 6', 1, '200.0000', '200.0000', '0.0000', 0, NULL),
(44, 19, 50, 0, 'Channa Dal, 1 kg Pouch', '1 kg Pouch', 1, '0.0000', '0.0000', '0.0000', 0, NULL),
(45, 20, 29, 0, 'Paneer', 'Product 2', 10, '279.9900', '2799.9000', '0.0000', 0, NULL),
(46, 20, 31, 0, 'Kabuli Channa', '1 kg Pouch', 2, '80.0000', '160.0000', '0.0000', 0, NULL),
(47, 20, 33, 0, 'Amul Ghee', 'Product 6', 1, '200.0000', '200.0000', '0.0000', 0, NULL),
(48, 20, 50, 0, 'Channa Dal, 1 kg Pouch', '1 kg Pouch', 1, '0.0000', '0.0000', '0.0000', 0, NULL),
(49, 21, 29, 0, 'Paneer', 'Product 2', 10, '279.9900', '2799.9000', '0.0000', 0, NULL),
(50, 21, 31, 0, 'Kabuli Channa', '1 kg Pouch', 2, '80.0000', '160.0000', '0.0000', 0, NULL),
(51, 21, 33, 0, 'Amul Ghee', 'Product 6', 1, '200.0000', '200.0000', '0.0000', 0, NULL),
(52, 21, 50, 0, 'Channa Dal, 1 kg Pouch', '1 kg Pouch', 1, '0.0000', '0.0000', '0.0000', 0, NULL),
(53, 22, 29, 0, 'Paneer', 'Product 2', 10, '279.9900', '2799.9000', '0.0000', 0, NULL),
(54, 22, 31, 0, 'Kabuli Channa', '1 kg Pouch', 2, '80.0000', '160.0000', '0.0000', 0, NULL),
(55, 22, 33, 0, 'Amul Ghee', 'Product 6', 1, '200.0000', '200.0000', '0.0000', 0, NULL),
(56, 22, 50, 0, 'Channa Dal, 1 kg Pouch', '1 kg Pouch', 1, '0.0000', '0.0000', '0.0000', 0, NULL),
(57, 23, 29, 0, 'Paneer', 'Product 2', 10, '279.9900', '2799.9000', '0.0000', 0, '6'),
(58, 23, 31, 0, 'Kabuli Channa', '1 kg Pouch', 2, '80.0000', '160.0000', '0.0000', 0, NULL),
(59, 23, 33, 0, 'Amul Ghee', 'Product 6', 1, '200.0000', '200.0000', '0.0000', 0, NULL),
(60, 23, 50, 0, 'Channa Dal, 1 kg Pouch', '1 kg Pouch', 1, '0.0000', '0.0000', '0.0000', 0, NULL),
(61, 24, 29, 0, 'Paneer', 'Product 2', 11, '279.9900', '3079.8900', '0.0000', 0, NULL),
(62, 24, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(63, 25, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(64, 25, 46, 0, 'White Bread', 'White Bread', 3, '35.0000', '105.0000', '0.0000', 0, NULL),
(65, 26, 60, 0, '(Sev) Apple', '500g', 1, '130.0000', '130.0000', '0.0000', 0, NULL),
(66, 26, 62, 0, 'Chini (Sugar)', '500g', 7, '30.0000', '210.0000', '0.0000', 0, NULL),
(67, 27, 60, 0, '(Sev) Apple', '500g', 1, '130.0000', '130.0000', '0.0000', 0, NULL),
(68, 27, 62, 0, 'Chini (Sugar)', '500g', 7, '30.0000', '210.0000', '0.0000', 0, NULL),
(69, 27, 64, 0, 'Birthday Cake', '500g', 1, '295.0000', '295.0000', '0.0000', 0, NULL),
(70, 28, 60, 0, '(Sev) Apple', '500g', 1, '130.0000', '130.0000', '0.0000', 0, NULL),
(71, 28, 62, 0, 'Chini (Sugar)', '500g', 7, '30.0000', '210.0000', '0.0000', 0, NULL),
(72, 28, 64, 0, 'Birthday Cake', '500g', 1, '295.0000', '295.0000', '0.0000', 0, NULL),
(73, 29, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(74, 30, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(75, 31, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(76, 32, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(77, 33, 29, 0, 'Paneer', 'Product 2', 1, '279.9900', '279.9900', '0.0000', 0, NULL),
(78, 33, 46, 0, 'White Bread', 'White Bread', 2, '35.0000', '70.0000', '0.0000', 0, NULL),
(79, 34, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(80, 34, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(81, 35, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(82, 35, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(83, 36, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(84, 36, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(85, 37, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(86, 37, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(87, 38, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(88, 38, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(89, 39, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(90, 39, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(91, 40, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(92, 40, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(93, 41, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(94, 41, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(95, 42, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(96, 42, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(97, 43, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(98, 43, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(99, 44, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(100, 44, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(101, 45, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(102, 45, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(103, 46, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(104, 46, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(105, 47, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(106, 47, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(107, 48, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(108, 48, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(109, 49, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(110, 49, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(111, 50, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(112, 50, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(113, 51, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(114, 51, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(115, 52, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(116, 52, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(117, 53, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(118, 53, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(119, 54, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(120, 54, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(121, 55, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(122, 55, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(123, 56, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(124, 56, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(125, 57, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(126, 57, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(127, 58, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(128, 58, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(129, 59, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(130, 59, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(131, 60, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(132, 60, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(133, 61, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(134, 61, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(135, 62, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(136, 62, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(137, 63, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(138, 63, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(139, 64, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(140, 64, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(141, 65, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(142, 65, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(143, 66, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(144, 66, 46, 0, 'White Bread', 'White Bread', 1, '35.0000', '35.0000', '0.0000', 0, NULL),
(147, 68, 29, 0, 'Paneer', 'Product 2', 1, '279.9900', '279.9900', '0.0000', 0, NULL),
(148, 68, 47, 0, 'Moong Dal', '1 kg Pouch', 1, '120.0000', '120.0000', '0.0000', 0, NULL),
(149, 69, 29, 0, 'Paneer', 'Product 2', 1, '279.9900', '279.9900', '0.0000', 0, NULL),
(150, 69, 47, 0, 'Moong Dal', '1 kg Pouch', 1, '120.0000', '120.0000', '0.0000', 0, NULL),
(151, 70, 29, 0, 'Paneer', 'Product 2', 1, '279.9900', '279.9900', '0.0000', 0, NULL),
(152, 70, 47, 0, 'Moong Dal', '1 kg Pouch', 1, '120.0000', '120.0000', '0.0000', 0, NULL),
(153, 71, 29, 0, 'Paneer', 'Product 2', 1, '279.9900', '279.9900', '0.0000', 0, NULL),
(154, 71, 47, 0, 'Moong Dal', '1 kg Pouch', 1, '120.0000', '120.0000', '0.0000', 0, NULL),
(155, 72, 29, 0, 'Paneer', 'Product 2', 1, '279.9900', '279.9900', '0.0000', 0, NULL),
(156, 72, 47, 0, 'Moong Dal', '1 kg Pouch', 1, '120.0000', '120.0000', '0.0000', 0, NULL),
(157, 73, 29, 0, 'Paneer', 'Product 2', 1, '279.9900', '279.9900', '0.0000', 0, NULL),
(158, 73, 47, 0, 'Moong Dal', '1 kg Pouch', 1, '120.0000', '120.0000', '0.0000', 0, NULL),
(159, 74, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(160, 74, 47, 0, 'Moong Dal', '1 kg Pouch', 1, '120.0000', '120.0000', '0.0000', 0, NULL),
(161, 75, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(162, 75, 47, 0, 'Moong Dal', '1 kg Pouch', 1, '120.0000', '120.0000', '0.0000', 0, NULL),
(163, 76, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(164, 77, 29, 0, 'Paneer', 'Product 2', 2, '279.9900', '559.9800', '0.0000', 0, NULL),
(165, 78, 29, 0, 'Paneer', 'Product 2', 1, '279.9900', '279.9900', '0.0000', 0, NULL),
(166, 78, 47, 0, 'Moong Dal', '1 kg Pouch', 1, '120.0000', '120.0000', '0.0000', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_recurring`
--

DROP TABLE IF EXISTS `oc_order_recurring`;
CREATE TABLE IF NOT EXISTS `oc_order_recurring` (
  `order_recurring_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `recurring_name` varchar(255) NOT NULL,
  `recurring_description` varchar(255) NOT NULL,
  `recurring_frequency` varchar(25) NOT NULL,
  `recurring_cycle` smallint(6) NOT NULL,
  `recurring_duration` smallint(6) NOT NULL,
  `recurring_price` decimal(10,4) NOT NULL,
  `trial` tinyint(1) NOT NULL,
  `trial_frequency` varchar(25) NOT NULL,
  `trial_cycle` smallint(6) NOT NULL,
  `trial_duration` smallint(6) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`order_recurring_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_recurring_transaction`
--

DROP TABLE IF EXISTS `oc_order_recurring_transaction`;
CREATE TABLE IF NOT EXISTS `oc_order_recurring_transaction` (
  `order_recurring_transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_recurring_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `amount` decimal(10,4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`order_recurring_transaction_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_shipment`
--

DROP TABLE IF EXISTS `oc_order_shipment`;
CREATE TABLE IF NOT EXISTS `oc_order_shipment` (
  `order_shipment_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `shipping_courier_id` varchar(255) NOT NULL,
  `tracking_number` varchar(255) NOT NULL,
  PRIMARY KEY (`order_shipment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_status`
--

DROP TABLE IF EXISTS `oc_order_status`;
CREATE TABLE IF NOT EXISTS `oc_order_status` (
  `order_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`order_status_id`,`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_order_status`
--

INSERT INTO `oc_order_status` (`order_status_id`, `language_id`, `name`) VALUES
(2, 1, 'Processing'),
(3, 1, 'Shipped'),
(7, 1, 'Canceled'),
(5, 1, 'Complete'),
(8, 1, 'Denied'),
(9, 1, 'Canceled Reversal'),
(10, 1, 'Failed'),
(11, 1, 'Refunded'),
(12, 1, 'Reversed'),
(13, 1, 'Chargeback'),
(1, 1, 'Pending'),
(16, 1, 'Voided'),
(15, 1, 'Processed'),
(14, 1, 'Expired'),
(17, 1, 'Canceled (by Admin)');

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_total`
--

DROP TABLE IF EXISTS `oc_order_total`;
CREATE TABLE IF NOT EXISTS `oc_order_total` (
  `order_total_id` int(10) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`order_total_id`),
  KEY `order_id` (`order_id`)
) ENGINE=MyISAM AUTO_INCREMENT=247 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_order_total`
--

INSERT INTO `oc_order_total` (`order_total_id`, `order_id`, `code`, `title`, `value`, `sort_order`) VALUES
(1, 1, 'sub_total', 'Sub-Total', '279.9900', 1),
(2, 1, 'shipping', 'Flat Shipping Rate', '5.0000', 3),
(3, 1, 'total', 'Total', '284.9900', 9),
(8, 2, 'shipping', 'Flat Shipping Rate', '5.0000', 3),
(7, 2, 'sub_total', 'Sub-Total', '120.0000', 1),
(9, 2, 'total', 'Total', '125.0000', 9),
(10, 3, 'sub_total', 'Sub-Total', '559.9800', 1),
(11, 3, 'total', 'Total', '559.9800', 9),
(12, 4, 'sub_total', 'Sub-Total', '2879.9000', 1),
(13, 4, 'total', 'Total', '2879.9000', 9),
(19, 5, 'total', 'Total', '2000.0000', 9),
(18, 5, 'sub_total', 'Sub-Total', '2000.0000', 1),
(20, 6, 'sub_total', 'Sub-Total', '2730.0000', 1),
(21, 6, 'total', 'Total', '2730.0000', 9),
(28, 7, 'sub_total', 'Sub-Total', '350.0000', 1),
(26, 8, 'sub_total', 'Sub-Total', '540.0000', 1),
(27, 8, 'total', 'Total', '540.0000', 9),
(29, 7, 'total', 'Total', '350.0000', 9),
(30, 9, 'sub_total', 'Sub-Total', '594.9800', 1),
(31, 9, 'total', 'Total', '594.9800', 9),
(32, 10, 'sub_total', 'Sub-Total', '594.9800', 1),
(33, 10, 'total', 'Total', '594.9800', 9),
(34, 11, 'sub_total', 'Sub-Total', '594.9800', 1),
(35, 11, 'total', 'Total', '594.9800', 9),
(36, 12, 'sub_total', 'Sub-Total', '594.9800', 1),
(37, 12, 'total', 'Total', '594.9800', 9),
(38, 13, 'sub_total', 'Sub-Total', '594.9800', 1),
(39, 13, 'total', 'Total', '594.9800', 9),
(40, 14, 'sub_total', 'Sub-Total', '559.9800', 1),
(41, 14, 'total', 'Total', '559.9800', 9),
(42, 15, 'sub_total', 'Sub-Total', '359.9900', 1),
(43, 15, 'total', 'Total', '359.9900', 9),
(44, 16, 'sub_total', 'Sub-Total', '359.9900', 1),
(45, 16, 'total', 'Total', '359.9900', 9),
(46, 17, 'sub_total', 'Sub-Total', '359.9900', 1),
(47, 17, 'total', 'Total', '359.9900', 9),
(48, 18, 'sub_total', 'Sub-Total', '359.9900', 1),
(49, 18, 'shipping', 'Flat Shipping Rate', '5.0000', 3),
(50, 18, 'total', 'Total', '364.9900', 9),
(51, 19, 'sub_total', 'Sub-Total', '3159.9000', 1),
(52, 19, 'shipping', 'Flat Shipping Rate', '5.0000', 3),
(53, 19, 'total', 'Total', '3164.9000', 9),
(54, 20, 'sub_total', 'Sub-Total', '3159.9000', 1),
(55, 20, 'shipping', 'Flat Shipping Rate', '5.0000', 3),
(56, 20, 'total', 'Total', '3164.9000', 9),
(57, 21, 'sub_total', 'Sub-Total', '3159.9000', 1),
(58, 21, 'shipping', 'Flat Shipping Rate', '5.0000', 3),
(59, 21, 'total', 'Total', '3164.9000', 9),
(60, 22, 'sub_total', 'Sub-Total', '3159.9000', 1),
(61, 22, 'shipping', 'Flat Shipping Rate', '5.0000', 3),
(62, 22, 'total', 'Total', '3164.9000', 9),
(63, 23, 'sub_total', 'Sub-Total', '3159.9000', 1),
(64, 23, 'shipping', 'Flat Shipping Rate', '5.0000', 3),
(65, 23, 'total', 'Total', '3164.9000', 9),
(66, 24, 'sub_total', 'Sub-Total', '3114.8900', 1),
(67, 24, 'shipping', 'Flat Shipping Rate', '5.0000', 3),
(68, 24, 'total', 'Total', '3119.8900', 9),
(69, 25, 'sub_total', 'Sub-Total', '664.9800', 1),
(70, 25, 'shipping', 'Pickup From Store', '0.0000', 3),
(71, 25, 'total', 'Total', '664.9800', 9),
(72, 26, 'sub_total', 'Sub-Total', '340.0000', 1),
(73, 26, 'shipping', 'Pickup From Store', '0.0000', 3),
(74, 26, 'total', 'Total', '340.0000', 9),
(75, 27, 'sub_total', 'Sub-Total', '635.0000', 1),
(76, 27, 'shipping', 'Pickup From Store', '0.0000', 3),
(77, 27, 'total', 'Total', '635.0000', 9),
(78, 28, 'sub_total', 'Sub-Total', '635.0000', 1),
(79, 28, 'shipping', 'Pickup From Store', '0.0000', 3),
(80, 28, 'total', 'Total', '635.0000', 9),
(81, 29, 'sub_total', 'Sub-Total', '559.9800', 1),
(82, 29, 'reward', 'Reward Points (2)', '-2.0000', 2),
(83, 29, 'shipping', 'Flat Shipping Rate', '5.0000', 3),
(84, 29, 'total', 'Total', '562.9800', 9),
(85, 30, 'sub_total', 'Sub-Total', '559.9800', 1),
(86, 30, 'reward', 'Reward Points (3)', '-3.0000', 2),
(87, 30, 'shipping', 'Flat Shipping Rate', '5.0000', 3),
(88, 30, 'total', 'Total', '561.9800', 9),
(89, 31, 'sub_total', 'Sub-Total', '559.9800', 1),
(90, 31, 'reward', 'Reward Points (3)', '-3.0000', 2),
(91, 31, 'shipping', 'Pickup From Store', '0.0000', 3),
(92, 31, 'total', 'Total', '556.9800', 9),
(93, 32, 'sub_total', 'Sub-Total', '559.9800', 1),
(94, 32, 'reward', 'Reward Points (3)', '-3.0000', 2),
(95, 32, 'shipping', 'Pickup From Store', '0.0000', 3),
(96, 32, 'total', 'Total', '556.9800', 9),
(97, 33, 'sub_total', 'Sub-Total', '349.9900', 1),
(98, 33, 'reward', 'Reward Points (4)', '-4.0000', 2),
(99, 33, 'shipping', 'Flat Shipping Rate', '5.0000', 3),
(100, 33, 'total', 'Total', '350.9900', 9),
(101, 34, 'sub_total', 'Sub-Total', '594.9800', 1),
(102, 34, 'shipping', 'Pickup From Store', '0.0000', 3),
(103, 34, 'total', 'Total', '594.9800', 9),
(104, 35, 'sub_total', 'Sub-Total', '594.9800', 1),
(105, 35, 'shipping', 'Pickup From Store', '0.0000', 3),
(106, 35, 'total', 'Total', '594.9800', 9),
(107, 36, 'sub_total', 'Sub-Total', '594.9800', 1),
(108, 36, 'shipping', 'Pickup From Store', '0.0000', 3),
(109, 36, 'total', 'Total', '594.9800', 9),
(110, 37, 'sub_total', 'Sub-Total', '594.9800', 1),
(111, 37, 'shipping', 'Pickup From Store', '0.0000', 3),
(112, 37, 'total', 'Total', '594.9800', 9),
(113, 38, 'sub_total', 'Sub-Total', '594.9800', 1),
(114, 38, 'shipping', 'Pickup From Store', '0.0000', 3),
(115, 38, 'total', 'Total', '594.9800', 9),
(116, 39, 'sub_total', 'Sub-Total', '594.9800', 1),
(117, 39, 'shipping', 'Pickup From Store', '0.0000', 3),
(118, 39, 'total', 'Total', '594.9800', 9),
(119, 40, 'sub_total', 'Sub-Total', '594.9800', 1),
(120, 40, 'shipping', 'Pickup From Store', '0.0000', 3),
(121, 40, 'total', 'Total', '594.9800', 9),
(122, 41, 'sub_total', 'Sub-Total', '594.9800', 1),
(123, 41, 'shipping', 'Pickup From Store', '0.0000', 3),
(124, 41, 'total', 'Total', '594.9800', 9),
(125, 42, 'sub_total', 'Sub-Total', '594.9800', 1),
(126, 42, 'shipping', 'Pickup From Store', '0.0000', 3),
(127, 42, 'total', 'Total', '594.9800', 9),
(128, 43, 'sub_total', 'Sub-Total', '594.9800', 1),
(129, 43, 'shipping', 'Pickup From Store', '0.0000', 3),
(130, 43, 'total', 'Total', '594.9800', 9),
(131, 44, 'sub_total', 'Sub-Total', '594.9800', 1),
(132, 44, 'shipping', 'Pickup From Store', '0.0000', 3),
(133, 44, 'total', 'Total', '594.9800', 9),
(134, 45, 'sub_total', 'Sub-Total', '594.9800', 1),
(135, 45, 'shipping', 'Pickup From Store', '0.0000', 3),
(136, 45, 'total', 'Total', '594.9800', 9),
(137, 46, 'sub_total', 'Sub-Total', '594.9800', 1),
(138, 46, 'shipping', 'Pickup From Store', '0.0000', 3),
(139, 46, 'total', 'Total', '594.9800', 9),
(140, 47, 'sub_total', 'Sub-Total', '594.9800', 1),
(141, 47, 'shipping', 'Pickup From Store', '0.0000', 3),
(142, 47, 'total', 'Total', '594.9800', 9),
(143, 48, 'sub_total', 'Sub-Total', '594.9800', 1),
(144, 48, 'shipping', 'Pickup From Store', '0.0000', 3),
(145, 48, 'total', 'Total', '594.9800', 9),
(146, 49, 'sub_total', 'Sub-Total', '594.9800', 1),
(147, 49, 'shipping', 'Pickup From Store', '0.0000', 3),
(148, 49, 'total', 'Total', '594.9800', 9),
(149, 50, 'sub_total', 'Sub-Total', '594.9800', 1),
(150, 50, 'shipping', 'Pickup From Store', '0.0000', 3),
(151, 50, 'total', 'Total', '594.9800', 9),
(152, 51, 'sub_total', 'Sub-Total', '594.9800', 1),
(153, 51, 'shipping', 'Pickup From Store', '0.0000', 3),
(154, 51, 'total', 'Total', '594.9800', 9),
(155, 52, 'sub_total', 'Sub-Total', '594.9800', 1),
(156, 52, 'shipping', 'Pickup From Store', '0.0000', 3),
(157, 52, 'total', 'Total', '594.9800', 9),
(158, 53, 'sub_total', 'Sub-Total', '594.9800', 1),
(159, 53, 'shipping', 'Pickup From Store', '0.0000', 3),
(160, 53, 'total', 'Total', '594.9800', 9),
(161, 54, 'sub_total', 'Sub-Total', '594.9800', 1),
(162, 54, 'shipping', 'Pickup From Store', '0.0000', 3),
(163, 54, 'total', 'Total', '594.9800', 9),
(164, 55, 'sub_total', 'Sub-Total', '594.9800', 1),
(165, 55, 'shipping', 'Pickup From Store', '0.0000', 3),
(166, 55, 'total', 'Total', '594.9800', 9),
(167, 56, 'sub_total', 'Sub-Total', '594.9800', 1),
(168, 56, 'shipping', 'Pickup From Store', '0.0000', 3),
(169, 56, 'total', 'Total', '594.9800', 9),
(170, 57, 'sub_total', 'Sub-Total', '594.9800', 1),
(171, 57, 'shipping', 'Pickup From Store', '0.0000', 3),
(172, 57, 'total', 'Total', '594.9800', 9),
(173, 58, 'sub_total', 'Sub-Total', '594.9800', 1),
(174, 58, 'shipping', 'Pickup From Store', '0.0000', 3),
(175, 58, 'total', 'Total', '594.9800', 9),
(176, 59, 'sub_total', 'Sub-Total', '594.9800', 1),
(177, 59, 'shipping', 'Pickup From Store', '0.0000', 3),
(178, 59, 'total', 'Total', '594.9800', 9),
(179, 60, 'sub_total', 'Sub-Total', '594.9800', 1),
(180, 60, 'shipping', 'Pickup From Store', '0.0000', 3),
(181, 60, 'total', 'Total', '594.9800', 9),
(182, 61, 'sub_total', 'Sub-Total', '594.9800', 1),
(183, 61, 'shipping', 'Pickup From Store', '0.0000', 3),
(184, 61, 'total', 'Total', '594.9800', 9),
(185, 62, 'sub_total', 'Sub-Total', '594.9800', 1),
(186, 62, 'shipping', 'Pickup From Store', '0.0000', 3),
(187, 62, 'total', 'Total', '594.9800', 9),
(188, 63, 'sub_total', 'Sub-Total', '594.9800', 1),
(189, 63, 'shipping', 'Pickup From Store', '0.0000', 3),
(190, 63, 'total', 'Total', '594.9800', 9),
(191, 64, 'sub_total', 'Sub-Total', '594.9800', 1),
(192, 64, 'shipping', 'Pickup From Store', '0.0000', 3),
(193, 64, 'total', 'Total', '594.9800', 9),
(194, 65, 'sub_total', 'Sub-Total', '594.9800', 1),
(195, 65, 'shipping', 'Pickup From Store', '0.0000', 3),
(196, 65, 'total', 'Total', '594.9800', 9),
(197, 66, 'sub_total', 'Sub-Total', '594.9800', 1),
(198, 66, 'shipping', 'Pickup From Store', '0.0000', 3),
(199, 66, 'total', 'Total', '594.9800', 9),
(203, 68, 'sub_total', 'Sub-Total', '399.9900', 1),
(204, 68, 'shipping', 'Flat Shipping Rate', '5.0000', 3),
(205, 68, 'total', 'Total', '404.9900', 9),
(206, 69, 'sub_total', 'Sub-Total', '399.9900', 1),
(207, 69, 'shipping', 'Flat Shipping Rate', '5.0000', 3),
(208, 69, 'total', 'Total', '404.9900', 9),
(209, 70, 'sub_total', 'Sub-Total', '399.9900', 1),
(210, 70, 'shipping', 'Flat Shipping Rate', '5.0000', 3),
(211, 70, 'total', 'Total', '404.9900', 9),
(212, 71, 'sub_total', 'Sub-Total', '399.9900', 1),
(213, 71, 'shipping', 'Flat Shipping Rate', '5.0000', 3),
(214, 71, 'total', 'Total', '404.9900', 9),
(215, 72, 'sub_total', 'Sub-Total', '399.9900', 1),
(216, 72, 'shipping', 'Flat Shipping Rate', '5.0000', 3),
(217, 72, 'total', 'Total', '404.9900', 9),
(218, 73, 'sub_total', 'Sub-Total', '399.9900', 1),
(219, 73, 'shipping', 'Flat Shipping Rate', '5.0000', 3),
(220, 73, 'total', 'Total', '404.9900', 9),
(221, 74, 'sub_total', 'Sub-Total', '679.9800', 1),
(222, 74, 'shipping', 'Flat Shipping Rate', '5.0000', 3),
(223, 74, 'total', 'Total', '684.9800', 9),
(224, 75, 'sub_total', 'Sub-Total', '679.9800', 1),
(225, 75, 'shipping', 'Flat Shipping Rate', '5.0000', 3),
(226, 75, 'total', 'Total', '684.9800', 9),
(227, 76, 'sub_total', 'Sub-Total', '559.9800', 1),
(228, 76, 'shipping', 'Flat Shipping Rate', '5.0000', 3),
(229, 76, 'total', 'Total', '564.9800', 9),
(230, 77, 'sub_total', 'Sub-Total', '559.9800', 1),
(231, 77, 'shipping', 'Flat Shipping Rate', '5.0000', 3),
(232, 77, 'total', 'Total', '564.9800', 9),
(233, 78, 'sub_total', 'Sub-Total', '399.9900', 1),
(234, 78, 'shipping', 'Flat Shipping Rate', '5.0000', 3),
(235, 78, 'total', 'Total', '404.9900', 9);

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_voucher`
--

DROP TABLE IF EXISTS `oc_order_voucher`;
CREATE TABLE IF NOT EXISTS `oc_order_voucher` (
  `order_voucher_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  PRIMARY KEY (`order_voucher_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product`
--

DROP TABLE IF EXISTS `oc_product`;
CREATE TABLE IF NOT EXISTS `oc_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `master_id` int(11) NOT NULL,
  `model` varchar(64) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `upc` varchar(12) NOT NULL,
  `ean` varchar(14) NOT NULL,
  `jan` varchar(13) NOT NULL,
  `isbn` varchar(17) NOT NULL,
  `mpn` varchar(64) NOT NULL,
  `location` varchar(128) NOT NULL,
  `variant` text NOT NULL,
  `override` text NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `stock_status_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `shipping` tinyint(1) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `points` int(8) NOT NULL DEFAULT '0',
  `tax_class_id` int(11) NOT NULL,
  `date_available` date NOT NULL DEFAULT '0000-00-00',
  `weight` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `weight_class_id` int(11) NOT NULL DEFAULT '0',
  `length` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `width` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `height` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `length_class_id` int(11) NOT NULL DEFAULT '0',
  `subtract` tinyint(1) NOT NULL DEFAULT '1',
  `minimum` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `viewed` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product`
--

INSERT INTO `oc_product` (`product_id`, `master_id`, `model`, `sku`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `variant`, `override`, `quantity`, `stock_status_id`, `image`, `manufacturer_id`, `shipping`, `price`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `viewed`, `date_added`, `date_modified`) VALUES
(28, 0, '5 Kg', '', '', '', '', '', '', '', '', '', 939, 7, 'catalog/grocery/oil-and-ghee/225x265_Products_0000_Kachi-ghani-2-Ltr-Pet-new--1.png', 5, 1, '1100.0000', 200, 0, '2020-03-28', '5.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 10, 1, 1, '2009-02-03 16:06:50', '2020-03-28 19:10:13'),
(29, 0, '250g', '', '', '', '', '', '', '', '', '', 967, 6, 'catalog/grocery/paneer.jpg', 6, 1, '279.9900', 0, 9, '2009-02-03', '250.00000000', 2, '0.00000000', '0.00000000', '0.00000000', 3, 1, 100, 7, 1, 2, '2009-02-03 16:42:17', '2020-03-28 19:06:04'),
(30, 0, '1 kg Pouch', '', '', '', '', '', '', '', '', '', 7, 6, 'catalog/grocery/sugar-salt-jaggery/AASHIRVAD-SALT-1-KG.jpg', 8, 1, '15.0000', 0, 9, '2020-02-29', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 0, 1, 3, '2009-02-03 16:59:00', '2020-03-28 15:34:54'),
(31, 0, '1 kg Pouch', '', '', '', '', '', '', '', '', '', 999, 5, 'catalog/grocery/kabuli-channa-1528523795-3960607.jpeg', 0, 1, '80.0000', 0, 9, '2009-02-03', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 3, 1, 1, 0, 1, 0, '2009-02-03 17:00:10', '2020-02-15 17:25:39'),
(33, 0, 'Product 6', '', '', '', '', '', '', '', '', '', 1000, 6, 'catalog/grocery/oil-and-ghee/amul-ghee-1L.jpg', 0, 1, '200.0000', 0, 9, '2009-02-03', '5.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, 3, '2009-02-03 17:08:31', '2020-03-21 22:39:38'),
(35, 0, '1 kg Pouch', '', '', '', '', '', '', '', '', '', 1000, 5, 'catalog/grocery/milk-breads/milk_PNG12734.png', 0, 0, '100.0000', 0, 9, '2009-02-03', '5.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 0, 1, 0, '2009-02-03 18:08:31', '2020-02-15 17:18:28'),
(40, 0, '200g', '', '', '', '', '', '', '', '', '', 970, 5, 'catalog/grocery/500-hot-sweet-tomato-chilli-glass-bottle-sauce-maggi.jpeg', 10, 1, '101.0000', 0, 9, '2009-02-03', '10.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 0, 1, 0, '2009-02-03 21:07:12', '2020-03-28 11:47:24'),
(41, 0, 'Pack of 6', '', '', '', '', '', '', '', '', '', 977, 5, 'catalog/grocery/eggs-meat-fish/egg-tray-500x500.jpg', 0, 1, '336.5000', 0, 9, '2009-02-03', '5.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 0, 1, 0, '2009-02-03 21:07:26', '2020-02-29 22:47:03'),
(42, 0, '1 kg Packet', '', '', '', '', '', '', '', '', '', 990, 5, 'catalog/grocery/oil-and-ghee/pro_amul_yellow-cow-ghee.jpg', 8, 1, '550.0000', 400, 9, '2020-03-01', '12.50000000', 1, '1.00000000', '2.00000000', '3.00000000', 1, 1, 1, 0, 1, 0, '2009-02-03 21:07:37', '2020-03-23 22:35:22'),
(43, 0, '950 g', '', '', '', '', '', '', '', '', '', 929, 5, 'catalog/grocery/950-fresh-tomato-ketchup-pouch-ketchup-kissan.jpeg', 8, 0, '195.0000', 0, 9, '2020-03-28', '950.00000000', 2, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 2, 1, 0, '2009-02-03 21:07:49', '2020-03-28 11:50:21'),
(44, 0, '5 Kg', '', '', '', '', '', '', '', '', '', 1000, 5, 'catalog/grocery/daawat_pulav_basmati_rice_5kg.jpg', 8, 1, '520.0000', 0, 9, '2009-02-03', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, 0, '2009-02-03 21:08:00', '2020-03-01 12:57:37'),
(45, 0, '200g', '', '', '', '', '', '', '', '', '', 998, 5, 'catalog/grocery/vegetagles/fresh-button-mushroom-spawn-500x500.jpg', 0, 1, '60.0000', 0, 0, '2020-03-28', '200.00000000', 2, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 4, 1, 3, '2009-02-03 21:08:17', '2020-03-28 19:02:58'),
(46, 0, 'White Bread', '', '', '', '', '', '', '', '', '', 994, 5, 'catalog/grocery/milk-breads/4881035-bread-01.jpg', 0, 1, '35.0000', 0, 0, '2020-03-04', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, 2, '2009-02-03 21:08:29', '2020-02-29 22:54:34'),
(47, 0, '1 kg Pouch', '', '', '', '', '', '', '', '', '', 997, 5, 'catalog/grocery/moong-dal.jpg', 0, 1, '120.0000', 0, 9, '2020-02-15', '1.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 0, 1, 5, '2009-02-03 21:08:40', '2020-03-01 16:44:06'),
(49, 0, '250g', '', '', '', '', '', '', '', '', '', 0, 8, 'catalog/grocery/vegetagles/garlic-lahsun.jpg', 0, 1, '199.9900', 0, 9, '2011-04-25', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 4, '2011-04-26 08:57:34', '2020-03-01 12:28:34'),
(50, 0, '1 kg Pouch', '', '', '', '', '', '', '', '', '', 108, 5, 'catalog/grocery/channa-dal-1-kg.jpg', 0, 1, '0.0000', 0, 0, '2020-02-15', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-02-15 15:20:52', '2020-03-01 16:43:40'),
(51, 0, '250g', '', '', '', '', '', '', '', '', '', 4, 6, 'catalog/grocery/chhuara-dry-dates.jpg', 0, 1, '230.0000', 0, 0, '2020-02-22', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-02-22 22:41:38', '2020-03-23 22:35:42'),
(52, 0, '250g', '', '', '', '', '', '', '', '', '', 100, 6, 'catalog/grocery/dry-fruits/cashew-kaaju.jpg', 0, 1, '140.0000', 0, 0, '2020-02-22', '250.00000000', 2, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-02-22 22:43:09', '2020-03-28 19:08:08'),
(53, 0, '500g', '', '', '', '', '', '', '', '', '', 10, 6, 'catalog/grocery/rajama.jpg', 0, 1, '85.0000', 0, 0, '2020-02-22', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 4, '2020-02-22 22:48:18', '2020-03-01 13:13:47'),
(54, 0, '100g', '', '', '', '', '', '', '', '', '', 50, 6, 'catalog/grocery/vegetagles/dhaniya-768x511.jpg', 0, 1, '15.0000', 0, 0, '2020-03-01', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-03-01 12:19:46', '2020-03-01 12:19:46'),
(55, 0, '200g', '', '', '', '', '', '', '', '', '', 45, 6, 'catalog/grocery/vegetagles/green-chilli-mirch.jpg', 0, 1, '35.0000', 0, 0, '2020-03-01', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-03-01 12:22:32', '2020-03-01 12:22:32'),
(56, 0, '200g', '', '', '', '', '', '', '', '', '', 1000, 6, 'catalog/grocery/masala-spices/gol-marich-masala.jpeg', 0, 1, '120.0000', 0, 0, '2020-03-01', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-03-01 12:28:00', '2020-03-01 12:28:00'),
(57, 0, '250g', '', '', '', '', '', '', '', '', '', 20, 6, 'catalog/grocery/dabur-honey-250-gm.jpg', 0, 1, '250.0000', 0, 0, '2020-03-01', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-03-01 12:54:47', '2020-03-01 12:54:47'),
(58, 0, '250g', '', '', '', '', '', '', '', '', '', 100, 6, 'catalog/grocery/fruits/grapes.jpg', 0, 1, '30.0000', 0, 0, '2020-03-01', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 1, '2020-03-01 13:04:45', '2020-03-21 22:40:07'),
(59, 0, 'Pack of 6', '', '', '', '', '', '', '', '', '', 1, 6, 'catalog/grocery/fruits/ioana-cristiana-0WW38q7lGZA-unsplash.jpg', 0, 1, '25.0000', 0, 0, '2020-03-01', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-03-01 13:05:56', '2020-03-28 19:11:27'),
(60, 0, '500g', '', '', '', '', '', '', '', '', '', 1, 6, 'catalog/grocery/fruits/apples.jpg', 0, 1, '130.0000', 0, 0, '2020-03-01', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 3, '2020-03-01 13:08:16', '2020-03-07 13:02:05'),
(61, 0, '250g', '', '', '', '', '', '', '', '', '', 10, 6, 'catalog/grocery/sattu.jpg', 0, 1, '40.0000', 0, 0, '2020-03-01', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-03-01 13:11:22', '2020-03-28 11:57:01'),
(62, 0, '500g', '', '', '', '', '', '', '', '', '', 30, 6, 'catalog/grocery/sugar-salt-jaggery/sugar-chini-500g.jpg', 0, 1, '30.0000', 0, 0, '2020-03-01', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-03-01 13:16:02', '2020-03-21 22:39:15'),
(63, 0, '2 Kg', '', '', '', '', '', '', '', '', '', 5, 6, 'catalog/grocery/other-items/MuscleBlaze-MB-100-Whey-Protein-Supplement-Powder.jpg', 0, 1, '5000.0000', 0, 0, '2020-03-01', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-03-01 13:27:13', '2020-03-01 13:32:14'),
(64, 0, '500g', '', '', '', '', '', '', '', '', '', 10, 6, 'catalog/grocery/birthday-cakes/birthday-cake.jpg', 0, 1, '295.0000', 0, 0, '2020-03-01', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-03-01 16:51:03', '2020-03-01 16:51:03'),
(65, 0, '1 kg Pouch', '', '', '', '', '', '', '', '', '', 100, 6, 'catalog/grocery/bhuna-chana.jpg', 0, 1, '150.0000', 0, 0, '2020-03-28', '1.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-03-28 11:56:02', '2020-03-28 11:56:02'),
(66, 45, '400g', '', '', '', '', '', '', '', '', '{\"model\":\"1\",\"price\":\"1\"}', 998, 5, 'catalog/grocery/vegetagles/fresh-button-mushroom-spawn-500x500.jpg', 0, 1, '120.0000', 0, 0, '2020-03-28', '200.00000000', 2, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 4, 1, 0, '2020-03-28 19:04:43', '2020-03-28 19:04:43');

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_attribute`
--

DROP TABLE IF EXISTS `oc_product_attribute`;
CREATE TABLE IF NOT EXISTS `oc_product_attribute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`product_id`,`attribute_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_attribute`
--

INSERT INTO `oc_product_attribute` (`product_id`, `attribute_id`, `language_id`, `text`) VALUES
(43, 2, 1, '1'),
(42, 3, 1, '100mhz'),
(43, 4, 1, '8gb');

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_description`
--

DROP TABLE IF EXISTS `oc_product_description`;
CREATE TABLE IF NOT EXISTS `oc_product_description` (
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `tag` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`product_id`,`language_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_description`
--

INSERT INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(35, 1, 'Milk', '&lt;p&gt;Milk&lt;/p&gt;\r\n', 'Milk', 'Milk', 'Milk', 'Milk'),
(40, 1, 'Tomato Chilli Sauce - Hot &amp; Sweet', '&lt;p&gt;Tomato Chilli Sauce - Hot &amp;amp; Sweet&lt;/p&gt;\r\n', 'tomato-chilli-sauce', 'Tomato Chilli Sauce - Hot &amp; Sweet', 'Tomato Chilli Sauce - Hot &amp; Sweet', 'Tomato Chilli Sauce - Hot &amp; Sweet'),
(28, 1, 'Fortune Oil', '&lt;p&gt;Fortune Oil&lt;/p&gt;\r\n', 'fortune, oil', 'Fortune Oil', 'Fortune Oil', 'Fortune Oil'),
(44, 1, 'Daawat Basamati Rice', '&lt;p&gt;Daawat Basamati Rice&lt;/p&gt;\r\n', 'daawat-basmati-rice', 'Daawat Basamati Rice', 'Daawat Basamati Rice', 'Daawat Basamati Rice'),
(45, 1, 'Mushrooms', '&lt;p&gt;&lt;strong&gt;Mushrooms&lt;/strong&gt;&lt;/p&gt;\r\n&lt;!-- cpt_container_end --&gt;', 'mushroom', 'Mushrooms', 'Mushrooms', 'Mushrooms'),
(29, 1, 'Paneer', '&lt;p&gt;Paneer&lt;/p&gt;\r\n', 'paneer', 'Paneer', 'Paneer', 'Paneer'),
(46, 1, 'White Bread', '&lt;p&gt;White Bread&lt;/p&gt;\r\n', 'White Bread', 'White Bread', 'White Bread', 'White Bread'),
(47, 1, 'Moong Dal', '&lt;p&gt;Moong Dal&lt;/p&gt;\r\n', 'moong-dal', 'Moong Dal', 'Moong Dal', 'Moong Dal'),
(41, 1, 'Brown Eggs', '&lt;p&gt;Brown Eggs&lt;/p&gt;\r\n', 'brown-eggs', 'Brown Eggs', 'Brown Eggs', 'Brown Eggs'),
(33, 1, 'Amul Ghee', '&lt;p&gt;Amul Ghee&lt;/p&gt;\r\n', 'amul-ghee', 'Amul Ghee', 'Amul Ghee', 'Amul Ghee'),
(43, 1, 'Ketchup Tomato', '&lt;p&gt;Ketchup Tomato&lt;/p&gt;\r\n', 'tomato-ketchup', 'Ketchup Tomato', 'Ketchup Tomato', 'Ketchup Tomato'),
(51, 1, 'Chhuara (Dry Dates)', '&lt;p&gt;Chhuara (Dry Dates)&lt;/p&gt;\r\n', 'drydates-chhuara', 'Chhuara (Dry Dates)', 'Chhuara (Dry Dates)', 'Chhuara (Dry Dates)'),
(52, 1, 'Kaju (Cashews)', '&lt;p&gt;Kaju (Cashews)&lt;/p&gt;\r\n', 'kaju, cashew', 'Kaju (Cashews)', 'Kaju (Cashews)', 'Kaju (Cashews)'),
(53, 1, 'Rajama', '&lt;p&gt;Rajama&lt;/p&gt;\r\n', 'rajama', 'Rajama', 'Rajama', 'Rajama'),
(31, 1, 'Kabuli Channa', '&lt;p&gt;Kabuli Channa&lt;/p&gt;\r\n&lt;!-- cpt_container_end --&gt;', 'Kabuli Channa', 'Kabuli Channa', 'Kabuli Channa', 'Kabuli Channa'),
(49, 1, 'Lehsun (Garlic)', '&lt;p&gt;Lehsun (Garlic)&lt;/p&gt;\r\n', 'lehsun, garlic', 'Lehsun (Garlic)', 'Lehsun (Garlic)', 'Lehsun (Garlic)'),
(42, 1, 'Amul Cow Ghee', '&lt;p&gt;&lt;span style=&quot;font-family:helvetica,geneva,arial; font-size:small&quot;&gt;&lt;span style=&quot;font-family:Helvetica; font-size:small&quot;&gt;Amul Cow Ghee&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'cow-ghee', 'Amul Cow Ghee', 'Amul Cow Ghee', 'Amul Cow Ghee'),
(30, 1, 'Aashirwad Salt', '&lt;p&gt;Aashirwad Salt&lt;/p&gt;\r\n', 'aashirwad-salt', 'Aashirwad Salt', 'Aashirwad Salt', 'Aashirwad Salt'),
(50, 1, 'Channa Dal, 1 kg Pouch', '&lt;p&gt;Channa Dal, 1 kg Pouch&lt;/p&gt;\r\n', 'channa-dal', 'Channa Dal, 1 kg Pouch', 'Channa Dal, 1 kg Pouch', 'Channa Dal, 1 kg Pouch'),
(54, 1, 'धनिया (dhaniya)', '&lt;p&gt;Dhaniya (coriander)&lt;/p&gt;\r\n', 'coriander, dhaniya', 'Dhaniya (coriander)', 'Dhaniya (coriander)', 'Dhaniya (coriander)'),
(55, 1, 'Mirch (Green Chilli)', '&lt;p&gt;Mirch (Green Chilli)&lt;/p&gt;\r\n', 'mirch, green-chilli', 'Mirch (Green Chilli)', 'Mirch (Green Chilli)', 'Mirch (Green Chilli)'),
(56, 1, 'Kala Gol Marich (Black Pepper)', '&lt;p&gt;Kala Gol Marich (Black Pepper)&lt;/p&gt;\r\n', 'kala-marich, black-pepper', 'Kala Gol Marich (Black Pepper)', 'Kala Gol Marich (Black Pepper)', 'Kala Gol Marich (Black Pepper)'),
(57, 1, 'Honey', '&lt;p&gt;Honey&lt;/p&gt;\r\n', 'honey', 'Honey', 'Honey', 'Honey'),
(58, 1, 'Angoor (Grapes)', '&lt;p&gt;Angoor (Grapes)&lt;/p&gt;\r\n', 'angoor-grapes', 'Angoor (Grapes)', 'Angoor (Grapes)', 'Angoor (Grapes)'),
(59, 1, 'Kela (Banana)', '&lt;p&gt;Kela (Banana)&lt;/p&gt;\r\n', 'banana-kela', 'Kela (Banana)', 'Kela (Banana)', 'Kela (Banana)'),
(60, 1, '(Sev) Apple', '&lt;p&gt;(Sev) Apple&lt;/p&gt;\r\n', 'apple-sev', '(Sev) Apple', '(Sev) Apple', '(Sev) Apple'),
(61, 1, 'Sattu', '&lt;p&gt;Sattu&lt;/p&gt;\r\n', 'sattu', 'Sattu', 'Sattu', 'Sattu'),
(62, 1, 'Chini (Sugar)', '&lt;p&gt;Chini (Sugar)&lt;/p&gt;\r\n', 'chini, sugar', 'Chini (Sugar)', 'Chini (Sugar)', 'Chini (Sugar)'),
(63, 1, 'Whey Protein', '&lt;p&gt;Whey Protein&lt;/p&gt;\r\n', 'whey-protein', 'Whey Protein', 'Whey Protein', 'Whey Protein'),
(64, 1, 'Birthday Cake', '&lt;p&gt;Birthday Cake&lt;/p&gt;\r\n', 'birthday-cake', 'Birthday Cake', 'Birthday Cake', 'Birthday Cake'),
(65, 1, 'Bhuna Chana', '&lt;p&gt;Bhuna Chana&lt;/p&gt;\r\n', 'Bhuna Chana', 'Bhuna Chana', 'Bhuna Chana', 'Bhuna Chana'),
(66, 1, 'Mushrooms', '&lt;p&gt;&lt;strong&gt;Mushrooms&lt;/strong&gt;&lt;/p&gt;\r\n&lt;!-- cpt_container_end --&gt;', 'mushroom', 'Mushrooms', 'Mushrooms', 'Mushrooms');

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_discount`
--

DROP TABLE IF EXISTS `oc_product_discount`;
CREATE TABLE IF NOT EXISTS `oc_product_discount` (
  `product_discount_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`product_discount_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=457 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_discount`
--

INSERT INTO `oc_product_discount` (`product_discount_id`, `product_id`, `customer_group_id`, `quantity`, `priority`, `price`, `date_start`, `date_end`) VALUES
(454, 42, 1, 6, 1, '510.0000', '2020-03-01', '2020-05-16'),
(453, 42, 1, 4, 1, '520.0000', '2020-02-26', '0000-00-00'),
(452, 42, 1, 2, 1, '530.0000', '2020-03-04', '0000-00-00'),
(445, 41, 1, 1, 0, '336.5000', '2020-02-29', '0000-00-00'),
(455, 40, 1, 1, 1, '100.0000', '2020-03-28', '2020-05-31'),
(456, 65, 1, 1, 1, '140.0000', '2020-03-28', '2020-06-30');

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_filter`
--

DROP TABLE IF EXISTS `oc_product_filter`;
CREATE TABLE IF NOT EXISTS `oc_product_filter` (
  `product_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`filter_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_image`
--

DROP TABLE IF EXISTS `oc_product_image`;
CREATE TABLE IF NOT EXISTS `oc_product_image` (
  `product_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_image_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2460 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_image`
--

INSERT INTO `oc_product_image` (`product_image_id`, `product_id`, `image`, `sort_order`) VALUES
(2454, 30, 'catalog/demo/canon_eos_5d_2.jpg', 0),
(2409, 41, 'catalog/demo/imac_3.jpg', 0),
(2367, 31, 'catalog/demo/nikon_d300_5.jpg', 0),
(2366, 31, 'catalog/demo/nikon_d300_4.jpg', 0),
(2459, 29, 'catalog/demo/palm_treo_pro_3.jpg', 0),
(2413, 46, 'catalog/demo/sony_vaio_5.jpg', 0),
(2412, 46, 'catalog/demo/sony_vaio_4.jpg', 0),
(2453, 30, 'catalog/demo/canon_eos_5d_3.jpg', 0),
(2441, 47, 'catalog/grocery/moong-dal.jpg', 0),
(2408, 41, 'catalog/demo/imac_2.jpg', 0),
(2365, 31, 'catalog/demo/nikon_d300_2.jpg', 0),
(2364, 31, 'catalog/demo/nikon_d300_3.jpg', 0),
(2458, 29, 'catalog/demo/palm_treo_pro_2.jpg', 0),
(2411, 46, 'catalog/demo/sony_vaio_3.jpg', 0),
(2410, 46, 'catalog/demo/sony_vaio_2.jpg', 0),
(2419, 49, 'catalog/demo/samsung_tab_7.jpg', 0),
(2418, 49, 'catalog/demo/samsung_tab_6.jpg', 0),
(2417, 49, 'catalog/demo/samsung_tab_5.jpg', 0),
(2416, 49, 'catalog/demo/samsung_tab_4.jpg', 0),
(2415, 49, 'catalog/demo/samsung_tab_3.jpg', 0),
(2414, 49, 'catalog/demo/samsung_tab_2.jpg', 0),
(2448, 42, 'catalog/demo/compaq_presario.jpg', 0),
(2447, 42, 'catalog/demo/canon_eos_5d_1.jpg', 0),
(2440, 47, 'catalog/grocery/moong-dal.jpg', 0),
(2446, 42, 'catalog/demo/canon_eos_5d_2.jpg', 0),
(2442, 64, 'catalog/grocery/birthday-cakes/birthday-cake1.jpg', 0),
(2443, 64, 'catalog/grocery/birthday-cakes/white-round-cake-topped-with-yellow-slice-fruit-140831.jpg', 0),
(2449, 42, 'catalog/demo/hp_1.jpg', 0),
(2450, 42, 'catalog/demo/canon_logo.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_option`
--

DROP TABLE IF EXISTS `oc_product_option`;
CREATE TABLE IF NOT EXISTS `oc_product_option` (
  `product_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `required` tinyint(1) NOT NULL,
  PRIMARY KEY (`product_option_id`)
) ENGINE=MyISAM AUTO_INCREMENT=227 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_option`
--

INSERT INTO `oc_product_option` (`product_option_id`, `product_id`, `option_id`, `value`, `required`) VALUES
(224, 35, 11, '', 1),
(225, 47, 12, '2020-02-16', 1),
(209, 42, 6, '', 1),
(208, 42, 4, 'test', 1),
(218, 42, 1, '', 1),
(219, 42, 8, '2011-02-20', 1),
(222, 42, 7, '', 1),
(221, 42, 9, '22:25', 1),
(220, 42, 10, '2011-02-20 22:25', 1),
(217, 42, 5, '', 1),
(223, 42, 2, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_option_value`
--

DROP TABLE IF EXISTS `oc_product_option_value`;
CREATE TABLE IF NOT EXISTS `oc_product_option_value` (
  `product_option_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  `quantity` int(3) NOT NULL,
  `subtract` tinyint(1) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `price_prefix` varchar(1) NOT NULL,
  `points` int(8) NOT NULL,
  `points_prefix` varchar(1) NOT NULL,
  `weight` decimal(15,8) NOT NULL,
  `weight_prefix` varchar(1) NOT NULL,
  PRIMARY KEY (`product_option_value_id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_option_value`
--

INSERT INTO `oc_product_option_value` (`product_option_value_id`, `product_option_id`, `product_id`, `option_id`, `option_value_id`, `quantity`, `subtract`, `price`, `price_prefix`, `points`, `points_prefix`, `weight`, `weight_prefix`) VALUES
(8, 223, 42, 2, 23, 48, 1, '10.0000', '+', 0, '+', '10.00000000', '+'),
(2, 217, 42, 5, 42, 200, 1, '2.0000', '+', 0, '+', '2.00000000', '+'),
(1, 217, 42, 5, 41, 100, 0, '1.0000', '+', 0, '+', '1.00000000', '+'),
(3, 217, 42, 5, 40, 300, 0, '3.0000', '+', 0, '+', '3.00000000', '+'),
(4, 217, 42, 5, 39, 92, 1, '4.0000', '+', 0, '+', '4.00000000', '+'),
(7, 218, 42, 1, 43, 300, 1, '30.0000', '+', 3, '+', '30.00000000', '+'),
(14, 224, 35, 11, 48, 15, 1, '15.0000', '+', 0, '+', '0.00000000', '+'),
(13, 224, 35, 11, 47, 10, 1, '10.0000', '+', 0, '+', '0.00000000', '+'),
(12, 224, 35, 11, 46, 0, 1, '5.0000', '+', 0, '+', '0.00000000', '+'),
(6, 218, 42, 1, 31, 146, 1, '20.0000', '+', 2, '-', '20.00000000', '+'),
(5, 218, 42, 1, 32, 96, 1, '10.0000', '+', 1, '+', '10.00000000', '+'),
(9, 223, 42, 2, 24, 194, 1, '20.0000', '+', 0, '+', '20.00000000', '+'),
(10, 223, 42, 2, 44, 2696, 1, '30.0000', '+', 0, '+', '30.00000000', '+'),
(11, 223, 42, 2, 45, 3998, 1, '40.0000', '+', 0, '+', '40.00000000', '+');

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_recurring`
--

DROP TABLE IF EXISTS `oc_product_recurring`;
CREATE TABLE IF NOT EXISTS `oc_product_recurring` (
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`recurring_id`,`customer_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_related`
--

DROP TABLE IF EXISTS `oc_product_related`;
CREATE TABLE IF NOT EXISTS `oc_product_related` (
  `product_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`related_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_related`
--

INSERT INTO `oc_product_related` (`product_id`, `related_id`) VALUES
(40, 42),
(41, 42),
(42, 40),
(42, 41),
(58, 60),
(59, 60),
(60, 58),
(60, 59);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_reward`
--

DROP TABLE IF EXISTS `oc_product_reward`;
CREATE TABLE IF NOT EXISTS `oc_product_reward` (
  `product_reward_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `points` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_reward_id`)
) ENGINE=MyISAM AUTO_INCREMENT=581 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_reward`
--

INSERT INTO `oc_product_reward` (`product_reward_id`, `product_id`, `customer_group_id`, `points`) VALUES
(563, 42, 1, 100),
(580, 28, 1, 400),
(564, 43, 1, 600),
(566, 30, 1, 200),
(561, 44, 1, 700),
(576, 45, 3, 10),
(558, 49, 1, 1000),
(575, 45, 2, 10),
(574, 45, 1, 10),
(577, 66, 3, 10),
(578, 66, 2, 10),
(579, 66, 1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_special`
--

DROP TABLE IF EXISTS `oc_product_special`;
CREATE TABLE IF NOT EXISTS `oc_product_special` (
  `product_special_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`product_special_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=460 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_special`
--

INSERT INTO `oc_product_special` (`product_special_id`, `product_id`, `customer_group_id`, `priority`, `price`, `date_start`, `date_end`) VALUES
(455, 42, 1, 2, '540.0000', '2020-03-01', '2020-03-20'),
(459, 30, 1, 2, '90.0000', '0000-00-00', '0000-00-00'),
(458, 30, 1, 1, '13.5000', '2020-02-29', '2020-06-30');

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_category`
--

DROP TABLE IF EXISTS `oc_product_to_category`;
CREATE TABLE IF NOT EXISTS `oc_product_to_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`category_id`),
  KEY `category_id` (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_to_category`
--

INSERT INTO `oc_product_to_category` (`product_id`, `category_id`) VALUES
(30, 65),
(30, 71),
(31, 33),
(33, 28),
(33, 71),
(41, 50),
(42, 28),
(42, 71),
(43, 18),
(44, 18),
(45, 71),
(46, 45),
(47, 63),
(49, 62),
(50, 63),
(51, 69),
(51, 71),
(52, 69),
(54, 62),
(55, 62),
(56, 64),
(58, 68),
(58, 71),
(59, 66),
(60, 67),
(62, 65),
(62, 71),
(63, 70),
(64, 60),
(65, 71),
(66, 71);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_download`
--

DROP TABLE IF EXISTS `oc_product_to_download`;
CREATE TABLE IF NOT EXISTS `oc_product_to_download` (
  `product_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`download_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_layout`
--

DROP TABLE IF EXISTS `oc_product_to_layout`;
CREATE TABLE IF NOT EXISTS `oc_product_to_layout` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_to_layout`
--

INSERT INTO `oc_product_to_layout` (`product_id`, `store_id`, `layout_id`) VALUES
(50, 0, 0),
(50, 1, 0),
(47, 1, 0),
(47, 0, 0),
(30, 1, 0),
(30, 2, 0),
(28, 2, 0),
(28, 0, 0),
(41, 1, 0),
(41, 0, 0),
(35, 0, 0),
(35, 1, 0),
(46, 1, 0),
(46, 0, 0),
(31, 0, 0),
(31, 1, 0),
(42, 2, 0),
(42, 0, 0),
(44, 0, 0),
(44, 1, 0),
(29, 2, 0),
(29, 0, 0),
(33, 2, 0),
(33, 0, 0),
(49, 1, 0),
(49, 0, 0),
(45, 2, 0),
(45, 0, 0),
(40, 2, 0),
(40, 0, 0),
(43, 2, 0),
(43, 0, 0),
(51, 2, 0),
(51, 0, 0),
(52, 2, 0),
(52, 0, 0),
(53, 1, 0),
(53, 0, 0),
(54, 0, 0),
(54, 1, 0),
(55, 0, 0),
(55, 1, 0),
(56, 0, 0),
(56, 1, 0),
(57, 0, 0),
(57, 1, 0),
(58, 2, 0),
(58, 0, 0),
(59, 2, 0),
(59, 0, 0),
(60, 0, 0),
(60, 1, 0),
(61, 2, 0),
(61, 0, 0),
(62, 0, 0),
(62, 2, 0),
(63, 1, 0),
(63, 0, 0),
(64, 0, 0),
(64, 1, 0),
(60, 2, 0),
(30, 0, 0),
(62, 1, 0),
(33, 1, 0),
(58, 1, 0),
(42, 1, 0),
(51, 1, 0),
(40, 1, 0),
(43, 1, 0),
(65, 0, 0),
(65, 2, 0),
(65, 1, 0),
(61, 1, 0),
(45, 1, 0),
(66, 0, 0),
(66, 1, 0),
(66, 2, 0),
(29, 1, 0),
(52, 1, 0),
(28, 1, 0),
(59, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_store`
--

DROP TABLE IF EXISTS `oc_product_to_store`;
CREATE TABLE IF NOT EXISTS `oc_product_to_store` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_to_store`
--

INSERT INTO `oc_product_to_store` (`product_id`, `store_id`) VALUES
(28, 0),
(29, 0),
(30, 0),
(31, 0),
(33, 0),
(35, 0),
(40, 0),
(41, 0),
(42, 0),
(43, 0),
(44, 0),
(45, 0),
(46, 0),
(46, 1),
(47, 0),
(47, 1),
(49, 0),
(50, 0),
(51, 0),
(52, 0),
(53, 0),
(54, 0),
(54, 1),
(55, 0),
(56, 0),
(57, 0),
(58, 0),
(59, 0),
(60, 0),
(60, 1),
(61, 0),
(62, 0),
(63, 0),
(64, 0),
(65, 0),
(66, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_recurring`
--

DROP TABLE IF EXISTS `oc_recurring`;
CREATE TABLE IF NOT EXISTS `oc_recurring` (
  `recurring_id` int(11) NOT NULL AUTO_INCREMENT,
  `price` decimal(10,4) NOT NULL,
  `frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `duration` int(10) NOT NULL,
  `cycle` int(10) NOT NULL,
  `trial_status` tinyint(4) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `trial_frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `trial_duration` int(10) NOT NULL,
  `trial_cycle` int(10) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`recurring_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_recurring`
--

INSERT INTO `oc_recurring` (`recurring_id`, `price`, `frequency`, `duration`, `cycle`, `trial_status`, `trial_price`, `trial_frequency`, `trial_duration`, `trial_cycle`, `status`, `sort_order`) VALUES
(1, '500.0000', 'week', 4, 2, 0, '0.0000', 'day', 0, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_recurring_description`
--

DROP TABLE IF EXISTS `oc_recurring_description`;
CREATE TABLE IF NOT EXISTS `oc_recurring_description` (
  `recurring_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`recurring_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_recurring_description`
--

INSERT INTO `oc_recurring_description` (`recurring_id`, `language_id`, `name`) VALUES
(1, 1, 'Amit Chaurasia');

-- --------------------------------------------------------

--
-- Table structure for table `oc_return`
--

DROP TABLE IF EXISTS `oc_return`;
CREATE TABLE IF NOT EXISTS `oc_return` (
  `return_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `product` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `opened` tinyint(1) NOT NULL,
  `return_reason_id` int(11) NOT NULL,
  `return_action_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_ordered` date NOT NULL DEFAULT '0000-00-00',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`return_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_return`
--

INSERT INTO `oc_return` (`return_id`, `order_id`, `product_id`, `customer_id`, `firstname`, `lastname`, `email`, `telephone`, `product`, `model`, `quantity`, `opened`, `return_reason_id`, `return_action_id`, `return_status_id`, `comment`, `date_ordered`, `date_added`, `date_modified`) VALUES
(1, 3, 0, 8, 'Abhishek', 'Chaurasia', 'chaurasiaab.hi09@gmail.com', '5465465534', 'Paneer', 'Product 2', 1, 0, 3, 0, 3, 'gfhfgh', '2020-02-27', '2020-02-27 15:52:20', '2020-02-27 15:53:39'),
(2, 4, 0, 8, 'Abhishek', 'Chaurasia', 'chaurasiaab.hi09@gmail.com', '5465465534', 'Kabuli Channa', '1 kg Pouch', 1, 0, 2, 0, 2, 'cdfc', '2020-02-28', '2020-02-28 12:32:58', '2020-02-28 12:32:58'),
(3, 4, 31, 8, 'Abhishek', 'Chaurasia', 'chaurasiaab.hi09@gmail.com', '5465465534', 'Kabuli Channa', '1 kg Pouch', 1, 0, 5, 0, 2, 'vdf', '2020-02-28', '2020-02-28 12:38:20', '2020-02-28 12:38:20'),
(4, 7, 46, 9, 'Amit', 'Chaurasia', 'someoneelse@gmail.com', '9703465708', 'White Bread', 'White Bread', 1, 0, 3, 0, 2, '', '2020-02-29', '2020-02-29 22:36:26', '2020-02-29 22:36:26'),
(5, 6, 43, 9, 'Amit', 'Chaurasia', 'xxammuxx@gmail.com', '9703465708', 'Ketchup Tomato', '200 ML', 1, 0, 1, 0, 2, '', '2020-02-29', '2020-02-29 23:19:26', '2020-03-01 11:28:09'),
(6, 23, 29, 7, 'Abhishek', '', 'chaurasiaabhi09@gmail.com', '357834906754906790548069', 'Paneer', 'Product 2', 1, 0, 5, 0, 2, 'erwtretr', '2020-03-07', '2020-03-07 13:20:22', '2020-03-07 13:20:22');

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_action`
--

DROP TABLE IF EXISTS `oc_return_action`;
CREATE TABLE IF NOT EXISTS `oc_return_action` (
  `return_action_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`return_action_id`,`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_return_action`
--

INSERT INTO `oc_return_action` (`return_action_id`, `language_id`, `name`) VALUES
(1, 1, 'Refunded'),
(2, 1, 'Credit Issued'),
(3, 1, 'Replacement Sent');

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_history`
--

DROP TABLE IF EXISTS `oc_return_history`;
CREATE TABLE IF NOT EXISTS `oc_return_history` (
  `return_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `return_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`return_history_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_return_history`
--

INSERT INTO `oc_return_history` (`return_history_id`, `return_id`, `return_status_id`, `notify`, `comment`, `date_added`) VALUES
(1, 1, 3, 0, 'refunded', '2020-02-27 15:53:39');

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_reason`
--

DROP TABLE IF EXISTS `oc_return_reason`;
CREATE TABLE IF NOT EXISTS `oc_return_reason` (
  `return_reason_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`return_reason_id`,`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_return_reason`
--

INSERT INTO `oc_return_reason` (`return_reason_id`, `language_id`, `name`) VALUES
(1, 1, 'Stale Grocery'),
(2, 1, 'Received Wrong Item'),
(3, 1, 'Order Error'),
(4, 1, 'Faulty, please supply details'),
(5, 1, 'Other, please supply details');

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_status`
--

DROP TABLE IF EXISTS `oc_return_status`;
CREATE TABLE IF NOT EXISTS `oc_return_status` (
  `return_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`return_status_id`,`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_return_status`
--

INSERT INTO `oc_return_status` (`return_status_id`, `language_id`, `name`) VALUES
(1, 1, 'Pending'),
(3, 1, 'Complete'),
(2, 1, 'Awaiting Products');

-- --------------------------------------------------------

--
-- Table structure for table `oc_review`
--

DROP TABLE IF EXISTS `oc_review`;
CREATE TABLE IF NOT EXISTS `oc_review` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) NOT NULL,
  `text` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`review_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_seo_regex`
--

DROP TABLE IF EXISTS `oc_seo_regex`;
CREATE TABLE IF NOT EXISTS `oc_seo_regex` (
  `seo_regex_id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `name` varchar(64) NOT NULL,
  `regex` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`seo_regex_id`),
  KEY `regex` (`regex`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_seo_regex`
--

INSERT INTO `oc_seo_regex` (`seo_regex_id`, `key`, `name`, `regex`, `sort_order`) VALUES
(1, NULL, 'Product', '(product_id=\\d+)(?:[&]|$)', 100),
(2, NULL, 'Category Level 1', '(path=\\d+)(?:[_&]|$)', 1),
(3, NULL, 'Category Level 2', '(path=\\d+_\\d+)(?:[_&]|$)', 2),
(4, NULL, 'Category Level 3', '(path=\\d+_\\d+_\\d+)(?:[_&]|$)', 3),
(5, NULL, 'Information', '(information_id=\\d+)(?:[&]|$)', 1),
(6, NULL, 'Manufacturer', '(manufacturer_id=\\d+)(?:[&]|$)', 2),
(7, NULL, 'Language', '(language=[a-z-]+)(?:[&]|$)', -1),
(8, NULL, 'Route', '(route=[a-zA-Z0-9\\/]+)(?:[&]|$)', 0),
(9, NULL, 'Category Level 4', '(path=\\d+_\\d+_\\d+_\\d+)(?:[_&]|$)', 4),
(10, NULL, 'Category Level 5', '(path=\\d+_\\d+_\\d+_\\d+_\\d+)(?:[_&]|$)', 5),
(11, NULL, 'Manufacturer', '(manufacturer_id=)', 1),
(12, NULL, 'Information', '(information_id=)', 1),
(13, NULL, 'Category', '(path=)', 1),
(14, NULL, 'Product', '(product_id=)', 99);

-- --------------------------------------------------------

--
-- Table structure for table `oc_seo_url`
--

DROP TABLE IF EXISTS `oc_seo_url`;
CREATE TABLE IF NOT EXISTS `oc_seo_url` (
  `seo_url_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `query` varchar(255) NOT NULL,
  `push` varchar(255) NOT NULL,
  PRIMARY KEY (`seo_url_id`),
  KEY `query` (`query`),
  KEY `keyword` (`keyword`)
) ENGINE=MyISAM AUTO_INCREMENT=365 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_seo_url`
--

INSERT INTO `oc_seo_url` (`seo_url_id`, `store_id`, `language_id`, `keyword`, `query`, `push`) VALUES
(1, 0, 1, 'desktops', 'path=20', 'route=product/category&path=20'),
(2, 0, 1, 'pc', 'path=20_26', 'route=product/category&path=20_26'),
(3, 0, 1, 'mac', 'path=20_27', 'route=product/category&path=20_27'),
(84, 0, 1, 'milk-and-breads', 'path=18', 'route=product/category&path=18'),
(178, 0, 1, 'milk', 'path=18_46', 'route=product/category&path=18_46'),
(180, 0, 1, 'bread', 'path=18_45', 'route=product/category&path=18_45'),
(94, 0, 1, 'oil-ghee', 'path=25', 'route=product/category&path=25'),
(102, 0, 1, 'ghee', 'path=25_29', 'route=product/category&path=25_29'),
(182, 0, 1, 'edible-oil', 'path=25_28', 'route=product/category&path=25_28'),
(10, 0, 1, 'printer', 'path=25_30', 'route=product/category&path=25_30'),
(11, 0, 1, 'scanner', 'path=25_31', 'route=product/category&path=25_31'),
(12, 0, 1, 'web-camera', 'path=25_32', 'route=product/category&path=25_32'),
(13, 0, 1, 'tablet', 'path=57', 'route=product/category&path=57'),
(14, 0, 1, 'software', 'path=17', 'route=product/category&path=17'),
(15, 0, 1, 'smartphone', 'path=24', 'route=product/category&path=24'),
(92, 0, 1, 'sauce-jam-pickles', 'path=33', 'route=product/category&path=33'),
(78, 0, 1, 'eggs-meat-fish', 'path=34', 'route=product/category&path=34'),
(18, 0, 1, 'test-11', 'path=34_43', 'route=product/category&path=34_43'),
(19, 0, 1, 'test-12', 'path=34_44', 'route=product/category&path=34_44'),
(20, 0, 1, 'test-15', 'path=34_47', 'route=product/category&path=34_47'),
(21, 0, 1, 'test-16', 'path=34_48', 'route=product/category&path=34_48'),
(22, 0, 1, 'test-17', 'path=34_49', 'route=product/category&path=34_49'),
(96, 0, 1, 'eggs', 'path=34_50', 'route=product/category&path=34_50'),
(24, 0, 1, 'test-19', 'path=34_51', 'route=product/category&path=34_51'),
(98, 0, 1, 'fish', 'path=34_52', 'route=product/category&path=34_52'),
(26, 0, 1, 'test-25', 'path=34_58', 'route=product/category&path=34_58'),
(27, 0, 1, 'test-21', 'path=34_53', 'route=product/category&path=34_53'),
(28, 0, 1, 'test-22', 'path=34_54', 'route=product/category&path=34_54'),
(29, 0, 1, 'test-23', 'path=34_55', 'route=product/category&path=34_55'),
(30, 0, 1, 'test-24', 'path=34_56', 'route=product/category&path=34_56'),
(31, 0, 1, 'test-4', 'path=34_38', 'route=product/category&path=34_38'),
(32, 0, 1, 'test-5', 'path=34_37', 'route=product/category&path=34_37'),
(100, 0, 1, 'meat', 'path=34_39', 'route=product/category&path=34_39'),
(34, 0, 1, 'test-7', 'path=34_40', 'route=product/category&path=34_40'),
(35, 0, 1, 'test-8', 'path=34_41', 'route=product/category&path=34_41'),
(36, 0, 1, 'test-9', 'path=34_42', 'route=product/category&path=34_42'),
(336, 2, 1, 'salt-1kg', 'product_id=30', 'route=product/product&product_id=30'),
(263, 1, 1, 'moong-dal', 'product_id=47', 'route=product/product&product_id=47'),
(127, 1, 1, 'kabuli-channa', 'product_id=31', 'route=product/product&product_id=31'),
(360, 2, 1, 'fortune-oil', 'product_id=28', 'route=product/product&product_id=28'),
(324, 2, 1, 'tomato-ketchup', 'product_id=43', 'route=product/product&product_id=43'),
(126, 0, 1, 'kabuli-channa', 'product_id=31', 'route=product/product&product_id=31'),
(354, 2, 1, 'paneer', 'product_id=29', 'route=product/product&product_id=29'),
(122, 0, 1, 'milk-1-kg', 'product_id=35', 'route=product/product&product_id=35'),
(215, 1, 1, 'lehsun-garlic', 'product_id=49', 'route=product/product&product_id=49'),
(297, 2, 1, 'amul-ghee', 'product_id=33', 'route=product/product&product_id=33'),
(205, 1, 1, 'white-bread', 'product_id=46', 'route=product/product&product_id=46'),
(203, 1, 1, 'brown-eggs', 'product_id=41', 'route=product/product&product_id=41'),
(204, 0, 1, 'white-bread', 'product_id=46', 'route=product/product&product_id=46'),
(123, 1, 1, 'milk-1-kg', 'product_id=35', 'route=product/product&product_id=35'),
(150, 0, 1, 'aashirwad', 'manufacturer_id=8', 'route=product/manufacturer/info&manufacturer_id=8'),
(152, 0, 1, 'amul', 'manufacturer_id=9', 'route=product/manufacturer/info&manufacturer_id=9'),
(156, 0, 1, 'fortune', 'manufacturer_id=5', 'route=product/manufacturer/info&manufacturer_id=5'),
(154, 0, 1, 'patanjali', 'manufacturer_id=7', 'route=product/manufacturer/info&manufacturer_id=7'),
(59, 0, 1, 'palm', 'manufacturer_id=6', 'route=product/manufacturer/info&manufacturer_id=6'),
(158, 0, 1, 'nestle', 'manufacturer_id=10', 'route=product/manufacturer/info&manufacturer_id=10'),
(258, 0, 1, 'about-us', 'information_id=1', 'route=information/information&information_id=1'),
(75, 0, 1, 'privacy', 'information_id=3', 'route=information/information&information_id=3'),
(77, 0, 1, 'terms', 'information_id=2', 'route=information/information&information_id=2'),
(65, 0, 1, 'en-gb', 'language=en-gb', 'language=en-gb'),
(66, 0, 1, 'brands', 'route=product/manufacturer', 'route=product/manufacturer'),
(67, 0, 1, 'brands', 'manufacturer_id=', ''),
(68, 0, 1, '', 'route=information/information', ''),
(69, 0, 1, '', 'route=product/category', ''),
(70, 0, 1, '', 'route=product/manufacturer/info', ''),
(71, 0, 1, '', 'route=product/product', ''),
(79, 1, 1, 'eggs-meat-fish', 'path=34', 'route=product/category&path=34'),
(83, 1, 1, 'milk-breads', 'path=59', 'route=product/category&path=59'),
(82, 0, 1, 'milk-breads', 'path=59', 'route=product/category&path=59'),
(85, 1, 1, 'milk-and-breads', 'path=18', 'route=product/category&path=18'),
(261, 1, 1, 'channa-dal', 'product_id=50', 'route=product/product&product_id=50'),
(260, 0, 1, 'channa-dal', 'product_id=50', 'route=product/product&product_id=50'),
(262, 0, 1, 'moong-dal', 'product_id=47', 'route=product/product&product_id=47'),
(93, 1, 1, 'sauce-jam-pickles', 'path=33', 'route=product/category&path=33'),
(95, 1, 1, 'oil-ghee', 'path=25', 'route=product/category&path=25'),
(97, 1, 1, 'eggs', 'path=34_50', 'route=product/category&path=34_50'),
(99, 1, 1, 'fish', 'path=34_52', 'route=product/category&path=34_52'),
(101, 1, 1, 'meat', 'path=34_39', 'route=product/category&path=34_39'),
(103, 1, 1, 'ghee', 'path=25_29', 'route=product/category&path=25_29'),
(129, 1, 1, 'birthday-cakes', 'path=60', 'route=product/category&path=60'),
(128, 0, 1, 'birthday-cakes', 'path=60', 'route=product/category&path=60'),
(106, 0, 1, 'fruits', 'path=61', 'route=product/category&path=61'),
(107, 1, 1, 'fruits', 'path=61', 'route=product/category&path=61'),
(108, 0, 1, 'vegetables', 'path=62', 'route=product/category&path=62'),
(109, 1, 1, 'vegetables', 'path=62', 'route=product/category&path=62'),
(110, 0, 1, 'dal-and-pulses', 'path=63', 'route=product/category&path=63'),
(111, 1, 1, 'dal-and-pulses', 'path=63', 'route=product/category&path=63'),
(112, 0, 1, 'masala-spices', 'path=64', 'route=product/category&path=64'),
(113, 1, 1, 'masala-spices', 'path=64', 'route=product/category&path=64'),
(114, 0, 1, 'sugar-salt-jaggery', 'path=65', 'route=product/category&path=65'),
(115, 1, 1, 'sugar-salt-jaggery', 'path=65', 'route=product/category&path=65'),
(337, 1, 1, 'salt-1kg', 'product_id=30', 'route=product/product&product_id=30'),
(359, 0, 1, 'fortune-oil', 'product_id=28', 'route=product/product&product_id=28'),
(202, 0, 1, 'brown-eggs', 'product_id=41', 'route=product/product&product_id=41'),
(130, 0, 1, 'banana', 'path=61_66', 'route=product/category&path=61_66'),
(131, 1, 1, 'banana', 'path=61_66', 'route=product/category&path=61_66'),
(132, 0, 1, 'apples', 'path=61_67', 'route=product/category&path=61_67'),
(133, 1, 1, 'apples', 'path=61_67', 'route=product/category&path=61_67'),
(318, 2, 1, 'grapes', 'path=61_68', 'route=product/category&path=61_68'),
(317, 0, 1, 'grapes', 'path=61_68', 'route=product/category&path=61_68'),
(306, 2, 1, 'amul-cow-ghee', 'product_id=42', 'route=product/product&product_id=42'),
(229, 1, 1, 'daawat-basmati-rice', 'product_id=44', 'route=product/product&product_id=44'),
(228, 0, 1, 'daawat-basmati-rice', 'product_id=44', 'route=product/product&product_id=44'),
(353, 0, 1, 'paneer', 'product_id=29', 'route=product/product&product_id=29'),
(296, 0, 1, 'amul-ghee', 'product_id=33', 'route=product/product&product_id=33'),
(214, 0, 1, 'lehsun-garlic', 'product_id=49', 'route=product/product&product_id=49'),
(348, 2, 1, 'mushroom', 'product_id=45', 'route=product/product&product_id=45'),
(151, 1, 1, 'aashirwad', 'manufacturer_id=8', 'route=product/manufacturer/info&manufacturer_id=8'),
(153, 1, 1, 'amul', 'manufacturer_id=9', 'route=product/manufacturer/info&manufacturer_id=9'),
(155, 1, 1, 'patanjali', 'manufacturer_id=7', 'route=product/manufacturer/info&manufacturer_id=7'),
(157, 1, 1, 'fortune', 'manufacturer_id=5', 'route=product/manufacturer/info&manufacturer_id=5'),
(159, 1, 1, 'nestle', 'manufacturer_id=10', 'route=product/manufacturer/info&manufacturer_id=10'),
(160, 0, 1, 'dry-fruits', 'path=69', 'route=product/category&path=69'),
(161, 1, 1, 'dry-fruits', 'path=69', 'route=product/category&path=69'),
(321, 2, 1, 'tomato-chilli-sauce', 'product_id=40', 'route=product/product&product_id=40'),
(323, 0, 1, 'tomato-ketchup', 'product_id=43', 'route=product/product&product_id=43'),
(309, 2, 1, 'drydates-chhuara', 'product_id=51', 'route=product/product&product_id=51'),
(308, 0, 1, 'drydates-chhuara', 'product_id=51', 'route=product/product&product_id=51'),
(357, 2, 1, 'kaju-cashew', 'product_id=52', 'route=product/product&product_id=52'),
(356, 0, 1, 'kaju-cashew', 'product_id=52', 'route=product/product&product_id=52'),
(243, 1, 1, 'rajama', 'product_id=53', 'route=product/product&product_id=53'),
(242, 0, 1, 'rajama', 'product_id=53', 'route=product/product&product_id=53'),
(179, 1, 1, 'milk', 'path=18_46', 'route=product/category&path=18_46'),
(181, 1, 1, 'bread', 'path=18_45', 'route=product/category&path=18_45'),
(183, 1, 1, 'edible-oil', 'path=25_28', 'route=product/category&path=25_28'),
(266, 0, 1, 'refund_return_policy', 'information_id=5', 'route=information/information&information_id=5'),
(267, 1, 1, 'refund_return_policy', 'information_id=5', 'route=information/information&information_id=5'),
(208, 0, 1, 'dhaniya', 'product_id=54', 'route=product/product&product_id=54'),
(209, 1, 1, 'dhaniya', 'product_id=54', 'route=product/product&product_id=54'),
(210, 0, 1, 'mirch-greenchilli', 'product_id=55', 'route=product/product&product_id=55'),
(211, 1, 1, 'mirch-greenchilli', 'product_id=55', 'route=product/product&product_id=55'),
(212, 0, 1, 'kala-marich', 'product_id=56', 'route=product/product&product_id=56'),
(213, 1, 1, 'kala-marich', 'product_id=56', 'route=product/product&product_id=56'),
(222, 0, 1, 'dabur-honey', 'product_id=57', 'route=product/product&product_id=57'),
(223, 1, 1, 'dabur-honey', 'product_id=57', 'route=product/product&product_id=57'),
(305, 0, 1, 'amul-cow-ghee', 'product_id=42', 'route=product/product&product_id=42'),
(320, 0, 1, 'tomato-chilli-sauce', 'product_id=40', 'route=product/product&product_id=40'),
(300, 2, 1, 'angoor-grapes', 'product_id=58', 'route=product/product&product_id=58'),
(299, 0, 1, 'angoor-grapes', 'product_id=58', 'route=product/product&product_id=58'),
(363, 2, 1, 'banana-kela', 'product_id=59', 'route=product/product&product_id=59'),
(362, 0, 1, 'banana-kela', 'product_id=59', 'route=product/product&product_id=59'),
(286, 1, 1, 'apple-sev', 'product_id=60', 'route=product/product&product_id=60'),
(330, 2, 1, 'sattu', 'product_id=61', 'route=product/product&product_id=61'),
(329, 0, 1, 'sattu', 'product_id=61', 'route=product/product&product_id=61'),
(240, 0, 1, 'other-items', 'path=70', 'route=product/category&path=70'),
(241, 1, 1, 'other-items', 'path=70', 'route=product/category&path=70'),
(294, 2, 1, 'sugar', 'product_id=62', 'route=product/product&product_id=62'),
(293, 0, 1, 'sugar', 'product_id=62', 'route=product/product&product_id=62'),
(251, 1, 1, 'whey-protein', 'product_id=63', 'route=product/product&product_id=63'),
(250, 0, 1, 'whey-protein', 'product_id=63', 'route=product/product&product_id=63'),
(259, 1, 1, 'about-us', 'information_id=1', 'route=information/information&information_id=1'),
(264, 0, 1, 'birthday-cake', 'product_id=64', 'route=product/product&product_id=64'),
(265, 1, 1, 'birthday-cake', 'product_id=64', 'route=product/product&product_id=64'),
(271, 1, 1, 'delivery_shipping_policy', 'information_id=4', 'route=information/information&information_id=4'),
(270, 0, 1, 'delivery_shipping_policy', 'information_id=4', 'route=information/information&information_id=4'),
(284, 0, 1, 'apple-sev', 'product_id=60', 'route=product/product&product_id=60'),
(285, 2, 1, 'apple-sev', 'product_id=60', 'route=product/product&product_id=60'),
(314, 0, 1, 'offers-sale', 'path=71', 'route=product/category&path=71'),
(315, 2, 1, 'offers-sale', 'path=71', 'route=product/category&path=71'),
(316, 1, 1, 'offers-sale', 'path=71', 'route=product/category&path=71'),
(335, 0, 1, 'salt-1kg', 'product_id=30', 'route=product/product&product_id=30'),
(295, 1, 1, 'sugar', 'product_id=62', 'route=product/product&product_id=62'),
(298, 1, 1, 'amul-ghee', 'product_id=33', 'route=product/product&product_id=33'),
(301, 1, 1, 'angoor-grapes', 'product_id=58', 'route=product/product&product_id=58'),
(307, 1, 1, 'amul-cow-ghee', 'product_id=42', 'route=product/product&product_id=42'),
(310, 1, 1, 'drydates-chhuara', 'product_id=51', 'route=product/product&product_id=51'),
(319, 1, 1, 'grapes', 'path=61_68', 'route=product/category&path=61_68'),
(322, 1, 1, 'tomato-chilli-sauce', 'product_id=40', 'route=product/product&product_id=40'),
(325, 1, 1, 'tomato-ketchup', 'product_id=43', 'route=product/product&product_id=43'),
(326, 0, 1, 'bhuna-chana', 'product_id=65', 'route=product/product&product_id=65'),
(327, 2, 1, 'bhuna-chana', 'product_id=65', 'route=product/product&product_id=65'),
(328, 1, 1, 'bhuna-chana', 'product_id=65', 'route=product/product&product_id=65'),
(331, 1, 1, 'sattu', 'product_id=61', 'route=product/product&product_id=61'),
(349, 1, 1, 'mushroom', 'product_id=45', 'route=product/product&product_id=45'),
(347, 0, 1, 'mushroom', 'product_id=45', 'route=product/product&product_id=45'),
(350, 0, 1, 'mushroom-400g', 'product_id=66', 'route=product/product&product_id=66'),
(351, 2, 1, 'mushroom-400g', 'product_id=66', 'route=product/product&product_id=66'),
(352, 1, 1, 'mushroom-400g', 'product_id=66', 'route=product/product&product_id=66'),
(355, 1, 1, 'paneer', 'product_id=29', 'route=product/product&product_id=29'),
(358, 1, 1, 'kaju-cashew', 'product_id=52', 'route=product/product&product_id=52'),
(361, 1, 1, 'fortune-oil', 'product_id=28', 'route=product/product&product_id=28'),
(364, 1, 1, 'banana-kela', 'product_id=59', 'route=product/product&product_id=59');

-- --------------------------------------------------------

--
-- Table structure for table `oc_session`
--

DROP TABLE IF EXISTS `oc_session`;
CREATE TABLE IF NOT EXISTS `oc_session` (
  `session_id` varchar(32) NOT NULL,
  `data` text NOT NULL,
  `expire` datetime NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_session`
--

INSERT INTO `oc_session` (`session_id`, `data`, `expire`) VALUES
('01e3f2af8e0ff518f3ab0ae012', '{\"api_id\":\"1\"}', '2020-02-22 18:13:01'),
('054e2105cd9a20ebbbf796bdeb', '{\"currency\":\"INR\"}', '2020-02-16 14:44:15'),
('07428a019e183e6763011d9427', '{\"api_id\":\"1\",\"currency\":\"INR\"}', '2020-03-21 23:22:18'),
('09226605861413b0a9707b9da7', '{\"api_id\":\"1\"}', '2020-03-01 11:44:13'),
('0a97da31ae3adaeb6c8e044e00', '{\"currency\":\"INR\",\"user_id\":\"1\",\"user_token\":\"74e700563308c0ed5c6d8408e2f83538\"}', '2020-02-15 18:47:44'),
('0b74d66fbfa92745c020f8a3c5', '{\"api_id\":\"1\",\"currency\":\"INR\"}', '2020-03-16 17:10:55'),
('10750eaf8ba989bea2d4e716fa', '{\"api_id\":\"1\"}', '2020-03-02 14:00:51'),
('15e2c2674f2b288c190ec36311', '{\"currency\":\"INR\",\"just_loggedin\":true,\"razorpay_order_id\":\"order_ERV07JDxlnKqHi\"}', '2020-03-13 11:16:15'),
('1633b0bb5f1e0a54bf1c155504', '{\"api_id\":\"1\"}', '2020-03-21 23:25:04'),
('17a17924d514d1ccd70a2c5d7a', '{\"currency\":\"INR\",\"just_loggedin\":true,\"redirect\":\"http:\\/\\/localhost\\/theorderapp\\/index.php?route==account\\/register\",\"customer_id\":\"7\"}', '2020-03-10 20:45:09'),
('180caf4b4107082f736d56d707', '{\"api_id\":\"1\"}', '2020-03-21 23:24:45'),
('18780cbc5faa61b19ce11a3a23', '{\"currency\":\"INR\",\"user_id\":\"1\",\"user_token\":\"12529d5725cfa8fd69e5454ca3a75d2c\",\"redirect\":\"http:\\/\\/localhost\\/theorderapp\\/index.php?route==account\\/login\",\"customer_id\":\"7\",\"just_loggedin\":true,\"payment_address\":{\"address_id\":\"4\",\"firstname\":\"Abhishek\",\"lastname\":\"\",\"company\":\"\",\"address_1\":\"\",\"address_2\":\"tyty\",\"postcode\":\"274001\",\"city\":\"Deoria\",\"zone_id\":\"12\",\"zone\":\"Jowzjan\",\"zone_code\":\"JOW\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":null}}', '2020-03-04 21:50:32'),
('18a1fccdaeccd6239a5318290b', '{\"user_id\":\"1\",\"user_token\":\"21dc184637cf409244041ce939f3c4e7\",\"currency\":\"INR\",\"just_loggedin\":true,\"customer_id\":\"8\",\"shipping_address\":{\"address_id\":\"2\",\"firstname\":\"Abhishek\",\"lastname\":\"Chaurasia\",\"company\":\"\",\"address_1\":\"Amar Jyoti Road, Garulpar, Deoria\",\"address_2\":\"\",\"postcode\":\"274001\",\"city\":\"Deoria\",\"zone_id\":\"1505\",\"zone\":\"Uttar Pradesh\",\"zone_code\":\"UP\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":null},\"payment_address\":{\"address_id\":\"2\",\"firstname\":\"Abhishek\",\"lastname\":\"Chaurasia\",\"company\":\"\",\"address_1\":\"Amar Jyoti Road, Garulpar, Deoria\",\"address_2\":\"\",\"postcode\":\"274001\",\"city\":\"Deoria\",\"zone_id\":\"1505\",\"zone\":\"Uttar Pradesh\",\"zone_code\":\"UP\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":null}}', '2020-02-28 17:10:09'),
('18bb78a82693acb1bee123f582', '{\"api_id\":\"1\"}', '2020-03-19 17:49:47'),
('19d83b58225c93cdfa3e720fcb', '{\"api_id\":\"1\",\"currency\":\"INR\",\"customer_id\":\"9\",\"customer\":{\"customer_id\":\"9\",\"customer_group_id\":\"1\",\"firstname\":\"Amit\",\"lastname\":\"Chaurasia\",\"email\":\"xxammuxx@gmail.com\",\"telephone\":\"9703465708\",\"custom_field\":{\"10\":\"\"}},\"payment_address\":{\"firstname\":\"Amit\",\"lastname\":\"Chaurasia\",\"company\":\"None\",\"address_1\":\"Balaji Delux mens Hostel (Sai Serenity)\",\"address_2\":\"Opp. Gruhasiri Pride, Gachibowli\",\"postcode\":\"500032\",\"city\":\"Hyderabad\",\"zone_id\":\"4231\",\"zone\":\"Telangana\",\"zone_code\":\"TS\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":[]},\"payment_methods\":{\"razorpay\":{\"code\":\"razorpay\",\"title\":\"Credit Card \\/ Debit Card \\/ Net Banking (Razorpay)\",\"terms\":\"\",\"sort_order\":\"2\"},\"cod\":{\"code\":\"cod\",\"title\":\"Cash On Delivery\",\"terms\":\"\",\"sort_order\":\"5\"}},\"shipping_address\":{\"firstname\":\"Amit\",\"lastname\":\"Chaurasia\",\"company\":\"None\",\"address_1\":\"Balaji Delux mens Hostel (Sai Serenity)\",\"address_2\":\"Opp. Gruhasiri Pride, Gachibowli\",\"postcode\":\"500032\",\"city\":\"Hyderabad\",\"zone_id\":\"4231\",\"zone\":\"Telangana\",\"zone_code\":\"TS\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":[]},\"shipping_methods\":{\"pickup\":{\"title\":\"Pickup\",\"quote\":{\"pickup\":{\"code\":\"pickup.pickup\",\"title\":\"Pickup From Store\",\"cost\":0,\"tax_class_id\":0,\"text\":\"\\u20b90.00\"}},\"sort_order\":\"\",\"error\":false},\"flat\":{\"title\":\"Flat Rate\",\"quote\":{\"flat\":{\"code\":\"flat.flat\",\"title\":\"Flat Shipping Rate\",\"cost\":\"5.00\",\"tax_class_id\":\"9\",\"text\":\"\\u20b95.00\"}},\"sort_order\":\"1\",\"error\":false}},\"payment_method\":{\"code\":\"razorpay\",\"title\":\"Credit Card \\/ Debit Card \\/ Net Banking (Razorpay)\",\"terms\":\"\",\"sort_order\":\"2\"},\"shipping_method\":{\"code\":\"flat.flat\",\"title\":\"Flat Shipping Rate\",\"cost\":\"5.00\",\"tax_class_id\":\"9\",\"text\":\"\\u20b95.00\"}}', '2020-02-29 23:27:43'),
('1da04060412fd09004b1b04a28', '{\"user_id\":\"1\",\"user_token\":\"445df23da9994dc9419376d6ecfeb4de\"}', '2020-03-01 13:51:29'),
('20da5fb99b6752b68a0d04c5d3', '{\"currency\":\"INR\"}', '2020-03-12 20:50:19'),
('217c9429c5fc122a3b0b9ebf18', '{\"currency\":\"INR\",\"user_id\":\"1\",\"user_token\":\"0f49bff5c3d580490c4870dfb34981e6\"}', '2020-02-20 20:34:52'),
('23e03563b456321feb300d22f0', '{\"currency\":\"INR\",\"just_loggedin\":true,\"user_id\":\"1\",\"user_token\":\"6267f9f6505e2c500f4ea20ac4d124c7\",\"razorpay_order_id\":\"order_EPFeHBMsF71uNK\",\"customer_id\":\"8\",\"shipping_address\":{\"address_id\":\"2\",\"firstname\":\"Abhishek\",\"lastname\":\"Chaurasia\",\"company\":\"\",\"address_1\":\"Amar Jyoti Road, Garulpar, Deoria\",\"address_2\":\"\",\"postcode\":\"274001\",\"city\":\"Deoria\",\"zone_id\":\"1505\",\"zone\":\"Uttar Pradesh\",\"zone_code\":\"UP\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":null}}', '2020-03-07 20:39:59'),
('280be2848ee3de50f0d5b9e38a', '{\"currency\":\"INR\"}', '2020-02-22 23:52:14'),
('2843b41d5ff4ea43a80a15b22e', '{\"user_id\":\"1\",\"user_token\":\"4c1104c261510f999b65be9d6cd2d08b\",\"currency\":\"INR\",\"account\":\"guest\",\"payment_address\":{\"firstname\":\"Abhishek\",\"lastname\":\"Chaurasia\",\"company\":\"\",\"address_1\":\"Amar Jyoti Road, Garulpar, Deoria\",\"address_2\":\"Amar Jyoti Road, Garulpar, Deoria\",\"postcode\":\"274001\",\"city\":\"Deoria\",\"country_id\":\"99\",\"zone_id\":\"1505\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":[],\"zone\":\"Uttar Pradesh\",\"zone_code\":\"UP\"},\"shipping_address\":{\"firstname\":\"Abhishek\",\"lastname\":\"Chaurasia\",\"company\":\"\",\"address_1\":\"Amar Jyoti Road, Garulpar, Deoria\",\"address_2\":\"Amar Jyoti Road, Garulpar, Deoria\",\"postcode\":\"274001\",\"city\":\"Deoria\",\"country_id\":\"99\",\"zone_id\":\"1505\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"zone\":\"Uttar Pradesh\",\"zone_code\":\"UP\",\"custom_field\":[]},\"razorpay_order_id\":\"order_EJj6R8p8fqjANz\"}', '2020-02-22 18:53:20'),
('284b5d118d0323bd5a17085679', '{\"api_id\":\"1\"}', '2020-02-29 23:45:47'),
('2f58e33a1b30955db47e919a1e', '{\"api_id\":\"1\"}', '2020-03-02 14:01:37'),
('2fd9a8b87baa35bbd606c8c09d', '{\"api_id\":\"1\"}', '2020-03-02 14:03:50'),
('302055844556857045c4c51bb3', '{\"api_id\":\"1\",\"currency\":\"INR\"}', '2020-02-29 22:50:03'),
('336ed186396d50bc554bb78f2b', '{\"api_id\":\"1\"}', '2020-03-21 23:17:38'),
('37fc2a199b72570fbd3691739b', '{\"currency\":\"INR\",\"user_id\":\"1\",\"user_token\":\"091c71b93db255618ffbf02f6fa4809a\"}', '2020-03-01 13:08:44'),
('38b0cbac1aa1fe30968c8e5fea', '{\"api_id\":\"1\",\"currency\":\"INR\"}', '2020-03-05 22:26:14'),
('3a7361278635b340b5bea6a2c9', '{\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"7271f949fc5b8a3e37159fb42b4b2c82\"}', '2020-02-08 15:00:16'),
('3bbce954a2562d963fb0e80c68', '{\"api_id\":\"1\",\"currency\":\"INR\"}', '2020-02-27 19:58:51'),
('3f699d299be73261b2ea3a1f19', '{\"api_id\":\"1\"}', '2020-03-01 11:42:47'),
('428910426e1c060f1d695e5981', '{\"currency\":\"INR\",\"redirect\":\"http:\\/\\/localhost\\/theorderapp\\/index.php?route==account\\/login\",\"just_loggedin\":true,\"user_id\":\"1\",\"user_token\":\"c5015b8a6dd6f69f45406924137dea2c\"}', '2020-03-06 18:33:41'),
('442a1cded44775186d53bb4604', '{\"api_id\":\"1\"}', '2020-02-29 22:52:31'),
('45a8a0901bce832618145be21c', '{\"currency\":\"INR\",\"user_id\":\"1\",\"user_token\":\"ee313d6cc12edf96f82ff29845509a49\"}', '2020-03-20 17:32:47'),
('45c8e5b87fd5ed766e78670c0e', '{\"api_id\":\"1\",\"currency\":\"INR\"}', '2020-03-21 23:27:15'),
('499ae23badd5c40dbd175e1ce1', '{\"api_id\":\"1\",\"currency\":\"INR\",\"customer_id\":\"12\",\"customer\":{\"customer_id\":\"12\",\"customer_group_id\":\"1\",\"firstname\":\"Amit\",\"lastname\":\"Chaurasia\",\"email\":\"xxammuxx@gmail.com\",\"telephone\":\"8019773696\",\"custom_field\":{\"10\":\"\"}},\"payment_address\":{\"firstname\":\"Amit\",\"lastname\":\"Chaurasia\",\"company\":\"\",\"address_1\":\"Balaji Delux mens Hostel (Sai Serenity)\",\"address_2\":\"Opp. Gruhasiri Pride, Gachibowli\",\"postcode\":\"274001\",\"city\":\"Deoria\",\"zone_id\":\"1505\",\"zone\":\"Uttar Pradesh\",\"zone_code\":\"UP\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":[]},\"payment_methods\":{\"razorpay\":{\"code\":\"razorpay\",\"title\":\"Credit Card \\/ Debit Card \\/ Net Banking (Razorpay)\",\"terms\":\"\",\"sort_order\":\"2\"},\"cod\":{\"code\":\"cod\",\"title\":\"Cash On Delivery\",\"terms\":\"\",\"sort_order\":\"5\"}},\"shipping_address\":{\"firstname\":\"Amit\",\"lastname\":\"Chaurasia\",\"company\":\"\",\"address_1\":\"Balaji Delux mens Hostel (Sai Serenity)\",\"address_2\":\"Opp. Gruhasiri Pride, Gachibowli\",\"postcode\":\"274001\",\"city\":\"Deoria\",\"zone_id\":\"1505\",\"zone\":\"Uttar Pradesh\",\"zone_code\":\"UP\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":[]},\"shipping_methods\":{\"pickup\":{\"title\":\"Pickup\",\"quote\":{\"pickup\":{\"code\":\"pickup.pickup\",\"title\":\"Pickup From Store\",\"cost\":0,\"tax_class_id\":0,\"text\":\"\\u20b90.00\"}},\"sort_order\":\"\",\"error\":false},\"flat\":{\"title\":\"Flat Rate\",\"quote\":{\"flat\":{\"code\":\"flat.flat\",\"title\":\"Flat Shipping Rate\",\"cost\":\"5.00\",\"tax_class_id\":\"9\",\"text\":\"\\u20b95.00\"}},\"sort_order\":\"1\",\"error\":false}},\"payment_method\":{\"code\":\"razorpay\",\"title\":\"Credit Card \\/ Debit Card \\/ Net Banking (Razorpay)\",\"terms\":\"\",\"sort_order\":\"2\"},\"shipping_method\":{\"code\":\"flat.flat\",\"title\":\"Flat Shipping Rate\",\"cost\":\"5.00\",\"tax_class_id\":\"9\",\"text\":\"\\u20b95.00\"}}', '2020-03-21 23:19:18'),
('4b0d3744298c3a6475eef97958', '{\"api_id\":\"1\",\"currency\":\"INR\"}', '2020-03-08 15:38:33'),
('4bee9dcba36402e1a9129c554f', '{\"api_id\":\"1\",\"currency\":\"INR\"}', '2020-03-08 15:44:12'),
('4c80bd763cbf90cf36a7b1f656', '{\"api_id\":\"1\",\"currency\":\"INR\"}', '2020-03-05 22:17:04'),
('4cd39502f1a691f323a1cd504d', '{\"api_id\":\"1\"}', '2020-02-08 14:45:28'),
('4d0b03b137f11b691e4d099015', '{\"user_id\":\"1\",\"user_token\":\"9cf5a396f4562591d34a2c2ecba5e6c1\",\"currency\":\"INR\",\"just_loggedin\":true,\"redirect\":\"http:\\/\\/localhost\\/theorderapp\\/index.php?route==account\\/login\",\"customer_id\":\"7\"}', '2020-03-03 18:06:04'),
('4f548687c7493fd5c93c0a0814', '{\"api_id\":\"1\"}', '2020-02-29 23:24:22'),
('51bb2257bd0db68309dd022b54', '{\"api_id\":\"1\"}', '2020-03-21 23:12:45'),
('52075d7b18be129bd170e02ad0', '{\"user_id\":\"1\",\"user_token\":\"d9fe194d98dee06ca81708e67a7f61de\",\"currency\":\"INR\",\"customer_id\":\"1\"}', '2020-02-15 17:29:38'),
('57594230127710de0323e9ad37', '{\"api_id\":\"1\"}', '2020-02-28 13:55:53'),
('5c8bcd28f6256b957ab67ac7c6', '{\"api_id\":\"1\"}', '2020-03-07 19:48:20'),
('5d0c3589e8d734018a429c2be2', '{\"user_id\":\"1\",\"user_token\":\"73c8edfcb5cb4a9c968428815418cbc8\",\"currency\":\"INR\"}', '2020-02-24 02:05:19'),
('5d10e44ee34e8eeda64da761df', '{\"api_id\":\"1\"}', '2020-02-29 22:49:57'),
('5f03f84625eaaaf83ad9c1bf62', '{\"api_id\":\"1\"}', '2020-03-05 22:15:58'),
('630a8fdafb27f5cedde134bea5', '{\"currency\":\"INR\",\"user_id\":\"1\",\"user_token\":\"bb784d9e52d4588d17ef908608752f81\",\"redirect\":\"http:\\/\\/localhost\\/theorderapp\\/index.php?route==account\\/login\",\"customer_id\":\"7\",\"just_loggedin\":true,\"payment_address\":{\"address_id\":\"9\",\"firstname\":\"Abhishek\",\"lastname\":\"\",\"company\":\"\",\"address_1\":\"\",\"address_2\":\"qwerty\",\"postcode\":\"274001\",\"city\":\"Deoria\",\"zone_id\":\"1505\",\"zone\":\"Uttar Pradesh\",\"zone_code\":\"UP\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":null},\"shipping_address\":{\"address_id\":\"9\",\"firstname\":\"Abhishek\",\"lastname\":\"\",\"company\":\"\",\"address_1\":\"\",\"address_2\":\"qwerty\",\"postcode\":\"274001\",\"city\":\"Deoria\",\"zone_id\":\"1505\",\"zone\":\"Uttar Pradesh\",\"zone_code\":\"UP\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":null},\"shipping_methods\":{\"pickup\":{\"title\":\"Pickup\",\"quote\":{\"pickup\":{\"code\":\"pickup.pickup\",\"title\":\"Pickup From Store\",\"cost\":0,\"tax_class_id\":0,\"text\":\"\\u20b90.00\"}},\"sort_order\":\"\",\"error\":false},\"flat\":{\"title\":\"Flat Rate\",\"quote\":{\"flat\":{\"code\":\"flat.flat\",\"title\":\"Flat Shipping Rate\",\"cost\":\"5.00\",\"tax_class_id\":\"9\",\"text\":\"\\u20b95.00\"}},\"sort_order\":\"1\",\"error\":false}},\"shipping_method\":{\"code\":\"pickup.pickup\",\"title\":\"Pickup From Store\",\"cost\":0,\"tax_class_id\":0,\"text\":\"\\u20b90.00\"},\"comment\":\"\",\"payment_methods\":{\"razorpay\":{\"code\":\"razorpay\",\"title\":\"Credit Card \\/ Debit Card \\/ Net Banking (Razorpay)\",\"terms\":\"\",\"sort_order\":\"2\"},\"cod\":{\"code\":\"cod\",\"title\":\"Cash On Delivery\",\"terms\":\"\",\"sort_order\":\"5\"}},\"payment_method\":{\"code\":\"cod\",\"title\":\"Cash On Delivery\",\"terms\":\"\",\"sort_order\":\"5\"},\"order_id\":26}', '2020-03-08 15:58:06'),
('6438110f202751e8b8d597ec4d', '{\"currency\":\"INR\",\"customer_id\":\"10\",\"just_loggedin\":true,\"payment_address\":{\"address_id\":\"10\",\"firstname\":\"Abhishek\",\"lastname\":\"\",\"company\":\"\",\"address_1\":\"dth\",\"address_2\":\"acfdf\",\"postcode\":\"274001\",\"city\":\"Deoria\",\"zone_id\":\"1505\",\"zone\":\"Uttar Pradesh\",\"zone_code\":\"UP\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":null},\"shipping_address\":{\"address_id\":\"10\",\"firstname\":\"Abhishek\",\"lastname\":\"\",\"company\":\"\",\"address_1\":\"dth\",\"address_2\":\"acfdf\",\"postcode\":\"274001\",\"city\":\"Deoria\",\"zone_id\":\"1505\",\"zone\":\"Uttar Pradesh\",\"zone_code\":\"UP\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":null},\"razorpay_order_id\":\"order_ETHcY8TsaFibL8\",\"shipping_methods\":{\"pickup\":{\"title\":\"Pickup\",\"quote\":{\"pickup\":{\"code\":\"pickup.pickup\",\"title\":\"Pickup From Store\",\"cost\":0,\"tax_class_id\":0,\"text\":\"\\u20b90.00\"}},\"sort_order\":\"\",\"error\":false},\"flat\":{\"title\":\"Flat Rate\",\"quote\":{\"flat\":{\"code\":\"flat.flat\",\"title\":\"Flat Shipping Rate\",\"cost\":\"5.00\",\"tax_class_id\":\"9\",\"text\":\"\\u20b95.00\"}},\"sort_order\":\"1\",\"error\":false}},\"shipping_method\":{\"code\":\"flat.flat\",\"title\":\"Flat Shipping Rate\",\"cost\":\"5.00\",\"tax_class_id\":\"9\",\"text\":\"\\u20b95.00\"},\"comment\":\"\",\"order_id\":73,\"payment_methods\":{\"razorpay\":{\"code\":\"razorpay\",\"title\":\"Credit Card \\/ Debit Card \\/ Net Banking (Razorpay)\",\"terms\":\"\",\"sort_order\":\"2\"},\"cod\":{\"code\":\"cod\",\"title\":\"Cash On Delivery\",\"terms\":\"\",\"sort_order\":\"5\"}},\"payment_method\":{\"code\":\"razorpay\",\"title\":\"Credit Card \\/ Debit Card \\/ Net Banking (Razorpay)\",\"terms\":\"\",\"sort_order\":\"2\"}}', '2020-03-17 21:48:52'),
('653ef7bb85404d987367261cda', '{\"currency\":\"INR\",\"user_id\":\"1\",\"user_token\":\"5270fe7943137ed384b1377763be89ec\",\"just_loggedin\":true}', '2020-03-19 18:29:50'),
('67da8b8f1b30a14aa685242f14', '{\"api_id\":\"1\"}', '2020-02-29 22:53:34'),
('6a1e565cb6b393b9fa6c2df62a', '{\"api_id\":\"1\"}', '2020-03-02 14:04:45'),
('6c6f2390fcd9ec280675c4e020', '{\"api_id\":\"1\"}', '2020-03-21 23:25:00'),
('6f277989c3c2bb7151643eb5e1', '{\"api_id\":\"1\"}', '2020-03-02 13:57:32'),
('70f4712643317d17813b34a4fa', '{\"currency\":\"INR\"}', '2020-02-29 23:26:02'),
('72a1dbf6ff70309d69e14430fa', '{\"api_id\":\"1\"}', '2020-02-27 14:55:14'),
('72d0a1fe62d59e55ce12f01274', '{\"api_id\":\"1\"}', '2020-03-21 23:27:15'),
('7352defbaed5a1d5828253476f', '', '2020-02-22 19:00:08'),
('7401a853c24c79de4c0b8e1283', '{\"api_id\":\"1\"}', '2020-02-29 23:25:22'),
('75c9eb5ef5622d5a02efc03436', '{\"currency\":\"INR\",\"user_id\":\"1\",\"user_token\":\"70ecb7d5f98e96f96227943915aaf254\",\"just_loggedin\":true,\"redirect\":\"http:\\/\\/localhost\\/theorderapp\\/index.php?route==account\\/register\",\"customer_id\":\"10\",\"payment_address\":{\"address_id\":\"10\",\"firstname\":\"Abhishek\",\"lastname\":\"\",\"company\":\"\",\"address_1\":\"dth\",\"address_2\":\"acfdf\",\"postcode\":\"274001\",\"city\":\"Deoria\",\"zone_id\":\"1505\",\"zone\":\"Uttar Pradesh\",\"zone_code\":\"UP\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":null},\"shipping_address\":{\"address_id\":\"10\",\"firstname\":\"Abhishek\",\"lastname\":\"\",\"company\":\"\",\"address_1\":\"dth\",\"address_2\":\"acfdf\",\"postcode\":\"274001\",\"city\":\"Deoria\",\"zone_id\":\"1505\",\"zone\":\"Uttar Pradesh\",\"zone_code\":\"UP\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":null},\"shipping_methods\":{\"pickup\":{\"title\":\"Pickup\",\"quote\":{\"pickup\":{\"code\":\"pickup.pickup\",\"title\":\"Pickup From Store\",\"cost\":0,\"tax_class_id\":0,\"text\":\"\\u20b90.00\"}},\"sort_order\":\"\",\"error\":false},\"flat\":{\"title\":\"Flat Rate\",\"quote\":{\"flat\":{\"code\":\"flat.flat\",\"title\":\"Flat Shipping Rate\",\"cost\":\"5.00\",\"tax_class_id\":\"9\",\"text\":\"\\u20b95.00\"}},\"sort_order\":\"1\",\"error\":false}}}', '2020-03-16 19:41:47'),
('785ae5161dbd790f299b581654', '{\"api_id\":\"1\"}', '2020-03-02 14:07:37'),
('78757b7cf8e83cd808354dd7eb', '{\"user_id\":\"1\",\"user_token\":\"cfdc9b8dd744ba4c4ae9e22738af3cd2\",\"currency\":\"INR\"}', '2020-02-17 18:19:30'),
('7bce05766e3485bee2459e6e85', '{\"api_id\":\"1\",\"currency\":\"INR\",\"customer_id\":\"9\",\"customer\":{\"customer_id\":\"9\",\"customer_group_id\":\"1\",\"firstname\":\"Amit\",\"lastname\":\"Chaurasia\",\"email\":\"xxammuxx@gmail.com\",\"telephone\":\"+918019773695\",\"custom_field\":{\"10\":\"\"}},\"payment_address\":{\"firstname\":\"Amit\",\"lastname\":\"Chaurasia\",\"company\":\"None\",\"address_1\":\"None\",\"address_2\":\"None\",\"postcode\":\"243\",\"city\":\"Deoria\",\"zone_id\":\"1496\",\"zone\":\"Mizoram\",\"zone_code\":\"MI\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":[]},\"payment_methods\":{\"razorpay\":{\"code\":\"razorpay\",\"title\":\"Credit Card \\/ Debit Card \\/ Net Banking (Razorpay)\",\"terms\":\"\",\"sort_order\":\"2\"},\"cod\":{\"code\":\"cod\",\"title\":\"Cash On Delivery\",\"terms\":\"\",\"sort_order\":\"5\"}},\"shipping_address\":{\"firstname\":\"Amit\",\"lastname\":\"Chaurasia\",\"company\":\"None\",\"address_1\":\"None\",\"address_2\":\"None\",\"postcode\":\"243\",\"city\":\"Deoria\",\"zone_id\":\"1496\",\"zone\":\"Mizoram\",\"zone_code\":\"MI\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":[]},\"shipping_methods\":{\"pickup\":{\"title\":\"Pickup\",\"quote\":{\"pickup\":{\"code\":\"pickup.pickup\",\"title\":\"Pickup From Store\",\"cost\":0,\"tax_class_id\":0,\"text\":\"\\u20b90.00\"}},\"sort_order\":\"\",\"error\":false},\"flat\":{\"title\":\"Flat Rate\",\"quote\":{\"flat\":{\"code\":\"flat.flat\",\"title\":\"Flat Shipping Rate\",\"cost\":\"5.00\",\"tax_class_id\":\"9\",\"text\":\"\\u20b95.00\"}},\"sort_order\":\"1\",\"error\":false}},\"payment_method\":{\"code\":\"cod\",\"title\":\"Cash On Delivery\",\"terms\":\"\",\"sort_order\":\"5\"},\"shipping_method\":{\"code\":\"flat.flat\",\"title\":\"Flat Shipping Rate\",\"cost\":\"5.00\",\"tax_class_id\":\"9\",\"text\":\"\\u20b95.00\"}}', '2020-02-29 22:52:27'),
('7c4571c76df9f68b295db42c44', '{\"api_id\":\"1\"}', '2020-02-22 18:13:04'),
('7e14f4e756f747666245f3f899', '{\"api_id\":\"1\"}', '2020-02-29 23:01:13'),
('7f3a7e007f7f0491ef8471190f', '{\"api_id\":\"1\",\"currency\":\"INR\"}', '2020-03-07 17:36:48'),
('812f02d6fd17525937cdd966d8', '{\"currency\":\"INR\"}', '2020-02-29 23:36:57'),
('84efd2d5291915f6b6b812bce4', '{\"currency\":\"INR\",\"user_id\":\"1\",\"user_token\":\"8f5cb33ddf35490a480b1ea13184d84b\"}', '2020-02-21 14:06:52'),
('864f146d6b04f1c337c48ef972', '{\"api_id\":\"1\"}', '2020-03-05 22:14:42'),
('89a533b28b2f481bd8b6d2c047', '{\"api_id\":\"1\"}', '2020-02-27 19:58:22'),
('89c2e2b03c7265ac850d506cb8', '{\"api_id\":\"1\"}', '2020-03-02 14:07:49'),
('8a8251285c69517ac7c790f530', '{\"currency\":\"INR\",\"user_id\":\"1\",\"user_token\":\"402d7cc947fa6e1095ae7cc957e23891\"}', '2020-03-10 13:20:14'),
('914decc5cabe8e8693ff107293', '{\"api_id\":\"1\"}', '2020-02-29 23:28:20'),
('94fe99ac46cce55e19bb16a453', '{\"api_id\":\"1\"}', '2020-02-29 22:53:44'),
('96cdcf0fc3ae200f3743e8c277', '{\"api_id\":\"1\",\"currency\":\"INR\",\"customer_id\":\"9\",\"customer\":{\"customer_id\":\"9\",\"customer_group_id\":\"1\",\"firstname\":\"Amit\",\"lastname\":\"Chaurasia\",\"email\":\"xxammuxx@gmail.com\",\"telephone\":\"9703465708\",\"custom_field\":{\"10\":\"\"}},\"payment_address\":{\"firstname\":\"Amit\",\"lastname\":\"Chaurasia\",\"company\":\"None\",\"address_1\":\"Balaji Delux mens Hostel (Sai Serenity)\",\"address_2\":\"Opp. Gruhasiri Pride, Gachibowli\",\"postcode\":\"500032\",\"city\":\"Hyderabad\",\"zone_id\":\"4231\",\"zone\":\"Telangana\",\"zone_code\":\"TS\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":[]},\"payment_methods\":{\"razorpay\":{\"code\":\"razorpay\",\"title\":\"Credit Card \\/ Debit Card \\/ Net Banking (Razorpay)\",\"terms\":\"\",\"sort_order\":\"2\"},\"cod\":{\"code\":\"cod\",\"title\":\"Cash On Delivery\",\"terms\":\"\",\"sort_order\":\"5\"}},\"shipping_address\":{\"firstname\":\"Amit\",\"lastname\":\"Chaurasia\",\"company\":\"None\",\"address_1\":\"Balaji Delux mens Hostel (Sai Serenity)\",\"address_2\":\"Opp. Gruhasiri Pride, Gachibowli\",\"postcode\":\"500032\",\"city\":\"Hyderabad\",\"zone_id\":\"4231\",\"zone\":\"Telangana\",\"zone_code\":\"TS\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":[]},\"shipping_methods\":{\"pickup\":{\"title\":\"Pickup\",\"quote\":{\"pickup\":{\"code\":\"pickup.pickup\",\"title\":\"Pickup From Store\",\"cost\":0,\"tax_class_id\":0,\"text\":\"\\u20b90.00\"}},\"sort_order\":\"\",\"error\":false},\"flat\":{\"title\":\"Flat Rate\",\"quote\":{\"flat\":{\"code\":\"flat.flat\",\"title\":\"Flat Shipping Rate\",\"cost\":\"5.00\",\"tax_class_id\":\"9\",\"text\":\"\\u20b95.00\"}},\"sort_order\":\"1\",\"error\":false}},\"payment_method\":{\"code\":\"razorpay\",\"title\":\"Credit Card \\/ Debit Card \\/ Net Banking (Razorpay)\",\"terms\":\"\",\"sort_order\":\"2\"},\"shipping_method\":{\"code\":\"pickup.pickup\",\"title\":\"Pickup From Store\",\"cost\":0,\"tax_class_id\":0,\"text\":\"\\u20b90.00\"}}', '2020-02-29 23:25:01'),
('9bcfdade84e587162d7db8749b', '{\"currency\":\"INR\",\"user_id\":\"1\",\"user_token\":\"991cdbaf449dafd06219485e682f4f54\"}', '2020-02-24 15:13:30'),
('9df614d1d4110f42bc165bd7c1', '{\"api_id\":\"1\"}', '2020-03-02 14:03:31'),
('a00355c04016f1b28928db6896', '{\"currency\":\"INR\",\"user_id\":\"1\",\"user_token\":\"982cdc20ff977f0e3de8671c4a5514d5\",\"redirect\":\"http:\\/\\/localhost\\/theorderapp\\/index.php?route==account\\/register\",\"customer_id\":\"7\",\"just_loggedin\":true}', '2020-03-11 19:29:25'),
('a05f783934512c30de27e17101', '{\"currency\":\"INR\",\"user_id\":\"1\",\"user_token\":\"79a704bca507c34b366411a013ba8673\"}', '2020-03-02 21:29:53'),
('a3f446ac81d226c0ce49438951', '{\"api_id\":\"1\"}', '2020-02-29 22:50:40'),
('a477b08bbb721b3bac19ae6447', '{\"api_id\":\"1\"}', '2020-02-28 17:03:50'),
('ac42f31ca7a4a6aa6ecda4d505', '{\"currency\":\"INR\"}', '2020-02-29 15:20:49'),
('b2f48adc20b9c68125051c50cd', '{\"api_id\":\"1\"}', '2020-03-05 22:14:50'),
('b356b599996f9ac6ac4d0458f9', '{\"user_id\":\"1\",\"user_token\":\"3f530934847d00899d61fe16b1bf949c\",\"currency\":\"INR\"}', '2020-02-22 15:40:17'),
('b6ff5b6436f209b599c5946a72', '{\"user_id\":\"1\",\"user_token\":\"26859574c9a2cd14cfaf63a445f8729f\",\"currency\":\"INR\"}', '2020-03-28 20:24:45'),
('b7874069c66676c33769df6f0f', '{\"currency\":\"INR\",\"user_id\":\"1\",\"user_token\":\"a453815273a5f36d743b40537b07c61f\",\"coupon\":\"3333\"}', '2020-02-26 20:37:33'),
('b82bf9dba33e19fb43d7fa7430', '{\"currency\":\"INR\",\"just_loggedin\":true,\"redirect\":\"http:\\/\\/localhost\\/theorderapp\\/index.php?route==account\\/login\",\"customer_id\":\"2\"}', '2020-03-09 17:05:26'),
('b99924596a7e40f87eb1dda54d', '{\"api_id\":\"1\"}', '2020-02-22 19:48:14'),
('bcf832f76221a58b63b6e5ccb0', '{\"api_id\":\"1\"}', '2020-03-05 22:25:46'),
('c09cabae611b6ff6f21af158da', '{\"api_id\":\"1\"}', '2020-03-11 19:28:17'),
('c0f4c5713d1084105a0fae6323', '{\"api_id\":\"1\"}', '2020-02-21 13:56:43'),
('c377ca5ad587537e8337ad3d89', '{\"api_id\":\"1\",\"currency\":\"INR\",\"customer_id\":\"1\",\"customer\":{\"customer_id\":\"1\",\"customer_group_id\":\"1\",\"firstname\":\"Amit\",\"lastname\":\"Chaurasia\",\"email\":\"xxammuxx@gmail.com\",\"telephone\":\"8019773695\",\"custom_field\":[]},\"payment_address\":{\"firstname\":\"Amit\",\"lastname\":\"Chaurasia\",\"company\":\"None\",\"address_1\":\"Balaji Delux mens Hostel (Sai Serenity)\",\"address_2\":\"Opp. Gruhasiri Pride, Gachibowli\",\"postcode\":\"500032\",\"city\":\"Hyderabad\",\"zone_id\":\"4231\",\"zone\":\"Telangana\",\"zone_code\":\"TS\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":[]},\"payment_methods\":{\"razorpay\":{\"code\":\"razorpay\",\"title\":\"Credit Card \\/ Debit Card \\/ Net Banking (Razorpay)\",\"terms\":\"\",\"sort_order\":\"2\"},\"cod\":{\"code\":\"cod\",\"title\":\"Cash On Delivery\",\"terms\":\"\",\"sort_order\":\"5\"}},\"payment_method\":{\"code\":\"razorpay\",\"title\":\"Credit Card \\/ Debit Card \\/ Net Banking (Razorpay)\",\"terms\":\"\",\"sort_order\":\"2\"},\"shipping_address\":{\"firstname\":\"Amit\",\"lastname\":\"Chaurasia\",\"company\":\"None\",\"address_1\":\"Balaji Delux mens Hostel (Sai Serenity)\",\"address_2\":\"Opp. Gruhasiri Pride, Gachibowli\",\"postcode\":\"500032\",\"city\":\"Hyderabad\",\"zone_id\":\"4231\",\"zone\":\"Telangana\",\"zone_code\":\"TS\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":[]},\"shipping_methods\":{\"flat\":{\"title\":\"Flat Rate\",\"quote\":{\"flat\":{\"code\":\"flat.flat\",\"title\":\"Flat Shipping Rate\",\"cost\":\"5.00\",\"tax_class_id\":\"9\",\"text\":\"\\u20b95.00\"}},\"sort_order\":\"1\",\"error\":false}}}', '2020-02-22 19:50:17'),
('c3b0790c5efbeb6e2b51de0a20', '{\"api_id\":\"1\"}', '2020-03-21 23:22:28'),
('c5edf0e96e220067f16af2affb', '{\"currency\":\"INR\",\"just_loggedin\":true,\"redirect\":\"http:\\/\\/localhost\\/theorderapp\\/index.php?route==account\\/login\",\"customer_id\":\"7\",\"shipping_address\":{\"address_id\":\"8\",\"firstname\":\"Abhishek\",\"lastname\":\"\",\"company\":\"\",\"address_1\":\"near vijay takij\",\"address_2\":\"amar jyoti road\",\"postcode\":\"274001\",\"city\":\"Deoria\",\"zone_id\":\"1505\",\"zone\":\"Uttar Pradesh\",\"zone_code\":\"UP\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":null},\"payment_address\":{\"address_id\":\"8\",\"firstname\":\"Abhishek\",\"lastname\":\"\",\"company\":\"\",\"address_1\":\"near vijay takij\",\"address_2\":\"amar jyoti road\",\"postcode\":\"274001\",\"city\":\"Deoria\",\"zone_id\":\"1505\",\"zone\":\"Uttar Pradesh\",\"zone_code\":\"UP\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":null},\"user_id\":\"1\",\"user_token\":\"578692e6bbd2c2a3e6d6e67930baa94f\"}', '2020-03-05 22:26:14'),
('c9004f9f01fcf7ac10f46889c5', '{\"api_id\":\"1\"}', '2020-02-27 19:58:58'),
('c9a52bd70ede1e5c266c8639d8', '{\"api_id\":\"1\"}', '2020-03-11 19:28:10'),
('ca218440a58180e51cb484d3ae', '{\"api_id\":\"1\"}', '2020-02-29 23:45:33'),
('ca8fdd14d17304024d9d6e0929', '{\"api_id\":\"1\"}', '2020-03-16 17:10:32'),
('cabc72b5ce31fcc0e1384ab850', '{\"api_id\":\"1\"}', '2020-03-07 17:36:25'),
('cec7e7b25942283480368f7780', '{\"api_id\":\"1\"}', '2020-03-01 11:43:08'),
('d4468b3559bcc9e4b2637e8616', '{\"api_id\":\"1\"}', '2020-03-01 11:44:02'),
('d44b48d6fd85d7c1cb410e45e6', '{\"api_id\":\"1\"}', '2020-02-29 22:53:14'),
('d5a3e6a4475c6090d4ec1dd192', '{\"currency\":\"INR\",\"user_id\":\"1\",\"user_token\":\"648218408783c46842b2ee98a54e47d9\",\"just_loggedin\":true,\"redirect\":\"http:\\/\\/localhost\\/theorderapp\\/index.php?route==account\\/login\",\"customer_id\":\"8\",\"payment_address\":{\"address_id\":\"2\",\"firstname\":\"Abhishek\",\"lastname\":\"Chaurasia\",\"company\":\"\",\"address_1\":\"Amar Jyoti Road, Garulpar, Deoria\",\"address_2\":\"\",\"postcode\":\"274001\",\"city\":\"Deoria\",\"zone_id\":\"1505\",\"zone\":\"Uttar Pradesh\",\"zone_code\":\"UP\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":null},\"shipping_address\":{\"address_id\":\"2\",\"firstname\":\"Abhishek\",\"lastname\":\"Chaurasia\",\"company\":\"\",\"address_1\":\"Amar Jyoti Road, Garulpar, Deoria\",\"address_2\":\"\",\"postcode\":\"274001\",\"city\":\"Deoria\",\"zone_id\":\"1505\",\"zone\":\"Uttar Pradesh\",\"zone_code\":\"UP\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":null}}', '2020-02-27 20:10:38'),
('d72d2cf2bc01a61c2ba5b41be1', '{\"api_id\":\"1\",\"currency\":\"INR\"}', '2020-03-21 23:22:28'),
('d8c75365b21df76faff1e63a21', '{\"api_id\":\"1\"}', '2020-03-07 19:48:17'),
('d959ff2f6aec85ca038d3e5d91', '{\"api_id\":\"1\"}', '2020-03-12 14:48:05'),
('dd09469fc7b969db020d643b67', '{\"api_id\":\"1\"}', '2020-02-29 23:45:39'),
('dd712b5eda9a6db61ce546103c', '{\"api_id\":\"1\"}', '2020-03-02 14:01:29'),
('de96a4fb36a549efca87f23016', '{\"api_id\":\"1\",\"currency\":\"INR\"}', '2020-03-21 23:15:00'),
('e79dbfe9c9bef949d50ce89ede', '{\"api_id\":\"1\"}', '2020-03-08 15:43:59'),
('ea09e056e79f9c66351d9d0047', '{\"user_id\":\"1\",\"user_token\":\"4ce7d4333276f8e9927bbb3fc81e41d2\",\"currency\":\"USD\"}', '2020-02-12 21:14:06'),
('eecf8e3390c55042828050233b', '{\"api_id\":\"1\"}', '2020-03-08 15:38:20'),
('ef29396fe4d19aed93e1289d4e', '{\"api_id\":\"1\"}', '2020-02-28 13:55:49'),
('efb084887ab6046f50fdebd4bb', '{\"api_id\":\"1\",\"currency\":\"INR\"}', '2020-03-01 11:43:49'),
('f333bc7af4954092daa2f9f128', '{\"api_id\":\"1\"}', '2020-03-01 13:33:38'),
('f392a78e40297cc5a5e591ac35', '{\"api_id\":\"1\",\"currency\":\"INR\"}', '2020-03-21 23:26:36'),
('f6af6f1895baf477c8e4915449', '{\"api_id\":\"1\"}', '2020-03-02 14:03:58'),
('f7f052c8664bf04da0539c768f', '{\"currency\":\"INR\",\"user_id\":\"1\",\"user_token\":\"3e9d76536c6653e6ceedca667cf116cc\",\"install\":\"3f1a319863\"}', '2020-02-19 18:58:19');

-- --------------------------------------------------------

--
-- Table structure for table `oc_setting`
--

DROP TABLE IF EXISTS `oc_setting`;
CREATE TABLE IF NOT EXISTS `oc_setting` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(128) NOT NULL,
  `key` varchar(128) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint(1) NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5407 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_setting`
--

INSERT INTO `oc_setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES
(1520, 0, 'payment_razorpay', 'payment_razorpay_max_capture_delay', '', 0),
(1517, 0, 'payment_razorpay', 'payment_razorpay_status', '1', 0),
(1518, 0, 'payment_razorpay', 'payment_razorpay_sort_order', '2', 0),
(1519, 0, 'payment_razorpay', 'payment_razorpay_payment_action', 'capture', 0),
(1514, 0, 'payment_razorpay', 'payment_razorpay_key_id', 'rzp_test_briLUfr59E87WK', 0),
(1515, 0, 'payment_razorpay', 'payment_razorpay_key_secret', 'yLJQZasIAmUHviO5xelFn1Pu', 0),
(1516, 0, 'payment_razorpay', 'payment_razorpay_order_status_id', '2', 0),
(99, 0, 'developer', 'developer_sass', '1', 0),
(100, 0, 'currency_fixer', 'currency_fixer_status', '1', 0),
(101, 0, 'payment_free_checkout', 'payment_free_checkout_status', '1', 0),
(102, 0, 'payment_free_checkout', 'payment_free_checkout_order_status_id', '1', 0),
(103, 0, 'payment_free_checkout', 'payment_free_checkout_sort_order', '1', 0),
(104, 0, 'payment_cod', 'payment_cod_sort_order', '5', 0),
(105, 0, 'payment_cod', 'payment_cod_total', '0.01', 0),
(106, 0, 'payment_cod', 'payment_cod_order_status_id', '1', 0),
(107, 0, 'payment_cod', 'payment_cod_geo_zone_id', '0', 0),
(108, 0, 'payment_cod', 'payment_cod_status', '1', 0),
(109, 0, 'shipping_flat', 'shipping_flat_sort_order', '1', 0),
(110, 0, 'shipping_flat', 'shipping_flat_status', '1', 0),
(111, 0, 'shipping_flat', 'shipping_flat_geo_zone_id', '0', 0),
(112, 0, 'shipping_flat', 'shipping_flat_tax_class_id', '9', 0),
(113, 0, 'shipping_flat', 'shipping_flat_cost', '5.00', 0),
(4048, 0, 'total_shipping', 'total_shipping_sort_order', '3', 0),
(115, 0, 'total_sub_total', 'total_sub_total_sort_order', '1', 0),
(116, 0, 'total_sub_total', 'total_sub_total_status', '1', 0),
(118, 0, 'total_total', 'total_total_sort_order', '9', 0),
(119, 0, 'total_total', 'total_total_status', '1', 0),
(121, 0, 'total_credit', 'total_credit_sort_order', '7', 0),
(122, 0, 'total_credit', 'total_credit_status', '1', 0),
(123, 0, 'total_reward', 'total_reward_sort_order', '2', 0),
(124, 0, 'total_reward', 'total_reward_status', '1', 0),
(4047, 0, 'total_shipping', 'total_shipping_status', '1', 0),
(4046, 0, 'total_shipping', 'total_shipping_estimator', '1', 0),
(127, 0, 'total_coupon', 'total_coupon_sort_order', '4', 0),
(128, 0, 'total_coupon', 'total_coupon_status', '1', 0),
(1513, 0, 'module_category', 'module_category_status', '1', 0),
(132, 0, 'module_account', 'module_account_status', '1', 0),
(133, 0, 'theme_default', 'theme_default_pagination', '15', 0),
(134, 0, 'theme_default', 'theme_default_product_description_length', '100', 0),
(135, 0, 'theme_default', 'theme_default_image_thumb_width', '228', 0),
(136, 0, 'theme_default', 'theme_default_image_thumb_height', '228', 0),
(137, 0, 'theme_default', 'theme_default_image_popup_width', '500', 0),
(138, 0, 'theme_default', 'theme_default_image_popup_height', '500', 0),
(139, 0, 'theme_default', 'theme_default_image_category_width', '80', 0),
(140, 0, 'theme_default', 'theme_default_image_category_height', '80', 0),
(141, 0, 'theme_default', 'theme_default_image_product_width', '228', 0),
(142, 0, 'theme_default', 'theme_default_image_product_height', '228', 0),
(143, 0, 'theme_default', 'theme_default_image_additional_width', '74', 0),
(144, 0, 'theme_default', 'theme_default_image_additional_height', '74', 0),
(145, 0, 'theme_default', 'theme_default_image_related_width', '200', 0),
(146, 0, 'theme_default', 'theme_default_image_related_height', '200', 0),
(147, 0, 'theme_default', 'theme_default_image_compare_width', '90', 0),
(148, 0, 'theme_default', 'theme_default_image_compare_height', '90', 0),
(149, 0, 'theme_default', 'theme_default_image_wishlist_width', '47', 0),
(150, 0, 'theme_default', 'theme_default_image_wishlist_height', '47', 0),
(151, 0, 'theme_default', 'theme_default_image_cart_height', '47', 0),
(152, 0, 'theme_default', 'theme_default_image_cart_width', '47', 0),
(153, 0, 'theme_default', 'theme_default_image_location_height', '50', 0),
(154, 0, 'theme_default', 'theme_default_image_location_width', '268', 0),
(155, 0, 'theme_default', 'theme_default_directory', 'default', 0),
(156, 0, 'theme_default', 'theme_default_status', '1', 0),
(157, 0, 'dashboard_activity', 'dashboard_activity_status', '1', 0),
(158, 0, 'dashboard_activity', 'dashboard_activity_sort_order', '7', 0),
(159, 0, 'dashboard_sale', 'dashboard_sale_status', '1', 0),
(160, 0, 'dashboard_sale', 'dashboard_sale_width', '3', 0),
(161, 0, 'dashboard_chart', 'dashboard_chart_status', '1', 0),
(162, 0, 'dashboard_chart', 'dashboard_chart_width', '6', 0),
(163, 0, 'dashboard_customer', 'dashboard_customer_status', '1', 0),
(164, 0, 'dashboard_customer', 'dashboard_customer_width', '3', 0),
(165, 0, 'dashboard_map', 'dashboard_map_status', '1', 0),
(166, 0, 'dashboard_map', 'dashboard_map_width', '6', 0),
(167, 0, 'dashboard_online', 'dashboard_online_status', '1', 0),
(168, 0, 'dashboard_online', 'dashboard_online_width', '3', 0),
(169, 0, 'dashboard_order', 'dashboard_order_sort_order', '1', 0),
(170, 0, 'dashboard_order', 'dashboard_order_status', '1', 0),
(171, 0, 'dashboard_order', 'dashboard_order_width', '3', 0),
(172, 0, 'dashboard_sale', 'dashboard_sale_sort_order', '2', 0),
(173, 0, 'dashboard_customer', 'dashboard_customer_sort_order', '3', 0),
(174, 0, 'dashboard_online', 'dashboard_online_sort_order', '4', 0),
(175, 0, 'dashboard_map', 'dashboard_map_sort_order', '5', 0),
(176, 0, 'dashboard_chart', 'dashboard_chart_sort_order', '6', 0),
(177, 0, 'dashboard_recent', 'dashboard_recent_status', '1', 0),
(178, 0, 'dashboard_recent', 'dashboard_recent_sort_order', '8', 0),
(179, 0, 'dashboard_activity', 'dashboard_activity_width', '4', 0),
(180, 0, 'dashboard_recent', 'dashboard_recent_width', '8', 0),
(181, 0, 'report_customer_activity', 'report_customer_activity_status', '1', 0),
(182, 0, 'report_customer_activity', 'report_customer_activity_sort_order', '1', 0),
(183, 0, 'report_customer_order', 'report_customer_order_status', '1', 0),
(184, 0, 'report_customer_order', 'report_customer_order_sort_order', '2', 0),
(185, 0, 'report_customer_reward', 'report_customer_reward_status', '1', 0),
(186, 0, 'report_customer_reward', 'report_customer_reward_sort_order', '3', 0),
(187, 0, 'report_customer_search', 'report_customer_search_sort_order', '3', 0),
(188, 0, 'report_customer_search', 'report_customer_search_status', '1', 0),
(189, 0, 'report_customer_transaction', 'report_customer_transaction_status', '1', 0),
(190, 0, 'report_customer_transaction', 'report_customer_transaction_status_sort_order', '4', 0),
(191, 0, 'report_sale_tax', 'report_sale_tax_status', '1', 0),
(192, 0, 'report_sale_tax', 'report_sale_tax_sort_order', '5', 0),
(193, 0, 'report_sale_shipping', 'report_sale_shipping_status', '1', 0),
(194, 0, 'report_sale_shipping', 'report_sale_shipping_sort_order', '6', 0),
(195, 0, 'report_sale_return', 'report_sale_return_status', '1', 0),
(196, 0, 'report_sale_return', 'report_sale_return_sort_order', '7', 0),
(197, 0, 'report_sale_order', 'report_sale_order_status', '1', 0),
(198, 0, 'report_sale_order', 'report_sale_order_sort_order', '8', 0),
(199, 0, 'report_sale_coupon', 'report_sale_coupon_status', '1', 0),
(200, 0, 'report_sale_coupon', 'report_sale_coupon_sort_order', '9', 0),
(201, 0, 'report_product_viewed', 'report_product_viewed_status', '1', 0),
(202, 0, 'report_product_viewed', 'report_product_viewed_sort_order', '10', 0),
(203, 0, 'report_product_purchased', 'report_product_purchased_status', '1', 0),
(204, 0, 'report_product_purchased', 'report_product_purchased_sort_order', '11', 0),
(205, 0, 'report_marketing', 'report_marketing_status', '1', 0),
(206, 0, 'report_marketing', 'report_marketing_sort_order', '12', 0),
(5397, 0, 'config', 'config_compression', '4', 0),
(5398, 0, 'config', 'config_password', '1', 0),
(5399, 0, 'config', 'config_shared', '0', 0),
(5400, 0, 'config', 'config_encryption', '3feae0bfe7cc1a5231df2acafb26ae34d7e1de6dda730ff0827c528d50fe863f5eb2ebc8dd23f4246b9d1c663538ed7daedd272cdbbe6bfad8c1b4fa2127a4d78cf777db3b84105571d9f725f45de9ff4192573b11ec2d76d225fa6b9747d92347da5f15c41bb341807f5c4d314b14fcdc1f3d6d21c6aa14ee6ac8affa387e8c5a5c031be145401097017a1a7325f4216eb71bb3f5867ef1de989368f42fc2813c3db25cda0efdc329b6a1415c75b113fedd75170395ff68161c07e74ddee230cd29a76ebb19601e78fcc414bbd85e4149792943c866dd3088c693fd16093f1e77b22dfcb74d7364339897d3dbddc342587eb139ba24a410a672c8e5e93504ba757b613579100fcda419154f3aff12fc1adc4daec3b34a4601901e9bb65a516a1db561f0b95030f65e8dfb7d1bdf44b7dc35be636514a9ea8c7d38a025419adb2782ad1e378335e78644c88726c8802195d24464ba08b16666b171621e62951eb77f0389781f300696d685dfbba26fc919c2aa2bc93122aef398d8e5f6f6ddde3829220b2f465ee98ff22594159eaa2299fbef60678910f2cde12e55efe5f12682994e8460cf94fcf2dda080616393f8fcc9de95486067cd2b080eeda70fef7e15b04ca21d3df274ed6da58f014ba83962a73c75be9254a5a9dc201e5ac824afd60ff05281f0c1abf52b51498fbf4fb57df1c61207b262c941c1006540ac755a', 0),
(5401, 0, 'config', 'config_file_max_size', '300000', 0),
(5402, 0, 'config', 'config_file_ext_allowed', 'zip\r\ntxt\r\npng\r\njpe\r\njpeg\r\njpg\r\ngif\r\nbmp\r\nico\r\ntiff\r\ntif\r\nsvg\r\nsvgz\r\nzip\r\nrar\r\nmsi\r\ncab\r\nmp3\r\nqt\r\nmov\r\npdf\r\npsd\r\nai\r\neps\r\nps\r\ndoc', 0),
(480, 1, 'config', 'config_url', 'http://localhost/theorderapp/thefishstore/', 0),
(481, 1, 'config', 'config_meta_title', 'The Fish Store', 0),
(482, 1, 'config', 'config_meta_description', '', 0),
(483, 1, 'config', 'config_meta_keyword', '', 0),
(484, 1, 'config', 'config_theme', 'default', 0),
(485, 1, 'config', 'config_layout_id', '2', 0),
(486, 1, 'config', 'config_name', 'Shopee Fish', 0),
(487, 1, 'config', 'config_owner', 'Amit', 0),
(488, 1, 'config', 'config_address', 'Amar Jyoti Road\r\nGarulpar', 0),
(489, 1, 'config', 'config_geocode', 'India', 0),
(490, 1, 'config', 'config_email', 'xxammuxx@gmail.com', 0),
(491, 1, 'config', 'config_telephone', '8019773695', 0),
(492, 1, 'config', 'config_fax', '', 0),
(493, 1, 'config', 'config_image', '', 0),
(494, 1, 'config', 'config_open', '10 AM - 8 PM IST', 0),
(495, 1, 'config', 'config_comment', 'Online Payments Only', 0),
(496, 1, 'config', 'config_country_id', '99', 0),
(497, 1, 'config', 'config_zone_id', '1505', 0),
(498, 1, 'config', 'config_language', 'en-gb', 0),
(499, 1, 'config', 'config_currency', 'INR', 0),
(500, 1, 'config', 'config_cookie_id', '0', 0),
(501, 1, 'config', 'config_gdpr_id', '0', 0),
(502, 1, 'config', 'config_tax', '0', 0),
(503, 1, 'config', 'config_tax_default', '', 0),
(504, 1, 'config', 'config_tax_customer', '', 0),
(505, 1, 'config', 'config_customer_group_id', '1', 0),
(506, 1, 'config', 'config_customer_price', '0', 0),
(507, 1, 'config', 'config_account_id', '0', 0),
(508, 1, 'config', 'config_cart_weight', '0', 0),
(509, 1, 'config', 'config_checkout_guest', '0', 0),
(510, 1, 'config', 'config_checkout_id', '0', 0),
(511, 1, 'config', 'config_order_status_id', '7', 0),
(512, 1, 'config', 'config_stock_display', '0', 0),
(513, 1, 'config', 'config_stock_checkout', '0', 0),
(514, 1, 'config', 'config_logo', '', 0),
(515, 1, 'config', 'config_icon', '', 0),
(739, 0, 'theme_grocery_theme', 'theme_grocery_theme_image_wishlist_height', '47', 0),
(738, 0, 'theme_grocery_theme', 'theme_grocery_theme_image_wishlist_width', '47', 0),
(737, 0, 'theme_grocery_theme', 'theme_grocery_theme_image_compare_height', '90', 0),
(736, 0, 'theme_grocery_theme', 'theme_grocery_theme_image_compare_width', '90', 0),
(735, 0, 'theme_grocery_theme', 'theme_grocery_theme_image_related_height', '80', 0),
(734, 0, 'theme_grocery_theme', 'theme_grocery_theme_image_related_width', '80', 0),
(733, 0, 'theme_grocery_theme', 'theme_grocery_theme_image_additional_height', '74', 0),
(732, 0, 'theme_grocery_theme', 'theme_grocery_theme_image_additional_width', '74', 0),
(731, 0, 'theme_grocery_theme', 'theme_grocery_theme_image_product_height', '228', 0),
(730, 0, 'theme_grocery_theme', 'theme_grocery_theme_image_product_width', '228', 0),
(729, 0, 'theme_grocery_theme', 'theme_grocery_theme_image_popup_height', '500', 0),
(728, 0, 'theme_grocery_theme', 'theme_grocery_theme_image_popup_width', '500', 0),
(727, 0, 'theme_grocery_theme', 'theme_grocery_theme_image_thumb_height', '228', 0),
(726, 0, 'theme_grocery_theme', 'theme_grocery_theme_image_thumb_width', '228', 0),
(725, 0, 'theme_grocery_theme', 'theme_grocery_theme_image_category_height', '80', 0),
(724, 0, 'theme_grocery_theme', 'theme_grocery_theme_image_category_width', '80', 0),
(723, 0, 'theme_grocery_theme', 'theme_grocery_theme_product_description_length', '100', 0),
(722, 0, 'theme_grocery_theme', 'theme_grocery_theme_pagination', '15', 0),
(721, 0, 'theme_grocery_theme', 'theme_grocery_theme_status', '1', 0),
(720, 0, 'theme_grocery_theme', 'theme_grocery_theme_directory', 'grocery_theme', 0),
(740, 0, 'theme_grocery_theme', 'theme_grocery_theme_image_cart_width', '47', 0),
(741, 0, 'theme_grocery_theme', 'theme_grocery_theme_image_cart_height', '47', 0),
(742, 0, 'theme_grocery_theme', 'theme_grocery_theme_image_location_width', '268', 0),
(743, 0, 'theme_grocery_theme', 'theme_grocery_theme_image_location_height', '50', 0),
(1328, 0, 'theme_mintleaf', 'theme_mintleaf_image_cart_width', '47', 0),
(1327, 0, 'theme_mintleaf', 'theme_mintleaf_image_wishlist_height', '47', 0),
(1326, 0, 'theme_mintleaf', 'theme_mintleaf_image_wishlist_width', '47', 0),
(1325, 0, 'theme_mintleaf', 'theme_mintleaf_image_compare_height', '90', 0),
(1324, 0, 'theme_mintleaf', 'theme_mintleaf_image_compare_width', '90', 0),
(1323, 0, 'theme_mintleaf', 'theme_mintleaf_image_related_height', '80', 0),
(1322, 0, 'theme_mintleaf', 'theme_mintleaf_image_related_width', '80', 0),
(1321, 0, 'theme_mintleaf', 'theme_mintleaf_image_additional_height', '74', 0),
(1320, 0, 'theme_mintleaf', 'theme_mintleaf_image_additional_width', '74', 0),
(1319, 0, 'theme_mintleaf', 'theme_mintleaf_image_product_height', '228', 0),
(1318, 0, 'theme_mintleaf', 'theme_mintleaf_image_product_width', '228', 0),
(1317, 0, 'theme_mintleaf', 'theme_mintleaf_image_popup_height', '500', 0),
(1316, 0, 'theme_mintleaf', 'theme_mintleaf_image_popup_width', '500', 0),
(1315, 0, 'theme_mintleaf', 'theme_mintleaf_image_thumb_height', '228', 0),
(1314, 0, 'theme_mintleaf', 'theme_mintleaf_image_thumb_width', '228', 0),
(1313, 0, 'theme_mintleaf', 'theme_mintleaf_image_category_height', '80', 0),
(1312, 0, 'theme_mintleaf', 'theme_mintleaf_image_category_width', '80', 0),
(1311, 0, 'theme_mintleaf', 'theme_mintleaf_product_description_length', '100', 0),
(1310, 0, 'theme_mintleaf', 'theme_mintleaf_product_limit', '15', 0),
(1309, 0, 'theme_mintleaf', 'theme_mintleaf_status', '1', 0),
(1308, 0, 'theme_mintleaf', 'theme_mintleaf_directory', 'mintleaf', 0),
(1329, 0, 'theme_mintleaf', 'theme_mintleaf_image_cart_height', '47', 0),
(1330, 0, 'theme_mintleaf', 'theme_mintleaf_image_location_width', '268', 0),
(1331, 0, 'theme_mintleaf', 'theme_mintleaf_image_location_height', '50', 0),
(1422, 0, 'theme_mintleaf', 'theme_mintleaf_pagination', '15', 0),
(1521, 0, 'payment_razorpay', 'payment_razorpay_webhook_status', '1', 0),
(1522, 0, 'payment_razorpay', 'payment_razorpay_webhook_secret', 'orderapp', 0),
(5389, 0, 'config', 'config_twitter', 'https://twitter.com', 0),
(5390, 0, 'config', 'config_instagram', 'https://instagram.com', 0),
(5391, 0, 'config', 'config_linkedin', 'https://linkedin.com', 0),
(5392, 0, 'config', 'config_youtube', 'https://youtube.com', 0),
(5393, 0, 'config', 'config_android_link', 'https://play.google.com/store/apps/details?id=in.startv.hotstar&amp;hl=en', 0),
(5394, 0, 'config', 'config_maintenance', '0', 0),
(5395, 0, 'config', 'config_seo_url', '0', 0),
(5396, 0, 'config', 'config_robots', 'abot\r\ndbot\r\nebot\r\nhbot\r\nkbot\r\nlbot\r\nmbot\r\nnbot\r\nobot\r\npbot\r\nrbot\r\nsbot\r\ntbot\r\nvbot\r\nybot\r\nzbot\r\nbot.\r\nbot/\r\n_bot\r\n.bot\r\n/bot\r\n-bot\r\n:bot\r\n(bot\r\ncrawl\r\nslurp\r\nspider\r\nseek\r\naccoona\r\nacoon\r\nadressendeutschland\r\nah-ha.com\r\nahoy\r\naltavista\r\nananzi\r\nanthill\r\nappie\r\narachnophilia\r\narale\r\naraneo\r\naranha\r\narchitext\r\naretha\r\narks\r\nasterias\r\natlocal\r\natn\r\natomz\r\naugurfind\r\nbackrub\r\nbannana_bot\r\nbaypup\r\nbdfetch\r\nbig brother\r\nbiglotron\r\nbjaaland\r\nblackwidow\r\nblaiz\r\nblog\r\nblo.\r\nbloodhound\r\nboitho\r\nbooch\r\nbradley\r\nbutterfly\r\ncalif\r\ncassandra\r\nccubee\r\ncfetch\r\ncharlotte\r\nchurl\r\ncienciaficcion\r\ncmc\r\ncollective\r\ncomagent\r\ncombine\r\ncomputingsite\r\ncsci\r\ncurl\r\ncusco\r\ndaumoa\r\ndeepindex\r\ndelorie\r\ndepspid\r\ndeweb\r\ndie blinde kuh\r\ndigger\r\nditto\r\ndmoz\r\ndocomo\r\ndownload express\r\ndtaagent\r\ndwcp\r\nebiness\r\nebingbong\r\ne-collector\r\nejupiter\r\nemacs-w3 search engine\r\nesther\r\nevliya celebi\r\nezresult\r\nfalcon\r\nfelix ide\r\nferret\r\nfetchrover\r\nfido\r\nfindlinks\r\nfireball\r\nfish search\r\nfouineur\r\nfunnelweb\r\ngazz\r\ngcreep\r\ngenieknows\r\ngetterroboplus\r\ngeturl\r\nglx\r\ngoforit\r\ngolem\r\ngrabber\r\ngrapnel\r\ngralon\r\ngriffon\r\ngromit\r\ngrub\r\ngulliver\r\nhamahakki\r\nharvest\r\nhavindex\r\nhelix\r\nheritrix\r\nhku www octopus\r\nhomerweb\r\nhtdig\r\nhtml index\r\nhtml_analyzer\r\nhtmlgobble\r\nhubater\r\nhyper-decontextualizer\r\nia_archiver\r\nibm_planetwide\r\nichiro\r\niconsurf\r\niltrovatore\r\nimage.kapsi.net\r\nimagelock\r\nincywincy\r\nindexer\r\ninfobee\r\ninformant\r\ningrid\r\ninktomisearch.com\r\ninspector web\r\nintelliagent\r\ninternet shinchakubin\r\nip3000\r\niron33\r\nisraeli-search\r\nivia\r\njack\r\njakarta\r\njavabee\r\njetbot\r\njumpstation\r\nkatipo\r\nkdd-explorer\r\nkilroy\r\nknowledge\r\nkototoi\r\nkretrieve\r\nlabelgrabber\r\nlachesis\r\nlarbin\r\nlegs\r\nlibwww\r\nlinkalarm\r\nlink validator\r\nlinkscan\r\nlockon\r\nlwp\r\nlycos\r\nmagpie\r\nmantraagent\r\nmapoftheinternet\r\nmarvin/\r\nmattie\r\nmediafox\r\nmediapartners\r\nmercator\r\nmerzscope\r\nmicrosoft url control\r\nminirank\r\nmiva\r\nmj12\r\nmnogosearch\r\nmoget\r\nmonster\r\nmoose\r\nmotor\r\nmultitext\r\nmuncher\r\nmuscatferret\r\nmwd.search\r\nmyweb\r\nnajdi\r\nnameprotect\r\nnationaldirectory\r\nnazilla\r\nncsa beta\r\nnec-meshexplorer\r\nnederland.zoek\r\nnetcarta webmap engine\r\nnetmechanic\r\nnetresearchserver\r\nnetscoop\r\nnewscan-online\r\nnhse\r\nnokia6682/\r\nnomad\r\nnoyona\r\nnutch\r\nnzexplorer\r\nobjectssearch\r\noccam\r\nomni\r\nopen text\r\nopenfind\r\nopenintelligencedata\r\norb search\r\nosis-project\r\npack rat\r\npageboy\r\npagebull\r\npage_verifier\r\npanscient\r\nparasite\r\npartnersite\r\npatric\r\npear.\r\npegasus\r\nperegrinator\r\npgp key agent\r\nphantom\r\nphpdig\r\npicosearch\r\npiltdownman\r\npimptrain\r\npinpoint\r\npioneer\r\npiranha\r\nplumtreewebaccessor\r\npogodak\r\npoirot\r\npompos\r\npoppelsdorf\r\npoppi\r\npopular iconoclast\r\npsycheclone\r\npublisher\r\npython\r\nrambler\r\nraven search\r\nroach\r\nroad runner\r\nroadhouse\r\nrobbie\r\nrobofox\r\nrobozilla\r\nrules\r\nsalty\r\nsbider\r\nscooter\r\nscoutjet\r\nscrubby\r\nsearch.\r\nsearchprocess\r\nsemanticdiscovery\r\nsenrigan\r\nsg-scout\r\nshai\'hulud\r\nshark\r\nshopwiki\r\nsidewinder\r\nsift\r\nsilk\r\nsimmany\r\nsite searcher\r\nsite valet\r\nsitetech-rover\r\nskymob.com\r\nsleek\r\nsmartwit\r\nsna-\r\nsnappy\r\nsnooper\r\nsohu\r\nspeedfind\r\nsphere\r\nsphider\r\nspinner\r\nspyder\r\nsteeler/\r\nsuke\r\nsuntek\r\nsupersnooper\r\nsurfnomore\r\nsven\r\nsygol\r\nszukacz\r\ntach black widow\r\ntarantula\r\ntempleton\r\n/teoma\r\nt-h-u-n-d-e-r-s-t-o-n-e\r\ntheophrastus\r\ntitan\r\ntitin\r\ntkwww\r\ntoutatis\r\nt-rex\r\ntutorgig\r\ntwiceler\r\ntwisted\r\nucsd\r\nudmsearch\r\nurl check\r\nupdated\r\nvagabondo\r\nvalkyrie\r\nverticrawl\r\nvictoria\r\nvision-search\r\nvolcano\r\nvoyager/\r\nvoyager-hc\r\nw3c_validator\r\nw3m2\r\nw3mir\r\nwalker\r\nwallpaper\r\nwanderer\r\nwauuu\r\nwavefire\r\nweb core\r\nweb hopper\r\nweb wombat\r\nwebbandit\r\nwebcatcher\r\nwebcopy\r\nwebfoot\r\nweblayers\r\nweblinker\r\nweblog monitor\r\nwebmirror\r\nwebmonkey\r\nwebquest\r\nwebreaper\r\nwebsitepulse\r\nwebsnarf\r\nwebstolperer\r\nwebvac\r\nwebwalk\r\nwebwatch\r\nwebwombat\r\nwebzinger\r\nwhizbang\r\nwhowhere\r\nwild ferret\r\nworldlight\r\nwwwc\r\nwwwster\r\nxenu\r\nxget\r\nxift\r\nxirq\r\nyandex\r\nyanga\r\nyeti\r\nyodao\r\nzao\r\nzippp\r\nzyborg', 0),
(5388, 0, 'config', 'config_facebook', 'https://www.facebook.com', 0),
(5387, 0, 'config', 'config_fb_secret', 'd0cefa57d5e11750639696b36042caf5', 0),
(5386, 0, 'config', 'config_fb_app_id', '338571580194503', 0),
(1902, 0, 'shipping_free', 'shipping_free_total', '1200', 0),
(1903, 0, 'shipping_free', 'shipping_free_geo_zone_id', '5', 0),
(1904, 0, 'shipping_free', 'shipping_free_status', '1', 0),
(1905, 0, 'shipping_free', 'shipping_free_sort_order', '', 0),
(1906, 0, 'shipping_pickup', 'shipping_pickup_geo_zone_id', '0', 0),
(1907, 0, 'shipping_pickup', 'shipping_pickup_status', '1', 0),
(1908, 0, 'shipping_pickup', 'shipping_pickup_sort_order', '', 0),
(1909, 0, 'captcha_basic', 'captcha_basic_status', '1', 0),
(5385, 0, 'config', 'config_google_client_secret', 'A8WLupLGq06lPf1V07R7Z24w', 0),
(5384, 0, 'config', 'config_google_client_id', '609179086711-1huj8p8ni6b6ugcde543jj463j778qda.apps.googleusercontent.com', 0),
(5383, 0, 'config', 'config_mail_alert_email', '', 0),
(2294, 0, 'module_filter', 'module_filter_status', '1', 0),
(5382, 0, 'config', 'config_mail_alert', '[\"account\",\"order\"]', 1),
(5381, 0, 'config', 'config_mail_smtp_timeout', '50', 0),
(5380, 0, 'config', 'config_mail_smtp_port', '465', 0),
(5379, 0, 'config', 'config_mail_smtp_password', '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', 0),
(5378, 0, 'config', 'config_mail_smtp_username', 'chaurasiaabhi09@gmail.com', 0),
(5377, 0, 'config', 'config_mail_smtp_hostname', 'ssl://smtp.gmail.com', 0),
(5376, 0, 'config', 'config_mail_parameter', '', 0),
(5374, 0, 'config', 'config_icon', 'catalog/store_icon.png', 0),
(5375, 0, 'config', 'config_mail_engine', 'smtp', 0),
(5373, 0, 'config', 'config_logo', 'catalog/store_logo.png', 0),
(5372, 0, 'config', 'config_captcha_page', '[\"review\",\"return\",\"contact\"]', 1),
(5371, 0, 'config', 'config_captcha', '', 0),
(5370, 0, 'config', 'config_return_status_id', '2', 0),
(5369, 0, 'config', 'config_return_id', '5', 0),
(5368, 0, 'config', 'config_affiliate_id', '4', 0),
(5367, 0, 'config', 'config_affiliate_commission', '5', 0),
(5366, 0, 'config', 'config_affiliate_auto', '0', 0),
(5365, 0, 'config', 'config_affiliate_approval', '0', 0),
(5364, 0, 'config', 'config_affiliate_group_id', '1', 0),
(5363, 0, 'config', 'config_stock_checkout', '0', 0),
(5362, 0, 'config', 'config_stock_warning', '0', 0),
(5361, 0, 'config', 'config_stock_display', '0', 0),
(5360, 0, 'config', 'config_api_id', '1', 0),
(5359, 0, 'config', 'config_fraud_status_id', '8', 0),
(3959, 2, 'config', 'config_tax_customer', '', 0),
(3958, 2, 'config', 'config_tax_default', '', 0),
(3957, 2, 'config', 'config_tax', '0', 0),
(3956, 2, 'config', 'config_gdpr_id', '0', 0),
(3955, 2, 'config', 'config_cookie_id', '0', 0),
(3954, 2, 'config', 'config_currency', 'INR', 0),
(3953, 2, 'config', 'config_language', 'en-gb', 0),
(3952, 2, 'config', 'config_zone_id', '1505', 0),
(3951, 2, 'config', 'config_country_id', '99', 0),
(3950, 2, 'config', 'config_comment', '', 0),
(3949, 2, 'config', 'config_open', '9:00 AM - 9:00 PM', 0),
(3948, 2, 'config', 'config_image', '', 0),
(3947, 2, 'config', 'config_fax', '', 0),
(3946, 2, 'config', 'config_telephone', '8019773695', 0),
(3945, 2, 'config', 'config_email', 'xxammuxx@gmail.com', 0),
(3944, 2, 'config', 'config_geocode', '', 0),
(3943, 2, 'config', 'config_address', 'Deoria', 0),
(3942, 2, 'config', 'config_owner', 'Abhishek Chaurasia', 0),
(3941, 2, 'config', 'config_name', 'Patanjali Store', 0),
(3940, 2, 'config', 'config_layout_id', '1', 0),
(3939, 2, 'config', 'config_theme', 'mintleaf', 0),
(3938, 2, 'config', 'config_meta_keyword', 'Patanjali Store', 0),
(3937, 2, 'config', 'config_meta_description', 'Patanjali Store', 0),
(3936, 2, 'config', 'config_meta_title', 'Patanjali Store', 0),
(3935, 2, 'config', 'config_url', 'http://localhost/theorderapp/patanjalistore/', 0),
(3960, 2, 'config', 'config_customer_group_id', '1', 0),
(3961, 2, 'config', 'config_customer_group_display', '[\"1\",\"2\",\"3\"]', 1),
(3962, 2, 'config', 'config_customer_price', '0', 0),
(3963, 2, 'config', 'config_account_id', '0', 0),
(3964, 2, 'config', 'config_cart_weight', '0', 0),
(3965, 2, 'config', 'config_checkout_guest', '0', 0),
(3966, 2, 'config', 'config_checkout_id', '2', 0),
(3967, 2, 'config', 'config_order_status_id', '7', 0),
(3968, 2, 'config', 'config_stock_display', '1', 0),
(3969, 2, 'config', 'config_stock_checkout', '0', 0),
(3970, 2, 'config', 'config_logo', 'catalog/gogrocery-icon.png', 0),
(3971, 2, 'config', 'config_icon', '', 0),
(4045, 0, 'module_tawkto', 'module_tawkto_widget', '{\"widget_config_0\":{\"page_id\":\"5877529ae8239e1d977414a1\",\"widget_id\":\"default\",\"user_id\":\"1\"}}', 1),
(4009, 0, 'tawkto', 'tawkto_widget', '{\"widget_config_\":{\"page_id\":\"5877529ae8239e1d977414a1\",\"widget_id\":\"default\",\"user_id\":\"1\"},\"widget_config_0\":{\"page_id\":\"5877529ae8239e1d977414a1\",\"widget_id\":\"default\",\"user_id\":\"1\"}}', 1),
(4007, 0, 'tawkto', 'tawkto_status', '1', 0),
(4008, 0, 'tawkto', 'tawkto_visibility', '{\"always_display\":false,\"hide_oncustom\":[],\"show_onfrontpage\":true,\"show_oncategory\":false,\"show_onproduct\":false,\"show_oncustom\":\"[\\\"\\\"]\"}', 0),
(4043, 0, 'module_tawkto', 'module_tawkto_status', '1', 0),
(4044, 0, 'module_tawkto', 'module_tawkto_visibility', '{\"always_display\":false,\"hide_oncustom\":[],\"show_onfrontpage\":true,\"show_oncategory\":false,\"show_onproduct\":false,\"show_oncustom\":\"[\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/theorderapp\\\\\\/index.php?route=information\\\\\\/contact&language=en-gb\\\",\\\"\\\",\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/theorderapp\\\\\\/index.php?route=account\\\\\\/order&language=en-gb\\\",\\\"\\\",\\\"\\\",\\\"\\\",\\\"\\\"]\"}', 0),
(5358, 0, 'config', 'config_complete_status', '[\"5\",\"3\"]', 1),
(5357, 0, 'config', 'config_processing_status', '[\"5\",\"1\",\"2\",\"12\",\"3\"]', 1),
(5356, 0, 'config', 'config_order_status_id', '1', 0),
(5355, 0, 'config', 'config_checkout_id', '2', 0),
(5354, 0, 'config', 'config_checkout_guest', '0', 0),
(5353, 0, 'config', 'config_cart_weight', '1', 0),
(5352, 0, 'config', 'config_invoice_prefix', 'INV-2020-00', 0),
(5351, 0, 'config', 'config_account_id', '3', 0),
(5350, 0, 'config', 'config_reward_value_onsignup', '5', 0),
(5349, 0, 'config', 'config_reward_value', '10', 0),
(5348, 0, 'config', 'config_login_attempts', '5', 0),
(5347, 0, 'config', 'config_customer_price', '0', 0),
(5346, 0, 'config', 'config_customer_group_display', '[\"1\",\"2\",\"3\"]', 1),
(5345, 0, 'config', 'config_customer_group_id', '1', 0),
(5344, 0, 'config', 'config_customer_search', '1', 0),
(5343, 0, 'config', 'config_customer_activity', '0', 0),
(5342, 0, 'config', 'config_customer_online', '1', 0),
(5341, 0, 'config', 'config_tax_customer', 'shipping', 0),
(5340, 0, 'config', 'config_tax_default', 'shipping', 0),
(5339, 0, 'config', 'config_tax', '0', 0),
(5338, 0, 'config', 'config_gdpr_limit', '180', 0),
(5337, 0, 'config', 'config_gdpr_id', '0', 0),
(5336, 0, 'config', 'config_cookie_id', '0', 0),
(5335, 0, 'config', 'config_voucher_max', '1000', 0),
(5334, 0, 'config', 'config_voucher_min', '1', 0),
(5333, 0, 'config', 'config_review_guest', '1', 0),
(5332, 0, 'config', 'config_review_status', '1', 0),
(5331, 0, 'config', 'config_pagination', '10', 0),
(5330, 0, 'config', 'config_product_count', '1', 0),
(5329, 0, 'config', 'config_weight_class_id', '1', 0),
(5328, 0, 'config', 'config_length_class_id', '1', 0),
(5327, 0, 'config', 'config_currency_auto', '1', 0),
(5326, 0, 'config', 'config_currency_engine', 'fixer', 0),
(5325, 0, 'config', 'config_currency', 'INR', 0),
(5324, 0, 'config', 'config_admin_language', 'en-gb', 0),
(5323, 0, 'config', 'config_language', 'en-gb', 0),
(5322, 0, 'config', 'config_timezone', 'Asia/Kolkata', 0),
(5321, 0, 'config', 'config_zone_id', '1505', 0),
(5320, 0, 'config', 'config_country_id', '99', 0),
(5319, 0, 'config', 'config_comment', '', 0),
(5318, 0, 'config', 'config_open', '10am-8pm', 0),
(5317, 0, 'config', 'config_image', 'catalog/g6.png', 0),
(5316, 0, 'config', 'config_fax', '', 0),
(5315, 0, 'config', 'config_telephone', '8217763387', 0),
(5314, 0, 'config', 'config_email', 'chaurasiaabhi09@gmail.com', 0),
(5313, 0, 'config', 'config_geocode', '26.500000, 83.779999', 0),
(5312, 0, 'config', 'config_address', 'Amar Jyoti Road, Garulpar, Deoria', 0),
(5311, 0, 'config', 'config_minimum_order_free_delivery', '345', 0),
(5310, 0, 'config', 'config_serviceable_radius', '4', 0),
(5309, 0, 'config', 'config_minimum_order_amount', '340', 0),
(5308, 0, 'config', 'config_owner', 'Amit', 0),
(5307, 0, 'config', 'config_name', 'KiranaKorner', 0),
(5306, 0, 'config', 'config_layout_id', '1', 0),
(5305, 0, 'config', 'config_theme', 'mintleaf', 0),
(5304, 0, 'config', 'config_meta_keyword', 'Your complete Grocery Supply', 0),
(5303, 0, 'config', 'config_meta_description', 'Grocery Supply', 0),
(5302, 0, 'config', 'config_meta_title', 'KiranaKorner', 0),
(5403, 0, 'config', 'config_file_mime_allowed', 'text/plain\r\nimage/png\r\nimage/jpeg\r\nimage/gif\r\nimage/bmp\r\nimage/tiff\r\nimage/svg+xml\r\napplication/zip\r\n&quot;application/zip&quot;\r\napplication/x-zip\r\n&quot;application/x-zip&quot;\r\napplication/x-zip-compressed\r\n&quot;application/x-zip-compressed&quot;\r\napplication/rar\r\n&quot;application/rar&quot;\r\napplication/x-rar\r\n&quot;application/x-rar&quot;\r\napplication/x-rar-compressed\r\n&quot;application/x-rar-compressed&quot;\r\napplication/octet-stream\r\n&quot;application/octet-stream&quot;\r\naudio/mpeg\r\nvideo/quicktime\r\napplication/pdf', 0),
(5404, 0, 'config', 'config_error_display', '1', 0),
(5405, 0, 'config', 'config_error_log', '1', 0),
(5406, 0, 'config', 'config_error_filename', 'theorderapp_error.log', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_shipping_courier`
--

DROP TABLE IF EXISTS `oc_shipping_courier`;
CREATE TABLE IF NOT EXISTS `oc_shipping_courier` (
  `shipping_courier_id` int(11) NOT NULL,
  `shipping_courier_code` varchar(255) NOT NULL,
  `shipping_courier_name` varchar(255) NOT NULL,
  PRIMARY KEY (`shipping_courier_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_shipping_courier`
--

INSERT INTO `oc_shipping_courier` (`shipping_courier_id`, `shipping_courier_code`, `shipping_courier_name`) VALUES
(1, 'dhl', 'DHL'),
(2, 'fedex', 'Fedex'),
(3, 'ups', 'UPS'),
(4, 'royal-mail', 'Royal Mail'),
(5, 'usps', 'United States Postal Service'),
(6, 'auspost', 'Australia Post');

-- --------------------------------------------------------

--
-- Table structure for table `oc_statistics`
--

DROP TABLE IF EXISTS `oc_statistics`;
CREATE TABLE IF NOT EXISTS `oc_statistics` (
  `statistics_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(64) NOT NULL,
  `value` decimal(15,4) NOT NULL,
  PRIMARY KEY (`statistics_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_statistics`
--

INSERT INTO `oc_statistics` (`statistics_id`, `code`, `value`) VALUES
(1, 'order_sale', '11856.6400'),
(2, 'order_processing', '19.0000'),
(3, 'order_complete', '7.0000'),
(4, 'order_other', '4.0000'),
(5, 'return', '5.0000'),
(6, 'product', '1.0000'),
(7, 'review', '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `oc_stock_status`
--

DROP TABLE IF EXISTS `oc_stock_status`;
CREATE TABLE IF NOT EXISTS `oc_stock_status` (
  `stock_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`stock_status_id`,`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_stock_status`
--

INSERT INTO `oc_stock_status` (`stock_status_id`, `language_id`, `name`) VALUES
(7, 1, 'In Stock'),
(8, 1, 'Pre-Order'),
(5, 1, 'Out Of Stock'),
(6, 1, '3-4 Days');

-- --------------------------------------------------------

--
-- Table structure for table `oc_store`
--

DROP TABLE IF EXISTS `oc_store`;
CREATE TABLE IF NOT EXISTS `oc_store` (
  `store_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `url` varchar(255) NOT NULL,
  `ssl` varchar(255) NOT NULL,
  PRIMARY KEY (`store_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_store`
--

INSERT INTO `oc_store` (`store_id`, `name`, `url`, `ssl`) VALUES
(1, 'Shopee Fish', 'http://localhost/theorderapp/thefishstore/', ''),
(2, 'Patanjali Store', 'http://localhost/theorderapp/patanjalistore/', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_class`
--

DROP TABLE IF EXISTS `oc_tax_class`;
CREATE TABLE IF NOT EXISTS `oc_tax_class` (
  `tax_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`tax_class_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_class`
--

INSERT INTO `oc_tax_class` (`tax_class_id`, `title`, `description`, `date_added`, `date_modified`) VALUES
(9, 'Taxable Goods', 'Taxed goods', '2009-01-06 23:21:53', '2011-09-23 14:07:50'),
(10, 'Downloadable Products', 'Downloadable', '2011-09-21 22:19:39', '2011-09-22 10:27:36');

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_rate`
--

DROP TABLE IF EXISTS `oc_tax_rate`;
CREATE TABLE IF NOT EXISTS `oc_tax_rate` (
  `tax_rate_id` int(11) NOT NULL AUTO_INCREMENT,
  `geo_zone_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL,
  `rate` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `type` char(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`tax_rate_id`)
) ENGINE=MyISAM AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_rate`
--

INSERT INTO `oc_tax_rate` (`tax_rate_id`, `geo_zone_id`, `name`, `rate`, `type`, `date_added`, `date_modified`) VALUES
(86, 3, 'VAT (20%)', '20.0000', 'P', '2011-03-09 21:17:10', '2011-09-22 22:24:29'),
(87, 3, 'Eco Tax (-2.00)', '2.0000', 'F', '2011-09-21 21:49:23', '2011-09-23 00:40:19');

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_rate_to_customer_group`
--

DROP TABLE IF EXISTS `oc_tax_rate_to_customer_group`;
CREATE TABLE IF NOT EXISTS `oc_tax_rate_to_customer_group` (
  `tax_rate_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  PRIMARY KEY (`tax_rate_id`,`customer_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_rate_to_customer_group`
--

INSERT INTO `oc_tax_rate_to_customer_group` (`tax_rate_id`, `customer_group_id`) VALUES
(86, 1),
(87, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_rule`
--

DROP TABLE IF EXISTS `oc_tax_rule`;
CREATE TABLE IF NOT EXISTS `oc_tax_rule` (
  `tax_rule_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_class_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `based` varchar(10) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  PRIMARY KEY (`tax_rule_id`)
) ENGINE=MyISAM AUTO_INCREMENT=129 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_rule`
--

INSERT INTO `oc_tax_rule` (`tax_rule_id`, `tax_class_id`, `tax_rate_id`, `based`, `priority`) VALUES
(121, 10, 86, 'payment', 1),
(120, 10, 87, 'store', 0),
(128, 9, 86, 'shipping', 1),
(127, 9, 87, 'shipping', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oc_theme`
--

DROP TABLE IF EXISTS `oc_theme`;
CREATE TABLE IF NOT EXISTS `oc_theme` (
  `theme_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `theme` varchar(64) NOT NULL,
  `route` varchar(64) NOT NULL,
  `code` mediumtext NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`theme_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_theme`
--

INSERT INTO `oc_theme` (`theme_id`, `store_id`, `theme`, `route`, `code`, `date_added`) VALUES
(1, 0, 'default', 'common/currency', '{#\n{% if currencies|length &gt; 1 %}\n  &lt;form action=&quot;{{ action }}&quot; method=&quot;post&quot; enctype=&quot;multipart/form-data&quot; id=&quot;form-currency&quot;&gt;\n    &lt;div class=&quot;dropdown&quot;&gt;\n      &lt;div class=&quot;dropdown-toggle&quot; data-toggle=&quot;dropdown&quot;&gt;\n        {% for currency in currencies %}\n          {% if currency.symbol_left and currency.code == code %}\n            &lt;strong&gt;{{ currency.symbol_left }}&lt;/strong&gt;\n          {% elseif currency.symbol_right and currency.code == code %}\n            &lt;strong&gt;{{ currency.symbol_right }}&lt;/strong&gt;\n          {% endif %}\n        {% endfor %}\n        &lt;span class=&quot;d-none d-md-inline&quot;&gt;{{ text_currency }}&lt;/span&gt; &lt;i class=&quot;fas fa-caret-down&quot;&gt;&lt;/i&gt;\n      &lt;/div&gt;\n      &lt;div class=&quot;dropdown-menu&quot;&gt;\n        {% for currency in currencies %}\n          {% if currency.symbol_left %}\n            &lt;a href=&quot;{{ currency.code }}&quot; class=&quot;dropdown-item&quot;&gt;{{ currency.symbol_left }} {{ currency.title }}&lt;/a&gt;\n          {% else %}\n            &lt;a href=&quot;{{ currency.code }}&quot; class=&quot;dropdown-item&quot;&gt;{{ currency.symbol_right }} {{ currency.title }}&lt;/a&gt;\n          {% endif %}\n        {% endfor %}\n      &lt;/div&gt;\n    &lt;/div&gt;\n    &lt;input type=&quot;hidden&quot; name=&quot;code&quot; value=&quot;&quot;/&gt; &lt;input type=&quot;hidden&quot; name=&quot;redirect&quot; value=&quot;{{ redirect }}&quot;/&gt;\n  &lt;/form&gt;\n{% endif %}\n#}', '2020-02-15 17:44:00');

-- --------------------------------------------------------

--
-- Table structure for table `oc_translation`
--

DROP TABLE IF EXISTS `oc_translation`;
CREATE TABLE IF NOT EXISTS `oc_translation` (
  `translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_upload`
--

DROP TABLE IF EXISTS `oc_upload`;
CREATE TABLE IF NOT EXISTS `oc_upload` (
  `upload_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`upload_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_user`
--

DROP TABLE IF EXISTS `oc_user`;
CREATE TABLE IF NOT EXISTS `oc_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_group_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `image` varchar(255) NOT NULL,
  `code` varchar(40) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_user`
--

INSERT INTO `oc_user` (`user_id`, `user_group_id`, `username`, `password`, `salt`, `firstname`, `lastname`, `email`, `image`, `code`, `ip`, `status`, `date_added`) VALUES
(1, 1, 'admin', '$2y$10$MMqsFSPdu0Q.r4RWFEYnfO.YkvuPzLT1fjWlbaP/AhDMS04PsgPFC', '', 'Abhishek', 'Chaurasia', 'chaurasiaabhi09@gmail.com', 'catalog/profile-pic.png', '', '::1', 1, '2020-02-08 10:11:41'),
(2, 11, 'amit_view_only', '$2y$10$7nt6GNc5AV9VLBop4Kx4o.pvwR2yVbiPjqhEMqMewnLM8HpI6ECBe', '', 'Amit', 'Chaurasia', 'xxammuxx@gmail.com', 'catalog/profile-pic.png', '', '::1', 1, '2020-02-15 14:25:38');

-- --------------------------------------------------------

--
-- Table structure for table `oc_user_group`
--

DROP TABLE IF EXISTS `oc_user_group`;
CREATE TABLE IF NOT EXISTS `oc_user_group` (
  `user_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `permission` text NOT NULL,
  PRIMARY KEY (`user_group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_user_group`
--

INSERT INTO `oc_user_group` (`user_group_id`, `name`, `permission`) VALUES
(1, 'Administrator', '{\"access\":[\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/option\",\"catalog\\/product\",\"catalog\\/recurring\",\"catalog\\/review\",\"common\\/column_left\",\"common\\/cron\",\"common\\/developer\",\"common\\/filemanager\",\"common\\/pagination\",\"common\\/profile\",\"common\\/security\",\"cron\\/currency\",\"cron\\/gdpr\",\"customer\\/custom_field\",\"customer\\/customer\",\"customer\\/customer_approval\",\"customer\\/customer_group\",\"customer\\/gdpr\",\"design\\/banner\",\"design\\/layout\",\"design\\/seo_regex\",\"design\\/seo_url\",\"design\\/theme\",\"design\\/translation\",\"event\\/currency\",\"event\\/debug\",\"event\\/language\",\"event\\/statistics\",\"extension\\/analytics\\/google\",\"extension\\/captcha\\/basic\",\"extension\\/captcha\\/google\",\"extension\\/currency\\/ecb\",\"extension\\/currency\\/fixer\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/chart\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/map\",\"extension\\/dashboard\\/online\",\"extension\\/dashboard\\/order\",\"extension\\/dashboard\\/recent\",\"extension\\/dashboard\\/sale\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/currency\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/menu\",\"extension\\/extension\\/module\",\"extension\\/extension\\/payment\",\"extension\\/extension\\/promotion\",\"extension\\/extension\\/report\",\"extension\\/extension\\/shipping\",\"extension\\/extension\\/theme\",\"extension\\/extension\\/total\",\"extension\\/feed\\/google_base\",\"extension\\/feed\\/google_sitemap\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/module\\/account\",\"extension\\/module\\/amazon_login\",\"extension\\/module\\/amazon_pay\",\"extension\\/module\\/banner\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/featured\",\"extension\\/module\\/filter\",\"extension\\/module\\/google_calendar\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/html\",\"extension\\/module\\/information\",\"extension\\/module\\/klarna_checkout_module\",\"extension\\/module\\/latest\",\"extension\\/module\\/laybuy_layout\",\"extension\\/module\\/pilibaba_button\",\"extension\\/module\\/pp_braintree_button\",\"extension\\/module\\/pp_button\",\"extension\\/module\\/pp_login\",\"extension\\/module\\/sagepay_direct_cards\",\"extension\\/module\\/sagepay_server_cards\",\"extension\\/module\\/slideshow\",\"extension\\/module\\/special\",\"extension\\/module\\/store\",\"extension\\/payment\\/alipay\",\"extension\\/payment\\/alipay_cross\",\"extension\\/payment\\/amazon_login_pay\",\"extension\\/payment\\/authorizenet_aim\",\"extension\\/payment\\/authorizenet_sim\",\"extension\\/payment\\/bank_transfer\",\"extension\\/payment\\/bluepay_hosted\",\"extension\\/payment\\/bluepay_redirect\",\"extension\\/payment\\/cardconnect\",\"extension\\/payment\\/cardinity\",\"extension\\/payment\\/cheque\",\"extension\\/payment\\/cod\",\"extension\\/payment\\/divido\",\"extension\\/payment\\/eway\",\"extension\\/payment\\/firstdata\",\"extension\\/payment\\/firstdata_remote\",\"extension\\/payment\\/free_checkout\",\"extension\\/payment\\/g2apay\",\"extension\\/payment\\/globalpay\",\"extension\\/payment\\/globalpay_remote\",\"extension\\/payment\\/klarna_account\",\"extension\\/payment\\/klarna_checkout\",\"extension\\/payment\\/klarna_invoice\",\"extension\\/payment\\/laybuy\",\"extension\\/payment\\/liqpay\",\"extension\\/payment\\/nochex\",\"extension\\/payment\\/paymate\",\"extension\\/payment\\/paypoint\",\"extension\\/payment\\/payza\",\"extension\\/payment\\/perpetual_payments\",\"extension\\/payment\\/pilibaba\",\"extension\\/payment\\/pp_braintree\",\"extension\\/payment\\/pp_express\",\"extension\\/payment\\/pp_payflow\",\"extension\\/payment\\/pp_payflow_iframe\",\"extension\\/payment\\/pp_pro\",\"extension\\/payment\\/pp_pro_iframe\",\"extension\\/payment\\/pp_standard\",\"extension\\/payment\\/realex\",\"extension\\/payment\\/realex_remote\",\"extension\\/payment\\/sagepay_direct\",\"extension\\/payment\\/sagepay_server\",\"extension\\/payment\\/sagepay_us\",\"extension\\/payment\\/securetrading_pp\",\"extension\\/payment\\/securetrading_ws\",\"extension\\/payment\\/skrill\",\"extension\\/payment\\/squareup\",\"extension\\/payment\\/twocheckout\",\"extension\\/payment\\/web_payment_software\",\"extension\\/payment\\/wechat_pay\",\"extension\\/payment\\/worldpay\",\"extension\\/report\\/customer_activity\",\"extension\\/report\\/customer_order\",\"extension\\/report\\/customer_reward\",\"extension\\/report\\/customer_search\",\"extension\\/report\\/customer_transaction\",\"extension\\/report\\/marketing\",\"extension\\/report\\/product_purchased\",\"extension\\/report\\/product_viewed\",\"extension\\/report\\/sale_coupon\",\"extension\\/report\\/sale_order\",\"extension\\/report\\/sale_return\",\"extension\\/report\\/sale_shipping\",\"extension\\/report\\/sale_tax\",\"extension\\/shipping\\/auspost\",\"extension\\/shipping\\/ec_ship\",\"extension\\/shipping\\/fedex\",\"extension\\/shipping\\/flat\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/item\",\"extension\\/shipping\\/parcelforce_48\",\"extension\\/shipping\\/pickup\",\"extension\\/shipping\\/royal_mail\",\"extension\\/shipping\\/ups\",\"extension\\/shipping\\/usps\",\"extension\\/shipping\\/weight\",\"extension\\/theme\\/default\",\"extension\\/total\\/coupon\",\"extension\\/total\\/credit\",\"extension\\/total\\/handling\",\"extension\\/total\\/klarna_fee\",\"extension\\/total\\/low_order_fee\",\"extension\\/total\\/reward\",\"extension\\/total\\/shipping\",\"extension\\/total\\/sub_total\",\"extension\\/total\\/tax\",\"extension\\/total\\/total\",\"extension\\/total\\/voucher\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/length_class\",\"localisation\\/location\",\"localisation\\/order_status\",\"localisation\\/return_action\",\"localisation\\/return_reason\",\"localisation\\/return_status\",\"localisation\\/stock_status\",\"localisation\\/tax_class\",\"localisation\\/tax_rate\",\"localisation\\/weight_class\",\"localisation\\/zone\",\"mail\\/affiliate\",\"mail\\/customer\",\"mail\\/forgotten\",\"mail\\/gdpr\",\"mail\\/return\",\"mail\\/reward\",\"mail\\/transaction\",\"marketing\\/affiliate\",\"marketing\\/contact\",\"marketing\\/coupon\",\"marketing\\/marketing\",\"marketplace\\/api\",\"marketplace\\/cron\",\"marketplace\\/event\",\"marketplace\\/extension\",\"marketplace\\/install\",\"marketplace\\/installer\",\"marketplace\\/marketplace\",\"marketplace\\/modification\",\"report\\/online\",\"report\\/report\",\"report\\/statistics\",\"sale\\/order\",\"sale\\/recurring\",\"sale\\/return\",\"sale\\/voucher\",\"sale\\/voucher_theme\",\"setting\\/setting\",\"setting\\/store\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"tool\\/backup\",\"tool\\/log\",\"tool\\/upgrade\",\"tool\\/upload\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\",\"extension\\/theme\\/grocery_theme\",\"extension\\/theme\\/mintleaf\",\"extension\\/theme\\/mintleaf\",\"extension\\/module\\/topcategory\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/latest\",\"extension\\/module\\/latest\",\"extension\\/module\\/store\",\"extension\\/module\\/special\",\"extension\\/module\\/bannergrid\",\"extension\\/payment\\/razorpay\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/pickup\",\"extension\\/captcha\\/basic\",\"extension\\/module\\/filter\",\"extension\\/module\\/filter\",\"extension\\/module\\/filter\",\"extension\\/module\\/filter\",\"extension\\/fraud\\/ip\",\"extension\\/module\\/html\",\"extension\\/module\\/tawkto\",\"extension\\/module\\/open_sendgrid\"],\"modify\":[\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/option\",\"catalog\\/product\",\"catalog\\/recurring\",\"catalog\\/review\",\"common\\/column_left\",\"common\\/cron\",\"common\\/developer\",\"common\\/filemanager\",\"common\\/pagination\",\"common\\/profile\",\"common\\/security\",\"cron\\/currency\",\"cron\\/gdpr\",\"customer\\/custom_field\",\"customer\\/customer\",\"customer\\/customer_approval\",\"customer\\/customer_group\",\"customer\\/gdpr\",\"design\\/banner\",\"design\\/layout\",\"design\\/seo_regex\",\"design\\/seo_url\",\"design\\/theme\",\"design\\/translation\",\"event\\/currency\",\"event\\/debug\",\"event\\/language\",\"event\\/statistics\",\"extension\\/analytics\\/google\",\"extension\\/captcha\\/basic\",\"extension\\/captcha\\/google\",\"extension\\/currency\\/ecb\",\"extension\\/currency\\/fixer\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/chart\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/map\",\"extension\\/dashboard\\/online\",\"extension\\/dashboard\\/order\",\"extension\\/dashboard\\/recent\",\"extension\\/dashboard\\/sale\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/currency\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/menu\",\"extension\\/extension\\/module\",\"extension\\/extension\\/payment\",\"extension\\/extension\\/promotion\",\"extension\\/extension\\/report\",\"extension\\/extension\\/shipping\",\"extension\\/extension\\/theme\",\"extension\\/extension\\/total\",\"extension\\/feed\\/google_base\",\"extension\\/feed\\/google_sitemap\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/module\\/account\",\"extension\\/module\\/amazon_login\",\"extension\\/module\\/amazon_pay\",\"extension\\/module\\/banner\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/featured\",\"extension\\/module\\/filter\",\"extension\\/module\\/google_calendar\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/html\",\"extension\\/module\\/information\",\"extension\\/module\\/klarna_checkout_module\",\"extension\\/module\\/latest\",\"extension\\/module\\/laybuy_layout\",\"extension\\/module\\/pilibaba_button\",\"extension\\/module\\/pp_braintree_button\",\"extension\\/module\\/pp_button\",\"extension\\/module\\/pp_login\",\"extension\\/module\\/sagepay_direct_cards\",\"extension\\/module\\/sagepay_server_cards\",\"extension\\/module\\/slideshow\",\"extension\\/module\\/special\",\"extension\\/module\\/store\",\"extension\\/payment\\/alipay\",\"extension\\/payment\\/alipay_cross\",\"extension\\/payment\\/amazon_login_pay\",\"extension\\/payment\\/authorizenet_aim\",\"extension\\/payment\\/authorizenet_sim\",\"extension\\/payment\\/bank_transfer\",\"extension\\/payment\\/bluepay_hosted\",\"extension\\/payment\\/bluepay_redirect\",\"extension\\/payment\\/cardconnect\",\"extension\\/payment\\/cardinity\",\"extension\\/payment\\/cheque\",\"extension\\/payment\\/cod\",\"extension\\/payment\\/divido\",\"extension\\/payment\\/eway\",\"extension\\/payment\\/firstdata\",\"extension\\/payment\\/firstdata_remote\",\"extension\\/payment\\/free_checkout\",\"extension\\/payment\\/g2apay\",\"extension\\/payment\\/globalpay\",\"extension\\/payment\\/globalpay_remote\",\"extension\\/payment\\/klarna_account\",\"extension\\/payment\\/klarna_checkout\",\"extension\\/payment\\/klarna_invoice\",\"extension\\/payment\\/laybuy\",\"extension\\/payment\\/liqpay\",\"extension\\/payment\\/nochex\",\"extension\\/payment\\/paymate\",\"extension\\/payment\\/paypoint\",\"extension\\/payment\\/payza\",\"extension\\/payment\\/perpetual_payments\",\"extension\\/payment\\/pilibaba\",\"extension\\/payment\\/pp_braintree\",\"extension\\/payment\\/pp_express\",\"extension\\/payment\\/pp_payflow\",\"extension\\/payment\\/pp_payflow_iframe\",\"extension\\/payment\\/pp_pro\",\"extension\\/payment\\/pp_pro_iframe\",\"extension\\/payment\\/pp_standard\",\"extension\\/payment\\/realex\",\"extension\\/payment\\/realex_remote\",\"extension\\/payment\\/sagepay_direct\",\"extension\\/payment\\/sagepay_server\",\"extension\\/payment\\/sagepay_us\",\"extension\\/payment\\/securetrading_pp\",\"extension\\/payment\\/securetrading_ws\",\"extension\\/payment\\/skrill\",\"extension\\/payment\\/squareup\",\"extension\\/payment\\/twocheckout\",\"extension\\/payment\\/web_payment_software\",\"extension\\/payment\\/wechat_pay\",\"extension\\/payment\\/worldpay\",\"extension\\/report\\/customer_activity\",\"extension\\/report\\/customer_order\",\"extension\\/report\\/customer_reward\",\"extension\\/report\\/customer_search\",\"extension\\/report\\/customer_transaction\",\"extension\\/report\\/marketing\",\"extension\\/report\\/product_purchased\",\"extension\\/report\\/product_viewed\",\"extension\\/report\\/sale_coupon\",\"extension\\/report\\/sale_order\",\"extension\\/report\\/sale_return\",\"extension\\/report\\/sale_shipping\",\"extension\\/report\\/sale_tax\",\"extension\\/shipping\\/auspost\",\"extension\\/shipping\\/ec_ship\",\"extension\\/shipping\\/fedex\",\"extension\\/shipping\\/flat\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/item\",\"extension\\/shipping\\/parcelforce_48\",\"extension\\/shipping\\/pickup\",\"extension\\/shipping\\/royal_mail\",\"extension\\/shipping\\/ups\",\"extension\\/shipping\\/usps\",\"extension\\/shipping\\/weight\",\"extension\\/theme\\/default\",\"extension\\/total\\/coupon\",\"extension\\/total\\/credit\",\"extension\\/total\\/handling\",\"extension\\/total\\/klarna_fee\",\"extension\\/total\\/low_order_fee\",\"extension\\/total\\/reward\",\"extension\\/total\\/shipping\",\"extension\\/total\\/sub_total\",\"extension\\/total\\/tax\",\"extension\\/total\\/total\",\"extension\\/total\\/voucher\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/length_class\",\"localisation\\/location\",\"localisation\\/order_status\",\"localisation\\/return_action\",\"localisation\\/return_reason\",\"localisation\\/return_status\",\"localisation\\/stock_status\",\"localisation\\/tax_class\",\"localisation\\/tax_rate\",\"localisation\\/weight_class\",\"localisation\\/zone\",\"mail\\/affiliate\",\"mail\\/customer\",\"mail\\/forgotten\",\"mail\\/gdpr\",\"mail\\/return\",\"mail\\/reward\",\"mail\\/transaction\",\"marketing\\/affiliate\",\"marketing\\/contact\",\"marketing\\/coupon\",\"marketing\\/marketing\",\"marketplace\\/api\",\"marketplace\\/cron\",\"marketplace\\/event\",\"marketplace\\/extension\",\"marketplace\\/install\",\"marketplace\\/installer\",\"marketplace\\/marketplace\",\"marketplace\\/modification\",\"report\\/online\",\"report\\/report\",\"report\\/statistics\",\"sale\\/order\",\"sale\\/recurring\",\"sale\\/return\",\"sale\\/voucher\",\"sale\\/voucher_theme\",\"setting\\/setting\",\"setting\\/store\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"tool\\/backup\",\"tool\\/log\",\"tool\\/upgrade\",\"tool\\/upload\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\",\"extension\\/theme\\/grocery_theme\",\"extension\\/theme\\/mintleaf\",\"extension\\/theme\\/mintleaf\",\"extension\\/module\\/topcategory\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/latest\",\"extension\\/module\\/latest\",\"extension\\/module\\/store\",\"extension\\/module\\/special\",\"extension\\/module\\/bannergrid\",\"extension\\/payment\\/razorpay\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/pickup\",\"extension\\/captcha\\/basic\",\"extension\\/module\\/filter\",\"extension\\/module\\/filter\",\"extension\\/module\\/filter\",\"extension\\/module\\/filter\",\"extension\\/fraud\\/ip\",\"extension\\/module\\/html\",\"extension\\/module\\/tawkto\",\"extension\\/module\\/open_sendgrid\"]}'),
(10, 'Demonstration', ''),
(11, 'View Rights Only', '{\"access\":[\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/option\",\"catalog\\/product\",\"catalog\\/recurring\",\"catalog\\/review\",\"common\\/column_left\",\"common\\/cron\",\"common\\/developer\",\"common\\/filemanager\",\"common\\/pagination\",\"common\\/profile\",\"common\\/security\",\"cron\\/currency\",\"cron\\/gdpr\",\"customer\\/custom_field\",\"customer\\/customer\",\"customer\\/customer_approval\",\"customer\\/customer_group\",\"customer\\/gdpr\",\"design\\/banner\",\"design\\/layout\",\"design\\/seo_regex\",\"design\\/seo_url\",\"design\\/theme\",\"design\\/translation\",\"event\\/currency\",\"event\\/debug\",\"event\\/language\",\"event\\/statistics\",\"extension\\/captcha\\/basic\",\"extension\\/currency\\/ecb\",\"extension\\/currency\\/fixer\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/chart\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/map\",\"extension\\/dashboard\\/online\",\"extension\\/dashboard\\/order\",\"extension\\/dashboard\\/recent\",\"extension\\/dashboard\\/sale\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/currency\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/module\",\"extension\\/extension\\/payment\",\"extension\\/extension\\/promotion\",\"extension\\/extension\\/report\",\"extension\\/extension\\/shipping\",\"extension\\/extension\\/theme\",\"extension\\/extension\\/total\",\"extension\\/fraud\\/ip\",\"extension\\/module\\/account\",\"extension\\/module\\/banner\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/featured\",\"extension\\/module\\/filter\",\"extension\\/module\\/html\",\"extension\\/module\\/information\",\"extension\\/module\\/latest\",\"extension\\/module\\/slideshow\",\"extension\\/module\\/special\",\"extension\\/module\\/store\",\"extension\\/payment\\/bank_transfer\",\"extension\\/payment\\/cheque\",\"extension\\/payment\\/cod\",\"extension\\/payment\\/free_checkout\",\"extension\\/report\\/customer_activity\",\"extension\\/report\\/customer_order\",\"extension\\/report\\/customer_reward\",\"extension\\/report\\/customer_search\",\"extension\\/report\\/customer_transaction\",\"extension\\/report\\/marketing\",\"extension\\/report\\/product_purchased\",\"extension\\/report\\/product_viewed\",\"extension\\/report\\/sale_coupon\",\"extension\\/report\\/sale_order\",\"extension\\/report\\/sale_return\",\"extension\\/report\\/sale_shipping\",\"extension\\/report\\/sale_tax\",\"extension\\/shipping\\/flat\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/item\",\"extension\\/shipping\\/pickup\",\"extension\\/shipping\\/weight\",\"extension\\/theme\\/default\",\"extension\\/total\\/coupon\",\"extension\\/total\\/credit\",\"extension\\/total\\/handling\",\"extension\\/total\\/low_order_fee\",\"extension\\/total\\/reward\",\"extension\\/total\\/shipping\",\"extension\\/total\\/sub_total\",\"extension\\/total\\/tax\",\"extension\\/total\\/total\",\"extension\\/total\\/voucher\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/length_class\",\"localisation\\/location\",\"localisation\\/order_status\",\"localisation\\/return_action\",\"localisation\\/return_reason\",\"localisation\\/return_status\",\"localisation\\/stock_status\",\"localisation\\/tax_class\",\"localisation\\/tax_rate\",\"localisation\\/weight_class\",\"localisation\\/zone\",\"mail\\/affiliate\",\"mail\\/customer\",\"mail\\/forgotten\",\"mail\\/gdpr\",\"mail\\/return\",\"mail\\/reward\",\"mail\\/transaction\",\"marketing\\/affiliate\",\"marketing\\/contact\",\"marketing\\/coupon\",\"marketing\\/marketing\",\"marketplace\\/api\",\"marketplace\\/cron\",\"marketplace\\/event\",\"marketplace\\/extension\",\"marketplace\\/install\",\"marketplace\\/installer\",\"marketplace\\/marketplace\",\"marketplace\\/modification\",\"report\\/online\",\"report\\/report\",\"report\\/statistics\",\"sale\\/order\",\"sale\\/recurring\",\"sale\\/return\",\"sale\\/voucher\",\"sale\\/voucher_theme\",\"setting\\/setting\",\"setting\\/store\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"tool\\/backup\",\"tool\\/log\",\"tool\\/upgrade\",\"tool\\/upload\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\"]}');

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher`
--

DROP TABLE IF EXISTS `oc_voucher`;
CREATE TABLE IF NOT EXISTS `oc_voucher` (
  `voucher_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`voucher_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher_history`
--

DROP TABLE IF EXISTS `oc_voucher_history`;
CREATE TABLE IF NOT EXISTS `oc_voucher_history` (
  `voucher_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`voucher_history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher_theme`
--

DROP TABLE IF EXISTS `oc_voucher_theme`;
CREATE TABLE IF NOT EXISTS `oc_voucher_theme` (
  `voucher_theme_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`voucher_theme_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_voucher_theme`
--

INSERT INTO `oc_voucher_theme` (`voucher_theme_id`, `image`) VALUES
(8, 'catalog/demo/canon_eos_5d_2.jpg'),
(7, 'catalog/demo/gift-voucher-birthday.jpg'),
(6, 'catalog/demo/apple_logo.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher_theme_description`
--

DROP TABLE IF EXISTS `oc_voucher_theme_description`;
CREATE TABLE IF NOT EXISTS `oc_voucher_theme_description` (
  `voucher_theme_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`voucher_theme_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_voucher_theme_description`
--

INSERT INTO `oc_voucher_theme_description` (`voucher_theme_id`, `language_id`, `name`) VALUES
(6, 1, 'Christmas'),
(7, 1, 'Birthday'),
(8, 1, 'General');

-- --------------------------------------------------------

--
-- Table structure for table `oc_weight_class`
--

DROP TABLE IF EXISTS `oc_weight_class`;
CREATE TABLE IF NOT EXISTS `oc_weight_class` (
  `weight_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `value` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  PRIMARY KEY (`weight_class_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_weight_class`
--

INSERT INTO `oc_weight_class` (`weight_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '1000.00000000'),
(5, '2.20460000'),
(6, '35.27400000');

-- --------------------------------------------------------

--
-- Table structure for table `oc_weight_class_description`
--

DROP TABLE IF EXISTS `oc_weight_class_description`;
CREATE TABLE IF NOT EXISTS `oc_weight_class_description` (
  `weight_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL,
  PRIMARY KEY (`weight_class_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_weight_class_description`
--

INSERT INTO `oc_weight_class_description` (`weight_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Kilogram', 'kg'),
(2, 1, 'Gram', 'g'),
(5, 1, 'Pound ', 'lb'),
(6, 1, 'Ounce', 'oz');

-- --------------------------------------------------------

--
-- Table structure for table `oc_zone`
--

DROP TABLE IF EXISTS `oc_zone`;
CREATE TABLE IF NOT EXISTS `oc_zone` (
  `zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`zone_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4239 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_zone`
--

INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1, 1, 'Badakhshan', 'BDS', 1),
(2, 1, 'Badghis', 'BDG', 1),
(3, 1, 'Baghlan', 'BGL', 1),
(4, 1, 'Balkh', 'BAL', 1),
(5, 1, 'Bamian', 'BAM', 1),
(6, 1, 'Farah', 'FRA', 1),
(7, 1, 'Faryab', 'FYB', 1),
(8, 1, 'Ghazni', 'GHA', 1),
(9, 1, 'Ghowr', 'GHO', 1),
(10, 1, 'Helmand', 'HEL', 1),
(11, 1, 'Herat', 'HER', 1),
(12, 1, 'Jowzjan', 'JOW', 1),
(13, 1, 'Kabul', 'KAB', 1),
(14, 1, 'Kandahar', 'KAN', 1),
(15, 1, 'Kapisa', 'KAP', 1),
(16, 1, 'Khost', 'KHO', 1),
(17, 1, 'Konar', 'KNR', 1),
(18, 1, 'Kondoz', 'KDZ', 1),
(19, 1, 'Laghman', 'LAG', 1),
(20, 1, 'Lowgar', 'LOW', 1),
(21, 1, 'Nangrahar', 'NAN', 1),
(22, 1, 'Nimruz', 'NIM', 1),
(23, 1, 'Nurestan', 'NUR', 1),
(24, 1, 'Oruzgan', 'ORU', 1),
(25, 1, 'Paktia', 'PIA', 1),
(26, 1, 'Paktika', 'PKA', 1),
(27, 1, 'Parwan', 'PAR', 1),
(28, 1, 'Samangan', 'SAM', 1),
(29, 1, 'Sar-e Pol', 'SAR', 1),
(30, 1, 'Takhar', 'TAK', 1),
(31, 1, 'Wardak', 'WAR', 1),
(32, 1, 'Zabol', 'ZAB', 1),
(33, 2, 'Berat', 'BR', 1),
(34, 2, 'Bulqize', 'BU', 1),
(35, 2, 'Delvine', 'DL', 1),
(36, 2, 'Devoll', 'DV', 1),
(37, 2, 'Diber', 'DI', 1),
(38, 2, 'Durres', 'DR', 1),
(39, 2, 'Elbasan', 'EL', 1),
(40, 2, 'Kolonje', 'ER', 1),
(41, 2, 'Fier', 'FR', 1),
(42, 2, 'Gjirokaster', 'GJ', 1),
(43, 2, 'Gramsh', 'GR', 1),
(44, 2, 'Has', 'HA', 1),
(45, 2, 'Kavaje', 'KA', 1),
(46, 2, 'Kurbin', 'KB', 1),
(47, 2, 'Kucove', 'KC', 1),
(48, 2, 'Korce', 'KO', 1),
(49, 2, 'Kruje', 'KR', 1),
(50, 2, 'Kukes', 'KU', 1),
(51, 2, 'Librazhd', 'LB', 1),
(52, 2, 'Lezhe', 'LE', 1),
(53, 2, 'Lushnje', 'LU', 1),
(54, 2, 'Malesi e Madhe', 'MM', 1),
(55, 2, 'Mallakaster', 'MK', 1),
(56, 2, 'Mat', 'MT', 1),
(57, 2, 'Mirdite', 'MR', 1),
(58, 2, 'Peqin', 'PQ', 1),
(59, 2, 'Permet', 'PR', 1),
(60, 2, 'Pogradec', 'PG', 1),
(61, 2, 'Puke', 'PU', 1),
(62, 2, 'Shkoder', 'SH', 1),
(63, 2, 'Skrapar', 'SK', 1),
(64, 2, 'Sarande', 'SR', 1),
(65, 2, 'Tepelene', 'TE', 1),
(66, 2, 'Tropoje', 'TP', 1),
(67, 2, 'Tirane', 'TR', 1),
(68, 2, 'Vlore', 'VL', 1),
(69, 3, 'Adrar', 'ADR', 1),
(70, 3, 'Ain Defla', 'ADE', 1),
(71, 3, 'Ain Temouchent', 'ATE', 1),
(72, 3, 'Alger', 'ALG', 1),
(73, 3, 'Annaba', 'ANN', 1),
(74, 3, 'Batna', 'BAT', 1),
(75, 3, 'Bechar', 'BEC', 1),
(76, 3, 'Bejaia', 'BEJ', 1),
(77, 3, 'Biskra', 'BIS', 1),
(78, 3, 'Blida', 'BLI', 1),
(79, 3, 'Bordj Bou Arreridj', 'BBA', 1),
(80, 3, 'Bouira', 'BOA', 1),
(81, 3, 'Boumerdes', 'BMD', 1),
(82, 3, 'Chlef', 'CHL', 1),
(83, 3, 'Constantine', 'CON', 1),
(84, 3, 'Djelfa', 'DJE', 1),
(85, 3, 'El Bayadh', 'EBA', 1),
(86, 3, 'El Oued', 'EOU', 1),
(87, 3, 'El Tarf', 'ETA', 1),
(88, 3, 'Ghardaia', 'GHA', 1),
(89, 3, 'Guelma', 'GUE', 1),
(90, 3, 'Illizi', 'ILL', 1),
(91, 3, 'Jijel', 'JIJ', 1),
(92, 3, 'Khenchela', 'KHE', 1),
(93, 3, 'Laghouat', 'LAG', 1),
(94, 3, 'Muaskar', 'MUA', 1),
(95, 3, 'Medea', 'MED', 1),
(96, 3, 'Mila', 'MIL', 1),
(97, 3, 'Mostaganem', 'MOS', 1),
(98, 3, 'M\'Sila', 'MSI', 1),
(99, 3, 'Naama', 'NAA', 1),
(100, 3, 'Oran', 'ORA', 1),
(101, 3, 'Ouargla', 'OUA', 1),
(102, 3, 'Oum el-Bouaghi', 'OEB', 1),
(103, 3, 'Relizane', 'REL', 1),
(104, 3, 'Saida', 'SAI', 1),
(105, 3, 'Setif', 'SET', 1),
(106, 3, 'Sidi Bel Abbes', 'SBA', 1),
(107, 3, 'Skikda', 'SKI', 1),
(108, 3, 'Souk Ahras', 'SAH', 1),
(109, 3, 'Tamanghasset', 'TAM', 1),
(110, 3, 'Tebessa', 'TEB', 1),
(111, 3, 'Tiaret', 'TIA', 1),
(112, 3, 'Tindouf', 'TIN', 1),
(113, 3, 'Tipaza', 'TIP', 1),
(114, 3, 'Tissemsilt', 'TIS', 1),
(115, 3, 'Tizi Ouzou', 'TOU', 1),
(116, 3, 'Tlemcen', 'TLE', 1),
(117, 4, 'Eastern', 'E', 1),
(118, 4, 'Manu\'a', 'M', 1),
(119, 4, 'Rose Island', 'R', 1),
(120, 4, 'Swains Island', 'S', 1),
(121, 4, 'Western', 'W', 1),
(122, 5, 'Andorra la Vella', 'ALV', 1),
(123, 5, 'Canillo', 'CAN', 1),
(124, 5, 'Encamp', 'ENC', 1),
(125, 5, 'Escaldes-Engordany', 'ESE', 1),
(126, 5, 'La Massana', 'LMA', 1),
(127, 5, 'Ordino', 'ORD', 1),
(128, 5, 'Sant Julia de Loria', 'SJL', 1),
(129, 6, 'Bengo', 'BGO', 1),
(130, 6, 'Benguela', 'BGU', 1),
(131, 6, 'Bie', 'BIE', 1),
(132, 6, 'Cabinda', 'CAB', 1),
(133, 6, 'Cuando-Cubango', 'CCU', 1),
(134, 6, 'Cuanza Norte', 'CNO', 1),
(135, 6, 'Cuanza Sul', 'CUS', 1),
(136, 6, 'Cunene', 'CNN', 1),
(137, 6, 'Huambo', 'HUA', 1),
(138, 6, 'Huila', 'HUI', 1),
(139, 6, 'Luanda', 'LUA', 1),
(140, 6, 'Lunda Norte', 'LNO', 1),
(141, 6, 'Lunda Sul', 'LSU', 1),
(142, 6, 'Malange', 'MAL', 1),
(143, 6, 'Moxico', 'MOX', 1),
(144, 6, 'Namibe', 'NAM', 1),
(145, 6, 'Uige', 'UIG', 1),
(146, 6, 'Zaire', 'ZAI', 1),
(147, 9, 'Saint George', 'ASG', 1),
(148, 9, 'Saint John', 'ASJ', 1),
(149, 9, 'Saint Mary', 'ASM', 1),
(150, 9, 'Saint Paul', 'ASL', 1),
(151, 9, 'Saint Peter', 'ASR', 1),
(152, 9, 'Saint Philip', 'ASH', 1),
(153, 9, 'Barbuda', 'BAR', 1),
(154, 9, 'Redonda', 'RED', 1),
(155, 10, 'Antartida e Islas del Atlantico', 'AN', 1),
(156, 10, 'Buenos Aires', 'BA', 1),
(157, 10, 'Catamarca', 'CA', 1),
(158, 10, 'Chaco', 'CH', 1),
(159, 10, 'Chubut', 'CU', 1),
(160, 10, 'Cordoba', 'CO', 1),
(161, 10, 'Corrientes', 'CR', 1),
(162, 10, 'Distrito Federal', 'DF', 1),
(163, 10, 'Entre Rios', 'ER', 1),
(164, 10, 'Formosa', 'FO', 1),
(165, 10, 'Jujuy', 'JU', 1),
(166, 10, 'La Pampa', 'LP', 1),
(167, 10, 'La Rioja', 'LR', 1),
(168, 10, 'Mendoza', 'ME', 1),
(169, 10, 'Misiones', 'MI', 1),
(170, 10, 'Neuquen', 'NE', 1),
(171, 10, 'Rio Negro', 'RN', 1),
(172, 10, 'Salta', 'SA', 1),
(173, 10, 'San Juan', 'SJ', 1),
(174, 10, 'San Luis', 'SL', 1),
(175, 10, 'Santa Cruz', 'SC', 1),
(176, 10, 'Santa Fe', 'SF', 1),
(177, 10, 'Santiago del Estero', 'SD', 1),
(178, 10, 'Tierra del Fuego', 'TF', 1),
(179, 10, 'Tucuman', 'TU', 1),
(180, 11, 'Aragatsotn', 'AGT', 1),
(181, 11, 'Ararat', 'ARR', 1),
(182, 11, 'Armavir', 'ARM', 1),
(183, 11, 'Geghark\'unik\'', 'GEG', 1),
(184, 11, 'Kotayk\'', 'KOT', 1),
(185, 11, 'Lorri', 'LOR', 1),
(186, 11, 'Shirak', 'SHI', 1),
(187, 11, 'Syunik\'', 'SYU', 1),
(188, 11, 'Tavush', 'TAV', 1),
(189, 11, 'Vayots\' Dzor', 'VAY', 1),
(190, 11, 'Yerevan', 'YER', 1),
(191, 13, 'Australian Capital Territory', 'ACT', 1),
(192, 13, 'New South Wales', 'NSW', 1),
(193, 13, 'Northern Territory', 'NT', 1),
(194, 13, 'Queensland', 'QLD', 1),
(195, 13, 'South Australia', 'SA', 1),
(196, 13, 'Tasmania', 'TAS', 1),
(197, 13, 'Victoria', 'VIC', 1),
(198, 13, 'Western Australia', 'WA', 1),
(199, 14, 'Burgenland', 'BUR', 1),
(200, 14, 'Kärnten', 'KAR', 1),
(201, 14, 'Niederösterreich', 'NOS', 1),
(202, 14, 'Oberösterreich', 'OOS', 1),
(203, 14, 'Salzburg', 'SAL', 1),
(204, 14, 'Steiermark', 'STE', 1),
(205, 14, 'Tirol', 'TIR', 1),
(206, 14, 'Vorarlberg', 'VOR', 1),
(207, 14, 'Wien', 'WIE', 1),
(208, 15, 'Ali Bayramli', 'AB', 1),
(209, 15, 'Abseron', 'ABS', 1),
(210, 15, 'AgcabAdi', 'AGC', 1),
(211, 15, 'Agdam', 'AGM', 1),
(212, 15, 'Agdas', 'AGS', 1),
(213, 15, 'Agstafa', 'AGA', 1),
(214, 15, 'Agsu', 'AGU', 1),
(215, 15, 'Astara', 'AST', 1),
(216, 15, 'Baki', 'BA', 1),
(217, 15, 'BabAk', 'BAB', 1),
(218, 15, 'BalakAn', 'BAL', 1),
(219, 15, 'BArdA', 'BAR', 1),
(220, 15, 'Beylaqan', 'BEY', 1),
(221, 15, 'Bilasuvar', 'BIL', 1),
(222, 15, 'Cabrayil', 'CAB', 1),
(223, 15, 'Calilabab', 'CAL', 1),
(224, 15, 'Culfa', 'CUL', 1),
(225, 15, 'Daskasan', 'DAS', 1),
(226, 15, 'Davaci', 'DAV', 1),
(227, 15, 'Fuzuli', 'FUZ', 1),
(228, 15, 'Ganca', 'GA', 1),
(229, 15, 'Gadabay', 'GAD', 1),
(230, 15, 'Goranboy', 'GOR', 1),
(231, 15, 'Goycay', 'GOY', 1),
(232, 15, 'Haciqabul', 'HAC', 1),
(233, 15, 'Imisli', 'IMI', 1),
(234, 15, 'Ismayilli', 'ISM', 1),
(235, 15, 'Kalbacar', 'KAL', 1),
(236, 15, 'Kurdamir', 'KUR', 1),
(237, 15, 'Lankaran', 'LA', 1),
(238, 15, 'Lacin', 'LAC', 1),
(239, 15, 'Lankaran', 'LAN', 1),
(240, 15, 'Lerik', 'LER', 1),
(241, 15, 'Masalli', 'MAS', 1),
(242, 15, 'Mingacevir', 'MI', 1),
(243, 15, 'Naftalan', 'NA', 1),
(244, 15, 'Neftcala', 'NEF', 1),
(245, 15, 'Oguz', 'OGU', 1),
(246, 15, 'Ordubad', 'ORD', 1),
(247, 15, 'Qabala', 'QAB', 1),
(248, 15, 'Qax', 'QAX', 1),
(249, 15, 'Qazax', 'QAZ', 1),
(250, 15, 'Qobustan', 'QOB', 1),
(251, 15, 'Quba', 'QBA', 1),
(252, 15, 'Qubadli', 'QBI', 1),
(253, 15, 'Qusar', 'QUS', 1),
(254, 15, 'Saki', 'SA', 1),
(255, 15, 'Saatli', 'SAT', 1),
(256, 15, 'Sabirabad', 'SAB', 1),
(257, 15, 'Sadarak', 'SAD', 1),
(258, 15, 'Sahbuz', 'SAH', 1),
(259, 15, 'Saki', 'SAK', 1),
(260, 15, 'Salyan', 'SAL', 1),
(261, 15, 'Sumqayit', 'SM', 1),
(262, 15, 'Samaxi', 'SMI', 1),
(263, 15, 'Samkir', 'SKR', 1),
(264, 15, 'Samux', 'SMX', 1),
(265, 15, 'Sarur', 'SAR', 1),
(266, 15, 'Siyazan', 'SIY', 1),
(267, 15, 'Susa', 'SS', 1),
(268, 15, 'Susa', 'SUS', 1),
(269, 15, 'Tartar', 'TAR', 1),
(270, 15, 'Tovuz', 'TOV', 1),
(271, 15, 'Ucar', 'UCA', 1),
(272, 15, 'Xankandi', 'XA', 1),
(273, 15, 'Xacmaz', 'XAC', 1),
(274, 15, 'Xanlar', 'XAN', 1),
(275, 15, 'Xizi', 'XIZ', 1),
(276, 15, 'Xocali', 'XCI', 1),
(277, 15, 'Xocavand', 'XVD', 1),
(278, 15, 'Yardimli', 'YAR', 1),
(279, 15, 'Yevlax', 'YEV', 1),
(280, 15, 'Zangilan', 'ZAN', 1),
(281, 15, 'Zaqatala', 'ZAQ', 1),
(282, 15, 'Zardab', 'ZAR', 1),
(283, 15, 'Naxcivan', 'NX', 1),
(284, 16, 'Acklins', 'ACK', 1),
(285, 16, 'Berry Islands', 'BER', 1),
(286, 16, 'Bimini', 'BIM', 1),
(287, 16, 'Black Point', 'BLK', 1),
(288, 16, 'Cat Island', 'CAT', 1),
(289, 16, 'Central Abaco', 'CAB', 1),
(290, 16, 'Central Andros', 'CAN', 1),
(291, 16, 'Central Eleuthera', 'CEL', 1),
(292, 16, 'City of Freeport', 'FRE', 1),
(293, 16, 'Crooked Island', 'CRO', 1),
(294, 16, 'East Grand Bahama', 'EGB', 1),
(295, 16, 'Exuma', 'EXU', 1),
(296, 16, 'Grand Cay', 'GRD', 1),
(297, 16, 'Harbour Island', 'HAR', 1),
(298, 16, 'Hope Town', 'HOP', 1),
(299, 16, 'Inagua', 'INA', 1),
(300, 16, 'Long Island', 'LNG', 1),
(301, 16, 'Mangrove Cay', 'MAN', 1),
(302, 16, 'Mayaguana', 'MAY', 1),
(303, 16, 'Moore\'s Island', 'MOO', 1),
(304, 16, 'North Abaco', 'NAB', 1),
(305, 16, 'North Andros', 'NAN', 1),
(306, 16, 'North Eleuthera', 'NEL', 1),
(307, 16, 'Ragged Island', 'RAG', 1),
(308, 16, 'Rum Cay', 'RUM', 1),
(309, 16, 'San Salvador', 'SAL', 1),
(310, 16, 'South Abaco', 'SAB', 1),
(311, 16, 'South Andros', 'SAN', 1),
(312, 16, 'South Eleuthera', 'SEL', 1),
(313, 16, 'Spanish Wells', 'SWE', 1),
(314, 16, 'West Grand Bahama', 'WGB', 1),
(315, 17, 'Capital', 'CAP', 1),
(316, 17, 'Central', 'CEN', 1),
(317, 17, 'Muharraq', 'MUH', 1),
(318, 17, 'Northern', 'NOR', 1),
(319, 17, 'Southern', 'SOU', 1),
(320, 18, 'Barisal', 'BAR', 1),
(321, 18, 'Chittagong', 'CHI', 1),
(322, 18, 'Dhaka', 'DHA', 1),
(323, 18, 'Khulna', 'KHU', 1),
(324, 18, 'Rajshahi', 'RAJ', 1),
(325, 18, 'Sylhet', 'SYL', 1),
(326, 19, 'Christ Church', 'CC', 1),
(327, 19, 'Saint Andrew', 'AND', 1),
(328, 19, 'Saint George', 'GEO', 1),
(329, 19, 'Saint James', 'JAM', 1),
(330, 19, 'Saint John', 'JOH', 1),
(331, 19, 'Saint Joseph', 'JOS', 1),
(332, 19, 'Saint Lucy', 'LUC', 1),
(333, 19, 'Saint Michael', 'MIC', 1),
(334, 19, 'Saint Peter', 'PET', 1),
(335, 19, 'Saint Philip', 'PHI', 1),
(336, 19, 'Saint Thomas', 'THO', 1),
(337, 20, 'Brestskaya (Brest)', 'BR', 1),
(338, 20, 'Homyel\'skaya (Homyel\')', 'HO', 1),
(339, 20, 'Horad Minsk', 'HM', 1),
(340, 20, 'Hrodzyenskaya (Hrodna)', 'HR', 1),
(341, 20, 'Mahilyowskaya (Mahilyow)', 'MA', 1),
(342, 20, 'Minskaya', 'MI', 1),
(343, 20, 'Vitsyebskaya (Vitsyebsk)', 'VI', 1),
(344, 21, 'Antwerpen', 'VAN', 1),
(345, 21, 'Brabant Wallon', 'WBR', 1),
(346, 21, 'Hainaut', 'WHT', 1),
(347, 21, 'Liège', 'WLG', 1),
(348, 21, 'Limburg', 'VLI', 1),
(349, 21, 'Luxembourg', 'WLX', 1),
(350, 21, 'Namur', 'WNA', 1),
(351, 21, 'Oost-Vlaanderen', 'VOV', 1),
(352, 21, 'Vlaams Brabant', 'VBR', 1),
(353, 21, 'West-Vlaanderen', 'VWV', 1),
(354, 22, 'Belize', 'BZ', 1),
(355, 22, 'Cayo', 'CY', 1),
(356, 22, 'Corozal', 'CR', 1),
(357, 22, 'Orange Walk', 'OW', 1),
(358, 22, 'Stann Creek', 'SC', 1),
(359, 22, 'Toledo', 'TO', 1),
(360, 23, 'Alibori', 'AL', 1),
(361, 23, 'Atakora', 'AK', 1),
(362, 23, 'Atlantique', 'AQ', 1),
(363, 23, 'Borgou', 'BO', 1),
(364, 23, 'Collines', 'CO', 1),
(365, 23, 'Donga', 'DO', 1),
(366, 23, 'Kouffo', 'KO', 1),
(367, 23, 'Littoral', 'LI', 1),
(368, 23, 'Mono', 'MO', 1),
(369, 23, 'Oueme', 'OU', 1),
(370, 23, 'Plateau', 'PL', 1),
(371, 23, 'Zou', 'ZO', 1),
(372, 24, 'Devonshire', 'DS', 1),
(373, 24, 'Hamilton City', 'HC', 1),
(374, 24, 'Hamilton', 'HA', 1),
(375, 24, 'Paget', 'PG', 1),
(376, 24, 'Pembroke', 'PB', 1),
(377, 24, 'Saint George City', 'GC', 1),
(378, 24, 'Saint George\'s', 'SG', 1),
(379, 24, 'Sandys', 'SA', 1),
(380, 24, 'Smith\'s', 'SM', 1),
(381, 24, 'Southampton', 'SH', 1),
(382, 24, 'Warwick', 'WA', 1),
(383, 25, 'Bumthang', 'BUM', 1),
(384, 25, 'Chukha', 'CHU', 1),
(385, 25, 'Dagana', 'DAG', 1),
(386, 25, 'Gasa', 'GAS', 1),
(387, 25, 'Haa', 'HAA', 1),
(388, 25, 'Lhuntse', 'LHU', 1),
(389, 25, 'Mongar', 'MON', 1),
(390, 25, 'Paro', 'PAR', 1),
(391, 25, 'Pemagatshel', 'PEM', 1),
(392, 25, 'Punakha', 'PUN', 1),
(393, 25, 'Samdrup Jongkhar', 'SJO', 1),
(394, 25, 'Samtse', 'SAT', 1),
(395, 25, 'Sarpang', 'SAR', 1),
(396, 25, 'Thimphu', 'THI', 1),
(397, 25, 'Trashigang', 'TRG', 1),
(398, 25, 'Trashiyangste', 'TRY', 1),
(399, 25, 'Trongsa', 'TRO', 1),
(400, 25, 'Tsirang', 'TSI', 1),
(401, 25, 'Wangdue Phodrang', 'WPH', 1),
(402, 25, 'Zhemgang', 'ZHE', 1),
(403, 26, 'Beni', 'BEN', 1),
(404, 26, 'Chuquisaca', 'CHU', 1),
(405, 26, 'Cochabamba', 'COC', 1),
(406, 26, 'La Paz', 'LPZ', 1),
(407, 26, 'Oruro', 'ORU', 1),
(408, 26, 'Pando', 'PAN', 1),
(409, 26, 'Potosi', 'POT', 1),
(410, 26, 'Santa Cruz', 'SCZ', 1),
(411, 26, 'Tarija', 'TAR', 1),
(412, 27, 'Brcko district', 'BRO', 1),
(413, 27, 'Unsko-Sanski Kanton', 'FUS', 1),
(414, 27, 'Posavski Kanton', 'FPO', 1),
(415, 27, 'Tuzlanski Kanton', 'FTU', 1),
(416, 27, 'Zenicko-Dobojski Kanton', 'FZE', 1),
(417, 27, 'Bosanskopodrinjski Kanton', 'FBP', 1),
(418, 27, 'Srednjebosanski Kanton', 'FSB', 1),
(419, 27, 'Hercegovacko-neretvanski Kanton', 'FHN', 1),
(420, 27, 'Zapadnohercegovacka Zupanija', 'FZH', 1),
(421, 27, 'Kanton Sarajevo', 'FSA', 1),
(422, 27, 'Zapadnobosanska', 'FZA', 1),
(423, 27, 'Banja Luka', 'SBL', 1),
(424, 27, 'Doboj', 'SDO', 1),
(425, 27, 'Bijeljina', 'SBI', 1),
(426, 27, 'Vlasenica', 'SVL', 1),
(427, 27, 'Sarajevo-Romanija or Sokolac', 'SSR', 1),
(428, 27, 'Foca', 'SFO', 1),
(429, 27, 'Trebinje', 'STR', 1),
(430, 28, 'Central', 'CE', 1),
(431, 28, 'Ghanzi', 'GH', 1),
(432, 28, 'Kgalagadi', 'KD', 1),
(433, 28, 'Kgatleng', 'KT', 1),
(434, 28, 'Kweneng', 'KW', 1),
(435, 28, 'Ngamiland', 'NG', 1),
(436, 28, 'North East', 'NE', 1),
(437, 28, 'North West', 'NW', 1),
(438, 28, 'South East', 'SE', 1),
(439, 28, 'Southern', 'SO', 1),
(440, 30, 'Acre', 'AC', 1),
(441, 30, 'Alagoas', 'AL', 1),
(442, 30, 'Amapá', 'AP', 1),
(443, 30, 'Amazonas', 'AM', 1),
(444, 30, 'Bahia', 'BA', 1),
(445, 30, 'Ceará', 'CE', 1),
(446, 30, 'Distrito Federal', 'DF', 1),
(447, 30, 'Espírito Santo', 'ES', 1),
(448, 30, 'Goiás', 'GO', 1),
(449, 30, 'Maranhão', 'MA', 1),
(450, 30, 'Mato Grosso', 'MT', 1),
(451, 30, 'Mato Grosso do Sul', 'MS', 1),
(452, 30, 'Minas Gerais', 'MG', 1),
(453, 30, 'Pará', 'PA', 1),
(454, 30, 'Paraíba', 'PB', 1),
(455, 30, 'Paraná', 'PR', 1),
(456, 30, 'Pernambuco', 'PE', 1),
(457, 30, 'Piauí', 'PI', 1),
(458, 30, 'Rio de Janeiro', 'RJ', 1),
(459, 30, 'Rio Grande do Norte', 'RN', 1),
(460, 30, 'Rio Grande do Sul', 'RS', 1),
(461, 30, 'Rondônia', 'RO', 1),
(462, 30, 'Roraima', 'RR', 1),
(463, 30, 'Santa Catarina', 'SC', 1),
(464, 30, 'São Paulo', 'SP', 1),
(465, 30, 'Sergipe', 'SE', 1),
(466, 30, 'Tocantins', 'TO', 1),
(467, 31, 'Peros Banhos', 'PB', 1),
(468, 31, 'Salomon Islands', 'SI', 1),
(469, 31, 'Nelsons Island', 'NI', 1),
(470, 31, 'Three Brothers', 'TB', 1),
(471, 31, 'Eagle Islands', 'EA', 1),
(472, 31, 'Danger Island', 'DI', 1),
(473, 31, 'Egmont Islands', 'EG', 1),
(474, 31, 'Diego Garcia', 'DG', 1),
(475, 32, 'Belait', 'BEL', 1),
(476, 32, 'Brunei and Muara', 'BRM', 1),
(477, 32, 'Temburong', 'TEM', 1),
(478, 32, 'Tutong', 'TUT', 1),
(479, 33, 'Blagoevgrad', '', 1),
(480, 33, 'Burgas', '', 1),
(481, 33, 'Dobrich', '', 1),
(482, 33, 'Gabrovo', '', 1),
(483, 33, 'Haskovo', '', 1),
(484, 33, 'Kardjali', '', 1),
(485, 33, 'Kyustendil', '', 1),
(486, 33, 'Lovech', '', 1),
(487, 33, 'Montana', '', 1),
(488, 33, 'Pazardjik', '', 1),
(489, 33, 'Pernik', '', 1),
(490, 33, 'Pleven', '', 1),
(491, 33, 'Plovdiv', '', 1),
(492, 33, 'Razgrad', '', 1),
(493, 33, 'Shumen', '', 1),
(494, 33, 'Silistra', '', 1),
(495, 33, 'Sliven', '', 1),
(496, 33, 'Smolyan', '', 1),
(497, 33, 'Sofia', '', 1),
(498, 33, 'Sofia - town', '', 1),
(499, 33, 'Stara Zagora', '', 1),
(500, 33, 'Targovishte', '', 1),
(501, 33, 'Varna', '', 1),
(502, 33, 'Veliko Tarnovo', '', 1),
(503, 33, 'Vidin', '', 1),
(504, 33, 'Vratza', '', 1),
(505, 33, 'Yambol', '', 1),
(506, 34, 'Bale', 'BAL', 1),
(507, 34, 'Bam', 'BAM', 1),
(508, 34, 'Banwa', 'BAN', 1),
(509, 34, 'Bazega', 'BAZ', 1),
(510, 34, 'Bougouriba', 'BOR', 1),
(511, 34, 'Boulgou', 'BLG', 1),
(512, 34, 'Boulkiemde', 'BOK', 1),
(513, 34, 'Comoe', 'COM', 1),
(514, 34, 'Ganzourgou', 'GAN', 1),
(515, 34, 'Gnagna', 'GNA', 1),
(516, 34, 'Gourma', 'GOU', 1),
(517, 34, 'Houet', 'HOU', 1),
(518, 34, 'Ioba', 'IOA', 1),
(519, 34, 'Kadiogo', 'KAD', 1),
(520, 34, 'Kenedougou', 'KEN', 1),
(521, 34, 'Komondjari', 'KOD', 1),
(522, 34, 'Kompienga', 'KOP', 1),
(523, 34, 'Kossi', 'KOS', 1),
(524, 34, 'Koulpelogo', 'KOL', 1),
(525, 34, 'Kouritenga', 'KOT', 1),
(526, 34, 'Kourweogo', 'KOW', 1),
(527, 34, 'Leraba', 'LER', 1),
(528, 34, 'Loroum', 'LOR', 1),
(529, 34, 'Mouhoun', 'MOU', 1),
(530, 34, 'Nahouri', 'NAH', 1),
(531, 34, 'Namentenga', 'NAM', 1),
(532, 34, 'Nayala', 'NAY', 1),
(533, 34, 'Noumbiel', 'NOU', 1),
(534, 34, 'Oubritenga', 'OUB', 1),
(535, 34, 'Oudalan', 'OUD', 1),
(536, 34, 'Passore', 'PAS', 1),
(537, 34, 'Poni', 'PON', 1),
(538, 34, 'Sanguie', 'SAG', 1),
(539, 34, 'Sanmatenga', 'SAM', 1),
(540, 34, 'Seno', 'SEN', 1),
(541, 34, 'Sissili', 'SIS', 1),
(542, 34, 'Soum', 'SOM', 1),
(543, 34, 'Sourou', 'SOR', 1),
(544, 34, 'Tapoa', 'TAP', 1),
(545, 34, 'Tuy', 'TUY', 1),
(546, 34, 'Yagha', 'YAG', 1),
(547, 34, 'Yatenga', 'YAT', 1),
(548, 34, 'Ziro', 'ZIR', 1),
(549, 34, 'Zondoma', 'ZOD', 1),
(550, 34, 'Zoundweogo', 'ZOW', 1),
(551, 35, 'Bubanza', 'BB', 1),
(552, 35, 'Bujumbura', 'BJ', 1),
(553, 35, 'Bururi', 'BR', 1),
(554, 35, 'Cankuzo', 'CA', 1),
(555, 35, 'Cibitoke', 'CI', 1),
(556, 35, 'Gitega', 'GI', 1),
(557, 35, 'Karuzi', 'KR', 1),
(558, 35, 'Kayanza', 'KY', 1),
(559, 35, 'Kirundo', 'KI', 1),
(560, 35, 'Makamba', 'MA', 1),
(561, 35, 'Muramvya', 'MU', 1),
(562, 35, 'Muyinga', 'MY', 1),
(563, 35, 'Mwaro', 'MW', 1),
(564, 35, 'Ngozi', 'NG', 1),
(565, 35, 'Rutana', 'RT', 1),
(566, 35, 'Ruyigi', 'RY', 1),
(567, 36, 'Phnom Penh', 'PP', 1),
(568, 36, 'Preah Seihanu (Kompong Som or Sihanoukville)', 'PS', 1),
(569, 36, 'Pailin', 'PA', 1),
(570, 36, 'Keb', 'KB', 1),
(571, 36, 'Banteay Meanchey', 'BM', 1),
(572, 36, 'Battambang', 'BA', 1),
(573, 36, 'Kampong Cham', 'KM', 1),
(574, 36, 'Kampong Chhnang', 'KN', 1),
(575, 36, 'Kampong Speu', 'KU', 1),
(576, 36, 'Kampong Som', 'KO', 1),
(577, 36, 'Kampong Thom', 'KT', 1),
(578, 36, 'Kampot', 'KP', 1),
(579, 36, 'Kandal', 'KL', 1),
(580, 36, 'Kaoh Kong', 'KK', 1),
(581, 36, 'Kratie', 'KR', 1),
(582, 36, 'Mondul Kiri', 'MK', 1),
(583, 36, 'Oddar Meancheay', 'OM', 1),
(584, 36, 'Pursat', 'PU', 1),
(585, 36, 'Preah Vihear', 'PR', 1),
(586, 36, 'Prey Veng', 'PG', 1),
(587, 36, 'Ratanak Kiri', 'RK', 1),
(588, 36, 'Siemreap', 'SI', 1),
(589, 36, 'Stung Treng', 'ST', 1),
(590, 36, 'Svay Rieng', 'SR', 1),
(591, 36, 'Takeo', 'TK', 1),
(592, 37, 'Adamawa (Adamaoua)', 'ADA', 1),
(593, 37, 'Centre', 'CEN', 1),
(594, 37, 'East (Est)', 'EST', 1),
(595, 37, 'Extreme North (Extreme-Nord)', 'EXN', 1),
(596, 37, 'Littoral', 'LIT', 1),
(597, 37, 'North (Nord)', 'NOR', 1),
(598, 37, 'Northwest (Nord-Ouest)', 'NOT', 1),
(599, 37, 'West (Ouest)', 'OUE', 1),
(600, 37, 'South (Sud)', 'SUD', 1),
(601, 37, 'Southwest (Sud-Ouest).', 'SOU', 1),
(602, 38, 'Alberta', 'AB', 1),
(603, 38, 'British Columbia', 'BC', 1),
(604, 38, 'Manitoba', 'MB', 1),
(605, 38, 'New Brunswick', 'NB', 1),
(606, 38, 'Newfoundland and Labrador', 'NL', 1),
(607, 38, 'Northwest Territories', 'NT', 1),
(608, 38, 'Nova Scotia', 'NS', 1),
(609, 38, 'Nunavut', 'NU', 1),
(610, 38, 'Ontario', 'ON', 1),
(611, 38, 'Prince Edward Island', 'PE', 1),
(612, 38, 'Qu&eacute;bec', 'QC', 1),
(613, 38, 'Saskatchewan', 'SK', 1),
(614, 38, 'Yukon Territory', 'YT', 1),
(615, 39, 'Boa Vista', 'BV', 1),
(616, 39, 'Brava', 'BR', 1),
(617, 39, 'Calheta de Sao Miguel', 'CS', 1),
(618, 39, 'Maio', 'MA', 1),
(619, 39, 'Mosteiros', 'MO', 1),
(620, 39, 'Paul', 'PA', 1),
(621, 39, 'Porto Novo', 'PN', 1),
(622, 39, 'Praia', 'PR', 1),
(623, 39, 'Ribeira Grande', 'RG', 1),
(624, 39, 'Sal', 'SL', 1),
(625, 39, 'Santa Catarina', 'CA', 1),
(626, 39, 'Santa Cruz', 'CR', 1),
(627, 39, 'Sao Domingos', 'SD', 1),
(628, 39, 'Sao Filipe', 'SF', 1),
(629, 39, 'Sao Nicolau', 'SN', 1),
(630, 39, 'Sao Vicente', 'SV', 1),
(631, 39, 'Tarrafal', 'TA', 1),
(632, 40, 'Creek', 'CR', 1),
(633, 40, 'Eastern', 'EA', 1),
(634, 40, 'Midland', 'ML', 1),
(635, 40, 'South Town', 'ST', 1),
(636, 40, 'Spot Bay', 'SP', 1),
(637, 40, 'Stake Bay', 'SK', 1),
(638, 40, 'West End', 'WD', 1),
(639, 40, 'Western', 'WN', 1),
(640, 41, 'Bamingui-Bangoran', 'BBA', 1),
(641, 41, 'Basse-Kotto', 'BKO', 1),
(642, 41, 'Haute-Kotto', 'HKO', 1),
(643, 41, 'Haut-Mbomou', 'HMB', 1),
(644, 41, 'Kemo', 'KEM', 1),
(645, 41, 'Lobaye', 'LOB', 1),
(646, 41, 'Mambere-KadeÔ', 'MKD', 1),
(647, 41, 'Mbomou', 'MBO', 1),
(648, 41, 'Nana-Mambere', 'NMM', 1),
(649, 41, 'Ombella-M\'Poko', 'OMP', 1),
(650, 41, 'Ouaka', 'OUK', 1),
(651, 41, 'Ouham', 'OUH', 1),
(652, 41, 'Ouham-Pende', 'OPE', 1),
(653, 41, 'Vakaga', 'VAK', 1),
(654, 41, 'Nana-Grebizi', 'NGR', 1),
(655, 41, 'Sangha-Mbaere', 'SMB', 1),
(656, 41, 'Bangui', 'BAN', 1),
(657, 42, 'Batha', 'BA', 1),
(658, 42, 'Biltine', 'BI', 1),
(659, 42, 'Borkou-Ennedi-Tibesti', 'BE', 1),
(660, 42, 'Chari-Baguirmi', 'CB', 1),
(661, 42, 'Guera', 'GU', 1),
(662, 42, 'Kanem', 'KA', 1),
(663, 42, 'Lac', 'LA', 1),
(664, 42, 'Logone Occidental', 'LC', 1),
(665, 42, 'Logone Oriental', 'LR', 1),
(666, 42, 'Mayo-Kebbi', 'MK', 1),
(667, 42, 'Moyen-Chari', 'MC', 1),
(668, 42, 'Ouaddai', 'OU', 1),
(669, 42, 'Salamat', 'SA', 1),
(670, 42, 'Tandjile', 'TA', 1),
(671, 43, 'Aisen del General Carlos Ibanez', 'AI', 1),
(672, 43, 'Antofagasta', 'AN', 1),
(673, 43, 'Araucania', 'AR', 1),
(674, 43, 'Atacama', 'AT', 1),
(675, 43, 'Bio-Bio', 'BI', 1),
(676, 43, 'Coquimbo', 'CO', 1),
(677, 43, 'Libertador General Bernardo O\'Higgins', 'LI', 1),
(678, 43, 'Los Lagos', 'LL', 1),
(679, 43, 'Magallanes y de la Antartica Chilena', 'MA', 1),
(680, 43, 'Maule', 'ML', 1),
(681, 43, 'Region Metropolitana', 'RM', 1),
(682, 43, 'Tarapaca', 'TA', 1),
(683, 43, 'Valparaiso', 'VS', 1),
(684, 44, 'Anhui', 'AN', 1),
(685, 44, 'Beijing', 'BE', 1),
(686, 44, 'Chongqing', 'CH', 1),
(687, 44, 'Fujian', 'FU', 1),
(688, 44, 'Gansu', 'GA', 1),
(689, 44, 'Guangdong', 'GU', 1),
(690, 44, 'Guangxi', 'GX', 1),
(691, 44, 'Guizhou', 'GZ', 1),
(692, 44, 'Hainan', 'HA', 1),
(693, 44, 'Hebei', 'HB', 1),
(694, 44, 'Heilongjiang', 'HL', 1),
(695, 44, 'Henan', 'HE', 1),
(696, 44, 'Hong Kong', 'HK', 1),
(697, 44, 'Hubei', 'HU', 1),
(698, 44, 'Hunan', 'HN', 1),
(699, 44, 'Inner Mongolia', 'IM', 1),
(700, 44, 'Jiangsu', 'JI', 1),
(701, 44, 'Jiangxi', 'JX', 1),
(702, 44, 'Jilin', 'JL', 1),
(703, 44, 'Liaoning', 'LI', 1),
(704, 44, 'Macau', 'MA', 1),
(705, 44, 'Ningxia', 'NI', 1),
(706, 44, 'Shaanxi', 'SH', 1),
(707, 44, 'Shandong', 'SA', 1),
(708, 44, 'Shanghai', 'SG', 1),
(709, 44, 'Shanxi', 'SX', 1),
(710, 44, 'Sichuan', 'SI', 1),
(711, 44, 'Tianjin', 'TI', 1),
(712, 44, 'Xinjiang', 'XI', 1),
(713, 44, 'Yunnan', 'YU', 1),
(714, 44, 'Zhejiang', 'ZH', 1),
(715, 46, 'Direction Island', 'D', 1),
(716, 46, 'Home Island', 'H', 1),
(717, 46, 'Horsburgh Island', 'O', 1),
(718, 46, 'South Island', 'S', 1),
(719, 46, 'West Island', 'W', 1),
(720, 47, 'Amazonas', 'AMZ', 1),
(721, 47, 'Antioquia', 'ANT', 1),
(722, 47, 'Arauca', 'ARA', 1),
(723, 47, 'Atlantico', 'ATL', 1),
(724, 47, 'Bogota D.C.', 'BDC', 1),
(725, 47, 'Bolivar', 'BOL', 1),
(726, 47, 'Boyaca', 'BOY', 1),
(727, 47, 'Caldas', 'CAL', 1),
(728, 47, 'Caqueta', 'CAQ', 1),
(729, 47, 'Casanare', 'CAS', 1),
(730, 47, 'Cauca', 'CAU', 1),
(731, 47, 'Cesar', 'CES', 1),
(732, 47, 'Choco', 'CHO', 1),
(733, 47, 'Cordoba', 'COR', 1),
(734, 47, 'Cundinamarca', 'CAM', 1),
(735, 47, 'Guainia', 'GNA', 1),
(736, 47, 'Guajira', 'GJR', 1),
(737, 47, 'Guaviare', 'GVR', 1),
(738, 47, 'Huila', 'HUI', 1),
(739, 47, 'Magdalena', 'MAG', 1),
(740, 47, 'Meta', 'MET', 1),
(741, 47, 'Narino', 'NAR', 1),
(742, 47, 'Norte de Santander', 'NDS', 1),
(743, 47, 'Putumayo', 'PUT', 1),
(744, 47, 'Quindio', 'QUI', 1),
(745, 47, 'Risaralda', 'RIS', 1),
(746, 47, 'San Andres y Providencia', 'SAP', 1),
(747, 47, 'Santander', 'SAN', 1),
(748, 47, 'Sucre', 'SUC', 1),
(749, 47, 'Tolima', 'TOL', 1),
(750, 47, 'Valle del Cauca', 'VDC', 1),
(751, 47, 'Vaupes', 'VAU', 1),
(752, 47, 'Vichada', 'VIC', 1),
(753, 48, 'Grande Comore', 'G', 1),
(754, 48, 'Anjouan', 'A', 1),
(755, 48, 'Moheli', 'M', 1),
(756, 49, 'Bouenza', 'BO', 1),
(757, 49, 'Brazzaville', 'BR', 1),
(758, 49, 'Cuvette', 'CU', 1),
(759, 49, 'Cuvette-Ouest', 'CO', 1),
(760, 49, 'Kouilou', 'KO', 1),
(761, 49, 'Lekoumou', 'LE', 1),
(762, 49, 'Likouala', 'LI', 1),
(763, 49, 'Niari', 'NI', 1),
(764, 49, 'Plateaux', 'PL', 1),
(765, 49, 'Pool', 'PO', 1),
(766, 49, 'Sangha', 'SA', 1),
(767, 50, 'Pukapuka', 'PU', 1),
(768, 50, 'Rakahanga', 'RK', 1),
(769, 50, 'Manihiki', 'MK', 1),
(770, 50, 'Penrhyn', 'PE', 1),
(771, 50, 'Nassau Island', 'NI', 1),
(772, 50, 'Surwarrow', 'SU', 1),
(773, 50, 'Palmerston', 'PA', 1),
(774, 50, 'Aitutaki', 'AI', 1),
(775, 50, 'Manuae', 'MA', 1),
(776, 50, 'Takutea', 'TA', 1),
(777, 50, 'Mitiaro', 'MT', 1),
(778, 50, 'Atiu', 'AT', 1),
(779, 50, 'Mauke', 'MU', 1),
(780, 50, 'Rarotonga', 'RR', 1),
(781, 50, 'Mangaia', 'MG', 1),
(782, 51, 'Alajuela', 'AL', 1),
(783, 51, 'Cartago', 'CA', 1),
(784, 51, 'Guanacaste', 'GU', 1),
(785, 51, 'Heredia', 'HE', 1),
(786, 51, 'Limon', 'LI', 1),
(787, 51, 'Puntarenas', 'PU', 1),
(788, 51, 'San Jose', 'SJ', 1),
(789, 52, 'Abengourou', 'ABE', 1),
(790, 52, 'Abidjan', 'ABI', 1),
(791, 52, 'Aboisso', 'ABO', 1),
(792, 52, 'Adiake', 'ADI', 1),
(793, 52, 'Adzope', 'ADZ', 1),
(794, 52, 'Agboville', 'AGB', 1),
(795, 52, 'Agnibilekrou', 'AGN', 1),
(796, 52, 'Alepe', 'ALE', 1),
(797, 52, 'Bocanda', 'BOC', 1),
(798, 52, 'Bangolo', 'BAN', 1),
(799, 52, 'Beoumi', 'BEO', 1),
(800, 52, 'Biankouma', 'BIA', 1),
(801, 52, 'Bondoukou', 'BDK', 1),
(802, 52, 'Bongouanou', 'BGN', 1),
(803, 52, 'Bouafle', 'BFL', 1),
(804, 52, 'Bouake', 'BKE', 1),
(805, 52, 'Bouna', 'BNA', 1),
(806, 52, 'Boundiali', 'BDL', 1),
(807, 52, 'Dabakala', 'DKL', 1),
(808, 52, 'Dabou', 'DBU', 1),
(809, 52, 'Daloa', 'DAL', 1),
(810, 52, 'Danane', 'DAN', 1),
(811, 52, 'Daoukro', 'DAO', 1),
(812, 52, 'Dimbokro', 'DIM', 1),
(813, 52, 'Divo', 'DIV', 1),
(814, 52, 'Duekoue', 'DUE', 1),
(815, 52, 'Ferkessedougou', 'FER', 1),
(816, 52, 'Gagnoa', 'GAG', 1),
(817, 52, 'Grand-Bassam', 'GBA', 1),
(818, 52, 'Grand-Lahou', 'GLA', 1),
(819, 52, 'Guiglo', 'GUI', 1),
(820, 52, 'Issia', 'ISS', 1),
(821, 52, 'Jacqueville', 'JAC', 1),
(822, 52, 'Katiola', 'KAT', 1),
(823, 52, 'Korhogo', 'KOR', 1),
(824, 52, 'Lakota', 'LAK', 1),
(825, 52, 'Man', 'MAN', 1),
(826, 52, 'Mankono', 'MKN', 1),
(827, 52, 'Mbahiakro', 'MBA', 1),
(828, 52, 'Odienne', 'ODI', 1),
(829, 52, 'Oume', 'OUM', 1),
(830, 52, 'Sakassou', 'SAK', 1),
(831, 52, 'San-Pedro', 'SPE', 1),
(832, 52, 'Sassandra', 'SAS', 1),
(833, 52, 'Seguela', 'SEG', 1),
(834, 52, 'Sinfra', 'SIN', 1),
(835, 52, 'Soubre', 'SOU', 1),
(836, 52, 'Tabou', 'TAB', 1),
(837, 52, 'Tanda', 'TAN', 1),
(838, 52, 'Tiebissou', 'TIE', 1),
(839, 52, 'Tingrela', 'TIN', 1),
(840, 52, 'Tiassale', 'TIA', 1),
(841, 52, 'Touba', 'TBA', 1),
(842, 52, 'Toulepleu', 'TLP', 1),
(843, 52, 'Toumodi', 'TMD', 1),
(844, 52, 'Vavoua', 'VAV', 1),
(845, 52, 'Yamoussoukro', 'YAM', 1),
(846, 52, 'Zuenoula', 'ZUE', 1),
(847, 53, 'Bjelovarsko-bilogorska', 'BB', 1),
(848, 53, 'Grad Zagreb', 'GZ', 1),
(849, 53, 'Dubrovačko-neretvanska', 'DN', 1),
(850, 53, 'Istarska', 'IS', 1),
(851, 53, 'Karlovačka', 'KA', 1),
(852, 53, 'Koprivničko-križevačka', 'KK', 1),
(853, 53, 'Krapinsko-zagorska', 'KZ', 1),
(854, 53, 'Ličko-senjska', 'LS', 1),
(855, 53, 'Međimurska', 'ME', 1),
(856, 53, 'Osječko-baranjska', 'OB', 1),
(857, 53, 'Požeško-slavonska', 'PS', 1),
(858, 53, 'Primorsko-goranska', 'PG', 1),
(859, 53, 'Šibensko-kninska', 'SK', 1),
(860, 53, 'Sisačko-moslavačka', 'SM', 1),
(861, 53, 'Brodsko-posavska', 'BP', 1),
(862, 53, 'Splitsko-dalmatinska', 'SD', 1),
(863, 53, 'Varaždinska', 'VA', 1),
(864, 53, 'Virovitičko-podravska', 'VP', 1),
(865, 53, 'Vukovarsko-srijemska', 'VS', 1),
(866, 53, 'Zadarska', 'ZA', 1),
(867, 53, 'Zagrebačka', 'ZG', 1),
(868, 54, 'Camaguey', 'CA', 1),
(869, 54, 'Ciego de Avila', 'CD', 1),
(870, 54, 'Cienfuegos', 'CI', 1),
(871, 54, 'Ciudad de La Habana', 'CH', 1),
(872, 54, 'Granma', 'GR', 1),
(873, 54, 'Guantanamo', 'GU', 1),
(874, 54, 'Holguin', 'HO', 1),
(875, 54, 'Isla de la Juventud', 'IJ', 1),
(876, 54, 'La Habana', 'LH', 1),
(877, 54, 'Las Tunas', 'LT', 1),
(878, 54, 'Matanzas', 'MA', 1),
(879, 54, 'Pinar del Rio', 'PR', 1),
(880, 54, 'Sancti Spiritus', 'SS', 1),
(881, 54, 'Santiago de Cuba', 'SC', 1),
(882, 54, 'Villa Clara', 'VC', 1),
(883, 55, 'Famagusta', 'F', 1),
(884, 55, 'Kyrenia', 'K', 1),
(885, 55, 'Larnaca', 'A', 1),
(886, 55, 'Limassol', 'I', 1),
(887, 55, 'Nicosia', 'N', 1),
(888, 55, 'Paphos', 'P', 1),
(889, 56, 'Ústecký', 'U', 1),
(890, 56, 'Jihočeský', 'C', 1),
(891, 56, 'Jihomoravský', 'B', 1),
(892, 56, 'Karlovarský', 'K', 1),
(893, 56, 'Královehradecký', 'H', 1),
(894, 56, 'Liberecký', 'L', 1),
(895, 56, 'Moravskoslezský', 'T', 1),
(896, 56, 'Olomoucký', 'M', 1),
(897, 56, 'Pardubický', 'E', 1),
(898, 56, 'Plzeňský', 'P', 1),
(899, 56, 'Praha', 'A', 1),
(900, 56, 'Středočeský', 'S', 1),
(901, 56, 'Vysočina', 'J', 1),
(902, 56, 'Zlínský', 'Z', 1),
(903, 57, 'Arhus', 'AR', 1),
(904, 57, 'Bornholm', 'BH', 1),
(905, 57, 'Copenhagen', 'CO', 1),
(906, 57, 'Faroe Islands', 'FO', 1),
(907, 57, 'Frederiksborg', 'FR', 1),
(908, 57, 'Fyn', 'FY', 1),
(909, 57, 'Kobenhavn', 'KO', 1),
(910, 57, 'Nordjylland', 'NO', 1),
(911, 57, 'Ribe', 'RI', 1),
(912, 57, 'Ringkobing', 'RK', 1),
(913, 57, 'Roskilde', 'RO', 1),
(914, 57, 'Sonderjylland', 'SO', 1),
(915, 57, 'Storstrom', 'ST', 1),
(916, 57, 'Vejle', 'VK', 1),
(917, 57, 'Vestj&aelig;lland', 'VJ', 1),
(918, 57, 'Viborg', 'VB', 1),
(919, 58, '\'Ali Sabih', 'S', 1),
(920, 58, 'Dikhil', 'K', 1),
(921, 58, 'Djibouti', 'J', 1),
(922, 58, 'Obock', 'O', 1),
(923, 58, 'Tadjoura', 'T', 1),
(924, 59, 'Saint Andrew Parish', 'AND', 1),
(925, 59, 'Saint David Parish', 'DAV', 1),
(926, 59, 'Saint George Parish', 'GEO', 1),
(927, 59, 'Saint John Parish', 'JOH', 1),
(928, 59, 'Saint Joseph Parish', 'JOS', 1),
(929, 59, 'Saint Luke Parish', 'LUK', 1),
(930, 59, 'Saint Mark Parish', 'MAR', 1),
(931, 59, 'Saint Patrick Parish', 'PAT', 1),
(932, 59, 'Saint Paul Parish', 'PAU', 1),
(933, 59, 'Saint Peter Parish', 'PET', 1),
(934, 60, 'Distrito Nacional', 'DN', 1),
(935, 60, 'Azua', 'AZ', 1),
(936, 60, 'Baoruco', 'BC', 1),
(937, 60, 'Barahona', 'BH', 1),
(938, 60, 'Dajabon', 'DJ', 1),
(939, 60, 'Duarte', 'DU', 1),
(940, 60, 'Elias Pina', 'EL', 1),
(941, 60, 'El Seybo', 'SY', 1),
(942, 60, 'Espaillat', 'ET', 1),
(943, 60, 'Hato Mayor', 'HM', 1),
(944, 60, 'Independencia', 'IN', 1),
(945, 60, 'La Altagracia', 'AL', 1),
(946, 60, 'La Romana', 'RO', 1),
(947, 60, 'La Vega', 'VE', 1),
(948, 60, 'Maria Trinidad Sanchez', 'MT', 1),
(949, 60, 'Monsenor Nouel', 'MN', 1),
(950, 60, 'Monte Cristi', 'MC', 1),
(951, 60, 'Monte Plata', 'MP', 1),
(952, 60, 'Pedernales', 'PD', 1),
(953, 60, 'Peravia (Bani)', 'PR', 1),
(954, 60, 'Puerto Plata', 'PP', 1),
(955, 60, 'Salcedo', 'SL', 1),
(956, 60, 'Samana', 'SM', 1),
(957, 60, 'Sanchez Ramirez', 'SH', 1),
(958, 60, 'San Cristobal', 'SC', 1),
(959, 60, 'San Jose de Ocoa', 'JO', 1),
(960, 60, 'San Juan', 'SJ', 1),
(961, 60, 'San Pedro de Macoris', 'PM', 1),
(962, 60, 'Santiago', 'SA', 1),
(963, 60, 'Santiago Rodriguez', 'ST', 1),
(964, 60, 'Santo Domingo', 'SD', 1),
(965, 60, 'Valverde', 'VA', 1),
(966, 61, 'Aileu', 'AL', 1),
(967, 61, 'Ainaro', 'AN', 1),
(968, 61, 'Baucau', 'BA', 1),
(969, 61, 'Bobonaro', 'BO', 1),
(970, 61, 'Cova Lima', 'CO', 1),
(971, 61, 'Dili', 'DI', 1),
(972, 61, 'Ermera', 'ER', 1),
(973, 61, 'Lautem', 'LA', 1),
(974, 61, 'Liquica', 'LI', 1),
(975, 61, 'Manatuto', 'MT', 1),
(976, 61, 'Manufahi', 'MF', 1),
(977, 61, 'Oecussi', 'OE', 1),
(978, 61, 'Viqueque', 'VI', 1),
(979, 62, 'Azuay', 'AZU', 1),
(980, 62, 'Bolivar', 'BOL', 1),
(981, 62, 'Ca&ntilde;ar', 'CAN', 1),
(982, 62, 'Carchi', 'CAR', 1),
(983, 62, 'Chimborazo', 'CHI', 1),
(984, 62, 'Cotopaxi', 'COT', 1),
(985, 62, 'El Oro', 'EOR', 1),
(986, 62, 'Esmeraldas', 'ESM', 1),
(987, 62, 'Gal&aacute;pagos', 'GPS', 1),
(988, 62, 'Guayas', 'GUA', 1),
(989, 62, 'Imbabura', 'IMB', 1),
(990, 62, 'Loja', 'LOJ', 1),
(991, 62, 'Los Rios', 'LRO', 1),
(992, 62, 'Manab&iacute;', 'MAN', 1),
(993, 62, 'Morona Santiago', 'MSA', 1),
(994, 62, 'Napo', 'NAP', 1),
(995, 62, 'Orellana', 'ORE', 1),
(996, 62, 'Pastaza', 'PAS', 1),
(997, 62, 'Pichincha', 'PIC', 1),
(998, 62, 'Sucumb&iacute;os', 'SUC', 1),
(999, 62, 'Tungurahua', 'TUN', 1),
(1000, 62, 'Zamora Chinchipe', 'ZCH', 1),
(1001, 63, 'Ad Daqahliyah', 'DHY', 1),
(1002, 63, 'Al Bahr al Ahmar', 'BAM', 1),
(1003, 63, 'Al Buhayrah', 'BHY', 1),
(1004, 63, 'Al Fayyum', 'FYM', 1),
(1005, 63, 'Al Gharbiyah', 'GBY', 1),
(1006, 63, 'Al Iskandariyah', 'IDR', 1),
(1007, 63, 'Al Isma\'iliyah', 'IML', 1),
(1008, 63, 'Al Jizah', 'JZH', 1),
(1009, 63, 'Al Minufiyah', 'MFY', 1),
(1010, 63, 'Al Minya', 'MNY', 1),
(1011, 63, 'Al Qahirah', 'QHR', 1),
(1012, 63, 'Al Qalyubiyah', 'QLY', 1),
(1013, 63, 'Al Wadi al Jadid', 'WJD', 1),
(1014, 63, 'Ash Sharqiyah', 'SHQ', 1),
(1015, 63, 'As Suways', 'SWY', 1),
(1016, 63, 'Aswan', 'ASW', 1),
(1017, 63, 'Asyut', 'ASY', 1),
(1018, 63, 'Bani Suwayf', 'BSW', 1),
(1019, 63, 'Bur Sa\'id', 'BSD', 1),
(1020, 63, 'Dumyat', 'DMY', 1),
(1021, 63, 'Janub Sina\'', 'JNS', 1),
(1022, 63, 'Kafr ash Shaykh', 'KSH', 1),
(1023, 63, 'Matruh', 'MAT', 1),
(1024, 63, 'Qina', 'QIN', 1),
(1025, 63, 'Shamal Sina\'', 'SHS', 1),
(1026, 63, 'Suhaj', 'SUH', 1),
(1027, 64, 'Ahuachapan', 'AH', 1),
(1028, 64, 'Cabanas', 'CA', 1),
(1029, 64, 'Chalatenango', 'CH', 1),
(1030, 64, 'Cuscatlan', 'CU', 1),
(1031, 64, 'La Libertad', 'LB', 1),
(1032, 64, 'La Paz', 'PZ', 1),
(1033, 64, 'La Union', 'UN', 1),
(1034, 64, 'Morazan', 'MO', 1),
(1035, 64, 'San Miguel', 'SM', 1),
(1036, 64, 'San Salvador', 'SS', 1),
(1037, 64, 'San Vicente', 'SV', 1),
(1038, 64, 'Santa Ana', 'SA', 1),
(1039, 64, 'Sonsonate', 'SO', 1),
(1040, 64, 'Usulutan', 'US', 1),
(1041, 65, 'Provincia Annobon', 'AN', 1),
(1042, 65, 'Provincia Bioko Norte', 'BN', 1),
(1043, 65, 'Provincia Bioko Sur', 'BS', 1),
(1044, 65, 'Provincia Centro Sur', 'CS', 1),
(1045, 65, 'Provincia Kie-Ntem', 'KN', 1),
(1046, 65, 'Provincia Litoral', 'LI', 1),
(1047, 65, 'Provincia Wele-Nzas', 'WN', 1),
(1048, 66, 'Central (Maekel)', 'MA', 1),
(1049, 66, 'Anseba (Keren)', 'KE', 1),
(1050, 66, 'Southern Red Sea (Debub-Keih-Bahri)', 'DK', 1),
(1051, 66, 'Northern Red Sea (Semien-Keih-Bahri)', 'SK', 1),
(1052, 66, 'Southern (Debub)', 'DE', 1),
(1053, 66, 'Gash-Barka (Barentu)', 'BR', 1),
(1054, 67, 'Harjumaa (Tallinn)', 'HA', 1),
(1055, 67, 'Hiiumaa (Kardla)', 'HI', 1),
(1056, 67, 'Ida-Virumaa (Johvi)', 'IV', 1),
(1057, 67, 'Jarvamaa (Paide)', 'JA', 1),
(1058, 67, 'Jogevamaa (Jogeva)', 'JO', 1),
(1059, 67, 'Laane-Virumaa (Rakvere)', 'LV', 1),
(1060, 67, 'Laanemaa (Haapsalu)', 'LA', 1),
(1061, 67, 'Parnumaa (Parnu)', 'PA', 1),
(1062, 67, 'Polvamaa (Polva)', 'PO', 1),
(1063, 67, 'Raplamaa (Rapla)', 'RA', 1),
(1064, 67, 'Saaremaa (Kuessaare)', 'SA', 1),
(1065, 67, 'Tartumaa (Tartu)', 'TA', 1),
(1066, 67, 'Valgamaa (Valga)', 'VA', 1),
(1067, 67, 'Viljandimaa (Viljandi)', 'VI', 1),
(1068, 67, 'Vorumaa (Voru)', 'VO', 1),
(1069, 68, 'Afar', 'AF', 1),
(1070, 68, 'Amhara', 'AH', 1),
(1071, 68, 'Benishangul-Gumaz', 'BG', 1),
(1072, 68, 'Gambela', 'GB', 1),
(1073, 68, 'Hariai', 'HR', 1),
(1074, 68, 'Oromia', 'OR', 1),
(1075, 68, 'Somali', 'SM', 1),
(1076, 68, 'Southern Nations - Nationalities and Peoples Region', 'SN', 1),
(1077, 68, 'Tigray', 'TG', 1),
(1078, 68, 'Addis Ababa', 'AA', 1),
(1079, 68, 'Dire Dawa', 'DD', 1),
(1080, 71, 'Central Division', 'C', 1),
(1081, 71, 'Northern Division', 'N', 1),
(1082, 71, 'Eastern Division', 'E', 1),
(1083, 71, 'Western Division', 'W', 1),
(1084, 71, 'Rotuma', 'R', 1),
(1085, 72, 'Ahvenanmaan lääni', 'AL', 1),
(1086, 72, 'Etelä-Suomen lääni', 'ES', 1),
(1087, 72, 'Itä-Suomen lääni', 'IS', 1),
(1088, 72, 'Länsi-Suomen lääni', 'LS', 1),
(1089, 72, 'Lapin lääni', 'LA', 1),
(1090, 72, 'Oulun lääni', 'OU', 1),
(1114, 74, 'Ain', '01', 1),
(1115, 74, 'Aisne', '02', 1),
(1116, 74, 'Allier', '03', 1),
(1117, 74, 'Alpes de Haute Provence', '04', 1),
(1118, 74, 'Hautes-Alpes', '05', 1),
(1119, 74, 'Alpes Maritimes', '06', 1),
(1120, 74, 'Ard&egrave;che', '07', 1),
(1121, 74, 'Ardennes', '08', 1),
(1122, 74, 'Ari&egrave;ge', '09', 1),
(1123, 74, 'Aube', '10', 1),
(1124, 74, 'Aude', '11', 1),
(1125, 74, 'Aveyron', '12', 1),
(1126, 74, 'Bouches du Rh&ocirc;ne', '13', 1),
(1127, 74, 'Calvados', '14', 1),
(1128, 74, 'Cantal', '15', 1),
(1129, 74, 'Charente', '16', 1),
(1130, 74, 'Charente Maritime', '17', 1),
(1131, 74, 'Cher', '18', 1),
(1132, 74, 'Corr&egrave;ze', '19', 1),
(1133, 74, 'Corse du Sud', '2A', 1),
(1134, 74, 'Haute Corse', '2B', 1),
(1135, 74, 'C&ocirc;te d&#039;or', '21', 1),
(1136, 74, 'C&ocirc;tes d&#039;Armor', '22', 1),
(1137, 74, 'Creuse', '23', 1),
(1138, 74, 'Dordogne', '24', 1),
(1139, 74, 'Doubs', '25', 1),
(1140, 74, 'Dr&ocirc;me', '26', 1),
(1141, 74, 'Eure', '27', 1),
(1142, 74, 'Eure et Loir', '28', 1),
(1143, 74, 'Finist&egrave;re', '29', 1),
(1144, 74, 'Gard', '30', 1),
(1145, 74, 'Haute Garonne', '31', 1),
(1146, 74, 'Gers', '32', 1),
(1147, 74, 'Gironde', '33', 1),
(1148, 74, 'H&eacute;rault', '34', 1),
(1149, 74, 'Ille et Vilaine', '35', 1),
(1150, 74, 'Indre', '36', 1),
(1151, 74, 'Indre et Loire', '37', 1),
(1152, 74, 'Is&eacute;re', '38', 1),
(1153, 74, 'Jura', '39', 1),
(1154, 74, 'Landes', '40', 1),
(1155, 74, 'Loir et Cher', '41', 1),
(1156, 74, 'Loire', '42', 1),
(1157, 74, 'Haute Loire', '43', 1),
(1158, 74, 'Loire Atlantique', '44', 1),
(1159, 74, 'Loiret', '45', 1),
(1160, 74, 'Lot', '46', 1),
(1161, 74, 'Lot et Garonne', '47', 1),
(1162, 74, 'Loz&egrave;re', '48', 1),
(1163, 74, 'Maine et Loire', '49', 1),
(1164, 74, 'Manche', '50', 1),
(1165, 74, 'Marne', '51', 1),
(1166, 74, 'Haute Marne', '52', 1),
(1167, 74, 'Mayenne', '53', 1),
(1168, 74, 'Meurthe et Moselle', '54', 1),
(1169, 74, 'Meuse', '55', 1),
(1170, 74, 'Morbihan', '56', 1),
(1171, 74, 'Moselle', '57', 1),
(1172, 74, 'Ni&egrave;vre', '58', 1),
(1173, 74, 'Nord', '59', 1),
(1174, 74, 'Oise', '60', 1),
(1175, 74, 'Orne', '61', 1),
(1176, 74, 'Pas de Calais', '62', 1),
(1177, 74, 'Puy de D&ocirc;me', '63', 1),
(1178, 74, 'Pyr&eacute;n&eacute;es Atlantiques', '64', 1),
(1179, 74, 'Hautes Pyr&eacute;n&eacute;es', '65', 1),
(1180, 74, 'Pyr&eacute;n&eacute;es Orientales', '66', 1),
(1181, 74, 'Bas Rhin', '67', 1),
(1182, 74, 'Haut Rhin', '68', 1),
(1183, 74, 'Rh&ocirc;ne', '69', 1),
(1184, 74, 'Haute Sa&ocirc;ne', '70', 1),
(1185, 74, 'Sa&ocirc;ne et Loire', '71', 1),
(1186, 74, 'Sarthe', '72', 1),
(1187, 74, 'Savoie', '73', 1),
(1188, 74, 'Haute Savoie', '74', 1),
(1189, 74, 'Paris', '75', 1),
(1190, 74, 'Seine Maritime', '76', 1),
(1191, 74, 'Seine et Marne', '77', 1),
(1192, 74, 'Yvelines', '78', 1),
(1193, 74, 'Deux S&egrave;vres', '79', 1),
(1194, 74, 'Somme', '80', 1),
(1195, 74, 'Tarn', '81', 1),
(1196, 74, 'Tarn et Garonne', '82', 1),
(1197, 74, 'Var', '83', 1),
(1198, 74, 'Vaucluse', '84', 1),
(1199, 74, 'Vend&eacute;e', '85', 1),
(1200, 74, 'Vienne', '86', 1),
(1201, 74, 'Haute Vienne', '87', 1),
(1202, 74, 'Vosges', '88', 1),
(1203, 74, 'Yonne', '89', 1),
(1204, 74, 'Territoire de Belfort', '90', 1),
(1205, 74, 'Essonne', '91', 1),
(1206, 74, 'Hauts de Seine', '92', 1),
(1207, 74, 'Seine St-Denis', '93', 1),
(1208, 74, 'Val de Marne', '94', 1),
(1209, 74, 'Val d\'Oise', '95', 1),
(1210, 76, 'Archipel des Marquises', 'M', 1),
(1211, 76, 'Archipel des Tuamotu', 'T', 1),
(1212, 76, 'Archipel des Tubuai', 'I', 1),
(1213, 76, 'Iles du Vent', 'V', 1),
(1214, 76, 'Iles Sous-le-Vent', 'S', 1),
(1215, 77, 'Iles Crozet', 'C', 1),
(1216, 77, 'Iles Kerguelen', 'K', 1),
(1217, 77, 'Ile Amsterdam', 'A', 1),
(1218, 77, 'Ile Saint-Paul', 'P', 1),
(1219, 77, 'Adelie Land', 'D', 1),
(1220, 78, 'Estuaire', 'ES', 1),
(1221, 78, 'Haut-Ogooue', 'HO', 1),
(1222, 78, 'Moyen-Ogooue', 'MO', 1),
(1223, 78, 'Ngounie', 'NG', 1),
(1224, 78, 'Nyanga', 'NY', 1),
(1225, 78, 'Ogooue-Ivindo', 'OI', 1),
(1226, 78, 'Ogooue-Lolo', 'OL', 1),
(1227, 78, 'Ogooue-Maritime', 'OM', 1),
(1228, 78, 'Woleu-Ntem', 'WN', 1),
(1229, 79, 'Banjul', 'BJ', 1),
(1230, 79, 'Basse', 'BS', 1),
(1231, 79, 'Brikama', 'BR', 1),
(1232, 79, 'Janjangbure', 'JA', 1),
(1233, 79, 'Kanifeng', 'KA', 1),
(1234, 79, 'Kerewan', 'KE', 1),
(1235, 79, 'Kuntaur', 'KU', 1),
(1236, 79, 'Mansakonko', 'MA', 1),
(1237, 79, 'Lower River', 'LR', 1),
(1238, 79, 'Central River', 'CR', 1),
(1239, 79, 'North Bank', 'NB', 1),
(1240, 79, 'Upper River', 'UR', 1),
(1241, 79, 'Western', 'WE', 1),
(1242, 80, 'Abkhazia', 'AB', 1),
(1243, 80, 'Ajaria', 'AJ', 1),
(1244, 80, 'Tbilisi', 'TB', 1),
(1245, 80, 'Guria', 'GU', 1),
(1246, 80, 'Imereti', 'IM', 1),
(1247, 80, 'Kakheti', 'KA', 1),
(1248, 80, 'Kvemo Kartli', 'KK', 1),
(1249, 80, 'Mtskheta-Mtianeti', 'MM', 1),
(1250, 80, 'Racha Lechkhumi and Kvemo Svanet', 'RL', 1),
(1251, 80, 'Samegrelo-Zemo Svaneti', 'SZ', 1),
(1252, 80, 'Samtskhe-Javakheti', 'SJ', 1),
(1253, 80, 'Shida Kartli', 'SK', 1),
(1254, 81, 'Baden-Württemberg', 'BAW', 1),
(1255, 81, 'Bayern', 'BAY', 1),
(1256, 81, 'Berlin', 'BER', 1),
(1257, 81, 'Brandenburg', 'BRG', 1),
(1258, 81, 'Bremen', 'BRE', 1),
(1259, 81, 'Hamburg', 'HAM', 1),
(1260, 81, 'Hessen', 'HES', 1),
(1261, 81, 'Mecklenburg-Vorpommern', 'MEC', 1),
(1262, 81, 'Niedersachsen', 'NDS', 1),
(1263, 81, 'Nordrhein-Westfalen', 'NRW', 1),
(1264, 81, 'Rheinland-Pfalz', 'RHE', 1),
(1265, 81, 'Saarland', 'SAR', 1),
(1266, 81, 'Sachsen', 'SAS', 1),
(1267, 81, 'Sachsen-Anhalt', 'SAC', 1),
(1268, 81, 'Schleswig-Holstein', 'SCN', 1),
(1269, 81, 'Thüringen', 'THE', 1),
(1270, 82, 'Ashanti Region', 'AS', 1),
(1271, 82, 'Brong-Ahafo Region', 'BA', 1),
(1272, 82, 'Central Region', 'CE', 1),
(1273, 82, 'Eastern Region', 'EA', 1),
(1274, 82, 'Greater Accra Region', 'GA', 1),
(1275, 82, 'Northern Region', 'NO', 1),
(1276, 82, 'Upper East Region', 'UE', 1),
(1277, 82, 'Upper West Region', 'UW', 1),
(1278, 82, 'Volta Region', 'VO', 1),
(1279, 82, 'Western Region', 'WE', 1),
(1280, 84, 'Attica', 'AT', 1),
(1281, 84, 'Central Greece', 'CN', 1),
(1282, 84, 'Central Macedonia', 'CM', 1),
(1283, 84, 'Crete', 'CR', 1),
(1284, 84, 'East Macedonia and Thrace', 'EM', 1),
(1285, 84, 'Epirus', 'EP', 1),
(1286, 84, 'Ionian Islands', 'II', 1),
(1287, 84, 'North Aegean', 'NA', 1),
(1288, 84, 'Peloponnesos', 'PP', 1),
(1289, 84, 'South Aegean', 'SA', 1),
(1290, 84, 'Thessaly', 'TH', 1),
(1291, 84, 'West Greece', 'WG', 1),
(1292, 84, 'West Macedonia', 'WM', 1),
(1293, 85, 'Avannaa', 'A', 1),
(1294, 85, 'Tunu', 'T', 1),
(1295, 85, 'Kitaa', 'K', 1),
(1296, 86, 'Saint Andrew', 'A', 1),
(1297, 86, 'Saint David', 'D', 1),
(1298, 86, 'Saint George', 'G', 1),
(1299, 86, 'Saint John', 'J', 1),
(1300, 86, 'Saint Mark', 'M', 1),
(1301, 86, 'Saint Patrick', 'P', 1),
(1302, 86, 'Carriacou', 'C', 1),
(1303, 86, 'Petit Martinique', 'Q', 1),
(1304, 89, 'Alta Verapaz', 'AV', 1),
(1305, 89, 'Baja Verapaz', 'BV', 1),
(1306, 89, 'Chimaltenango', 'CM', 1),
(1307, 89, 'Chiquimula', 'CQ', 1),
(1308, 89, 'El Peten', 'PE', 1),
(1309, 89, 'El Progreso', 'PR', 1),
(1310, 89, 'El Quiche', 'QC', 1),
(1311, 89, 'Escuintla', 'ES', 1),
(1312, 89, 'Guatemala', 'GU', 1),
(1313, 89, 'Huehuetenango', 'HU', 1),
(1314, 89, 'Izabal', 'IZ', 1),
(1315, 89, 'Jalapa', 'JA', 1),
(1316, 89, 'Jutiapa', 'JU', 1),
(1317, 89, 'Quetzaltenango', 'QZ', 1),
(1318, 89, 'Retalhuleu', 'RE', 1),
(1319, 89, 'Sacatepequez', 'ST', 1),
(1320, 89, 'San Marcos', 'SM', 1),
(1321, 89, 'Santa Rosa', 'SR', 1),
(1322, 89, 'Solola', 'SO', 1),
(1323, 89, 'Suchitepequez', 'SU', 1),
(1324, 89, 'Totonicapan', 'TO', 1),
(1325, 89, 'Zacapa', 'ZA', 1),
(1326, 90, 'Conakry', 'CNK', 1),
(1327, 90, 'Beyla', 'BYL', 1),
(1328, 90, 'Boffa', 'BFA', 1),
(1329, 90, 'Boke', 'BOK', 1),
(1330, 90, 'Coyah', 'COY', 1),
(1331, 90, 'Dabola', 'DBL', 1),
(1332, 90, 'Dalaba', 'DLB', 1),
(1333, 90, 'Dinguiraye', 'DGR', 1),
(1334, 90, 'Dubreka', 'DBR', 1),
(1335, 90, 'Faranah', 'FRN', 1),
(1336, 90, 'Forecariah', 'FRC', 1),
(1337, 90, 'Fria', 'FRI', 1),
(1338, 90, 'Gaoual', 'GAO', 1),
(1339, 90, 'Gueckedou', 'GCD', 1),
(1340, 90, 'Kankan', 'KNK', 1),
(1341, 90, 'Kerouane', 'KRN', 1),
(1342, 90, 'Kindia', 'KND', 1),
(1343, 90, 'Kissidougou', 'KSD', 1),
(1344, 90, 'Koubia', 'KBA', 1),
(1345, 90, 'Koundara', 'KDA', 1),
(1346, 90, 'Kouroussa', 'KRA', 1),
(1347, 90, 'Labe', 'LAB', 1),
(1348, 90, 'Lelouma', 'LLM', 1),
(1349, 90, 'Lola', 'LOL', 1),
(1350, 90, 'Macenta', 'MCT', 1),
(1351, 90, 'Mali', 'MAL', 1),
(1352, 90, 'Mamou', 'MAM', 1),
(1353, 90, 'Mandiana', 'MAN', 1),
(1354, 90, 'Nzerekore', 'NZR', 1),
(1355, 90, 'Pita', 'PIT', 1),
(1356, 90, 'Siguiri', 'SIG', 1),
(1357, 90, 'Telimele', 'TLM', 1),
(1358, 90, 'Tougue', 'TOG', 1),
(1359, 90, 'Yomou', 'YOM', 1),
(1360, 91, 'Bafata Region', 'BF', 1),
(1361, 91, 'Biombo Region', 'BB', 1),
(1362, 91, 'Bissau Region', 'BS', 1),
(1363, 91, 'Bolama Region', 'BL', 1),
(1364, 91, 'Cacheu Region', 'CA', 1),
(1365, 91, 'Gabu Region', 'GA', 1),
(1366, 91, 'Oio Region', 'OI', 1),
(1367, 91, 'Quinara Region', 'QU', 1),
(1368, 91, 'Tombali Region', 'TO', 1),
(1369, 92, 'Barima-Waini', 'BW', 1),
(1370, 92, 'Cuyuni-Mazaruni', 'CM', 1),
(1371, 92, 'Demerara-Mahaica', 'DM', 1),
(1372, 92, 'East Berbice-Corentyne', 'EC', 1),
(1373, 92, 'Essequibo Islands-West Demerara', 'EW', 1),
(1374, 92, 'Mahaica-Berbice', 'MB', 1),
(1375, 92, 'Pomeroon-Supenaam', 'PM', 1),
(1376, 92, 'Potaro-Siparuni', 'PI', 1),
(1377, 92, 'Upper Demerara-Berbice', 'UD', 1),
(1378, 92, 'Upper Takutu-Upper Essequibo', 'UT', 1),
(1379, 93, 'Artibonite', 'AR', 1),
(1380, 93, 'Centre', 'CE', 1),
(1381, 93, 'Grand\'Anse', 'GA', 1),
(1382, 93, 'Nord', 'ND', 1),
(1383, 93, 'Nord-Est', 'NE', 1),
(1384, 93, 'Nord-Ouest', 'NO', 1),
(1385, 93, 'Ouest', 'OU', 1),
(1386, 93, 'Sud', 'SD', 1),
(1387, 93, 'Sud-Est', 'SE', 1),
(1388, 94, 'Flat Island', 'F', 1),
(1389, 94, 'McDonald Island', 'M', 1),
(1390, 94, 'Shag Island', 'S', 1),
(1391, 94, 'Heard Island', 'H', 1),
(1392, 95, 'Atlantida', 'AT', 1),
(1393, 95, 'Choluteca', 'CH', 1),
(1394, 95, 'Colon', 'CL', 1),
(1395, 95, 'Comayagua', 'CM', 1),
(1396, 95, 'Copan', 'CP', 1),
(1397, 95, 'Cortes', 'CR', 1),
(1398, 95, 'El Paraiso', 'PA', 1),
(1399, 95, 'Francisco Morazan', 'FM', 1),
(1400, 95, 'Gracias a Dios', 'GD', 1),
(1401, 95, 'Intibuca', 'IN', 1),
(1402, 95, 'Islas de la Bahia (Bay Islands)', 'IB', 1),
(1403, 95, 'La Paz', 'PZ', 1),
(1404, 95, 'Lempira', 'LE', 1),
(1405, 95, 'Ocotepeque', 'OC', 1),
(1406, 95, 'Olancho', 'OL', 1),
(1407, 95, 'Santa Barbara', 'SB', 1),
(1408, 95, 'Valle', 'VA', 1),
(1409, 95, 'Yoro', 'YO', 1),
(1410, 96, 'Central and Western Hong Kong Island', 'HCW', 1),
(1411, 96, 'Eastern Hong Kong Island', 'HEA', 1),
(1412, 96, 'Southern Hong Kong Island', 'HSO', 1),
(1413, 96, 'Wan Chai Hong Kong Island', 'HWC', 1),
(1414, 96, 'Kowloon City Kowloon', 'KKC', 1),
(1415, 96, 'Kwun Tong Kowloon', 'KKT', 1),
(1416, 96, 'Sham Shui Po Kowloon', 'KSS', 1),
(1417, 96, 'Wong Tai Sin Kowloon', 'KWT', 1),
(1418, 96, 'Yau Tsim Mong Kowloon', 'KYT', 1),
(1419, 96, 'Islands New Territories', 'NIS', 1),
(1420, 96, 'Kwai Tsing New Territories', 'NKT', 1),
(1421, 96, 'North New Territories', 'NNO', 1),
(1422, 96, 'Sai Kung New Territories', 'NSK', 1),
(1423, 96, 'Sha Tin New Territories', 'NST', 1),
(1424, 96, 'Tai Po New Territories', 'NTP', 1),
(1425, 96, 'Tsuen Wan New Territories', 'NTW', 1),
(1426, 96, 'Tuen Mun New Territories', 'NTM', 1),
(1427, 96, 'Yuen Long New Territories', 'NYL', 1),
(1467, 98, 'Austurland', 'AL', 1),
(1468, 98, 'Hofuoborgarsvaeoi', 'HF', 1),
(1469, 98, 'Norourland eystra', 'NE', 1),
(1470, 98, 'Norourland vestra', 'NV', 1),
(1471, 98, 'Suourland', 'SL', 1),
(1472, 98, 'Suournes', 'SN', 1),
(1473, 98, 'Vestfiroir', 'VF', 1),
(1474, 98, 'Vesturland', 'VL', 1),
(1475, 99, 'Andaman and Nicobar Islands', 'AN', 1),
(1476, 99, 'Andhra Pradesh', 'AP', 1),
(1477, 99, 'Arunachal Pradesh', 'AR', 1),
(1478, 99, 'Assam', 'AS', 1),
(1479, 99, 'Bihar', 'BI', 1),
(1480, 99, 'Chandigarh', 'CH', 1),
(1481, 99, 'Dadra and Nagar Haveli', 'DA', 1),
(1482, 99, 'Daman and Diu', 'DM', 1),
(1483, 99, 'Delhi', 'DE', 1),
(1484, 99, 'Goa', 'GO', 1),
(1485, 99, 'Gujarat', 'GU', 1),
(1486, 99, 'Haryana', 'HA', 1),
(1487, 99, 'Himachal Pradesh', 'HP', 1),
(1488, 99, 'Jammu and Kashmir', 'JA', 1),
(1489, 99, 'Karnataka', 'KA', 1),
(1490, 99, 'Kerala', 'KE', 1),
(1491, 99, 'Lakshadweep Islands', 'LI', 1),
(1492, 99, 'Madhya Pradesh', 'MP', 1),
(1493, 99, 'Maharashtra', 'MA', 1),
(1494, 99, 'Manipur', 'MN', 1),
(1495, 99, 'Meghalaya', 'ME', 1),
(1496, 99, 'Mizoram', 'MI', 1),
(1497, 99, 'Nagaland', 'NA', 1),
(1498, 99, 'Orissa', 'OR', 1),
(1499, 99, 'Puducherry', 'PO', 1),
(1500, 99, 'Punjab', 'PU', 1),
(1501, 99, 'Rajasthan', 'RA', 1),
(1502, 99, 'Sikkim', 'SI', 1),
(1503, 99, 'Tamil Nadu', 'TN', 1),
(1504, 99, 'Tripura', 'TR', 1),
(1505, 99, 'Uttar Pradesh', 'UP', 1),
(1506, 99, 'West Bengal', 'WB', 1),
(1507, 100, 'Aceh', 'AC', 1),
(1508, 100, 'Bali', 'BA', 1),
(1509, 100, 'Banten', 'BT', 1),
(1510, 100, 'Bengkulu', 'BE', 1),
(1511, 100, 'Kalimantan Utara', 'BD', 1),
(1512, 100, 'Gorontalo', 'GO', 1),
(1513, 100, 'Jakarta', 'JK', 1),
(1514, 100, 'Jambi', 'JA', 1),
(1515, 100, 'Jawa Barat', 'JB', 1),
(1516, 100, 'Jawa Tengah', 'JT', 1),
(1517, 100, 'Jawa Timur', 'JI', 1),
(1518, 100, 'Kalimantan Barat', 'KB', 1),
(1519, 100, 'Kalimantan Selatan', 'KS', 1),
(1520, 100, 'Kalimantan Tengah', 'KT', 1),
(1521, 100, 'Kalimantan Timur', 'KI', 1),
(1522, 100, 'Kepulauan Bangka Belitung', 'BB', 1),
(1523, 100, 'Lampung', 'LA', 1),
(1524, 100, 'Maluku', 'MA', 1),
(1525, 100, 'Maluku Utara', 'MU', 1),
(1526, 100, 'Nusa Tenggara Barat', 'NB', 1),
(1527, 100, 'Nusa Tenggara Timur', 'NT', 1),
(1528, 100, 'Papua', 'PA', 1),
(1529, 100, 'Riau', 'RI', 1),
(1530, 100, 'Sulawesi Selatan', 'SN', 1),
(1531, 100, 'Sulawesi Tengah', 'ST', 1),
(1532, 100, 'Sulawesi Tenggara', 'SG', 1),
(1533, 100, 'Sulawesi Utara', 'SA', 1),
(1534, 100, 'Sumatera Barat', 'SB', 1),
(1535, 100, 'Sumatera Selatan', 'SS', 1),
(1536, 100, 'Sumatera Utara', 'SU', 1),
(1537, 100, 'Yogyakarta', 'YO', 1),
(1538, 101, 'Tehran', 'TEH', 1),
(1539, 101, 'Qom', 'QOM', 1),
(1540, 101, 'Markazi', 'MKZ', 1),
(1541, 101, 'Qazvin', 'QAZ', 1),
(1542, 101, 'Gilan', 'GIL', 1),
(1543, 101, 'Ardabil', 'ARD', 1),
(1544, 101, 'Zanjan', 'ZAN', 1),
(1545, 101, 'East Azarbaijan', 'EAZ', 1),
(1546, 101, 'West Azarbaijan', 'WEZ', 1),
(1547, 101, 'Kurdistan', 'KRD', 1),
(1548, 101, 'Hamadan', 'HMD', 1),
(1549, 101, 'Kermanshah', 'KRM', 1),
(1550, 101, 'Ilam', 'ILM', 1),
(1551, 101, 'Lorestan', 'LRS', 1),
(1552, 101, 'Khuzestan', 'KZT', 1),
(1553, 101, 'Chahar Mahaal and Bakhtiari', 'CMB', 1),
(1554, 101, 'Kohkiluyeh and Buyer Ahmad', 'KBA', 1),
(1555, 101, 'Bushehr', 'BSH', 1),
(1556, 101, 'Fars', 'FAR', 1),
(1557, 101, 'Hormozgan', 'HRM', 1),
(1558, 101, 'Sistan and Baluchistan', 'SBL', 1),
(1559, 101, 'Kerman', 'KRB', 1),
(1560, 101, 'Yazd', 'YZD', 1),
(1561, 101, 'Esfahan', 'EFH', 1),
(1562, 101, 'Semnan', 'SMN', 1),
(1563, 101, 'Mazandaran', 'MZD', 1),
(1564, 101, 'Golestan', 'GLS', 1),
(1565, 101, 'North Khorasan', 'NKH', 1),
(1566, 101, 'Razavi Khorasan', 'RKH', 1),
(1567, 101, 'South Khorasan', 'SKH', 1),
(1568, 102, 'Baghdad', 'BD', 1),
(1569, 102, 'Salah ad Din', 'SD', 1),
(1570, 102, 'Diyala', 'DY', 1),
(1571, 102, 'Wasit', 'WS', 1),
(1572, 102, 'Maysan', 'MY', 1),
(1573, 102, 'Al Basrah', 'BA', 1),
(1574, 102, 'Dhi Qar', 'DQ', 1),
(1575, 102, 'Al Muthanna', 'MU', 1),
(1576, 102, 'Al Qadisyah', 'QA', 1),
(1577, 102, 'Babil', 'BB', 1),
(1578, 102, 'Al Karbala', 'KB', 1),
(1579, 102, 'An Najaf', 'NJ', 1),
(1580, 102, 'Al Anbar', 'AB', 1),
(1581, 102, 'Ninawa', 'NN', 1),
(1582, 102, 'Dahuk', 'DH', 1),
(1583, 102, 'Arbil', 'AL', 1),
(1584, 102, 'At Ta\'mim', 'TM', 1),
(1585, 102, 'As Sulaymaniyah', 'SL', 1),
(1586, 103, 'Carlow', 'CA', 1),
(1587, 103, 'Cavan', 'CV', 1),
(1588, 103, 'Clare', 'CL', 1),
(1589, 103, 'Cork', 'CO', 1),
(1590, 103, 'Donegal', 'DO', 1),
(1591, 103, 'Dublin', 'DU', 1),
(1592, 103, 'Galway', 'GA', 1),
(1593, 103, 'Kerry', 'KE', 1),
(1594, 103, 'Kildare', 'KI', 1),
(1595, 103, 'Kilkenny', 'KL', 1),
(1596, 103, 'Laois', 'LA', 1),
(1597, 103, 'Leitrim', 'LE', 1);
INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1598, 103, 'Limerick', 'LI', 1),
(1599, 103, 'Longford', 'LO', 1),
(1600, 103, 'Louth', 'LU', 1),
(1601, 103, 'Mayo', 'MA', 1),
(1602, 103, 'Meath', 'ME', 1),
(1603, 103, 'Monaghan', 'MO', 1),
(1604, 103, 'Offaly', 'OF', 1),
(1605, 103, 'Roscommon', 'RO', 1),
(1606, 103, 'Sligo', 'SL', 1),
(1607, 103, 'Tipperary', 'TI', 1),
(1608, 103, 'Waterford', 'WA', 1),
(1609, 103, 'Westmeath', 'WE', 1),
(1610, 103, 'Wexford', 'WX', 1),
(1611, 103, 'Wicklow', 'WI', 1),
(1612, 104, 'Be\'er Sheva', 'BS', 1),
(1613, 104, 'Bika\'at Hayarden', 'BH', 1),
(1614, 104, 'Eilat and Arava', 'EA', 1),
(1615, 104, 'Galil', 'GA', 1),
(1616, 104, 'Haifa', 'HA', 1),
(1617, 104, 'Jehuda Mountains', 'JM', 1),
(1618, 104, 'Jerusalem', 'JE', 1),
(1619, 104, 'Negev', 'NE', 1),
(1620, 104, 'Semaria', 'SE', 1),
(1621, 104, 'Sharon', 'SH', 1),
(1622, 104, 'Tel Aviv (Gosh Dan)', 'TA', 1),
(3860, 105, 'Caltanissetta', 'CL', 1),
(3842, 105, 'Agrigento', 'AG', 1),
(3843, 105, 'Alessandria', 'AL', 1),
(3844, 105, 'Ancona', 'AN', 1),
(3845, 105, 'Aosta', 'AO', 1),
(3846, 105, 'Arezzo', 'AR', 1),
(3847, 105, 'Ascoli Piceno', 'AP', 1),
(3848, 105, 'Asti', 'AT', 1),
(3849, 105, 'Avellino', 'AV', 1),
(3850, 105, 'Bari', 'BA', 1),
(3851, 105, 'Belluno', 'BL', 1),
(3852, 105, 'Benevento', 'BN', 1),
(3853, 105, 'Bergamo', 'BG', 1),
(3854, 105, 'Biella', 'BI', 1),
(3855, 105, 'Bologna', 'BO', 1),
(3856, 105, 'Bolzano', 'BZ', 1),
(3857, 105, 'Brescia', 'BS', 1),
(3858, 105, 'Brindisi', 'BR', 1),
(3859, 105, 'Cagliari', 'CA', 1),
(1643, 106, 'Clarendon Parish', 'CLA', 1),
(1644, 106, 'Hanover Parish', 'HAN', 1),
(1645, 106, 'Kingston Parish', 'KIN', 1),
(1646, 106, 'Manchester Parish', 'MAN', 1),
(1647, 106, 'Portland Parish', 'POR', 1),
(1648, 106, 'Saint Andrew Parish', 'AND', 1),
(1649, 106, 'Saint Ann Parish', 'ANN', 1),
(1650, 106, 'Saint Catherine Parish', 'CAT', 1),
(1651, 106, 'Saint Elizabeth Parish', 'ELI', 1),
(1652, 106, 'Saint James Parish', 'JAM', 1),
(1653, 106, 'Saint Mary Parish', 'MAR', 1),
(1654, 106, 'Saint Thomas Parish', 'THO', 1),
(1655, 106, 'Trelawny Parish', 'TRL', 1),
(1656, 106, 'Westmoreland Parish', 'WML', 1),
(1657, 107, 'Aichi', 'AI', 1),
(1658, 107, 'Akita', 'AK', 1),
(1659, 107, 'Aomori', 'AO', 1),
(1660, 107, 'Chiba', 'CH', 1),
(1661, 107, 'Ehime', 'EH', 1),
(1662, 107, 'Fukui', 'FK', 1),
(1663, 107, 'Fukuoka', 'FU', 1),
(1664, 107, 'Fukushima', 'FS', 1),
(1665, 107, 'Gifu', 'GI', 1),
(1666, 107, 'Gumma', 'GU', 1),
(1667, 107, 'Hiroshima', 'HI', 1),
(1668, 107, 'Hokkaido', 'HO', 1),
(1669, 107, 'Hyogo', 'HY', 1),
(1670, 107, 'Ibaraki', 'IB', 1),
(1671, 107, 'Ishikawa', 'IS', 1),
(1672, 107, 'Iwate', 'IW', 1),
(1673, 107, 'Kagawa', 'KA', 1),
(1674, 107, 'Kagoshima', 'KG', 1),
(1675, 107, 'Kanagawa', 'KN', 1),
(1676, 107, 'Kochi', 'KO', 1),
(1677, 107, 'Kumamoto', 'KU', 1),
(1678, 107, 'Kyoto', 'KY', 1),
(1679, 107, 'Mie', 'MI', 1),
(1680, 107, 'Miyagi', 'MY', 1),
(1681, 107, 'Miyazaki', 'MZ', 1),
(1682, 107, 'Nagano', 'NA', 1),
(1683, 107, 'Nagasaki', 'NG', 1),
(1684, 107, 'Nara', 'NR', 1),
(1685, 107, 'Niigata', 'NI', 1),
(1686, 107, 'Oita', 'OI', 1),
(1687, 107, 'Okayama', 'OK', 1),
(1688, 107, 'Okinawa', 'ON', 1),
(1689, 107, 'Osaka', 'OS', 1),
(1690, 107, 'Saga', 'SA', 1),
(1691, 107, 'Saitama', 'SI', 1),
(1692, 107, 'Shiga', 'SH', 1),
(1693, 107, 'Shimane', 'SM', 1),
(1694, 107, 'Shizuoka', 'SZ', 1),
(1695, 107, 'Tochigi', 'TO', 1),
(1696, 107, 'Tokushima', 'TS', 1),
(1697, 107, 'Tokyo', 'TK', 1),
(1698, 107, 'Tottori', 'TT', 1),
(1699, 107, 'Toyama', 'TY', 1),
(1700, 107, 'Wakayama', 'WA', 1),
(1701, 107, 'Yamagata', 'YA', 1),
(1702, 107, 'Yamaguchi', 'YM', 1),
(1703, 107, 'Yamanashi', 'YN', 1),
(1704, 108, '\'Amman', 'AM', 1),
(1705, 108, 'Ajlun', 'AJ', 1),
(1706, 108, 'Al \'Aqabah', 'AA', 1),
(1707, 108, 'Al Balqa\'', 'AB', 1),
(1708, 108, 'Al Karak', 'AK', 1),
(1709, 108, 'Al Mafraq', 'AL', 1),
(1710, 108, 'At Tafilah', 'AT', 1),
(1711, 108, 'Az Zarqa\'', 'AZ', 1),
(1712, 108, 'Irbid', 'IR', 1),
(1713, 108, 'Jarash', 'JA', 1),
(1714, 108, 'Ma\'an', 'MA', 1),
(1715, 108, 'Madaba', 'MD', 1),
(1716, 109, 'Almaty', 'AL', 1),
(1717, 109, 'Almaty City', 'AC', 1),
(1718, 109, 'Aqmola', 'AM', 1),
(1719, 109, 'Aqtobe', 'AQ', 1),
(1720, 109, 'Astana City', 'AS', 1),
(1721, 109, 'Atyrau', 'AT', 1),
(1722, 109, 'Batys Qazaqstan', 'BA', 1),
(1723, 109, 'Bayqongyr City', 'BY', 1),
(1724, 109, 'Mangghystau', 'MA', 1),
(1725, 109, 'Ongtustik Qazaqstan', 'ON', 1),
(1726, 109, 'Pavlodar', 'PA', 1),
(1727, 109, 'Qaraghandy', 'QA', 1),
(1728, 109, 'Qostanay', 'QO', 1),
(1729, 109, 'Qyzylorda', 'QY', 1),
(1730, 109, 'Shyghys Qazaqstan', 'SH', 1),
(1731, 109, 'Soltustik Qazaqstan', 'SO', 1),
(1732, 109, 'Zhambyl', 'ZH', 1),
(1733, 110, 'Central', 'CE', 1),
(1734, 110, 'Coast', 'CO', 1),
(1735, 110, 'Eastern', 'EA', 1),
(1736, 110, 'Nairobi Area', 'NA', 1),
(1737, 110, 'North Eastern', 'NE', 1),
(1738, 110, 'Nyanza', 'NY', 1),
(1739, 110, 'Rift Valley', 'RV', 1),
(1740, 110, 'Western', 'WE', 1),
(1741, 111, 'Abaiang', 'AG', 1),
(1742, 111, 'Abemama', 'AM', 1),
(1743, 111, 'Aranuka', 'AK', 1),
(1744, 111, 'Arorae', 'AO', 1),
(1745, 111, 'Banaba', 'BA', 1),
(1746, 111, 'Beru', 'BE', 1),
(1747, 111, 'Butaritari', 'bT', 1),
(1748, 111, 'Kanton', 'KA', 1),
(1749, 111, 'Kiritimati', 'KR', 1),
(1750, 111, 'Kuria', 'KU', 1),
(1751, 111, 'Maiana', 'MI', 1),
(1752, 111, 'Makin', 'MN', 1),
(1753, 111, 'Marakei', 'ME', 1),
(1754, 111, 'Nikunau', 'NI', 1),
(1755, 111, 'Nonouti', 'NO', 1),
(1756, 111, 'Onotoa', 'ON', 1),
(1757, 111, 'Tabiteuea', 'TT', 1),
(1758, 111, 'Tabuaeran', 'TR', 1),
(1759, 111, 'Tamana', 'TM', 1),
(1760, 111, 'Tarawa', 'TW', 1),
(1761, 111, 'Teraina', 'TE', 1),
(1762, 112, 'Chagang-do', 'CHA', 1),
(1763, 112, 'Hamgyong-bukto', 'HAB', 1),
(1764, 112, 'Hamgyong-namdo', 'HAN', 1),
(1765, 112, 'Hwanghae-bukto', 'HWB', 1),
(1766, 112, 'Hwanghae-namdo', 'HWN', 1),
(1767, 112, 'Kangwon-do', 'KAN', 1),
(1768, 112, 'P\'yongan-bukto', 'PYB', 1),
(1769, 112, 'P\'yongan-namdo', 'PYN', 1),
(1770, 112, 'Ryanggang-do (Yanggang-do)', 'YAN', 1),
(1771, 112, 'Rason Directly Governed City', 'NAJ', 1),
(1772, 112, 'P\'yongyang Special City', 'PYO', 1),
(1773, 113, 'Ch\'ungch\'ong-bukto', 'CO', 1),
(1774, 113, 'Ch\'ungch\'ong-namdo', 'CH', 1),
(1775, 113, 'Cheju-do', 'CD', 1),
(1776, 113, 'Cholla-bukto', 'CB', 1),
(1777, 113, 'Cholla-namdo', 'CN', 1),
(1778, 113, 'Inch\'on-gwangyoksi', 'IG', 1),
(1779, 113, 'Kangwon-do', 'KA', 1),
(1780, 113, 'Kwangju-gwangyoksi', 'KG', 1),
(1781, 113, 'Kyonggi-do', 'KD', 1),
(1782, 113, 'Kyongsang-bukto', 'KB', 1),
(1783, 113, 'Kyongsang-namdo', 'KN', 1),
(1784, 113, 'Pusan-gwangyoksi', 'PG', 1),
(1785, 113, 'Soul-t\'ukpyolsi', 'SO', 1),
(1786, 113, 'Taegu-gwangyoksi', 'TA', 1),
(1787, 113, 'Taejon-gwangyoksi', 'TG', 1),
(1788, 114, 'Al \'Asimah', 'AL', 1),
(1789, 114, 'Al Ahmadi', 'AA', 1),
(1790, 114, 'Al Farwaniyah', 'AF', 1),
(1791, 114, 'Al Jahra\'', 'AJ', 1),
(1792, 114, 'Hawalli', 'HA', 1),
(1793, 115, 'Bishkek', 'GB', 1),
(1794, 115, 'Batken', 'B', 1),
(1795, 115, 'Chu', 'C', 1),
(1796, 115, 'Jalal-Abad', 'J', 1),
(1797, 115, 'Naryn', 'N', 1),
(1798, 115, 'Osh', 'O', 1),
(1799, 115, 'Talas', 'T', 1),
(1800, 115, 'Ysyk-Kol', 'Y', 1),
(1801, 116, 'Vientiane', 'VT', 1),
(1802, 116, 'Attapu', 'AT', 1),
(1803, 116, 'Bokeo', 'BK', 1),
(1804, 116, 'Bolikhamxai', 'BL', 1),
(1805, 116, 'Champasak', 'CH', 1),
(1806, 116, 'Houaphan', 'HO', 1),
(1807, 116, 'Khammouan', 'KH', 1),
(1808, 116, 'Louang Namtha', 'LM', 1),
(1809, 116, 'Louangphabang', 'LP', 1),
(1810, 116, 'Oudomxai', 'OU', 1),
(1811, 116, 'Phongsali', 'PH', 1),
(1812, 116, 'Salavan', 'SL', 1),
(1813, 116, 'Savannakhet', 'SV', 1),
(1814, 116, 'Vientiane', 'VI', 1),
(1815, 116, 'Xaignabouli', 'XA', 1),
(1816, 116, 'Xekong', 'XE', 1),
(1817, 116, 'Xiangkhoang', 'XI', 1),
(1818, 116, 'Xaisomboun', 'XN', 1),
(1852, 119, 'Berea', 'BE', 1),
(1853, 119, 'Butha-Buthe', 'BB', 1),
(1854, 119, 'Leribe', 'LE', 1),
(1855, 119, 'Mafeteng', 'MF', 1),
(1856, 119, 'Maseru', 'MS', 1),
(1857, 119, 'Mohale\'s Hoek', 'MH', 1),
(1858, 119, 'Mokhotlong', 'MK', 1),
(1859, 119, 'Qacha\'s Nek', 'QN', 1),
(1860, 119, 'Quthing', 'QT', 1),
(1861, 119, 'Thaba-Tseka', 'TT', 1),
(1862, 120, 'Bomi', 'BI', 1),
(1863, 120, 'Bong', 'BG', 1),
(1864, 120, 'Grand Bassa', 'GB', 1),
(1865, 120, 'Grand Cape Mount', 'CM', 1),
(1866, 120, 'Grand Gedeh', 'GG', 1),
(1867, 120, 'Grand Kru', 'GK', 1),
(1868, 120, 'Lofa', 'LO', 1),
(1869, 120, 'Margibi', 'MG', 1),
(1870, 120, 'Maryland', 'ML', 1),
(1871, 120, 'Montserrado', 'MS', 1),
(1872, 120, 'Nimba', 'NB', 1),
(1873, 120, 'River Cess', 'RC', 1),
(1874, 120, 'Sinoe', 'SN', 1),
(1875, 121, 'Ajdabiya', 'AJ', 1),
(1876, 121, 'Al \'Aziziyah', 'AZ', 1),
(1877, 121, 'Al Fatih', 'FA', 1),
(1878, 121, 'Al Jabal al Akhdar', 'JA', 1),
(1879, 121, 'Al Jufrah', 'JU', 1),
(1880, 121, 'Al Khums', 'KH', 1),
(1881, 121, 'Al Kufrah', 'KU', 1),
(1882, 121, 'An Nuqat al Khams', 'NK', 1),
(1883, 121, 'Ash Shati\'', 'AS', 1),
(1884, 121, 'Awbari', 'AW', 1),
(1885, 121, 'Az Zawiyah', 'ZA', 1),
(1886, 121, 'Banghazi', 'BA', 1),
(1887, 121, 'Darnah', 'DA', 1),
(1888, 121, 'Ghadamis', 'GD', 1),
(1889, 121, 'Gharyan', 'GY', 1),
(1890, 121, 'Misratah', 'MI', 1),
(1891, 121, 'Murzuq', 'MZ', 1),
(1892, 121, 'Sabha', 'SB', 1),
(1893, 121, 'Sawfajjin', 'SW', 1),
(1894, 121, 'Surt', 'SU', 1),
(1895, 121, 'Tarabulus (Tripoli)', 'TL', 1),
(1896, 121, 'Tarhunah', 'TH', 1),
(1897, 121, 'Tubruq', 'TU', 1),
(1898, 121, 'Yafran', 'YA', 1),
(1899, 121, 'Zlitan', 'ZL', 1),
(1900, 122, 'Vaduz', 'V', 1),
(1901, 122, 'Schaan', 'A', 1),
(1902, 122, 'Balzers', 'B', 1),
(1903, 122, 'Triesen', 'N', 1),
(1904, 122, 'Eschen', 'E', 1),
(1905, 122, 'Mauren', 'M', 1),
(1906, 122, 'Triesenberg', 'T', 1),
(1907, 122, 'Ruggell', 'R', 1),
(1908, 122, 'Gamprin', 'G', 1),
(1909, 122, 'Schellenberg', 'L', 1),
(1910, 122, 'Planken', 'P', 1),
(1911, 123, 'Alytus', 'AL', 1),
(1912, 123, 'Kaunas', 'KA', 1),
(1913, 123, 'Klaipeda', 'KL', 1),
(1914, 123, 'Marijampole', 'MA', 1),
(1915, 123, 'Panevezys', 'PA', 1),
(1916, 123, 'Siauliai', 'SI', 1),
(1917, 123, 'Taurage', 'TA', 1),
(1918, 123, 'Telsiai', 'TE', 1),
(1919, 123, 'Utena', 'UT', 1),
(1920, 123, 'Vilnius', 'VI', 1),
(1921, 124, 'Diekirch', 'DD', 1),
(1922, 124, 'Clervaux', 'DC', 1),
(1923, 124, 'Redange', 'DR', 1),
(1924, 124, 'Vianden', 'DV', 1),
(1925, 124, 'Wiltz', 'DW', 1),
(1926, 124, 'Grevenmacher', 'GG', 1),
(1927, 124, 'Echternach', 'GE', 1),
(1928, 124, 'Remich', 'GR', 1),
(1929, 124, 'Luxembourg', 'LL', 1),
(1930, 124, 'Capellen', 'LC', 1),
(1931, 124, 'Esch-sur-Alzette', 'LE', 1),
(1932, 124, 'Mersch', 'LM', 1),
(1933, 125, 'Our Lady Fatima Parish', 'OLF', 1),
(1934, 125, 'St. Anthony Parish', 'ANT', 1),
(1935, 125, 'St. Lazarus Parish', 'LAZ', 1),
(1936, 125, 'Cathedral Parish', 'CAT', 1),
(1937, 125, 'St. Lawrence Parish', 'LAW', 1),
(1938, 127, 'Antananarivo', 'AN', 1),
(1939, 127, 'Antsiranana', 'AS', 1),
(1940, 127, 'Fianarantsoa', 'FN', 1),
(1941, 127, 'Mahajanga', 'MJ', 1),
(1942, 127, 'Toamasina', 'TM', 1),
(1943, 127, 'Toliara', 'TL', 1),
(1944, 128, 'Balaka', 'BLK', 1),
(1945, 128, 'Blantyre', 'BLT', 1),
(1946, 128, 'Chikwawa', 'CKW', 1),
(1947, 128, 'Chiradzulu', 'CRD', 1),
(1948, 128, 'Chitipa', 'CTP', 1),
(1949, 128, 'Dedza', 'DDZ', 1),
(1950, 128, 'Dowa', 'DWA', 1),
(1951, 128, 'Karonga', 'KRG', 1),
(1952, 128, 'Kasungu', 'KSG', 1),
(1953, 128, 'Likoma', 'LKM', 1),
(1954, 128, 'Lilongwe', 'LLG', 1),
(1955, 128, 'Machinga', 'MCG', 1),
(1956, 128, 'Mangochi', 'MGC', 1),
(1957, 128, 'Mchinji', 'MCH', 1),
(1958, 128, 'Mulanje', 'MLJ', 1),
(1959, 128, 'Mwanza', 'MWZ', 1),
(1960, 128, 'Mzimba', 'MZM', 1),
(1961, 128, 'Ntcheu', 'NTU', 1),
(1962, 128, 'Nkhata Bay', 'NKB', 1),
(1963, 128, 'Nkhotakota', 'NKH', 1),
(1964, 128, 'Nsanje', 'NSJ', 1),
(1965, 128, 'Ntchisi', 'NTI', 1),
(1966, 128, 'Phalombe', 'PHL', 1),
(1967, 128, 'Rumphi', 'RMP', 1),
(1968, 128, 'Salima', 'SLM', 1),
(1969, 128, 'Thyolo', 'THY', 1),
(1970, 128, 'Zomba', 'ZBA', 1),
(1971, 129, 'Johor', 'MY-01', 1),
(1972, 129, 'Kedah', 'MY-02', 1),
(1973, 129, 'Kelantan', 'MY-03', 1),
(1974, 129, 'Labuan', 'MY-15', 1),
(1975, 129, 'Melaka', 'MY-04', 1),
(1976, 129, 'Negeri Sembilan', 'MY-05', 1),
(1977, 129, 'Pahang', 'MY-06', 1),
(1978, 129, 'Perak', 'MY-08', 1),
(1979, 129, 'Perlis', 'MY-09', 1),
(1980, 129, 'Pulau Pinang', 'MY-07', 1),
(1981, 129, 'Sabah', 'MY-12', 1),
(1982, 129, 'Sarawak', 'MY-13', 1),
(1983, 129, 'Selangor', 'MY-10', 1),
(1984, 129, 'Terengganu', 'MY-11', 1),
(1985, 129, 'Kuala Lumpur', 'MY-14', 1),
(4035, 129, 'Putrajaya', 'MY-16', 1),
(1986, 130, 'Thiladhunmathi Uthuru', 'THU', 1),
(1987, 130, 'Thiladhunmathi Dhekunu', 'THD', 1),
(1988, 130, 'Miladhunmadulu Uthuru', 'MLU', 1),
(1989, 130, 'Miladhunmadulu Dhekunu', 'MLD', 1),
(1990, 130, 'Maalhosmadulu Uthuru', 'MAU', 1),
(1991, 130, 'Maalhosmadulu Dhekunu', 'MAD', 1),
(1992, 130, 'Faadhippolhu', 'FAA', 1),
(1993, 130, 'Male Atoll', 'MAA', 1),
(1994, 130, 'Ari Atoll Uthuru', 'AAU', 1),
(1995, 130, 'Ari Atoll Dheknu', 'AAD', 1),
(1996, 130, 'Felidhe Atoll', 'FEA', 1),
(1997, 130, 'Mulaku Atoll', 'MUA', 1),
(1998, 130, 'Nilandhe Atoll Uthuru', 'NAU', 1),
(1999, 130, 'Nilandhe Atoll Dhekunu', 'NAD', 1),
(2000, 130, 'Kolhumadulu', 'KLH', 1),
(2001, 130, 'Hadhdhunmathi', 'HDH', 1),
(2002, 130, 'Huvadhu Atoll Uthuru', 'HAU', 1),
(2003, 130, 'Huvadhu Atoll Dhekunu', 'HAD', 1),
(2004, 130, 'Fua Mulaku', 'FMU', 1),
(2005, 130, 'Addu', 'ADD', 1),
(2006, 131, 'Gao', 'GA', 1),
(2007, 131, 'Kayes', 'KY', 1),
(2008, 131, 'Kidal', 'KD', 1),
(2009, 131, 'Koulikoro', 'KL', 1),
(2010, 131, 'Mopti', 'MP', 1),
(2011, 131, 'Segou', 'SG', 1),
(2012, 131, 'Sikasso', 'SK', 1),
(2013, 131, 'Tombouctou', 'TB', 1),
(2014, 131, 'Bamako Capital District', 'CD', 1),
(2015, 132, 'Attard', 'ATT', 1),
(2016, 132, 'Balzan', 'BAL', 1),
(2017, 132, 'Birgu', 'BGU', 1),
(2018, 132, 'Birkirkara', 'BKK', 1),
(2019, 132, 'Birzebbuga', 'BRZ', 1),
(2020, 132, 'Bormla', 'BOR', 1),
(2021, 132, 'Dingli', 'DIN', 1),
(2022, 132, 'Fgura', 'FGU', 1),
(2023, 132, 'Floriana', 'FLO', 1),
(2024, 132, 'Gudja', 'GDJ', 1),
(2025, 132, 'Gzira', 'GZR', 1),
(2026, 132, 'Gargur', 'GRG', 1),
(2027, 132, 'Gaxaq', 'GXQ', 1),
(2028, 132, 'Hamrun', 'HMR', 1),
(2029, 132, 'Iklin', 'IKL', 1),
(2030, 132, 'Isla', 'ISL', 1),
(2031, 132, 'Kalkara', 'KLK', 1),
(2032, 132, 'Kirkop', 'KRK', 1),
(2033, 132, 'Lija', 'LIJ', 1),
(2034, 132, 'Luqa', 'LUQ', 1),
(2035, 132, 'Marsa', 'MRS', 1),
(2036, 132, 'Marsaskala', 'MKL', 1),
(2037, 132, 'Marsaxlokk', 'MXL', 1),
(2038, 132, 'Mdina', 'MDN', 1),
(2039, 132, 'Melliea', 'MEL', 1),
(2040, 132, 'Mgarr', 'MGR', 1),
(2041, 132, 'Mosta', 'MST', 1),
(2042, 132, 'Mqabba', 'MQA', 1),
(2043, 132, 'Msida', 'MSI', 1),
(2044, 132, 'Mtarfa', 'MTF', 1),
(2045, 132, 'Naxxar', 'NAX', 1),
(2046, 132, 'Paola', 'PAO', 1),
(2047, 132, 'Pembroke', 'PEM', 1),
(2048, 132, 'Pieta', 'PIE', 1),
(2049, 132, 'Qormi', 'QOR', 1),
(2050, 132, 'Qrendi', 'QRE', 1),
(2051, 132, 'Rabat', 'RAB', 1),
(2052, 132, 'Safi', 'SAF', 1),
(2053, 132, 'San Giljan', 'SGI', 1),
(2054, 132, 'Santa Lucija', 'SLU', 1),
(2055, 132, 'San Pawl il-Bahar', 'SPB', 1),
(2056, 132, 'San Gwann', 'SGW', 1),
(2057, 132, 'Santa Venera', 'SVE', 1),
(2058, 132, 'Siggiewi', 'SIG', 1),
(2059, 132, 'Sliema', 'SLM', 1),
(2060, 132, 'Swieqi', 'SWQ', 1),
(2061, 132, 'Ta Xbiex', 'TXB', 1),
(2062, 132, 'Tarxien', 'TRX', 1),
(2063, 132, 'Valletta', 'VLT', 1),
(2064, 132, 'Xgajra', 'XGJ', 1),
(2065, 132, 'Zabbar', 'ZBR', 1),
(2066, 132, 'Zebbug', 'ZBG', 1),
(2067, 132, 'Zejtun', 'ZJT', 1),
(2068, 132, 'Zurrieq', 'ZRQ', 1),
(2069, 132, 'Fontana', 'FNT', 1),
(2070, 132, 'Ghajnsielem', 'GHJ', 1),
(2071, 132, 'Gharb', 'GHR', 1),
(2072, 132, 'Ghasri', 'GHS', 1),
(2073, 132, 'Kercem', 'KRC', 1),
(2074, 132, 'Munxar', 'MUN', 1),
(2075, 132, 'Nadur', 'NAD', 1),
(2076, 132, 'Qala', 'QAL', 1),
(2077, 132, 'Victoria', 'VIC', 1),
(2078, 132, 'San Lawrenz', 'SLA', 1),
(2079, 132, 'Sannat', 'SNT', 1),
(2080, 132, 'Xagra', 'ZAG', 1),
(2081, 132, 'Xewkija', 'XEW', 1),
(2082, 132, 'Zebbug', 'ZEB', 1),
(2083, 133, 'Ailinginae', 'ALG', 1),
(2084, 133, 'Ailinglaplap', 'ALL', 1),
(2085, 133, 'Ailuk', 'ALK', 1),
(2086, 133, 'Arno', 'ARN', 1),
(2087, 133, 'Aur', 'AUR', 1),
(2088, 133, 'Bikar', 'BKR', 1),
(2089, 133, 'Bikini', 'BKN', 1),
(2090, 133, 'Bokak', 'BKK', 1),
(2091, 133, 'Ebon', 'EBN', 1),
(2092, 133, 'Enewetak', 'ENT', 1),
(2093, 133, 'Erikub', 'EKB', 1),
(2094, 133, 'Jabat', 'JBT', 1),
(2095, 133, 'Jaluit', 'JLT', 1),
(2096, 133, 'Jemo', 'JEM', 1),
(2097, 133, 'Kili', 'KIL', 1),
(2098, 133, 'Kwajalein', 'KWJ', 1),
(2099, 133, 'Lae', 'LAE', 1),
(2100, 133, 'Lib', 'LIB', 1),
(2101, 133, 'Likiep', 'LKP', 1),
(2102, 133, 'Majuro', 'MJR', 1),
(2103, 133, 'Maloelap', 'MLP', 1),
(2104, 133, 'Mejit', 'MJT', 1),
(2105, 133, 'Mili', 'MIL', 1),
(2106, 133, 'Namorik', 'NMK', 1),
(2107, 133, 'Namu', 'NAM', 1),
(2108, 133, 'Rongelap', 'RGL', 1),
(2109, 133, 'Rongrik', 'RGK', 1),
(2110, 133, 'Toke', 'TOK', 1),
(2111, 133, 'Ujae', 'UJA', 1),
(2112, 133, 'Ujelang', 'UJL', 1),
(2113, 133, 'Utirik', 'UTK', 1),
(2114, 133, 'Wotho', 'WTH', 1),
(2115, 133, 'Wotje', 'WTJ', 1),
(2116, 135, 'Adrar', 'AD', 1),
(2117, 135, 'Assaba', 'AS', 1),
(2118, 135, 'Brakna', 'BR', 1),
(2119, 135, 'Dakhlet Nouadhibou', 'DN', 1),
(2120, 135, 'Gorgol', 'GO', 1),
(2121, 135, 'Guidimaka', 'GM', 1),
(2122, 135, 'Hodh Ech Chargui', 'HC', 1),
(2123, 135, 'Hodh El Gharbi', 'HG', 1),
(2124, 135, 'Inchiri', 'IN', 1),
(2125, 135, 'Tagant', 'TA', 1),
(2126, 135, 'Tiris Zemmour', 'TZ', 1),
(2127, 135, 'Trarza', 'TR', 1),
(2128, 135, 'Nouakchott', 'NO', 1),
(2129, 136, 'Beau Bassin-Rose Hill', 'BR', 1),
(2130, 136, 'Curepipe', 'CU', 1),
(2131, 136, 'Port Louis', 'PU', 1),
(2132, 136, 'Quatre Bornes', 'QB', 1),
(2133, 136, 'Vacoas-Phoenix', 'VP', 1),
(2134, 136, 'Agalega Islands', 'AG', 1),
(2135, 136, 'Cargados Carajos Shoals (Saint Brandon Islands)', 'CC', 1),
(2136, 136, 'Rodrigues', 'RO', 1),
(2137, 136, 'Black River', 'BL', 1),
(2138, 136, 'Flacq', 'FL', 1),
(2139, 136, 'Grand Port', 'GP', 1),
(2140, 136, 'Moka', 'MO', 1),
(2141, 136, 'Pamplemousses', 'PA', 1),
(2142, 136, 'Plaines Wilhems', 'PW', 1),
(2143, 136, 'Port Louis', 'PL', 1),
(2144, 136, 'Riviere du Rempart', 'RR', 1),
(2145, 136, 'Savanne', 'SA', 1),
(2146, 138, 'Baja California Norte', 'BN', 1),
(2147, 138, 'Baja California Sur', 'BS', 1),
(2148, 138, 'Campeche', 'CA', 1),
(2149, 138, 'Chiapas', 'CI', 1),
(2150, 138, 'Chihuahua', 'CH', 1),
(2151, 138, 'Coahuila de Zaragoza', 'CZ', 1),
(2152, 138, 'Colima', 'CL', 1),
(2153, 138, 'Distrito Federal', 'DF', 1),
(2154, 138, 'Durango', 'DU', 1),
(2155, 138, 'Guanajuato', 'GA', 1),
(2156, 138, 'Guerrero', 'GE', 1),
(2157, 138, 'Hidalgo', 'HI', 1),
(2158, 138, 'Jalisco', 'JA', 1),
(2159, 138, 'Mexico', 'ME', 1),
(2160, 138, 'Michoacan de Ocampo', 'MI', 1),
(2161, 138, 'Morelos', 'MO', 1),
(2162, 138, 'Nayarit', 'NA', 1),
(2163, 138, 'Nuevo Leon', 'NL', 1),
(2164, 138, 'Oaxaca', 'OA', 1),
(2165, 138, 'Puebla', 'PU', 1),
(2166, 138, 'Queretaro de Arteaga', 'QA', 1),
(2167, 138, 'Quintana Roo', 'QR', 1),
(2168, 138, 'San Luis Potosi', 'SA', 1),
(2169, 138, 'Sinaloa', 'SI', 1),
(2170, 138, 'Sonora', 'SO', 1),
(2171, 138, 'Tabasco', 'TB', 1),
(2172, 138, 'Tamaulipas', 'TM', 1),
(2173, 138, 'Tlaxcala', 'TL', 1),
(2174, 138, 'Veracruz-Llave', 'VE', 1),
(2175, 138, 'Yucatan', 'YU', 1),
(2176, 138, 'Zacatecas', 'ZA', 1),
(2177, 139, 'Chuuk', 'C', 1),
(2178, 139, 'Kosrae', 'K', 1),
(2179, 139, 'Pohnpei', 'P', 1),
(2180, 139, 'Yap', 'Y', 1),
(2181, 140, 'Gagauzia', 'GA', 1),
(2182, 140, 'Chisinau', 'CU', 1),
(2183, 140, 'Balti', 'BA', 1),
(2184, 140, 'Cahul', 'CA', 1),
(2185, 140, 'Edinet', 'ED', 1),
(2186, 140, 'Lapusna', 'LA', 1),
(2187, 140, 'Orhei', 'OR', 1),
(2188, 140, 'Soroca', 'SO', 1),
(2189, 140, 'Tighina', 'TI', 1),
(2190, 140, 'Ungheni', 'UN', 1),
(2191, 140, 'St‚nga Nistrului', 'SN', 1),
(2192, 141, 'Fontvieille', 'FV', 1),
(2193, 141, 'La Condamine', 'LC', 1),
(2194, 141, 'Monaco-Ville', 'MV', 1),
(2195, 141, 'Monte-Carlo', 'MC', 1),
(2196, 142, 'Ulanbaatar', '1', 1),
(2197, 142, 'Orhon', '035', 1),
(2198, 142, 'Darhan uul', '037', 1),
(2199, 142, 'Hentiy', '039', 1),
(2200, 142, 'Hovsgol', '041', 1),
(2201, 142, 'Hovd', '043', 1),
(2202, 142, 'Uvs', '046', 1),
(2203, 142, 'Tov', '047', 1),
(2204, 142, 'Selenge', '049', 1),
(2205, 142, 'Suhbaatar', '051', 1),
(2206, 142, 'Omnogovi', '053', 1),
(2207, 142, 'Ovorhangay', '055', 1),
(2208, 142, 'Dzavhan', '057', 1),
(2209, 142, 'DundgovL', '059', 1),
(2210, 142, 'Dornod', '061', 1),
(2211, 142, 'Dornogov', '063', 1),
(2212, 142, 'Govi-Sumber', '064', 1),
(2213, 142, 'Govi-Altay', '065', 1),
(2214, 142, 'Bulgan', '067', 1),
(2215, 142, 'Bayanhongor', '069', 1),
(2216, 142, 'Bayan-Olgiy', '071', 1),
(2217, 142, 'Arhangay', '073', 1),
(2218, 143, 'Saint Anthony', 'A', 1),
(2219, 143, 'Saint Georges', 'G', 1),
(2220, 143, 'Saint Peter', 'P', 1),
(2221, 144, 'Agadir', 'AGD', 1),
(2222, 144, 'Al Hoceima', 'HOC', 1),
(2223, 144, 'Azilal', 'AZI', 1),
(2224, 144, 'Beni Mellal', 'BME', 1),
(2225, 144, 'Ben Slimane', 'BSL', 1),
(2226, 144, 'Boulemane', 'BLM', 1),
(2227, 144, 'Casablanca', 'CBL', 1),
(2228, 144, 'Chaouen', 'CHA', 1),
(2229, 144, 'El Jadida', 'EJA', 1),
(2230, 144, 'El Kelaa des Sraghna', 'EKS', 1),
(2231, 144, 'Er Rachidia', 'ERA', 1),
(2232, 144, 'Essaouira', 'ESS', 1),
(2233, 144, 'Fes', 'FES', 1),
(2234, 144, 'Figuig', 'FIG', 1),
(2235, 144, 'Guelmim', 'GLM', 1),
(2236, 144, 'Ifrane', 'IFR', 1),
(2237, 144, 'Kenitra', 'KEN', 1),
(2238, 144, 'Khemisset', 'KHM', 1),
(2239, 144, 'Khenifra', 'KHN', 1),
(2240, 144, 'Khouribga', 'KHO', 1),
(2241, 144, 'Laayoune', 'LYN', 1),
(2242, 144, 'Larache', 'LAR', 1),
(2243, 144, 'Marrakech', 'MRK', 1),
(2244, 144, 'Meknes', 'MKN', 1),
(2245, 144, 'Nador', 'NAD', 1),
(2246, 144, 'Ouarzazate', 'ORZ', 1),
(2247, 144, 'Oujda', 'OUJ', 1),
(2248, 144, 'Rabat-Sale', 'RSA', 1),
(2249, 144, 'Safi', 'SAF', 1),
(2250, 144, 'Settat', 'SET', 1),
(2251, 144, 'Sidi Kacem', 'SKA', 1),
(2252, 144, 'Tangier', 'TGR', 1),
(2253, 144, 'Tan-Tan', 'TAN', 1),
(2254, 144, 'Taounate', 'TAO', 1),
(2255, 144, 'Taroudannt', 'TRD', 1),
(2256, 144, 'Tata', 'TAT', 1),
(2257, 144, 'Taza', 'TAZ', 1),
(2258, 144, 'Tetouan', 'TET', 1),
(2259, 144, 'Tiznit', 'TIZ', 1),
(2260, 144, 'Ad Dakhla', 'ADK', 1),
(2261, 144, 'Boujdour', 'BJD', 1),
(2262, 144, 'Es Smara', 'ESM', 1),
(2263, 145, 'Cabo Delgado', 'CD', 1),
(2264, 145, 'Gaza', 'GZ', 1),
(2265, 145, 'Inhambane', 'IN', 1),
(2266, 145, 'Manica', 'MN', 1),
(2267, 145, 'Maputo (city)', 'MC', 1),
(2268, 145, 'Maputo', 'MP', 1),
(2269, 145, 'Nampula', 'NA', 1),
(2270, 145, 'Niassa', 'NI', 1),
(2271, 145, 'Sofala', 'SO', 1),
(2272, 145, 'Tete', 'TE', 1),
(2273, 145, 'Zambezia', 'ZA', 1),
(2274, 146, 'Ayeyarwady', 'AY', 1),
(2275, 146, 'Bago', 'BG', 1),
(2276, 146, 'Magway', 'MG', 1),
(2277, 146, 'Mandalay', 'MD', 1),
(2278, 146, 'Sagaing', 'SG', 1),
(2279, 146, 'Tanintharyi', 'TN', 1),
(2280, 146, 'Yangon', 'YG', 1),
(2281, 146, 'Chin State', 'CH', 1),
(2282, 146, 'Kachin State', 'KC', 1),
(2283, 146, 'Kayah State', 'KH', 1),
(2284, 146, 'Kayin State', 'KN', 1),
(2285, 146, 'Mon State', 'MN', 1),
(2286, 146, 'Rakhine State', 'RK', 1),
(2287, 146, 'Shan State', 'SH', 1),
(2288, 147, 'Caprivi', 'CA', 1),
(2289, 147, 'Erongo', 'ER', 1),
(2290, 147, 'Hardap', 'HA', 1),
(2291, 147, 'Karas', 'KR', 1),
(2292, 147, 'Kavango', 'KV', 1),
(2293, 147, 'Khomas', 'KH', 1),
(2294, 147, 'Kunene', 'KU', 1),
(2295, 147, 'Ohangwena', 'OW', 1),
(2296, 147, 'Omaheke', 'OK', 1),
(2297, 147, 'Omusati', 'OT', 1),
(2298, 147, 'Oshana', 'ON', 1),
(2299, 147, 'Oshikoto', 'OO', 1),
(2300, 147, 'Otjozondjupa', 'OJ', 1),
(2301, 148, 'Aiwo', 'AO', 1),
(2302, 148, 'Anabar', 'AA', 1),
(2303, 148, 'Anetan', 'AT', 1),
(2304, 148, 'Anibare', 'AI', 1),
(2305, 148, 'Baiti', 'BA', 1),
(2306, 148, 'Boe', 'BO', 1),
(2307, 148, 'Buada', 'BU', 1),
(2308, 148, 'Denigomodu', 'DE', 1),
(2309, 148, 'Ewa', 'EW', 1),
(2310, 148, 'Ijuw', 'IJ', 1),
(2311, 148, 'Meneng', 'ME', 1),
(2312, 148, 'Nibok', 'NI', 1),
(2313, 148, 'Uaboe', 'UA', 1),
(2314, 148, 'Yaren', 'YA', 1),
(2315, 149, 'Bagmati', 'BA', 1),
(2316, 149, 'Bheri', 'BH', 1),
(2317, 149, 'Dhawalagiri', 'DH', 1),
(2318, 149, 'Gandaki', 'GA', 1),
(2319, 149, 'Janakpur', 'JA', 1),
(2320, 149, 'Karnali', 'KA', 1),
(2321, 149, 'Kosi', 'KO', 1),
(2322, 149, 'Lumbini', 'LU', 1),
(2323, 149, 'Mahakali', 'MA', 1),
(2324, 149, 'Mechi', 'ME', 1),
(2325, 149, 'Narayani', 'NA', 1),
(2326, 149, 'Rapti', 'RA', 1),
(2327, 149, 'Sagarmatha', 'SA', 1),
(2328, 149, 'Seti', 'SE', 1),
(2329, 150, 'Drenthe', 'DR', 1),
(2330, 150, 'Flevoland', 'FL', 1),
(2331, 150, 'Friesland', 'FR', 1),
(2332, 150, 'Gelderland', 'GE', 1),
(2333, 150, 'Groningen', 'GR', 1),
(2334, 150, 'Limburg', 'LI', 1),
(2335, 150, 'Noord-Brabant', 'NB', 1),
(2336, 150, 'Noord-Holland', 'NH', 1),
(2337, 150, 'Overijssel', 'OV', 1),
(2338, 150, 'Utrecht', 'UT', 1),
(2339, 150, 'Zeeland', 'ZE', 1),
(2340, 150, 'Zuid-Holland', 'ZH', 1),
(2341, 152, 'Iles Loyaute', 'L', 1),
(2342, 152, 'Nord', 'N', 1),
(2343, 152, 'Sud', 'S', 1),
(2344, 153, 'Auckland', 'AUK', 1),
(2345, 153, 'Bay of Plenty', 'BOP', 1),
(2346, 153, 'Canterbury', 'CAN', 1),
(2347, 153, 'Coromandel', 'COR', 1),
(2348, 153, 'Gisborne', 'GIS', 1),
(2349, 153, 'Fiordland', 'FIO', 1),
(2350, 153, 'Hawke\'s Bay', 'HKB', 1),
(2351, 153, 'Marlborough', 'MBH', 1),
(2352, 153, 'Manawatu-Wanganui', 'MWT', 1),
(2353, 153, 'Mt Cook-Mackenzie', 'MCM', 1),
(2354, 153, 'Nelson', 'NSN', 1),
(2355, 153, 'Northland', 'NTL', 1),
(2356, 153, 'Otago', 'OTA', 1),
(2357, 153, 'Southland', 'STL', 1),
(2358, 153, 'Taranaki', 'TKI', 1),
(2359, 153, 'Wellington', 'WGN', 1),
(2360, 153, 'Waikato', 'WKO', 1),
(2361, 153, 'Wairarapa', 'WAI', 1),
(2362, 153, 'West Coast', 'WTC', 1),
(2363, 154, 'Atlantico Norte', 'AN', 1),
(2364, 154, 'Atlantico Sur', 'AS', 1),
(2365, 154, 'Boaco', 'BO', 1),
(2366, 154, 'Carazo', 'CA', 1),
(2367, 154, 'Chinandega', 'CI', 1),
(2368, 154, 'Chontales', 'CO', 1),
(2369, 154, 'Esteli', 'ES', 1),
(2370, 154, 'Granada', 'GR', 1),
(2371, 154, 'Jinotega', 'JI', 1),
(2372, 154, 'Leon', 'LE', 1),
(2373, 154, 'Madriz', 'MD', 1),
(2374, 154, 'Managua', 'MN', 1),
(2375, 154, 'Masaya', 'MS', 1),
(2376, 154, 'Matagalpa', 'MT', 1),
(2377, 154, 'Nuevo Segovia', 'NS', 1),
(2378, 154, 'Rio San Juan', 'RS', 1),
(2379, 154, 'Rivas', 'RI', 1),
(2380, 155, 'Agadez', 'AG', 1),
(2381, 155, 'Diffa', 'DF', 1),
(2382, 155, 'Dosso', 'DS', 1),
(2383, 155, 'Maradi', 'MA', 1),
(2384, 155, 'Niamey', 'NM', 1),
(2385, 155, 'Tahoua', 'TH', 1),
(2386, 155, 'Tillaberi', 'TL', 1),
(2387, 155, 'Zinder', 'ZD', 1),
(2388, 156, 'Abia', 'AB', 1),
(2389, 156, 'Abuja Federal Capital Territory', 'CT', 1),
(2390, 156, 'Adamawa', 'AD', 1),
(2391, 156, 'Akwa Ibom', 'AK', 1),
(2392, 156, 'Anambra', 'AN', 1),
(2393, 156, 'Bauchi', 'BC', 1),
(2394, 156, 'Bayelsa', 'BY', 1),
(2395, 156, 'Benue', 'BN', 1),
(2396, 156, 'Borno', 'BO', 1),
(2397, 156, 'Cross River', 'CR', 1),
(2398, 156, 'Delta', 'DE', 1),
(2399, 156, 'Ebonyi', 'EB', 1),
(2400, 156, 'Edo', 'ED', 1),
(2401, 156, 'Ekiti', 'EK', 1),
(2402, 156, 'Enugu', 'EN', 1),
(2403, 156, 'Gombe', 'GO', 1),
(2404, 156, 'Imo', 'IM', 1),
(2405, 156, 'Jigawa', 'JI', 1),
(2406, 156, 'Kaduna', 'KD', 1),
(2407, 156, 'Kano', 'KN', 1),
(2408, 156, 'Katsina', 'KT', 1),
(2409, 156, 'Kebbi', 'KE', 1),
(2410, 156, 'Kogi', 'KO', 1),
(2411, 156, 'Kwara', 'KW', 1),
(2412, 156, 'Lagos', 'LA', 1),
(2413, 156, 'Nassarawa', 'NA', 1),
(2414, 156, 'Niger', 'NI', 1),
(2415, 156, 'Ogun', 'OG', 1),
(2416, 156, 'Ondo', 'ONG', 1),
(2417, 156, 'Osun', 'OS', 1),
(2418, 156, 'Oyo', 'OY', 1),
(2419, 156, 'Plateau', 'PL', 1),
(2420, 156, 'Rivers', 'RI', 1),
(2421, 156, 'Sokoto', 'SO', 1),
(2422, 156, 'Taraba', 'TA', 1),
(2423, 156, 'Yobe', 'YO', 1),
(2424, 156, 'Zamfara', 'ZA', 1),
(2425, 159, 'Northern Islands', 'N', 1),
(2426, 159, 'Rota', 'R', 1),
(2427, 159, 'Saipan', 'S', 1),
(2428, 159, 'Tinian', 'T', 1),
(2429, 160, 'Akershus', 'AK', 1),
(2430, 160, 'Aust-Agder', 'AA', 1),
(2431, 160, 'Buskerud', 'BU', 1),
(2432, 160, 'Finnmark', 'FM', 1),
(2433, 160, 'Hedmark', 'HM', 1),
(2434, 160, 'Hordaland', 'HL', 1),
(2435, 160, 'More og Romdal', 'MR', 1),
(2436, 160, 'Nord-Trondelag', 'NT', 1),
(2437, 160, 'Nordland', 'NL', 1),
(2438, 160, 'Ostfold', 'OF', 1),
(2439, 160, 'Oppland', 'OP', 1),
(2440, 160, 'Oslo', 'OL', 1),
(2441, 160, 'Rogaland', 'RL', 1),
(2442, 160, 'Sor-Trondelag', 'ST', 1),
(2443, 160, 'Sogn og Fjordane', 'SJ', 1),
(2444, 160, 'Svalbard', 'SV', 1),
(2445, 160, 'Telemark', 'TM', 1),
(2446, 160, 'Troms', 'TR', 1),
(2447, 160, 'Vest-Agder', 'VA', 1),
(2448, 160, 'Vestfold', 'VF', 1),
(2449, 161, 'Ad Dakhiliyah', 'DA', 1),
(2450, 161, 'Al Batinah', 'BA', 1),
(2451, 161, 'Al Wusta', 'WU', 1),
(2452, 161, 'Ash Sharqiyah', 'SH', 1),
(2453, 161, 'Az Zahirah', 'ZA', 1),
(2454, 161, 'Masqat', 'MA', 1),
(2455, 161, 'Musandam', 'MU', 1),
(2456, 161, 'Zufar', 'ZU', 1),
(2457, 162, 'Balochistan', 'B', 1),
(2458, 162, 'Federally Administered Tribal Areas', 'T', 1),
(2459, 162, 'Islamabad Capital Territory', 'I', 1),
(2460, 162, 'North-West Frontier', 'N', 1),
(2461, 162, 'Punjab', 'P', 1),
(2462, 162, 'Sindh', 'S', 1),
(2463, 163, 'Aimeliik', 'AM', 1),
(2464, 163, 'Airai', 'AR', 1),
(2465, 163, 'Angaur', 'AN', 1),
(2466, 163, 'Hatohobei', 'HA', 1),
(2467, 163, 'Kayangel', 'KA', 1),
(2468, 163, 'Koror', 'KO', 1),
(2469, 163, 'Melekeok', 'ME', 1),
(2470, 163, 'Ngaraard', 'NA', 1),
(2471, 163, 'Ngarchelong', 'NG', 1),
(2472, 163, 'Ngardmau', 'ND', 1),
(2473, 163, 'Ngatpang', 'NT', 1),
(2474, 163, 'Ngchesar', 'NC', 1),
(2475, 163, 'Ngeremlengui', 'NR', 1),
(2476, 163, 'Ngiwal', 'NW', 1),
(2477, 163, 'Peleliu', 'PE', 1),
(2478, 163, 'Sonsorol', 'SO', 1),
(2479, 164, 'Bocas del Toro', 'BT', 1),
(2480, 164, 'Chiriqui', 'CH', 1),
(2481, 164, 'Cocle', 'CC', 1),
(2482, 164, 'Colon', 'CL', 1),
(2483, 164, 'Darien', 'DA', 1),
(2484, 164, 'Herrera', 'HE', 1),
(2485, 164, 'Los Santos', 'LS', 1),
(2486, 164, 'Panama', 'PA', 1),
(2487, 164, 'San Blas', 'SB', 1),
(2488, 164, 'Veraguas', 'VG', 1),
(2489, 165, 'Bougainville', 'BV', 1),
(2490, 165, 'Central', 'CE', 1),
(2491, 165, 'Chimbu', 'CH', 1),
(2492, 165, 'Eastern Highlands', 'EH', 1),
(2493, 165, 'East New Britain', 'EB', 1),
(2494, 165, 'East Sepik', 'ES', 1),
(2495, 165, 'Enga', 'EN', 1),
(2496, 165, 'Gulf', 'GU', 1),
(2497, 165, 'Madang', 'MD', 1),
(2498, 165, 'Manus', 'MN', 1),
(2499, 165, 'Milne Bay', 'MB', 1),
(2500, 165, 'Morobe', 'MR', 1),
(2501, 165, 'National Capital', 'NC', 1),
(2502, 165, 'New Ireland', 'NI', 1),
(2503, 165, 'Northern', 'NO', 1),
(2504, 165, 'Sandaun', 'SA', 1),
(2505, 165, 'Southern Highlands', 'SH', 1),
(2506, 165, 'Western', 'WE', 1),
(2507, 165, 'Western Highlands', 'WH', 1),
(2508, 165, 'West New Britain', 'WB', 1),
(2509, 166, 'Alto Paraguay', 'AG', 1),
(2510, 166, 'Alto Parana', 'AN', 1),
(2511, 166, 'Amambay', 'AM', 1),
(2512, 166, 'Asuncion', 'AS', 1),
(2513, 166, 'Boqueron', 'BO', 1),
(2514, 166, 'Caaguazu', 'CG', 1),
(2515, 166, 'Caazapa', 'CZ', 1),
(2516, 166, 'Canindeyu', 'CN', 1),
(2517, 166, 'Central', 'CE', 1),
(2518, 166, 'Concepcion', 'CC', 1),
(2519, 166, 'Cordillera', 'CD', 1),
(2520, 166, 'Guaira', 'GU', 1),
(2521, 166, 'Itapua', 'IT', 1),
(2522, 166, 'Misiones', 'MI', 1),
(2523, 166, 'Neembucu', 'NE', 1),
(2524, 166, 'Paraguari', 'PA', 1),
(2525, 166, 'Presidente Hayes', 'PH', 1),
(2526, 166, 'San Pedro', 'SP', 1),
(2527, 167, 'Amazonas', 'AM', 1),
(2528, 167, 'Ancash', 'AN', 1),
(2529, 167, 'Apurimac', 'AP', 1),
(2530, 167, 'Arequipa', 'AR', 1),
(2531, 167, 'Ayacucho', 'AY', 1),
(2532, 167, 'Cajamarca', 'CJ', 1),
(2533, 167, 'Callao', 'CL', 1),
(2534, 167, 'Cusco', 'CU', 1),
(2535, 167, 'Huancavelica', 'HV', 1),
(2536, 167, 'Huanuco', 'HO', 1),
(2537, 167, 'Ica', 'IC', 1),
(2538, 167, 'Junin', 'JU', 1),
(2539, 167, 'La Libertad', 'LD', 1),
(2540, 167, 'Lambayeque', 'LY', 1),
(2541, 167, 'Lima', 'LI', 1),
(2542, 167, 'Loreto', 'LO', 1),
(2543, 167, 'Madre de Dios', 'MD', 1),
(2544, 167, 'Moquegua', 'MO', 1),
(2545, 167, 'Pasco', 'PA', 1),
(2546, 167, 'Piura', 'PI', 1),
(2547, 167, 'Puno', 'PU', 1),
(2548, 167, 'San Martin', 'SM', 1),
(2549, 167, 'Tacna', 'TA', 1),
(2550, 167, 'Tumbes', 'TU', 1),
(2551, 167, 'Ucayali', 'UC', 1),
(2552, 168, 'Abra', 'ABR', 1),
(2553, 168, 'Agusan del Norte', 'ANO', 1),
(2554, 168, 'Agusan del Sur', 'ASU', 1),
(2555, 168, 'Aklan', 'AKL', 1),
(2556, 168, 'Albay', 'ALB', 1),
(2557, 168, 'Antique', 'ANT', 1),
(2558, 168, 'Apayao', 'APY', 1),
(2559, 168, 'Aurora', 'AUR', 1),
(2560, 168, 'Basilan', 'BAS', 1),
(2561, 168, 'Bataan', 'BTA', 1),
(2562, 168, 'Batanes', 'BTE', 1),
(2563, 168, 'Batangas', 'BTG', 1),
(2564, 168, 'Biliran', 'BLR', 1),
(2565, 168, 'Benguet', 'BEN', 1),
(2566, 168, 'Bohol', 'BOL', 1),
(2567, 168, 'Bukidnon', 'BUK', 1),
(2568, 168, 'Bulacan', 'BUL', 1),
(2569, 168, 'Cagayan', 'CAG', 1),
(2570, 168, 'Camarines Norte', 'CNO', 1),
(2571, 168, 'Camarines Sur', 'CSU', 1),
(2572, 168, 'Camiguin', 'CAM', 1),
(2573, 168, 'Capiz', 'CAP', 1),
(2574, 168, 'Catanduanes', 'CAT', 1),
(2575, 168, 'Cavite', 'CAV', 1),
(2576, 168, 'Cebu', 'CEB', 1),
(2577, 168, 'Compostela', 'CMP', 1),
(2578, 168, 'Davao del Norte', 'DNO', 1),
(2579, 168, 'Davao del Sur', 'DSU', 1),
(2580, 168, 'Davao Oriental', 'DOR', 1),
(2581, 168, 'Eastern Samar', 'ESA', 1),
(2582, 168, 'Guimaras', 'GUI', 1),
(2583, 168, 'Ifugao', 'IFU', 1),
(2584, 168, 'Ilocos Norte', 'INO', 1),
(2585, 168, 'Ilocos Sur', 'ISU', 1),
(2586, 168, 'Iloilo', 'ILO', 1),
(2587, 168, 'Isabela', 'ISA', 1),
(2588, 168, 'Kalinga', 'KAL', 1),
(2589, 168, 'Laguna', 'LAG', 1),
(2590, 168, 'Lanao del Norte', 'LNO', 1),
(2591, 168, 'Lanao del Sur', 'LSU', 1),
(2592, 168, 'La Union', 'UNI', 1),
(2593, 168, 'Leyte', 'LEY', 1),
(2594, 168, 'Maguindanao', 'MAG', 1),
(2595, 168, 'Marinduque', 'MRN', 1),
(2596, 168, 'Masbate', 'MSB', 1),
(2597, 168, 'Mindoro Occidental', 'MIC', 1),
(2598, 168, 'Mindoro Oriental', 'MIR', 1),
(2599, 168, 'Misamis Occidental', 'MSC', 1),
(2600, 168, 'Misamis Oriental', 'MOR', 1),
(2601, 168, 'Mountain', 'MOP', 1),
(2602, 168, 'Negros Occidental', 'NOC', 1),
(2603, 168, 'Negros Oriental', 'NOR', 1),
(2604, 168, 'North Cotabato', 'NCT', 1),
(2605, 168, 'Northern Samar', 'NSM', 1),
(2606, 168, 'Nueva Ecija', 'NEC', 1),
(2607, 168, 'Nueva Vizcaya', 'NVZ', 1),
(2608, 168, 'Palawan', 'PLW', 1),
(2609, 168, 'Pampanga', 'PMP', 1),
(2610, 168, 'Pangasinan', 'PNG', 1),
(2611, 168, 'Quezon', 'QZN', 1),
(2612, 168, 'Quirino', 'QRN', 1),
(2613, 168, 'Rizal', 'RIZ', 1),
(2614, 168, 'Romblon', 'ROM', 1),
(2615, 168, 'Samar', 'SMR', 1),
(2616, 168, 'Sarangani', 'SRG', 1),
(2617, 168, 'Siquijor', 'SQJ', 1),
(2618, 168, 'Sorsogon', 'SRS', 1),
(2619, 168, 'South Cotabato', 'SCO', 1),
(2620, 168, 'Southern Leyte', 'SLE', 1),
(2621, 168, 'Sultan Kudarat', 'SKU', 1),
(2622, 168, 'Sulu', 'SLU', 1),
(2623, 168, 'Surigao del Norte', 'SNO', 1),
(2624, 168, 'Surigao del Sur', 'SSU', 1),
(2625, 168, 'Tarlac', 'TAR', 1),
(2626, 168, 'Tawi-Tawi', 'TAW', 1),
(2627, 168, 'Zambales', 'ZBL', 1),
(2628, 168, 'Zamboanga del Norte', 'ZNO', 1),
(2629, 168, 'Zamboanga del Sur', 'ZSU', 1),
(2630, 168, 'Zamboanga Sibugay', 'ZSI', 1),
(2631, 170, 'Dolnoslaskie', 'DO', 1),
(2632, 170, 'Kujawsko-Pomorskie', 'KP', 1),
(2633, 170, 'Lodzkie', 'LO', 1),
(2634, 170, 'Lubelskie', 'LL', 1),
(2635, 170, 'Lubuskie', 'LU', 1),
(2636, 170, 'Malopolskie', 'ML', 1),
(2637, 170, 'Mazowieckie', 'MZ', 1),
(2638, 170, 'Opolskie', 'OP', 1),
(2639, 170, 'Podkarpackie', 'PP', 1),
(2640, 170, 'Podlaskie', 'PL', 1),
(2641, 170, 'Pomorskie', 'PM', 1),
(2642, 170, 'Slaskie', 'SL', 1),
(2643, 170, 'Swietokrzyskie', 'SW', 1),
(2644, 170, 'Warminsko-Mazurskie', 'WM', 1),
(2645, 170, 'Wielkopolskie', 'WP', 1),
(2646, 170, 'Zachodniopomorskie', 'ZA', 1),
(2647, 198, 'Saint Pierre', 'P', 1),
(2648, 198, 'Miquelon', 'M', 1),
(2649, 171, 'A&ccedil;ores', 'AC', 1),
(2650, 171, 'Aveiro', 'AV', 1),
(2651, 171, 'Beja', 'BE', 1),
(2652, 171, 'Braga', 'BR', 1),
(2653, 171, 'Bragan&ccedil;a', 'BA', 1),
(2654, 171, 'Castelo Branco', 'CB', 1),
(2655, 171, 'Coimbra', 'CO', 1),
(2656, 171, '&Eacute;vora', 'EV', 1),
(2657, 171, 'Faro', 'FA', 1),
(2658, 171, 'Guarda', 'GU', 1),
(2659, 171, 'Leiria', 'LE', 1),
(2660, 171, 'Lisboa', 'LI', 1),
(2661, 171, 'Madeira', 'ME', 1),
(2662, 171, 'Portalegre', 'PO', 1),
(2663, 171, 'Porto', 'PR', 1),
(2664, 171, 'Santar&eacute;m', 'SA', 1),
(2665, 171, 'Set&uacute;bal', 'SE', 1),
(2666, 171, 'Viana do Castelo', 'VC', 1),
(2667, 171, 'Vila Real', 'VR', 1),
(2668, 171, 'Viseu', 'VI', 1),
(2669, 173, 'Ad Dawhah', 'DW', 1),
(2670, 173, 'Al Ghuwayriyah', 'GW', 1),
(2671, 173, 'Al Jumayliyah', 'JM', 1),
(2672, 173, 'Al Khawr', 'KR', 1),
(2673, 173, 'Al Wakrah', 'WK', 1),
(2674, 173, 'Ar Rayyan', 'RN', 1),
(2675, 173, 'Jarayan al Batinah', 'JB', 1),
(2676, 173, 'Madinat ash Shamal', 'MS', 1),
(2677, 173, 'Umm Sa\'id', 'UD', 1),
(2678, 173, 'Umm Salal', 'UL', 1),
(2679, 175, 'Alba', 'AB', 1),
(2680, 175, 'Arad', 'AR', 1),
(2681, 175, 'Argeș', 'AG', 1),
(2682, 175, 'Bacău', 'BC', 1),
(2683, 175, 'Bihor', 'BH', 1),
(2684, 175, 'Bistrița-Năsăud', 'BN', 1),
(2685, 175, 'Botoșani', 'BT', 1),
(2686, 175, 'Brașov', 'BV', 1),
(2687, 175, 'Brăila', 'BR', 1),
(2688, 175, 'București', 'B', 1),
(2689, 175, 'Buzău', 'BZ', 1),
(2690, 175, 'Caraș-Severin', 'CS', 1),
(2691, 175, 'Călărași', 'CL', 1),
(2692, 175, 'Cluj', 'CJ', 1),
(2693, 175, 'Constanța', 'CT', 1),
(2694, 175, 'Covasna', 'CV', 1),
(2695, 175, 'Dâmbovița', 'DB', 1),
(2696, 175, 'Dolj', 'DJ', 1),
(2697, 175, 'Galați', 'GL', 1),
(2698, 175, 'Giurgiu', 'GR', 1),
(2699, 175, 'Gorj', 'GJ', 1),
(2700, 175, 'Harghita', 'HR', 1),
(2701, 175, 'Hunedoara', 'HD', 1),
(2702, 175, 'Ialomița', 'IL', 1),
(2703, 175, 'Iași', 'IS', 1),
(2704, 175, 'Ilfov', 'IF', 1),
(2705, 175, 'Maramureș', 'MM', 1),
(2706, 175, 'Mehedinți', 'MH', 1),
(2707, 175, 'Mureș', 'MS', 1),
(2708, 175, 'Neamț', 'NT', 1),
(2709, 175, 'Olt', 'OT', 1),
(2710, 175, 'Prahova', 'PH', 1),
(2711, 175, 'Satu-Mare', 'SM', 1),
(2712, 175, 'Sălaj', 'SJ', 1),
(2713, 175, 'Sibiu', 'SB', 1),
(2714, 175, 'Suceava', 'SV', 1),
(2715, 175, 'Teleorman', 'TR', 1),
(2716, 175, 'Timiș', 'TM', 1),
(2717, 175, 'Tulcea', 'TL', 1),
(2718, 175, 'Vaslui', 'VS', 1),
(2719, 175, 'Vâlcea', 'VL', 1),
(2720, 175, 'Vrancea', 'VN', 1),
(2721, 176, 'Abakan', 'AB', 1),
(2722, 176, 'Aginskoye', 'AG', 1),
(2723, 176, 'Anadyr', 'AN', 1),
(2724, 176, 'Arkahangelsk', 'AR', 1),
(2725, 176, 'Astrakhan', 'AS', 1),
(2726, 176, 'Barnaul', 'BA', 1),
(2727, 176, 'Belgorod', 'BE', 1),
(2728, 176, 'Birobidzhan', 'BI', 1),
(2729, 176, 'Blagoveshchensk', 'BL', 1),
(2730, 176, 'Bryansk', 'BR', 1),
(2731, 176, 'Cheboksary', 'CH', 1),
(2732, 176, 'Chelyabinsk', 'CL', 1),
(2733, 176, 'Cherkessk', 'CR', 1),
(2734, 176, 'Chita', 'CI', 1),
(2735, 176, 'Dudinka', 'DU', 1),
(2736, 176, 'Elista', 'EL', 1),
(2738, 176, 'Gorno-Altaysk', 'GA', 1),
(2739, 176, 'Groznyy', 'GR', 1),
(2740, 176, 'Irkutsk', 'IR', 1),
(2741, 176, 'Ivanovo', 'IV', 1),
(2742, 176, 'Izhevsk', 'IZ', 1),
(2743, 176, 'Kalinigrad', 'KA', 1),
(2744, 176, 'Kaluga', 'KL', 1),
(2745, 176, 'Kasnodar', 'KS', 1),
(2746, 176, 'Kazan', 'KZ', 1),
(2747, 176, 'Kemerovo', 'KE', 1),
(2748, 176, 'Khabarovsk', 'KH', 1),
(2749, 176, 'Khanty-Mansiysk', 'KM', 1),
(2750, 176, 'Kostroma', 'KO', 1),
(2751, 176, 'Krasnodar', 'KR', 1),
(2752, 176, 'Krasnoyarsk', 'KN', 1),
(2753, 176, 'Kudymkar', 'KU', 1),
(2754, 176, 'Kurgan', 'KG', 1),
(2755, 176, 'Kursk', 'KK', 1),
(2756, 176, 'Kyzyl', 'KY', 1),
(2757, 176, 'Lipetsk', 'LI', 1),
(2758, 176, 'Magadan', 'MA', 1),
(2759, 176, 'Makhachkala', 'MK', 1),
(2760, 176, 'Maykop', 'MY', 1),
(2761, 176, 'Moscow', 'MO', 1),
(2762, 176, 'Murmansk', 'MU', 1),
(2763, 176, 'Nalchik', 'NA', 1),
(2764, 176, 'Naryan Mar', 'NR', 1),
(2765, 176, 'Nazran', 'NZ', 1),
(2766, 176, 'Nizhniy Novgorod', 'NI', 1),
(2767, 176, 'Novgorod', 'NO', 1),
(2768, 176, 'Novosibirsk', 'NV', 1),
(2769, 176, 'Omsk', 'OM', 1),
(2770, 176, 'Orel', 'OR', 1),
(2771, 176, 'Orenburg', 'OE', 1),
(2772, 176, 'Palana', 'PA', 1),
(2773, 176, 'Penza', 'PE', 1),
(2774, 176, 'Perm', 'PR', 1),
(2775, 176, 'Petropavlovsk-Kamchatskiy', 'PK', 1),
(2776, 176, 'Petrozavodsk', 'PT', 1),
(2777, 176, 'Pskov', 'PS', 1),
(2778, 176, 'Rostov-na-Donu', 'RO', 1),
(2779, 176, 'Ryazan', 'RY', 1),
(2780, 176, 'Salekhard', 'SL', 1),
(2781, 176, 'Samara', 'SA', 1),
(2782, 176, 'Saransk', 'SR', 1),
(2783, 176, 'Saratov', 'SV', 1),
(2784, 176, 'Smolensk', 'SM', 1),
(2785, 176, 'St. Petersburg', 'SP', 1),
(2786, 176, 'Stavropol', 'ST', 1),
(2787, 176, 'Syktyvkar', 'SY', 1),
(2788, 176, 'Tambov', 'TA', 1),
(2789, 176, 'Tomsk', 'TO', 1),
(2790, 176, 'Tula', 'TU', 1),
(2791, 176, 'Tura', 'TR', 1),
(2792, 176, 'Tver', 'TV', 1),
(2793, 176, 'Tyumen', 'TY', 1),
(2794, 176, 'Ufa', 'UF', 1),
(2795, 176, 'Ul\'yanovsk', 'UL', 1),
(2796, 176, 'Ulan-Ude', 'UU', 1),
(2797, 176, 'Ust\'-Ordynskiy', 'US', 1),
(2798, 176, 'Vladikavkaz', 'VL', 1),
(2799, 176, 'Vladimir', 'VA', 1),
(2800, 176, 'Vladivostok', 'VV', 1),
(2801, 176, 'Volgograd', 'VG', 1),
(2802, 176, 'Vologda', 'VD', 1),
(2803, 176, 'Voronezh', 'VO', 1),
(2804, 176, 'Vyatka', 'VY', 1),
(2805, 176, 'Yakutsk', 'YA', 1),
(2806, 176, 'Yaroslavl', 'YR', 1),
(2807, 176, 'Yekaterinburg', 'YE', 1),
(2808, 176, 'Yoshkar-Ola', 'YO', 1),
(2809, 177, 'Butare', 'BU', 1),
(2810, 177, 'Byumba', 'BY', 1),
(2811, 177, 'Cyangugu', 'CY', 1),
(2812, 177, 'Gikongoro', 'GK', 1),
(2813, 177, 'Gisenyi', 'GS', 1),
(2814, 177, 'Gitarama', 'GT', 1),
(2815, 177, 'Kibungo', 'KG', 1),
(2816, 177, 'Kibuye', 'KY', 1),
(2817, 177, 'Kigali Rurale', 'KR', 1),
(2818, 177, 'Kigali-ville', 'KV', 1),
(2819, 177, 'Ruhengeri', 'RU', 1),
(2820, 177, 'Umutara', 'UM', 1),
(2821, 178, 'Christ Church Nichola Town', 'CCN', 1),
(2822, 178, 'Saint Anne Sandy Point', 'SAS', 1),
(2823, 178, 'Saint George Basseterre', 'SGB', 1),
(2824, 178, 'Saint George Gingerland', 'SGG', 1),
(2825, 178, 'Saint James Windward', 'SJW', 1),
(2826, 178, 'Saint John Capesterre', 'SJC', 1),
(2827, 178, 'Saint John Figtree', 'SJF', 1),
(2828, 178, 'Saint Mary Cayon', 'SMC', 1),
(2829, 178, 'Saint Paul Capesterre', 'CAP', 1),
(2830, 178, 'Saint Paul Charlestown', 'CHA', 1),
(2831, 178, 'Saint Peter Basseterre', 'SPB', 1),
(2832, 178, 'Saint Thomas Lowland', 'STL', 1),
(2833, 178, 'Saint Thomas Middle Island', 'STM', 1),
(2834, 178, 'Trinity Palmetto Point', 'TPP', 1),
(2835, 179, 'Anse-la-Raye', 'AR', 1),
(2836, 179, 'Castries', 'CA', 1),
(2837, 179, 'Choiseul', 'CH', 1),
(2838, 179, 'Dauphin', 'DA', 1),
(2839, 179, 'Dennery', 'DE', 1),
(2840, 179, 'Gros-Islet', 'GI', 1),
(2841, 179, 'Laborie', 'LA', 1),
(2842, 179, 'Micoud', 'MI', 1),
(2843, 179, 'Praslin', 'PR', 1),
(2844, 179, 'Soufriere', 'SO', 1),
(2845, 179, 'Vieux-Fort', 'VF', 1),
(2846, 180, 'Charlotte', 'C', 1),
(2847, 180, 'Grenadines', 'R', 1),
(2848, 180, 'Saint Andrew', 'A', 1),
(2849, 180, 'Saint David', 'D', 1),
(2850, 180, 'Saint George', 'G', 1),
(2851, 180, 'Saint Patrick', 'P', 1),
(2852, 181, 'A\'ana', 'AN', 1),
(2853, 181, 'Aiga-i-le-Tai', 'AI', 1),
(2854, 181, 'Atua', 'AT', 1),
(2855, 181, 'Fa\'asaleleaga', 'FA', 1),
(2856, 181, 'Gaga\'emauga', 'GE', 1),
(2857, 181, 'Gagaifomauga', 'GF', 1),
(2858, 181, 'Palauli', 'PA', 1),
(2859, 181, 'Satupa\'itea', 'SA', 1),
(2860, 181, 'Tuamasaga', 'TU', 1),
(2861, 181, 'Va\'a-o-Fonoti', 'VF', 1),
(2862, 181, 'Vaisigano', 'VS', 1),
(2863, 182, 'Acquaviva', 'AC', 1),
(2864, 182, 'Borgo Maggiore', 'BM', 1),
(2865, 182, 'Chiesanuova', 'CH', 1),
(2866, 182, 'Domagnano', 'DO', 1),
(2867, 182, 'Faetano', 'FA', 1),
(2868, 182, 'Fiorentino', 'FI', 1),
(2869, 182, 'Montegiardino', 'MO', 1),
(2870, 182, 'Citta di San Marino', 'SM', 1),
(2871, 182, 'Serravalle', 'SE', 1),
(2872, 183, 'Sao Tome', 'S', 1),
(2873, 183, 'Principe', 'P', 1),
(2874, 184, 'Al Bahah', 'BH', 1),
(2875, 184, 'Al Hudud ash Shamaliyah', 'HS', 1),
(2876, 184, 'Al Jawf', 'JF', 1),
(2877, 184, 'Al Madinah', 'MD', 1),
(2878, 184, 'Al Qasim', 'QS', 1),
(2879, 184, 'Ar Riyad', 'RD', 1),
(2880, 184, 'Ash Sharqiyah (Eastern)', 'AQ', 1),
(2881, 184, '\'Asir', 'AS', 1),
(2882, 184, 'Ha\'il', 'HL', 1),
(2883, 184, 'Jizan', 'JZ', 1),
(2884, 184, 'Makkah', 'ML', 1),
(2885, 184, 'Najran', 'NR', 1),
(2886, 184, 'Tabuk', 'TB', 1),
(2887, 185, 'Dakar', 'DA', 1),
(2888, 185, 'Diourbel', 'DI', 1),
(2889, 185, 'Fatick', 'FA', 1),
(2890, 185, 'Kaolack', 'KA', 1),
(2891, 185, 'Kolda', 'KO', 1),
(2892, 185, 'Louga', 'LO', 1),
(2893, 185, 'Matam', 'MA', 1),
(2894, 185, 'Saint-Louis', 'SL', 1),
(2895, 185, 'Tambacounda', 'TA', 1),
(2896, 185, 'Thies', 'TH', 1),
(2897, 185, 'Ziguinchor', 'ZI', 1),
(2898, 186, 'Anse aux Pins', 'AP', 1),
(2899, 186, 'Anse Boileau', 'AB', 1),
(2900, 186, 'Anse Etoile', 'AE', 1),
(2901, 186, 'Anse Louis', 'AL', 1),
(2902, 186, 'Anse Royale', 'AR', 1),
(2903, 186, 'Baie Lazare', 'BL', 1),
(2904, 186, 'Baie Sainte Anne', 'BS', 1),
(2905, 186, 'Beau Vallon', 'BV', 1),
(2906, 186, 'Bel Air', 'BA', 1),
(2907, 186, 'Bel Ombre', 'BO', 1),
(2908, 186, 'Cascade', 'CA', 1),
(2909, 186, 'Glacis', 'GL', 1),
(2910, 186, 'Grand\' Anse (on Mahe)', 'GM', 1),
(2911, 186, 'Grand\' Anse (on Praslin)', 'GP', 1),
(2912, 186, 'La Digue', 'DG', 1),
(2913, 186, 'La Riviere Anglaise', 'RA', 1),
(2914, 186, 'Mont Buxton', 'MB', 1),
(2915, 186, 'Mont Fleuri', 'MF', 1),
(2916, 186, 'Plaisance', 'PL', 1),
(2917, 186, 'Pointe La Rue', 'PR', 1),
(2918, 186, 'Port Glaud', 'PG', 1),
(2919, 186, 'Saint Louis', 'SL', 1),
(2920, 186, 'Takamaka', 'TA', 1),
(2921, 187, 'Eastern', 'E', 1),
(2922, 187, 'Northern', 'N', 1),
(2923, 187, 'Southern', 'S', 1),
(2924, 187, 'Western', 'W', 1),
(2925, 189, 'Banskobystrický', 'BA', 1),
(2926, 189, 'Bratislavský', 'BR', 1),
(2927, 189, 'Košický', 'KO', 1),
(2928, 189, 'Nitriansky', 'NI', 1),
(2929, 189, 'Prešovský', 'PR', 1),
(2930, 189, 'Trenčiansky', 'TC', 1),
(2931, 189, 'Trnavský', 'TV', 1),
(2932, 189, 'Žilinský', 'ZI', 1),
(2933, 191, 'Central', 'CE', 1),
(2934, 191, 'Choiseul', 'CH', 1),
(2935, 191, 'Guadalcanal', 'GC', 1),
(2936, 191, 'Honiara', 'HO', 1),
(2937, 191, 'Isabel', 'IS', 1),
(2938, 191, 'Makira', 'MK', 1),
(2939, 191, 'Malaita', 'ML', 1),
(2940, 191, 'Rennell and Bellona', 'RB', 1),
(2941, 191, 'Temotu', 'TM', 1),
(2942, 191, 'Western', 'WE', 1),
(2943, 192, 'Awdal', 'AW', 1),
(2944, 192, 'Bakool', 'BK', 1),
(2945, 192, 'Banaadir', 'BN', 1),
(2946, 192, 'Bari', 'BR', 1),
(2947, 192, 'Bay', 'BY', 1),
(2948, 192, 'Galguduud', 'GA', 1),
(2949, 192, 'Gedo', 'GE', 1),
(2950, 192, 'Hiiraan', 'HI', 1),
(2951, 192, 'Jubbada Dhexe', 'JD', 1),
(2952, 192, 'Jubbada Hoose', 'JH', 1),
(2953, 192, 'Mudug', 'MU', 1),
(2954, 192, 'Nugaal', 'NU', 1),
(2955, 192, 'Sanaag', 'SA', 1),
(2956, 192, 'Shabeellaha Dhexe', 'SD', 1),
(2957, 192, 'Shabeellaha Hoose', 'SH', 1),
(2958, 192, 'Sool', 'SL', 1),
(2959, 192, 'Togdheer', 'TO', 1),
(2960, 192, 'Woqooyi Galbeed', 'WG', 1),
(2961, 193, 'Eastern Cape', 'EC', 1),
(2962, 193, 'Free State', 'FS', 1),
(2963, 193, 'Gauteng', 'GT', 1),
(2964, 193, 'KwaZulu-Natal', 'KN', 1),
(2965, 193, 'Limpopo', 'LP', 1),
(2966, 193, 'Mpumalanga', 'MP', 1),
(2967, 193, 'North West', 'NW', 1),
(2968, 193, 'Northern Cape', 'NC', 1),
(2969, 193, 'Western Cape', 'WC', 1),
(2970, 195, 'La Coru&ntilde;a', 'CA', 1),
(2971, 195, '&Aacute;lava', 'AL', 1),
(2972, 195, 'Albacete', 'AB', 1),
(2973, 195, 'Alicante', 'AC', 1),
(2974, 195, 'Almeria', 'AM', 1),
(2975, 195, 'Asturias', 'AS', 1),
(2976, 195, '&Aacute;vila', 'AV', 1),
(2977, 195, 'Badajoz', 'BJ', 1),
(2978, 195, 'Baleares', 'IB', 1),
(2979, 195, 'Barcelona', 'BA', 1),
(2980, 195, 'Burgos', 'BU', 1),
(2981, 195, 'C&aacute;ceres', 'CC', 1),
(2982, 195, 'C&aacute;diz', 'CZ', 1),
(2983, 195, 'Cantabria', 'CT', 1),
(2984, 195, 'Castell&oacute;n', 'CL', 1),
(2985, 195, 'Ceuta', 'CE', 1),
(2986, 195, 'Ciudad Real', 'CR', 1),
(2987, 195, 'C&oacute;rdoba', 'CD', 1),
(2988, 195, 'Cuenca', 'CU', 1),
(2989, 195, 'Girona', 'GI', 1),
(2990, 195, 'Granada', 'GD', 1),
(2991, 195, 'Guadalajara', 'GJ', 1),
(2992, 195, 'Guip&uacute;zcoa', 'GP', 1),
(2993, 195, 'Huelva', 'HL', 1),
(2994, 195, 'Huesca', 'HS', 1),
(2995, 195, 'Ja&eacute;n', 'JN', 1),
(2996, 195, 'La Rioja', 'RJ', 1),
(2997, 195, 'Las Palmas', 'PM', 1),
(2998, 195, 'Leon', 'LE', 1),
(2999, 195, 'Lleida', 'LL', 1),
(3000, 195, 'Lugo', 'LG', 1),
(3001, 195, 'Madrid', 'MD', 1),
(3002, 195, 'Malaga', 'MA', 1),
(3003, 195, 'Melilla', 'ML', 1),
(3004, 195, 'Murcia', 'MU', 1),
(3005, 195, 'Navarra', 'NV', 1),
(3006, 195, 'Ourense', 'OU', 1),
(3007, 195, 'Palencia', 'PL', 1),
(3008, 195, 'Pontevedra', 'PO', 1),
(3009, 195, 'Salamanca', 'SL', 1),
(3010, 195, 'Santa Cruz de Tenerife', 'SC', 1),
(3011, 195, 'Segovia', 'SG', 1),
(3012, 195, 'Sevilla', 'SV', 1),
(3013, 195, 'Soria', 'SO', 1),
(3014, 195, 'Tarragona', 'TA', 1),
(3015, 195, 'Teruel', 'TE', 1),
(3016, 195, 'Toledo', 'TO', 1),
(3017, 195, 'Valencia', 'VC', 1),
(3018, 195, 'Valladolid', 'VD', 1),
(3019, 195, 'Vizcaya', 'VZ', 1),
(3020, 195, 'Zamora', 'ZM', 1),
(3021, 195, 'Zaragoza', 'ZR', 1),
(3022, 196, 'Central', 'CE', 1),
(3023, 196, 'Eastern', 'EA', 1),
(3024, 196, 'North Central', 'NC', 1),
(3025, 196, 'Northern', 'NO', 1),
(3026, 196, 'North Western', 'NW', 1),
(3027, 196, 'Sabaragamuwa', 'SA', 1),
(3028, 196, 'Southern', 'SO', 1),
(3029, 196, 'Uva', 'UV', 1),
(3030, 196, 'Western', 'WE', 1),
(3032, 197, 'Saint Helena', 'S', 1),
(3034, 199, 'A\'ali an Nil', 'ANL', 1),
(3035, 199, 'Al Bahr al Ahmar', 'BAM', 1),
(3036, 199, 'Al Buhayrat', 'BRT', 1),
(3037, 199, 'Al Jazirah', 'JZR', 1),
(3038, 199, 'Al Khartum', 'KRT', 1),
(3039, 199, 'Al Qadarif', 'QDR', 1),
(3040, 199, 'Al Wahdah', 'WDH', 1),
(3041, 199, 'An Nil al Abyad', 'ANB', 1),
(3042, 199, 'An Nil al Azraq', 'ANZ', 1),
(3043, 199, 'Ash Shamaliyah', 'ASH', 1),
(3044, 199, 'Bahr al Jabal', 'BJA', 1),
(3045, 199, 'Gharb al Istiwa\'iyah', 'GIS', 1),
(3046, 199, 'Gharb Bahr al Ghazal', 'GBG', 1),
(3047, 199, 'Gharb Darfur', 'GDA', 1),
(3048, 199, 'Gharb Kurdufan', 'GKU', 1),
(3049, 199, 'Janub Darfur', 'JDA', 1),
(3050, 199, 'Janub Kurdufan', 'JKU', 1),
(3051, 199, 'Junqali', 'JQL', 1),
(3052, 199, 'Kassala', 'KSL', 1),
(3053, 199, 'Nahr an Nil', 'NNL', 1),
(3054, 199, 'Shamal Bahr al Ghazal', 'SBG', 1),
(3055, 199, 'Shamal Darfur', 'SDA', 1),
(3056, 199, 'Shamal Kurdufan', 'SKU', 1),
(3057, 199, 'Sharq al Istiwa\'iyah', 'SIS', 1),
(3058, 199, 'Sinnar', 'SNR', 1),
(3059, 199, 'Warab', 'WRB', 1),
(3060, 200, 'Brokopondo', 'BR', 1),
(3061, 200, 'Commewijne', 'CM', 1),
(3062, 200, 'Coronie', 'CR', 1),
(3063, 200, 'Marowijne', 'MA', 1),
(3064, 200, 'Nickerie', 'NI', 1),
(3065, 200, 'Para', 'PA', 1),
(3066, 200, 'Paramaribo', 'PM', 1),
(3067, 200, 'Saramacca', 'SA', 1),
(3068, 200, 'Sipaliwini', 'SI', 1),
(3069, 200, 'Wanica', 'WA', 1),
(3070, 202, 'Hhohho', 'H', 1),
(3071, 202, 'Lubombo', 'L', 1),
(3072, 202, 'Manzini', 'M', 1),
(3073, 202, 'Shishelweni', 'S', 1),
(3074, 203, 'Blekinge', 'K', 1),
(3075, 203, 'Dalarna', 'W', 1),
(3076, 203, 'Gävleborg', 'X', 1),
(3077, 203, 'Gotland', 'I', 1),
(3078, 203, 'Halland', 'N', 1),
(3079, 203, 'Jämtland', 'Z', 1),
(3080, 203, 'Jönköping', 'F', 1),
(3081, 203, 'Kalmar', 'H', 1),
(3082, 203, 'Kronoberg', 'G', 1),
(3083, 203, 'Norrbotten', 'BD', 1),
(3084, 203, 'Örebro', 'T', 1),
(3085, 203, 'Östergötland', 'E', 1),
(3086, 203, 'Sk&aring;ne', 'M', 1),
(3087, 203, 'Södermanland', 'D', 1),
(3088, 203, 'Stockholm', 'AB', 1),
(3089, 203, 'Uppsala', 'C', 1),
(3090, 203, 'Värmland', 'S', 1),
(3091, 203, 'Västerbotten', 'AC', 1),
(3092, 203, 'Västernorrland', 'Y', 1),
(3093, 203, 'Västmanland', 'U', 1),
(3094, 203, 'Västra Götaland', 'O', 1),
(3095, 204, 'Aargau', 'AG', 1),
(3096, 204, 'Appenzell Ausserrhoden', 'AR', 1),
(3097, 204, 'Appenzell Innerrhoden', 'AI', 1),
(3098, 204, 'Basel-Stadt', 'BS', 1),
(3099, 204, 'Basel-Landschaft', 'BL', 1),
(3100, 204, 'Bern', 'BE', 1),
(3101, 204, 'Fribourg', 'FR', 1),
(3102, 204, 'Gen&egrave;ve', 'GE', 1),
(3103, 204, 'Glarus', 'GL', 1),
(3104, 204, 'Graubünden', 'GR', 1),
(3105, 204, 'Jura', 'JU', 1),
(3106, 204, 'Luzern', 'LU', 1),
(3107, 204, 'Neuch&acirc;tel', 'NE', 1),
(3108, 204, 'Nidwald', 'NW', 1),
(3109, 204, 'Obwald', 'OW', 1),
(3110, 204, 'St. Gallen', 'SG', 1),
(3111, 204, 'Schaffhausen', 'SH', 1),
(3112, 204, 'Schwyz', 'SZ', 1),
(3113, 204, 'Solothurn', 'SO', 1),
(3114, 204, 'Thurgau', 'TG', 1),
(3115, 204, 'Ticino', 'TI', 1),
(3116, 204, 'Uri', 'UR', 1),
(3117, 204, 'Valais', 'VS', 1),
(3118, 204, 'Vaud', 'VD', 1),
(3119, 204, 'Zug', 'ZG', 1),
(3120, 204, 'Zürich', 'ZH', 1),
(3121, 205, 'Al Hasakah', 'HA', 1),
(3122, 205, 'Al Ladhiqiyah', 'LA', 1),
(3123, 205, 'Al Qunaytirah', 'QU', 1),
(3124, 205, 'Ar Raqqah', 'RQ', 1),
(3125, 205, 'As Suwayda', 'SU', 1),
(3126, 205, 'Dara', 'DA', 1),
(3127, 205, 'Dayr az Zawr', 'DZ', 1),
(3128, 205, 'Dimashq', 'DI', 1),
(3129, 205, 'Halab', 'HL', 1),
(3130, 205, 'Hamah', 'HM', 1),
(3131, 205, 'Hims', 'HI', 1),
(3132, 205, 'Idlib', 'ID', 1),
(3133, 205, 'Rif Dimashq', 'RD', 1),
(3134, 205, 'Tartus', 'TA', 1),
(3135, 206, 'Chang-hua', 'CH', 1),
(3136, 206, 'Chia-i', 'CI', 1);
INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(3137, 206, 'Hsin-chu', 'HS', 1),
(3138, 206, 'Hua-lien', 'HL', 1),
(3139, 206, 'I-lan', 'IL', 1),
(3140, 206, 'Kao-hsiung county', 'KH', 1),
(3141, 206, 'Kin-men', 'KM', 1),
(3142, 206, 'Lien-chiang', 'LC', 1),
(3143, 206, 'Miao-li', 'ML', 1),
(3144, 206, 'Nan-t\'ou', 'NT', 1),
(3145, 206, 'P\'eng-hu', 'PH', 1),
(3146, 206, 'P\'ing-tung', 'PT', 1),
(3147, 206, 'T\'ai-chung', 'TG', 1),
(3148, 206, 'T\'ai-nan', 'TA', 1),
(3149, 206, 'T\'ai-pei county', 'TP', 1),
(3150, 206, 'T\'ai-tung', 'TT', 1),
(3151, 206, 'T\'ao-yuan', 'TY', 1),
(3152, 206, 'Yun-lin', 'YL', 1),
(3153, 206, 'Chia-i city', 'CC', 1),
(3154, 206, 'Chi-lung', 'CL', 1),
(3155, 206, 'Hsin-chu', 'HC', 1),
(3156, 206, 'T\'ai-chung', 'TH', 1),
(3157, 206, 'T\'ai-nan', 'TN', 1),
(3158, 206, 'Kao-hsiung city', 'KC', 1),
(3159, 206, 'T\'ai-pei city', 'TC', 1),
(3160, 207, 'Gorno-Badakhstan', 'GB', 1),
(3161, 207, 'Khatlon', 'KT', 1),
(3162, 207, 'Sughd', 'SU', 1),
(3163, 208, 'Arusha', 'AR', 1),
(3164, 208, 'Dar es Salaam', 'DS', 1),
(3165, 208, 'Dodoma', 'DO', 1),
(3166, 208, 'Iringa', 'IR', 1),
(3167, 208, 'Kagera', 'KA', 1),
(3168, 208, 'Kigoma', 'KI', 1),
(3169, 208, 'Kilimanjaro', 'KJ', 1),
(3170, 208, 'Lindi', 'LN', 1),
(3171, 208, 'Manyara', 'MY', 1),
(3172, 208, 'Mara', 'MR', 1),
(3173, 208, 'Mbeya', 'MB', 1),
(3174, 208, 'Morogoro', 'MO', 1),
(3175, 208, 'Mtwara', 'MT', 1),
(3176, 208, 'Mwanza', 'MW', 1),
(3177, 208, 'Pemba North', 'PN', 1),
(3178, 208, 'Pemba South', 'PS', 1),
(3179, 208, 'Pwani', 'PW', 1),
(3180, 208, 'Rukwa', 'RK', 1),
(3181, 208, 'Ruvuma', 'RV', 1),
(3182, 208, 'Shinyanga', 'SH', 1),
(3183, 208, 'Singida', 'SI', 1),
(3184, 208, 'Tabora', 'TB', 1),
(3185, 208, 'Tanga', 'TN', 1),
(3186, 208, 'Zanzibar Central/South', 'ZC', 1),
(3187, 208, 'Zanzibar North', 'ZN', 1),
(3188, 208, 'Zanzibar Urban/West', 'ZU', 1),
(3189, 209, 'Amnat Charoen', 'Amnat Charoen', 1),
(3190, 209, 'Ang Thong', 'Ang Thong', 1),
(3191, 209, 'Ayutthaya', 'Ayutthaya', 1),
(3192, 209, 'Bangkok', 'Bangkok', 1),
(3193, 209, 'Buriram', 'Buriram', 1),
(3194, 209, 'Chachoengsao', 'Chachoengsao', 1),
(3195, 209, 'Chai Nat', 'Chai Nat', 1),
(3196, 209, 'Chaiyaphum', 'Chaiyaphum', 1),
(3197, 209, 'Chanthaburi', 'Chanthaburi', 1),
(3198, 209, 'Chiang Mai', 'Chiang Mai', 1),
(3199, 209, 'Chiang Rai', 'Chiang Rai', 1),
(3200, 209, 'Chon Buri', 'Chon Buri', 1),
(3201, 209, 'Chumphon', 'Chumphon', 1),
(3202, 209, 'Kalasin', 'Kalasin', 1),
(3203, 209, 'Kamphaeng Phet', 'Kamphaeng Phet', 1),
(3204, 209, 'Kanchanaburi', 'Kanchanaburi', 1),
(3205, 209, 'Khon Kaen', 'Khon Kaen', 1),
(3206, 209, 'Krabi', 'Krabi', 1),
(3207, 209, 'Lampang', 'Lampang', 1),
(3208, 209, 'Lamphun', 'Lamphun', 1),
(3209, 209, 'Loei', 'Loei', 1),
(3210, 209, 'Lop Buri', 'Lop Buri', 1),
(3211, 209, 'Mae Hong Son', 'Mae Hong Son', 1),
(3212, 209, 'Maha Sarakham', 'Maha Sarakham', 1),
(3213, 209, 'Mukdahan', 'Mukdahan', 1),
(3214, 209, 'Nakhon Nayok', 'Nakhon Nayok', 1),
(3215, 209, 'Nakhon Pathom', 'Nakhon Pathom', 1),
(3216, 209, 'Nakhon Phanom', 'Nakhon Phanom', 1),
(3217, 209, 'Nakhon Ratchasima', 'Nakhon Ratchasima', 1),
(3218, 209, 'Nakhon Sawan', 'Nakhon Sawan', 1),
(3219, 209, 'Nakhon Si Thammarat', 'Nakhon Si Thammarat', 1),
(3220, 209, 'Nan', 'Nan', 1),
(3221, 209, 'Narathiwat', 'Narathiwat', 1),
(3222, 209, 'Nong Bua Lamphu', 'Nong Bua Lamphu', 1),
(3223, 209, 'Nong Khai', 'Nong Khai', 1),
(3224, 209, 'Nonthaburi', 'Nonthaburi', 1),
(3225, 209, 'Pathum Thani', 'Pathum Thani', 1),
(3226, 209, 'Pattani', 'Pattani', 1),
(3227, 209, 'Phangnga', 'Phangnga', 1),
(3228, 209, 'Phatthalung', 'Phatthalung', 1),
(3229, 209, 'Phayao', 'Phayao', 1),
(3230, 209, 'Phetchabun', 'Phetchabun', 1),
(3231, 209, 'Phetchaburi', 'Phetchaburi', 1),
(3232, 209, 'Phichit', 'Phichit', 1),
(3233, 209, 'Phitsanulok', 'Phitsanulok', 1),
(3234, 209, 'Phrae', 'Phrae', 1),
(3235, 209, 'Phuket', 'Phuket', 1),
(3236, 209, 'Prachin Buri', 'Prachin Buri', 1),
(3237, 209, 'Prachuap Khiri Khan', 'Prachuap Khiri Khan', 1),
(3238, 209, 'Ranong', 'Ranong', 1),
(3239, 209, 'Ratchaburi', 'Ratchaburi', 1),
(3240, 209, 'Rayong', 'Rayong', 1),
(3241, 209, 'Roi Et', 'Roi Et', 1),
(3242, 209, 'Sa Kaeo', 'Sa Kaeo', 1),
(3243, 209, 'Sakon Nakhon', 'Sakon Nakhon', 1),
(3244, 209, 'Samut Prakan', 'Samut Prakan', 1),
(3245, 209, 'Samut Sakhon', 'Samut Sakhon', 1),
(3246, 209, 'Samut Songkhram', 'Samut Songkhram', 1),
(3247, 209, 'Sara Buri', 'Sara Buri', 1),
(3248, 209, 'Satun', 'Satun', 1),
(3249, 209, 'Sing Buri', 'Sing Buri', 1),
(3250, 209, 'Sisaket', 'Sisaket', 1),
(3251, 209, 'Songkhla', 'Songkhla', 1),
(3252, 209, 'Sukhothai', 'Sukhothai', 1),
(3253, 209, 'Suphan Buri', 'Suphan Buri', 1),
(3254, 209, 'Surat Thani', 'Surat Thani', 1),
(3255, 209, 'Surin', 'Surin', 1),
(3256, 209, 'Tak', 'Tak', 1),
(3257, 209, 'Trang', 'Trang', 1),
(3258, 209, 'Trat', 'Trat', 1),
(3259, 209, 'Ubon Ratchathani', 'Ubon Ratchathani', 1),
(3260, 209, 'Udon Thani', 'Udon Thani', 1),
(3261, 209, 'Uthai Thani', 'Uthai Thani', 1),
(3262, 209, 'Uttaradit', 'Uttaradit', 1),
(3263, 209, 'Yala', 'Yala', 1),
(3264, 209, 'Yasothon', 'Yasothon', 1),
(3265, 210, 'Kara', 'K', 1),
(3266, 210, 'Plateaux', 'P', 1),
(3267, 210, 'Savanes', 'S', 1),
(3268, 210, 'Centrale', 'C', 1),
(3269, 210, 'Maritime', 'M', 1),
(3270, 211, 'Atafu', 'A', 1),
(3271, 211, 'Fakaofo', 'F', 1),
(3272, 211, 'Nukunonu', 'N', 1),
(3273, 212, 'Ha\'apai', 'H', 1),
(3274, 212, 'Tongatapu', 'T', 1),
(3275, 212, 'Vava\'u', 'V', 1),
(3276, 213, 'Couva/Tabaquite/Talparo', 'CT', 1),
(3277, 213, 'Diego Martin', 'DM', 1),
(3278, 213, 'Mayaro/Rio Claro', 'MR', 1),
(3279, 213, 'Penal/Debe', 'PD', 1),
(3280, 213, 'Princes Town', 'PT', 1),
(3281, 213, 'Sangre Grande', 'SG', 1),
(3282, 213, 'San Juan/Laventille', 'SL', 1),
(3283, 213, 'Siparia', 'SI', 1),
(3284, 213, 'Tunapuna/Piarco', 'TP', 1),
(3285, 213, 'Port of Spain', 'PS', 1),
(3286, 213, 'San Fernando', 'SF', 1),
(3287, 213, 'Arima', 'AR', 1),
(3288, 213, 'Point Fortin', 'PF', 1),
(3289, 213, 'Chaguanas', 'CH', 1),
(3290, 213, 'Tobago', 'TO', 1),
(3291, 214, 'Ariana', 'AR', 1),
(3292, 214, 'Beja', 'BJ', 1),
(3293, 214, 'Ben Arous', 'BA', 1),
(3294, 214, 'Bizerte', 'BI', 1),
(3295, 214, 'Gabes', 'GB', 1),
(3296, 214, 'Gafsa', 'GF', 1),
(3297, 214, 'Jendouba', 'JE', 1),
(3298, 214, 'Kairouan', 'KR', 1),
(3299, 214, 'Kasserine', 'KS', 1),
(3300, 214, 'Kebili', 'KB', 1),
(3301, 214, 'Kef', 'KF', 1),
(3302, 214, 'Mahdia', 'MH', 1),
(3303, 214, 'Manouba', 'MN', 1),
(3304, 214, 'Medenine', 'ME', 1),
(3305, 214, 'Monastir', 'MO', 1),
(3306, 214, 'Nabeul', 'NA', 1),
(3307, 214, 'Sfax', 'SF', 1),
(3308, 214, 'Sidi', 'SD', 1),
(3309, 214, 'Siliana', 'SL', 1),
(3310, 214, 'Sousse', 'SO', 1),
(3311, 214, 'Tataouine', 'TA', 1),
(3312, 214, 'Tozeur', 'TO', 1),
(3313, 214, 'Tunis', 'TU', 1),
(3314, 214, 'Zaghouan', 'ZA', 1),
(3315, 215, 'Adana', 'ADA', 1),
(3316, 215, 'Adıyaman', 'ADI', 1),
(3317, 215, 'Afyonkarahisar', 'AFY', 1),
(3318, 215, 'Ağrı', 'AGR', 1),
(3319, 215, 'Aksaray', 'AKS', 1),
(3320, 215, 'Amasya', 'AMA', 1),
(3321, 215, 'Ankara', 'ANK', 1),
(3322, 215, 'Antalya', 'ANT', 1),
(3323, 215, 'Ardahan', 'ARD', 1),
(3324, 215, 'Artvin', 'ART', 1),
(3325, 215, 'Aydın', 'AYI', 1),
(3326, 215, 'Balıkesir', 'BAL', 1),
(3327, 215, 'Bartın', 'BAR', 1),
(3328, 215, 'Batman', 'BAT', 1),
(3329, 215, 'Bayburt', 'BAY', 1),
(3330, 215, 'Bilecik', 'BIL', 1),
(3331, 215, 'Bingöl', 'BIN', 1),
(3332, 215, 'Bitlis', 'BIT', 1),
(3333, 215, 'Bolu', 'BOL', 1),
(3334, 215, 'Burdur', 'BRD', 1),
(3335, 215, 'Bursa', 'BRS', 1),
(3336, 215, 'Çanakkale', 'CKL', 1),
(3337, 215, 'Çankırı', 'CKR', 1),
(3338, 215, 'Çorum', 'COR', 1),
(3339, 215, 'Denizli', 'DEN', 1),
(3340, 215, 'Diyarbakır', 'DIY', 1),
(3341, 215, 'Düzce', 'DUZ', 1),
(3342, 215, 'Edirne', 'EDI', 1),
(3343, 215, 'Elazığ', 'ELA', 1),
(3344, 215, 'Erzincan', 'EZC', 1),
(3345, 215, 'Erzurum', 'EZR', 1),
(3346, 215, 'Eskişehir', 'ESK', 1),
(3347, 215, 'Gaziantep', 'GAZ', 1),
(3348, 215, 'Giresun', 'GIR', 1),
(3349, 215, 'Gümüşhane', 'GMS', 1),
(3350, 215, 'Hakkari', 'HKR', 1),
(3351, 215, 'Hatay', 'HTY', 1),
(3352, 215, 'Iğdır', 'IGD', 1),
(3353, 215, 'Isparta', 'ISP', 1),
(3354, 215, 'İstanbul', 'IST', 1),
(3355, 215, 'İzmir', 'IZM', 1),
(3356, 215, 'Kahramanmaraş', 'KAH', 1),
(3357, 215, 'Karabük', 'KRB', 1),
(3358, 215, 'Karaman', 'KRM', 1),
(3359, 215, 'Kars', 'KRS', 1),
(3360, 215, 'Kastamonu', 'KAS', 1),
(3361, 215, 'Kayseri', 'KAY', 1),
(3362, 215, 'Kilis', 'KLS', 1),
(3363, 215, 'Kırıkkale', 'KRK', 1),
(3364, 215, 'Kırklareli', 'KLR', 1),
(3365, 215, 'Kırşehir', 'KRH', 1),
(3366, 215, 'Kocaeli', 'KOC', 1),
(3367, 215, 'Konya', 'KON', 1),
(3368, 215, 'Kütahya', 'KUT', 1),
(3369, 215, 'Malatya', 'MAL', 1),
(3370, 215, 'Manisa', 'MAN', 1),
(3371, 215, 'Mardin', 'MAR', 1),
(3372, 215, 'Mersin', 'MER', 1),
(3373, 215, 'Muğla', 'MUG', 1),
(3374, 215, 'Muş', 'MUS', 1),
(3375, 215, 'Nevşehir', 'NEV', 1),
(3376, 215, 'Niğde', 'NIG', 1),
(3377, 215, 'Ordu', 'ORD', 1),
(3378, 215, 'Osmaniye', 'OSM', 1),
(3379, 215, 'Rize', 'RIZ', 1),
(3380, 215, 'Sakarya', 'SAK', 1),
(3381, 215, 'Samsun', 'SAM', 1),
(3382, 215, 'Şanlıurfa', 'SAN', 1),
(3383, 215, 'Siirt', 'SII', 1),
(3384, 215, 'Sinop', 'SIN', 1),
(3385, 215, 'Şırnak', 'SIR', 1),
(3386, 215, 'Sivas', 'SIV', 1),
(3387, 215, 'Tekirdağ', 'TEL', 1),
(3388, 215, 'Tokat', 'TOK', 1),
(3389, 215, 'Trabzon', 'TRA', 1),
(3390, 215, 'Tunceli', 'TUN', 1),
(3391, 215, 'Uşak', 'USK', 1),
(3392, 215, 'Van', 'VAN', 1),
(3393, 215, 'Yalova', 'YAL', 1),
(3394, 215, 'Yozgat', 'YOZ', 1),
(3395, 215, 'Zonguldak', 'ZON', 1),
(3396, 216, 'Ahal Welayaty', 'A', 1),
(3397, 216, 'Balkan Welayaty', 'B', 1),
(3398, 216, 'Dashhowuz Welayaty', 'D', 1),
(3399, 216, 'Lebap Welayaty', 'L', 1),
(3400, 216, 'Mary Welayaty', 'M', 1),
(3401, 217, 'Ambergris Cays', 'AC', 1),
(3402, 217, 'Dellis Cay', 'DC', 1),
(3403, 217, 'French Cay', 'FC', 1),
(3404, 217, 'Little Water Cay', 'LW', 1),
(3405, 217, 'Parrot Cay', 'RC', 1),
(3406, 217, 'Pine Cay', 'PN', 1),
(3407, 217, 'Salt Cay', 'SL', 1),
(3408, 217, 'Grand Turk', 'GT', 1),
(3409, 217, 'South Caicos', 'SC', 1),
(3410, 217, 'East Caicos', 'EC', 1),
(3411, 217, 'Middle Caicos', 'MC', 1),
(3412, 217, 'North Caicos', 'NC', 1),
(3413, 217, 'Providenciales', 'PR', 1),
(3414, 217, 'West Caicos', 'WC', 1),
(3415, 218, 'Nanumanga', 'NMG', 1),
(3416, 218, 'Niulakita', 'NLK', 1),
(3417, 218, 'Niutao', 'NTO', 1),
(3418, 218, 'Funafuti', 'FUN', 1),
(3419, 218, 'Nanumea', 'NME', 1),
(3420, 218, 'Nui', 'NUI', 1),
(3421, 218, 'Nukufetau', 'NFT', 1),
(3422, 218, 'Nukulaelae', 'NLL', 1),
(3423, 218, 'Vaitupu', 'VAI', 1),
(3424, 219, 'Kalangala', 'KAL', 1),
(3425, 219, 'Kampala', 'KMP', 1),
(3426, 219, 'Kayunga', 'KAY', 1),
(3427, 219, 'Kiboga', 'KIB', 1),
(3428, 219, 'Luwero', 'LUW', 1),
(3429, 219, 'Masaka', 'MAS', 1),
(3430, 219, 'Mpigi', 'MPI', 1),
(3431, 219, 'Mubende', 'MUB', 1),
(3432, 219, 'Mukono', 'MUK', 1),
(3433, 219, 'Nakasongola', 'NKS', 1),
(3434, 219, 'Rakai', 'RAK', 1),
(3435, 219, 'Sembabule', 'SEM', 1),
(3436, 219, 'Wakiso', 'WAK', 1),
(3437, 219, 'Bugiri', 'BUG', 1),
(3438, 219, 'Busia', 'BUS', 1),
(3439, 219, 'Iganga', 'IGA', 1),
(3440, 219, 'Jinja', 'JIN', 1),
(3441, 219, 'Kaberamaido', 'KAB', 1),
(3442, 219, 'Kamuli', 'KML', 1),
(3443, 219, 'Kapchorwa', 'KPC', 1),
(3444, 219, 'Katakwi', 'KTK', 1),
(3445, 219, 'Kumi', 'KUM', 1),
(3446, 219, 'Mayuge', 'MAY', 1),
(3447, 219, 'Mbale', 'MBA', 1),
(3448, 219, 'Pallisa', 'PAL', 1),
(3449, 219, 'Sironko', 'SIR', 1),
(3450, 219, 'Soroti', 'SOR', 1),
(3451, 219, 'Tororo', 'TOR', 1),
(3452, 219, 'Adjumani', 'ADJ', 1),
(3453, 219, 'Apac', 'APC', 1),
(3454, 219, 'Arua', 'ARU', 1),
(3455, 219, 'Gulu', 'GUL', 1),
(3456, 219, 'Kitgum', 'KIT', 1),
(3457, 219, 'Kotido', 'KOT', 1),
(3458, 219, 'Lira', 'LIR', 1),
(3459, 219, 'Moroto', 'MRT', 1),
(3460, 219, 'Moyo', 'MOY', 1),
(3461, 219, 'Nakapiripirit', 'NAK', 1),
(3462, 219, 'Nebbi', 'NEB', 1),
(3463, 219, 'Pader', 'PAD', 1),
(3464, 219, 'Yumbe', 'YUM', 1),
(3465, 219, 'Bundibugyo', 'BUN', 1),
(3466, 219, 'Bushenyi', 'BSH', 1),
(3467, 219, 'Hoima', 'HOI', 1),
(3468, 219, 'Kabale', 'KBL', 1),
(3469, 219, 'Kabarole', 'KAR', 1),
(3470, 219, 'Kamwenge', 'KAM', 1),
(3471, 219, 'Kanungu', 'KAN', 1),
(3472, 219, 'Kasese', 'KAS', 1),
(3473, 219, 'Kibaale', 'KBA', 1),
(3474, 219, 'Kisoro', 'KIS', 1),
(3475, 219, 'Kyenjojo', 'KYE', 1),
(3476, 219, 'Masindi', 'MSN', 1),
(3477, 219, 'Mbarara', 'MBR', 1),
(3478, 219, 'Ntungamo', 'NTU', 1),
(3479, 219, 'Rukungiri', 'RUK', 1),
(3480, 220, 'Cherkas\'ka Oblast\'', '71', 1),
(3481, 220, 'Chernihivs\'ka Oblast\'', '74', 1),
(3482, 220, 'Chernivets\'ka Oblast\'', '77', 1),
(3483, 220, 'Crimea', '43', 1),
(3484, 220, 'Dnipropetrovs\'ka Oblast\'', '12', 1),
(3485, 220, 'Donets\'ka Oblast\'', '14', 1),
(3486, 220, 'Ivano-Frankivs\'ka Oblast\'', '26', 1),
(3487, 220, 'Khersons\'ka Oblast\'', '65', 1),
(3488, 220, 'Khmel\'nyts\'ka Oblast\'', '68', 1),
(3489, 220, 'Kirovohrads\'ka Oblast\'', '35', 1),
(3490, 220, 'Kyiv', '30', 1),
(3491, 220, 'Kyivs\'ka Oblast\'', '32', 1),
(3492, 220, 'Luhans\'ka Oblast\'', '09', 1),
(3493, 220, 'L\'vivs\'ka Oblast\'', '46', 1),
(3494, 220, 'Mykolayivs\'ka Oblast\'', '48', 1),
(3495, 220, 'Odes\'ka Oblast\'', '51', 1),
(3496, 220, 'Poltavs\'ka Oblast\'', '53', 1),
(3497, 220, 'Rivnens\'ka Oblast\'', '56', 1),
(3498, 220, 'Sevastopol\'', '40', 1),
(3499, 220, 'Sums\'ka Oblast\'', '59', 1),
(3500, 220, 'Ternopil\'s\'ka Oblast\'', '61', 1),
(3501, 220, 'Vinnyts\'ka Oblast\'', '05', 1),
(3502, 220, 'Volyns\'ka Oblast\'', '07', 1),
(3503, 220, 'Zakarpats\'ka Oblast\'', '21', 1),
(3504, 220, 'Zaporiz\'ka Oblast\'', '23', 1),
(3505, 220, 'Zhytomyrs\'ka oblast\'', '18', 1),
(3506, 221, 'Abu Dhabi', 'ADH', 1),
(3507, 221, '\'Ajman', 'AJ', 1),
(3508, 221, 'Al Fujayrah', 'FU', 1),
(3509, 221, 'Ash Shariqah', 'SH', 1),
(3510, 221, 'Dubai', 'DU', 1),
(3511, 221, 'R\'as al Khaymah', 'RK', 1),
(3512, 221, 'Umm al Qaywayn', 'UQ', 1),
(3513, 222, 'Aberdeen', 'ABN', 1),
(3514, 222, 'Aberdeenshire', 'ABNS', 1),
(3515, 222, 'Anglesey', 'ANG', 1),
(3516, 222, 'Angus', 'AGS', 1),
(3517, 222, 'Argyll and Bute', 'ARY', 1),
(3518, 222, 'Bedfordshire', 'BEDS', 1),
(3519, 222, 'Berkshire', 'BERKS', 1),
(3520, 222, 'Blaenau Gwent', 'BLA', 1),
(3521, 222, 'Bridgend', 'BRI', 1),
(3522, 222, 'Bristol', 'BSTL', 1),
(3523, 222, 'Buckinghamshire', 'BUCKS', 1),
(3524, 222, 'Caerphilly', 'CAE', 1),
(3525, 222, 'Cambridgeshire', 'CAMBS', 1),
(3526, 222, 'Cardiff', 'CDF', 1),
(3527, 222, 'Carmarthenshire', 'CARM', 1),
(3528, 222, 'Ceredigion', 'CDGN', 1),
(3529, 222, 'Cheshire', 'CHES', 1),
(3530, 222, 'Clackmannanshire', 'CLACK', 1),
(3531, 222, 'Conwy', 'CON', 1),
(3532, 222, 'Cornwall', 'CORN', 1),
(3533, 222, 'Denbighshire', 'DNBG', 1),
(3534, 222, 'Derbyshire', 'DERBY', 1),
(3535, 222, 'Devon', 'DVN', 1),
(3536, 222, 'Dorset', 'DOR', 1),
(3537, 222, 'Dumfries and Galloway', 'DGL', 1),
(3538, 222, 'Dundee', 'DUND', 1),
(3539, 222, 'Durham', 'DHM', 1),
(3540, 222, 'East Ayrshire', 'ARYE', 1),
(3541, 222, 'East Dunbartonshire', 'DUNBE', 1),
(3542, 222, 'East Lothian', 'LOTE', 1),
(3543, 222, 'East Renfrewshire', 'RENE', 1),
(3544, 222, 'East Riding of Yorkshire', 'ERYS', 1),
(3545, 222, 'East Sussex', 'SXE', 1),
(3546, 222, 'Edinburgh', 'EDIN', 1),
(3547, 222, 'Essex', 'ESX', 1),
(3548, 222, 'Falkirk', 'FALK', 1),
(3549, 222, 'Fife', 'FFE', 1),
(3550, 222, 'Flintshire', 'FLINT', 1),
(3551, 222, 'Glasgow', 'GLAS', 1),
(3552, 222, 'Gloucestershire', 'GLOS', 1),
(3553, 222, 'Greater London', 'LDN', 1),
(3554, 222, 'Greater Manchester', 'MCH', 1),
(3555, 222, 'Gwynedd', 'GDD', 1),
(3556, 222, 'Hampshire', 'HANTS', 1),
(3557, 222, 'Herefordshire', 'HWR', 1),
(3558, 222, 'Hertfordshire', 'HERTS', 1),
(3559, 222, 'Highlands', 'HLD', 1),
(3560, 222, 'Inverclyde', 'IVER', 1),
(3561, 222, 'Isle of Wight', 'IOW', 1),
(3562, 222, 'Kent', 'KNT', 1),
(3563, 222, 'Lancashire', 'LANCS', 1),
(3564, 222, 'Leicestershire', 'LEICS', 1),
(3565, 222, 'Lincolnshire', 'LINCS', 1),
(3566, 222, 'Merseyside', 'MSY', 1),
(3567, 222, 'Merthyr Tydfil', 'MERT', 1),
(3568, 222, 'Midlothian', 'MLOT', 1),
(3569, 222, 'Monmouthshire', 'MMOUTH', 1),
(3570, 222, 'Moray', 'MORAY', 1),
(3571, 222, 'Neath Port Talbot', 'NPRTAL', 1),
(3572, 222, 'Newport', 'NEWPT', 1),
(3573, 222, 'Norfolk', 'NOR', 1),
(3574, 222, 'North Ayrshire', 'ARYN', 1),
(3575, 222, 'North Lanarkshire', 'LANN', 1),
(3576, 222, 'North Yorkshire', 'YSN', 1),
(3577, 222, 'Northamptonshire', 'NHM', 1),
(3578, 222, 'Northumberland', 'NLD', 1),
(3579, 222, 'Nottinghamshire', 'NOT', 1),
(3580, 222, 'Orkney Islands', 'ORK', 1),
(3581, 222, 'Oxfordshire', 'OFE', 1),
(3582, 222, 'Pembrokeshire', 'PEM', 1),
(3583, 222, 'Perth and Kinross', 'PERTH', 1),
(3584, 222, 'Powys', 'PWS', 1),
(3585, 222, 'Renfrewshire', 'REN', 1),
(3586, 222, 'Rhondda Cynon Taff', 'RHON', 1),
(3587, 222, 'Rutland', 'RUT', 1),
(3588, 222, 'Scottish Borders', 'BOR', 1),
(3589, 222, 'Shetland Islands', 'SHET', 1),
(3590, 222, 'Shropshire', 'SPE', 1),
(3591, 222, 'Somerset', 'SOM', 1),
(3592, 222, 'South Ayrshire', 'ARYS', 1),
(3593, 222, 'South Lanarkshire', 'LANS', 1),
(3594, 222, 'South Yorkshire', 'YSS', 1),
(3595, 222, 'Staffordshire', 'SFD', 1),
(3596, 222, 'Stirling', 'STIR', 1),
(3597, 222, 'Suffolk', 'SFK', 1),
(3598, 222, 'Surrey', 'SRY', 1),
(3599, 222, 'Swansea', 'SWAN', 1),
(3600, 222, 'Torfaen', 'TORF', 1),
(3601, 222, 'Tyne and Wear', 'TWR', 1),
(3602, 222, 'Vale of Glamorgan', 'VGLAM', 1),
(3603, 222, 'Warwickshire', 'WARKS', 1),
(3604, 222, 'West Dunbartonshire', 'WDUN', 1),
(3605, 222, 'West Lothian', 'WLOT', 1),
(3606, 222, 'West Midlands', 'WMD', 1),
(3607, 222, 'West Sussex', 'SXW', 1),
(3608, 222, 'West Yorkshire', 'YSW', 1),
(3609, 222, 'Western Isles', 'WIL', 1),
(3610, 222, 'Wiltshire', 'WLT', 1),
(3611, 222, 'Worcestershire', 'WORCS', 1),
(3612, 222, 'Wrexham', 'WRX', 1),
(3613, 223, 'Alabama', 'AL', 1),
(3614, 223, 'Alaska', 'AK', 1),
(3615, 223, 'American Samoa', 'AS', 1),
(3616, 223, 'Arizona', 'AZ', 1),
(3617, 223, 'Arkansas', 'AR', 1),
(3618, 223, 'Armed Forces Africa', 'AF', 1),
(3619, 223, 'Armed Forces Americas', 'AA', 1),
(3620, 223, 'Armed Forces Canada', 'AC', 1),
(3621, 223, 'Armed Forces Europe', 'AE', 1),
(3622, 223, 'Armed Forces Middle East', 'AM', 1),
(3623, 223, 'Armed Forces Pacific', 'AP', 1),
(3624, 223, 'California', 'CA', 1),
(3625, 223, 'Colorado', 'CO', 1),
(3626, 223, 'Connecticut', 'CT', 1),
(3627, 223, 'Delaware', 'DE', 1),
(3628, 223, 'District of Columbia', 'DC', 1),
(3629, 223, 'Federated States Of Micronesia', 'FM', 1),
(3630, 223, 'Florida', 'FL', 1),
(3631, 223, 'Georgia', 'GA', 1),
(3632, 223, 'Guam', 'GU', 1),
(3633, 223, 'Hawaii', 'HI', 1),
(3634, 223, 'Idaho', 'ID', 1),
(3635, 223, 'Illinois', 'IL', 1),
(3636, 223, 'Indiana', 'IN', 1),
(3637, 223, 'Iowa', 'IA', 1),
(3638, 223, 'Kansas', 'KS', 1),
(3639, 223, 'Kentucky', 'KY', 1),
(3640, 223, 'Louisiana', 'LA', 1),
(3641, 223, 'Maine', 'ME', 1),
(3642, 223, 'Marshall Islands', 'MH', 1),
(3643, 223, 'Maryland', 'MD', 1),
(3644, 223, 'Massachusetts', 'MA', 1),
(3645, 223, 'Michigan', 'MI', 1),
(3646, 223, 'Minnesota', 'MN', 1),
(3647, 223, 'Mississippi', 'MS', 1),
(3648, 223, 'Missouri', 'MO', 1),
(3649, 223, 'Montana', 'MT', 1),
(3650, 223, 'Nebraska', 'NE', 1),
(3651, 223, 'Nevada', 'NV', 1),
(3652, 223, 'New Hampshire', 'NH', 1),
(3653, 223, 'New Jersey', 'NJ', 1),
(3654, 223, 'New Mexico', 'NM', 1),
(3655, 223, 'New York', 'NY', 1),
(3656, 223, 'North Carolina', 'NC', 1),
(3657, 223, 'North Dakota', 'ND', 1),
(3658, 223, 'Northern Mariana Islands', 'MP', 1),
(3659, 223, 'Ohio', 'OH', 1),
(3660, 223, 'Oklahoma', 'OK', 1),
(3661, 223, 'Oregon', 'OR', 1),
(3662, 223, 'Palau', 'PW', 1),
(3663, 223, 'Pennsylvania', 'PA', 1),
(3664, 223, 'Puerto Rico', 'PR', 1),
(3665, 223, 'Rhode Island', 'RI', 1),
(3666, 223, 'South Carolina', 'SC', 1),
(3667, 223, 'South Dakota', 'SD', 1),
(3668, 223, 'Tennessee', 'TN', 1),
(3669, 223, 'Texas', 'TX', 1),
(3670, 223, 'Utah', 'UT', 1),
(3671, 223, 'Vermont', 'VT', 1),
(3672, 223, 'Virgin Islands', 'VI', 1),
(3673, 223, 'Virginia', 'VA', 1),
(3674, 223, 'Washington', 'WA', 1),
(3675, 223, 'West Virginia', 'WV', 1),
(3676, 223, 'Wisconsin', 'WI', 1),
(3677, 223, 'Wyoming', 'WY', 1),
(3678, 224, 'Baker Island', 'BI', 1),
(3679, 224, 'Howland Island', 'HI', 1),
(3680, 224, 'Jarvis Island', 'JI', 1),
(3681, 224, 'Johnston Atoll', 'JA', 1),
(3682, 224, 'Kingman Reef', 'KR', 1),
(3683, 224, 'Midway Atoll', 'MA', 1),
(3684, 224, 'Navassa Island', 'NI', 1),
(3685, 224, 'Palmyra Atoll', 'PA', 1),
(3686, 224, 'Wake Island', 'WI', 1),
(3687, 225, 'Artigas', 'AR', 1),
(3688, 225, 'Canelones', 'CA', 1),
(3689, 225, 'Cerro Largo', 'CL', 1),
(3690, 225, 'Colonia', 'CO', 1),
(3691, 225, 'Durazno', 'DU', 1),
(3692, 225, 'Flores', 'FS', 1),
(3693, 225, 'Florida', 'FA', 1),
(3694, 225, 'Lavalleja', 'LA', 1),
(3695, 225, 'Maldonado', 'MA', 1),
(3696, 225, 'Montevideo', 'MO', 1),
(3697, 225, 'Paysandu', 'PA', 1),
(3698, 225, 'Rio Negro', 'RN', 1),
(3699, 225, 'Rivera', 'RV', 1),
(3700, 225, 'Rocha', 'RO', 1),
(3701, 225, 'Salto', 'SL', 1),
(3702, 225, 'San Jose', 'SJ', 1),
(3703, 225, 'Soriano', 'SO', 1),
(3704, 225, 'Tacuarembo', 'TA', 1),
(3705, 225, 'Treinta y Tres', 'TT', 1),
(3706, 226, 'Andijon', 'AN', 1),
(3707, 226, 'Buxoro', 'BU', 1),
(3708, 226, 'Farg\'ona', 'FA', 1),
(3709, 226, 'Jizzax', 'JI', 1),
(3710, 226, 'Namangan', 'NG', 1),
(3711, 226, 'Navoiy', 'NW', 1),
(3712, 226, 'Qashqadaryo', 'QA', 1),
(3713, 226, 'Qoraqalpog\'iston Republikasi', 'QR', 1),
(3714, 226, 'Samarqand', 'SA', 1),
(3715, 226, 'Sirdaryo', 'SI', 1),
(3716, 226, 'Surxondaryo', 'SU', 1),
(3717, 226, 'Toshkent City', 'TK', 1),
(3718, 226, 'Toshkent Region', 'TO', 1),
(3719, 226, 'Xorazm', 'XO', 1),
(3720, 227, 'Malampa', 'MA', 1),
(3721, 227, 'Penama', 'PE', 1),
(3722, 227, 'Sanma', 'SA', 1),
(3723, 227, 'Shefa', 'SH', 1),
(3724, 227, 'Tafea', 'TA', 1),
(3725, 227, 'Torba', 'TO', 1),
(3726, 229, 'Amazonas', 'AM', 1),
(3727, 229, 'Anzoategui', 'AN', 1),
(3728, 229, 'Apure', 'AP', 1),
(3729, 229, 'Aragua', 'AR', 1),
(3730, 229, 'Barinas', 'BA', 1),
(3731, 229, 'Bolivar', 'BO', 1),
(3732, 229, 'Carabobo', 'CA', 1),
(3733, 229, 'Cojedes', 'CO', 1),
(3734, 229, 'Delta Amacuro', 'DA', 1),
(3735, 229, 'Dependencias Federales', 'DF', 1),
(3736, 229, 'Distrito Federal', 'DI', 1),
(3737, 229, 'Falcon', 'FA', 1),
(3738, 229, 'Guarico', 'GU', 1),
(3739, 229, 'Lara', 'LA', 1),
(3740, 229, 'Merida', 'ME', 1),
(3741, 229, 'Miranda', 'MI', 1),
(3742, 229, 'Monagas', 'MO', 1),
(3743, 229, 'Nueva Esparta', 'NE', 1),
(3744, 229, 'Portuguesa', 'PO', 1),
(3745, 229, 'Sucre', 'SU', 1),
(3746, 229, 'Tachira', 'TA', 1),
(3747, 229, 'Trujillo', 'TR', 1),
(3748, 229, 'Vargas', 'VA', 1),
(3749, 229, 'Yaracuy', 'YA', 1),
(3750, 229, 'Zulia', 'ZU', 1),
(3751, 230, 'An Giang', 'AG', 1),
(3752, 230, 'Bac Giang', 'BG', 1),
(3753, 230, 'Bac Kan', 'BK', 1),
(3754, 230, 'Bac Lieu', 'BL', 1),
(3755, 230, 'Bac Ninh', 'BC', 1),
(3756, 230, 'Ba Ria-Vung Tau', 'BR', 1),
(3757, 230, 'Ben Tre', 'BN', 1),
(3758, 230, 'Binh Dinh', 'BH', 1),
(3759, 230, 'Binh Duong', 'BU', 1),
(3760, 230, 'Binh Phuoc', 'BP', 1),
(3761, 230, 'Binh Thuan', 'BT', 1),
(3762, 230, 'Ca Mau', 'CM', 1),
(3763, 230, 'Can Tho', 'CT', 1),
(3764, 230, 'Cao Bang', 'CB', 1),
(3765, 230, 'Dak Lak', 'DL', 1),
(3766, 230, 'Dak Nong', 'DG', 1),
(3767, 230, 'Da Nang', 'DN', 1),
(3768, 230, 'Dien Bien', 'DB', 1),
(3769, 230, 'Dong Nai', 'DI', 1),
(3770, 230, 'Dong Thap', 'DT', 1),
(3771, 230, 'Gia Lai', 'GL', 1),
(3772, 230, 'Ha Giang', 'HG', 1),
(3773, 230, 'Hai Duong', 'HD', 1),
(3774, 230, 'Hai Phong', 'HP', 1),
(3775, 230, 'Ha Nam', 'HM', 1),
(3776, 230, 'Ha Noi', 'HI', 1),
(3777, 230, 'Ha Tay', 'HT', 1),
(3778, 230, 'Ha Tinh', 'HH', 1),
(3779, 230, 'Hoa Binh', 'HB', 1),
(3780, 230, 'Ho Chi Minh City', 'HC', 1),
(3781, 230, 'Hau Giang', 'HU', 1),
(3782, 230, 'Hung Yen', 'HY', 1),
(3783, 232, 'Saint Croix', 'C', 1),
(3784, 232, 'Saint John', 'J', 1),
(3785, 232, 'Saint Thomas', 'T', 1),
(3786, 233, 'Alo', 'A', 1),
(3787, 233, 'Sigave', 'S', 1),
(3788, 233, 'Wallis', 'W', 1),
(3789, 235, 'Abyan', 'AB', 1),
(3790, 235, 'Adan', 'AD', 1),
(3791, 235, 'Amran', 'AM', 1),
(3792, 235, 'Al Bayda', 'BA', 1),
(3793, 235, 'Ad Dali', 'DA', 1),
(3794, 235, 'Dhamar', 'DH', 1),
(3795, 235, 'Hadramawt', 'HD', 1),
(3796, 235, 'Hajjah', 'HJ', 1),
(3797, 235, 'Al Hudaydah', 'HU', 1),
(3798, 235, 'Ibb', 'IB', 1),
(3799, 235, 'Al Jawf', 'JA', 1),
(3800, 235, 'Lahij', 'LA', 1),
(3801, 235, 'Ma\'rib', 'MA', 1),
(3802, 235, 'Al Mahrah', 'MR', 1),
(3803, 235, 'Al Mahwit', 'MW', 1),
(3804, 235, 'Sa\'dah', 'SD', 1),
(3805, 235, 'San\'a', 'SN', 1),
(3806, 235, 'Shabwah', 'SH', 1),
(3807, 235, 'Ta\'izz', 'TA', 1),
(3812, 237, 'Bas-Congo', 'BC', 1),
(3813, 237, 'Bandundu', 'BN', 1),
(3814, 237, 'Equateur', 'EQ', 1),
(3815, 237, 'Katanga', 'KA', 1),
(3816, 237, 'Kasai-Oriental', 'KE', 1),
(3817, 237, 'Kinshasa', 'KN', 1),
(3818, 237, 'Kasai-Occidental', 'KW', 1),
(3819, 237, 'Maniema', 'MA', 1),
(3820, 237, 'Nord-Kivu', 'NK', 1),
(3821, 237, 'Orientale', 'OR', 1),
(3822, 237, 'Sud-Kivu', 'SK', 1),
(3823, 238, 'Central', 'CE', 1),
(3824, 238, 'Copperbelt', 'CB', 1),
(3825, 238, 'Eastern', 'EA', 1),
(3826, 238, 'Luapula', 'LP', 1),
(3827, 238, 'Lusaka', 'LK', 1),
(3828, 238, 'Northern', 'NO', 1),
(3829, 238, 'North-Western', 'NW', 1),
(3830, 238, 'Southern', 'SO', 1),
(3831, 238, 'Western', 'WE', 1),
(3832, 239, 'Bulawayo', 'BU', 1),
(3833, 239, 'Harare', 'HA', 1),
(3834, 239, 'Manicaland', 'ML', 1),
(3835, 239, 'Mashonaland Central', 'MC', 1),
(3836, 239, 'Mashonaland East', 'ME', 1),
(3837, 239, 'Mashonaland West', 'MW', 1),
(3838, 239, 'Masvingo', 'MV', 1),
(3839, 239, 'Matabeleland North', 'MN', 1),
(3840, 239, 'Matabeleland South', 'MS', 1),
(3841, 239, 'Midlands', 'MD', 1),
(3861, 105, 'Campobasso', 'CB', 1),
(3863, 105, 'Caserta', 'CE', 1),
(3864, 105, 'Catania', 'CT', 1),
(3865, 105, 'Catanzaro', 'CZ', 1),
(3866, 105, 'Chieti', 'CH', 1),
(3867, 105, 'Como', 'CO', 1),
(3868, 105, 'Cosenza', 'CS', 1),
(3869, 105, 'Cremona', 'CR', 1),
(3870, 105, 'Crotone', 'KR', 1),
(3871, 105, 'Cuneo', 'CN', 1),
(3872, 105, 'Enna', 'EN', 1),
(3873, 105, 'Ferrara', 'FE', 1),
(3874, 105, 'Firenze', 'FI', 1),
(3875, 105, 'Foggia', 'FG', 1),
(3876, 105, 'Forli-Cesena', 'FC', 1),
(3877, 105, 'Frosinone', 'FR', 1),
(3878, 105, 'Genova', 'GE', 1),
(3879, 105, 'Gorizia', 'GO', 1),
(3880, 105, 'Grosseto', 'GR', 1),
(3881, 105, 'Imperia', 'IM', 1),
(3882, 105, 'Isernia', 'IS', 1),
(3883, 105, 'L&#39;Aquila', 'AQ', 1),
(3884, 105, 'La Spezia', 'SP', 1),
(3885, 105, 'Latina', 'LT', 1),
(3886, 105, 'Lecce', 'LE', 1),
(3887, 105, 'Lecco', 'LC', 1),
(3888, 105, 'Livorno', 'LI', 1),
(3889, 105, 'Lodi', 'LO', 1),
(3890, 105, 'Lucca', 'LU', 1),
(3891, 105, 'Macerata', 'MC', 1),
(3892, 105, 'Mantova', 'MN', 1),
(3893, 105, 'Massa-Carrara', 'MS', 1),
(3894, 105, 'Matera', 'MT', 1),
(3896, 105, 'Messina', 'ME', 1),
(3897, 105, 'Milano', 'MI', 1),
(3898, 105, 'Modena', 'MO', 1),
(3899, 105, 'Napoli', 'NA', 1),
(3900, 105, 'Novara', 'NO', 1),
(3901, 105, 'Nuoro', 'NU', 1),
(3904, 105, 'Oristano', 'OR', 1),
(3905, 105, 'Padova', 'PD', 1),
(3906, 105, 'Palermo', 'PA', 1),
(3907, 105, 'Parma', 'PR', 1),
(3908, 105, 'Pavia', 'PV', 1),
(3909, 105, 'Perugia', 'PG', 1),
(3910, 105, 'Pesaro e Urbino', 'PU', 1),
(3911, 105, 'Pescara', 'PE', 1),
(3912, 105, 'Piacenza', 'PC', 1),
(3913, 105, 'Pisa', 'PI', 1),
(3914, 105, 'Pistoia', 'PT', 1),
(3915, 105, 'Pordenone', 'PN', 1),
(3916, 105, 'Potenza', 'PZ', 1),
(3917, 105, 'Prato', 'PO', 1),
(3918, 105, 'Ragusa', 'RG', 1),
(3919, 105, 'Ravenna', 'RA', 1),
(3920, 105, 'Reggio Calabria', 'RC', 1),
(3921, 105, 'Reggio Emilia', 'RE', 1),
(3922, 105, 'Rieti', 'RI', 1),
(3923, 105, 'Rimini', 'RN', 1),
(3924, 105, 'Roma', 'RM', 1),
(3925, 105, 'Rovigo', 'RO', 1),
(3926, 105, 'Salerno', 'SA', 1),
(3927, 105, 'Sassari', 'SS', 1),
(3928, 105, 'Savona', 'SV', 1),
(3929, 105, 'Siena', 'SI', 1),
(3930, 105, 'Siracusa', 'SR', 1),
(3931, 105, 'Sondrio', 'SO', 1),
(3932, 105, 'Taranto', 'TA', 1),
(3933, 105, 'Teramo', 'TE', 1),
(3934, 105, 'Terni', 'TR', 1),
(3935, 105, 'Torino', 'TO', 1),
(3936, 105, 'Trapani', 'TP', 1),
(3937, 105, 'Trento', 'TN', 1),
(3938, 105, 'Treviso', 'TV', 1),
(3939, 105, 'Trieste', 'TS', 1),
(3940, 105, 'Udine', 'UD', 1),
(3941, 105, 'Varese', 'VA', 1),
(3942, 105, 'Venezia', 'VE', 1),
(3943, 105, 'Verbano-Cusio-Ossola', 'VB', 1),
(3944, 105, 'Vercelli', 'VC', 1),
(3945, 105, 'Verona', 'VR', 1),
(3946, 105, 'Vibo Valentia', 'VV', 1),
(3947, 105, 'Vicenza', 'VI', 1),
(3948, 105, 'Viterbo', 'VT', 1),
(3949, 222, 'County Antrim', 'ANT', 1),
(3950, 222, 'County Armagh', 'ARM', 1),
(3951, 222, 'County Down', 'DOW', 1),
(3952, 222, 'County Fermanagh', 'FER', 1),
(3953, 222, 'County Londonderry', 'LDY', 1),
(3954, 222, 'County Tyrone', 'TYR', 1),
(3955, 222, 'Cumbria', 'CMA', 1),
(3956, 190, 'Pomurska', '1', 1),
(3957, 190, 'Podravska', '2', 1),
(3958, 190, 'Koroška', '3', 1),
(3959, 190, 'Savinjska', '4', 1),
(3960, 190, 'Zasavska', '5', 1),
(3961, 190, 'Spodnjeposavska', '6', 1),
(3962, 190, 'Jugovzhodna Slovenija', '7', 1),
(3963, 190, 'Osrednjeslovenska', '8', 1),
(3964, 190, 'Gorenjska', '9', 1),
(3965, 190, 'Notranjsko-kraška', '10', 1),
(3966, 190, 'Goriška', '11', 1),
(3967, 190, 'Obalno-kraška', '12', 1),
(3968, 33, 'Ruse', '', 1),
(3969, 101, 'Alborz', 'ALB', 1),
(3970, 21, 'Brussels-Capital Region', 'BRU', 1),
(3971, 138, 'Aguascalientes', 'AG', 1),
(3973, 242, 'Andrijevica', '01', 1),
(3974, 242, 'Bar', '02', 1),
(3975, 242, 'Berane', '03', 1),
(3976, 242, 'Bijelo Polje', '04', 1),
(3977, 242, 'Budva', '05', 1),
(3978, 242, 'Cetinje', '06', 1),
(3979, 242, 'Danilovgrad', '07', 1),
(3980, 242, 'Herceg-Novi', '08', 1),
(3981, 242, 'Kolašin', '09', 1),
(3982, 242, 'Kotor', '10', 1),
(3983, 242, 'Mojkovac', '11', 1),
(3984, 242, 'Nikšić', '12', 1),
(3985, 242, 'Plav', '13', 1),
(3986, 242, 'Pljevlja', '14', 1),
(3987, 242, 'Plužine', '15', 1),
(3988, 242, 'Podgorica', '16', 1),
(3989, 242, 'Rožaje', '17', 1),
(3990, 242, 'Šavnik', '18', 1),
(3991, 242, 'Tivat', '19', 1),
(3992, 242, 'Ulcinj', '20', 1),
(3993, 242, 'Žabljak', '21', 1),
(3994, 243, 'Belgrade', '00', 1),
(3995, 243, 'North Bačka', '01', 1),
(3996, 243, 'Central Banat', '02', 1),
(3997, 243, 'North Banat', '03', 1),
(3998, 243, 'South Banat', '04', 1),
(3999, 243, 'West Bačka', '05', 1),
(4000, 243, 'South Bačka', '06', 1),
(4001, 243, 'Srem', '07', 1),
(4002, 243, 'Mačva', '08', 1),
(4003, 243, 'Kolubara', '09', 1),
(4004, 243, 'Podunavlje', '10', 1),
(4005, 243, 'Braničevo', '11', 1),
(4006, 243, 'Šumadija', '12', 1),
(4007, 243, 'Pomoravlje', '13', 1),
(4008, 243, 'Bor', '14', 1),
(4009, 243, 'Zaječar', '15', 1),
(4010, 243, 'Zlatibor', '16', 1),
(4011, 243, 'Moravica', '17', 1),
(4012, 243, 'Raška', '18', 1),
(4013, 243, 'Rasina', '19', 1),
(4014, 243, 'Nišava', '20', 1),
(4015, 243, 'Toplica', '21', 1),
(4016, 243, 'Pirot', '22', 1),
(4017, 243, 'Jablanica', '23', 1),
(4018, 243, 'Pčinja', '24', 1),
(4020, 245, 'Bonaire', 'BO', 1),
(4021, 245, 'Saba', 'SA', 1),
(4022, 245, 'Sint Eustatius', 'SE', 1),
(4023, 248, 'Central Equatoria', 'EC', 1),
(4024, 248, 'Eastern Equatoria', 'EE', 1),
(4025, 248, 'Jonglei', 'JG', 1),
(4026, 248, 'Lakes', 'LK', 1),
(4027, 248, 'Northern Bahr el-Ghazal', 'BN', 1),
(4028, 248, 'Unity', 'UY', 1),
(4029, 248, 'Upper Nile', 'NU', 1),
(4030, 248, 'Warrap', 'WR', 1),
(4031, 248, 'Western Bahr el-Ghazal', 'BW', 1),
(4032, 248, 'Western Equatoria', 'EW', 1),
(4036, 117, 'Ainaži, Salacgrīvas novads', '0661405', 1),
(4037, 117, 'Aizkraukle, Aizkraukles novads', '0320201', 1),
(4038, 117, 'Aizkraukles novads', '0320200', 1),
(4039, 117, 'Aizpute, Aizputes novads', '0640605', 1),
(4040, 117, 'Aizputes novads', '0640600', 1),
(4041, 117, 'Aknīste, Aknīstes novads', '0560805', 1),
(4042, 117, 'Aknīstes novads', '0560800', 1),
(4043, 117, 'Aloja, Alojas novads', '0661007', 1),
(4044, 117, 'Alojas novads', '0661000', 1),
(4045, 117, 'Alsungas novads', '0624200', 1),
(4046, 117, 'Alūksne, Alūksnes novads', '0360201', 1),
(4047, 117, 'Alūksnes novads', '0360200', 1),
(4048, 117, 'Amatas novads', '0424701', 1),
(4049, 117, 'Ape, Apes novads', '0360805', 1),
(4050, 117, 'Apes novads', '0360800', 1),
(4051, 117, 'Auce, Auces novads', '0460805', 1),
(4052, 117, 'Auces novads', '0460800', 1),
(4053, 117, 'Ādažu novads', '0804400', 1),
(4054, 117, 'Babītes novads', '0804900', 1),
(4055, 117, 'Baldone, Baldones novads', '0800605', 1),
(4056, 117, 'Baldones novads', '0800600', 1),
(4057, 117, 'Baloži, Ķekavas novads', '0800807', 1),
(4058, 117, 'Baltinavas novads', '0384400', 1),
(4059, 117, 'Balvi, Balvu novads', '0380201', 1),
(4060, 117, 'Balvu novads', '0380200', 1),
(4061, 117, 'Bauska, Bauskas novads', '0400201', 1),
(4062, 117, 'Bauskas novads', '0400200', 1),
(4063, 117, 'Beverīnas novads', '0964700', 1),
(4064, 117, 'Brocēni, Brocēnu novads', '0840605', 1),
(4065, 117, 'Brocēnu novads', '0840601', 1),
(4066, 117, 'Burtnieku novads', '0967101', 1),
(4067, 117, 'Carnikavas novads', '0805200', 1),
(4068, 117, 'Cesvaine, Cesvaines novads', '0700807', 1),
(4069, 117, 'Cesvaines novads', '0700800', 1),
(4070, 117, 'Cēsis, Cēsu novads', '0420201', 1),
(4071, 117, 'Cēsu novads', '0420200', 1),
(4072, 117, 'Ciblas novads', '0684901', 1),
(4073, 117, 'Dagda, Dagdas novads', '0601009', 1),
(4074, 117, 'Dagdas novads', '0601000', 1),
(4075, 117, 'Daugavpils', '0050000', 1),
(4076, 117, 'Daugavpils novads', '0440200', 1),
(4077, 117, 'Dobele, Dobeles novads', '0460201', 1),
(4078, 117, 'Dobeles novads', '0460200', 1),
(4079, 117, 'Dundagas novads', '0885100', 1),
(4080, 117, 'Durbe, Durbes novads', '0640807', 1),
(4081, 117, 'Durbes novads', '0640801', 1),
(4082, 117, 'Engures novads', '0905100', 1),
(4083, 117, 'Ērgļu novads', '0705500', 1),
(4084, 117, 'Garkalnes novads', '0806000', 1),
(4085, 117, 'Grobiņa, Grobiņas novads', '0641009', 1),
(4086, 117, 'Grobiņas novads', '0641000', 1),
(4087, 117, 'Gulbene, Gulbenes novads', '0500201', 1),
(4088, 117, 'Gulbenes novads', '0500200', 1),
(4089, 117, 'Iecavas novads', '0406400', 1),
(4090, 117, 'Ikšķile, Ikšķiles novads', '0740605', 1),
(4091, 117, 'Ikšķiles novads', '0740600', 1),
(4092, 117, 'Ilūkste, Ilūkstes novads', '0440807', 1),
(4093, 117, 'Ilūkstes novads', '0440801', 1),
(4094, 117, 'Inčukalna novads', '0801800', 1),
(4095, 117, 'Jaunjelgava, Jaunjelgavas novads', '0321007', 1),
(4096, 117, 'Jaunjelgavas novads', '0321000', 1),
(4097, 117, 'Jaunpiebalgas novads', '0425700', 1),
(4098, 117, 'Jaunpils novads', '0905700', 1),
(4099, 117, 'Jelgava', '0090000', 1),
(4100, 117, 'Jelgavas novads', '0540200', 1),
(4101, 117, 'Jēkabpils', '0110000', 1),
(4102, 117, 'Jēkabpils novads', '0560200', 1),
(4103, 117, 'Jūrmala', '0130000', 1),
(4104, 117, 'Kalnciems, Jelgavas novads', '0540211', 1),
(4105, 117, 'Kandava, Kandavas novads', '0901211', 1),
(4106, 117, 'Kandavas novads', '0901201', 1),
(4107, 117, 'Kārsava, Kārsavas novads', '0681009', 1),
(4108, 117, 'Kārsavas novads', '0681000', 1),
(4109, 117, 'Kocēnu novads ,bij. Valmieras)', '0960200', 1),
(4110, 117, 'Kokneses novads', '0326100', 1),
(4111, 117, 'Krāslava, Krāslavas novads', '0600201', 1),
(4112, 117, 'Krāslavas novads', '0600202', 1),
(4113, 117, 'Krimuldas novads', '0806900', 1),
(4114, 117, 'Krustpils novads', '0566900', 1),
(4115, 117, 'Kuldīga, Kuldīgas novads', '0620201', 1),
(4116, 117, 'Kuldīgas novads', '0620200', 1),
(4117, 117, 'Ķeguma novads', '0741001', 1),
(4118, 117, 'Ķegums, Ķeguma novads', '0741009', 1),
(4119, 117, 'Ķekavas novads', '0800800', 1),
(4120, 117, 'Lielvārde, Lielvārdes novads', '0741413', 1),
(4121, 117, 'Lielvārdes novads', '0741401', 1),
(4122, 117, 'Liepāja', '0170000', 1),
(4123, 117, 'Limbaži, Limbažu novads', '0660201', 1),
(4124, 117, 'Limbažu novads', '0660200', 1),
(4125, 117, 'Līgatne, Līgatnes novads', '0421211', 1),
(4126, 117, 'Līgatnes novads', '0421200', 1),
(4127, 117, 'Līvāni, Līvānu novads', '0761211', 1),
(4128, 117, 'Līvānu novads', '0761201', 1),
(4129, 117, 'Lubāna, Lubānas novads', '0701413', 1),
(4130, 117, 'Lubānas novads', '0701400', 1),
(4131, 117, 'Ludza, Ludzas novads', '0680201', 1),
(4132, 117, 'Ludzas novads', '0680200', 1),
(4133, 117, 'Madona, Madonas novads', '0700201', 1),
(4134, 117, 'Madonas novads', '0700200', 1),
(4135, 117, 'Mazsalaca, Mazsalacas novads', '0961011', 1),
(4136, 117, 'Mazsalacas novads', '0961000', 1),
(4137, 117, 'Mālpils novads', '0807400', 1),
(4138, 117, 'Mārupes novads', '0807600', 1),
(4139, 117, 'Mērsraga novads', '0887600', 1),
(4140, 117, 'Naukšēnu novads', '0967300', 1),
(4141, 117, 'Neretas novads', '0327100', 1),
(4142, 117, 'Nīcas novads', '0647900', 1),
(4143, 117, 'Ogre, Ogres novads', '0740201', 1),
(4144, 117, 'Ogres novads', '0740202', 1),
(4145, 117, 'Olaine, Olaines novads', '0801009', 1),
(4146, 117, 'Olaines novads', '0801000', 1),
(4147, 117, 'Ozolnieku novads', '0546701', 1),
(4148, 117, 'Pārgaujas novads', '0427500', 1),
(4149, 117, 'Pāvilosta, Pāvilostas novads', '0641413', 1),
(4150, 117, 'Pāvilostas novads', '0641401', 1),
(4151, 117, 'Piltene, Ventspils novads', '0980213', 1),
(4152, 117, 'Pļaviņas, Pļaviņu novads', '0321413', 1),
(4153, 117, 'Pļaviņu novads', '0321400', 1),
(4154, 117, 'Preiļi, Preiļu novads', '0760201', 1),
(4155, 117, 'Preiļu novads', '0760202', 1),
(4156, 117, 'Priekule, Priekules novads', '0641615', 1),
(4157, 117, 'Priekules novads', '0641600', 1),
(4158, 117, 'Priekuļu novads', '0427300', 1),
(4159, 117, 'Raunas novads', '0427700', 1),
(4160, 117, 'Rēzekne', '0210000', 1),
(4161, 117, 'Rēzeknes novads', '0780200', 1),
(4162, 117, 'Riebiņu novads', '0766300', 1),
(4163, 117, 'Rīga', '0010000', 1),
(4164, 117, 'Rojas novads', '0888300', 1),
(4165, 117, 'Ropažu novads', '0808400', 1),
(4166, 117, 'Rucavas novads', '0648500', 1),
(4167, 117, 'Rugāju novads', '0387500', 1),
(4168, 117, 'Rundāles novads', '0407700', 1),
(4169, 117, 'Rūjiena, Rūjienas novads', '0961615', 1),
(4170, 117, 'Rūjienas novads', '0961600', 1),
(4171, 117, 'Sabile, Talsu novads', '0880213', 1),
(4172, 117, 'Salacgrīva, Salacgrīvas novads', '0661415', 1),
(4173, 117, 'Salacgrīvas novads', '0661400', 1),
(4174, 117, 'Salas novads', '0568700', 1),
(4175, 117, 'Salaspils novads', '0801200', 1),
(4176, 117, 'Salaspils, Salaspils novads', '0801211', 1),
(4177, 117, 'Saldus novads', '0840200', 1),
(4178, 117, 'Saldus, Saldus novads', '0840201', 1),
(4179, 117, 'Saulkrasti, Saulkrastu novads', '0801413', 1),
(4180, 117, 'Saulkrastu novads', '0801400', 1),
(4181, 117, 'Seda, Strenču novads', '0941813', 1),
(4182, 117, 'Sējas novads', '0809200', 1),
(4183, 117, 'Sigulda, Siguldas novads', '0801615', 1),
(4184, 117, 'Siguldas novads', '0801601', 1),
(4185, 117, 'Skrīveru novads', '0328200', 1),
(4186, 117, 'Skrunda, Skrundas novads', '0621209', 1),
(4187, 117, 'Skrundas novads', '0621200', 1),
(4188, 117, 'Smiltene, Smiltenes novads', '0941615', 1),
(4189, 117, 'Smiltenes novads', '0941600', 1),
(4190, 117, 'Staicele, Alojas novads', '0661017', 1),
(4191, 117, 'Stende, Talsu novads', '0880215', 1),
(4192, 117, 'Stopiņu novads', '0809600', 1),
(4193, 117, 'Strenči, Strenču novads', '0941817', 1),
(4194, 117, 'Strenču novads', '0941800', 1),
(4195, 117, 'Subate, Ilūkstes novads', '0440815', 1),
(4196, 117, 'Talsi, Talsu novads', '0880201', 1),
(4197, 117, 'Talsu novads', '0880200', 1),
(4198, 117, 'Tērvetes novads', '0468900', 1),
(4199, 117, 'Tukuma novads', '0900200', 1),
(4200, 117, 'Tukums, Tukuma novads', '0900201', 1),
(4201, 117, 'Vaiņodes novads', '0649300', 1),
(4202, 117, 'Valdemārpils, Talsu novads', '0880217', 1),
(4203, 117, 'Valka, Valkas novads', '0940201', 1),
(4204, 117, 'Valkas novads', '0940200', 1),
(4205, 117, 'Valmiera', '0250000', 1),
(4206, 117, 'Vangaži, Inčukalna novads', '0801817', 1),
(4207, 117, 'Varakļāni, Varakļānu novads', '0701817', 1),
(4208, 117, 'Varakļānu novads', '0701800', 1),
(4209, 117, 'Vārkavas novads', '0769101', 1),
(4210, 117, 'Vecpiebalgas novads', '0429300', 1),
(4211, 117, 'Vecumnieku novads', '0409500', 1),
(4212, 117, 'Ventspils', '0270000', 1),
(4213, 117, 'Ventspils novads', '0980200', 1),
(4214, 117, 'Viesīte, Viesītes novads', '0561815', 1),
(4215, 117, 'Viesītes novads', '0561800', 1),
(4216, 117, 'Viļaka, Viļakas novads', '0381615', 1),
(4217, 117, 'Viļakas novads', '0381600', 1),
(4218, 117, 'Viļāni, Viļānu novads', '0781817', 1),
(4219, 117, 'Viļānu novads', '0781800', 1),
(4220, 117, 'Zilupe, Zilupes novads', '0681817', 1),
(4221, 117, 'Zilupes novads', '0681801', 1),
(4222, 43, 'Arica y Parinacota', 'AP', 1),
(4223, 43, 'Los Rios', 'LR', 1),
(4224, 220, 'Kharkivs\'ka Oblast\'', '63', 1),
(4225, 118, 'Beirut', 'LB-BR', 1),
(4226, 118, 'Bekaa', 'LB-BE', 1),
(4227, 118, 'Mount Lebanon', 'LB-ML', 1),
(4228, 118, 'Nabatieh', 'LB-NB', 1),
(4229, 118, 'North', 'LB-NR', 1),
(4230, 118, 'South', 'LB-ST', 1),
(4231, 99, 'Telangana', 'TS', 1),
(4232, 44, 'Qinghai', 'QH', 1),
(4233, 100, 'Papua Barat', 'PB', 1),
(4234, 100, 'Sulawesi Barat', 'SR', 1),
(4235, 100, 'Kepulauan Riau', 'KR', 1),
(4236, 105, 'Barletta-Andria-Trani', 'BT', 1),
(4237, 105, 'Fermo', 'FM', 1),
(4238, 105, 'Monza Brianza', 'MB', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_zone_to_geo_zone`
--

DROP TABLE IF EXISTS `oc_zone_to_geo_zone`;
CREATE TABLE IF NOT EXISTS `oc_zone_to_geo_zone` (
  `zone_to_geo_zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `geo_zone_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`zone_to_geo_zone_id`)
) ENGINE=MyISAM AUTO_INCREMENT=110 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_zone_to_geo_zone`
--

INSERT INTO `oc_zone_to_geo_zone` (`zone_to_geo_zone_id`, `country_id`, `zone_id`, `geo_zone_id`, `date_added`, `date_modified`) VALUES
(1, 222, 0, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 222, 3513, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 222, 3514, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 222, 3515, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 222, 3516, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 222, 3517, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 222, 3518, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 222, 3519, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 222, 3520, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 222, 3521, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 222, 3522, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 222, 3523, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 222, 3524, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 222, 3525, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 222, 3526, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 222, 3527, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 222, 3528, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 222, 3529, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 222, 3530, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 222, 3531, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 222, 3532, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 222, 3533, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 222, 3534, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 222, 3535, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 222, 3536, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 222, 3537, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 222, 3538, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 222, 3539, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 222, 3540, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 222, 3541, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 222, 3542, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 222, 3543, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 222, 3544, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 222, 3545, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 222, 3546, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 222, 3547, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 222, 3548, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 222, 3549, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 222, 3550, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 222, 3551, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 222, 3552, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 222, 3553, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 222, 3554, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 222, 3555, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 222, 3556, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 222, 3557, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 222, 3558, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 222, 3559, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 222, 3560, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 222, 3561, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 222, 3562, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 222, 3563, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 222, 3564, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 222, 3565, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 222, 3566, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 222, 3567, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 222, 3568, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 222, 3569, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 222, 3570, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 222, 3571, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 222, 3572, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 222, 3573, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 222, 3574, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 222, 3575, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 222, 3576, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 222, 3577, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 222, 3578, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 222, 3579, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 222, 3580, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 222, 3581, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 222, 3582, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 222, 3583, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 222, 3584, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 222, 3585, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 222, 3586, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 222, 3587, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 222, 3588, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 222, 3589, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 222, 3590, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 222, 3591, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 222, 3592, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 222, 3593, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 222, 3594, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 222, 3595, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 222, 3596, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 222, 3597, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 222, 3598, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 222, 3599, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 222, 3600, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 222, 3601, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 222, 3602, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 222, 3603, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 222, 3604, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 222, 3605, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 222, 3606, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 222, 3607, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 222, 3608, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 222, 3609, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 222, 3610, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 222, 3611, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 222, 3612, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 222, 3949, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 222, 3950, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 222, 3951, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 222, 3952, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 222, 3953, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 222, 3954, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 222, 3955, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 99, 1505, 5, '2020-02-15 18:23:44', '0000-00-00 00:00:00');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
