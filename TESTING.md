# STORE:




# ISSUES:
### ADMIN:
1. Unable to disable a user because password field is mandatory.
2. Unable to Add Address for a user because password field is mandatory.

### ADMIN UI:

### STORE UI:
1. Add something in cart and click on Bag icon, items not displayed properly.
2. Hover over a category in main menu having some sub-menu, the green hover effect is overflowing.

### STORE:
1. Login using google/fb, go and edit your emai address, next time you login another account gets created. It should not be editable for such users. [I have created two Customer categories from Admin - Fb users, google users - use it and restrict editing].
2. Don't allow fb/google users to change/set their password.

# RESOLVED:
### ADMIN:

### ADMIN UI:

### STORE UI:

### STORE:

# COMPLETED

1. Login
2. Logout
3. Register
4. Place an order.
5. Login using Google
6. Search on Home page.
7. Validate links in footer - sitemap, return policy, etc.
8. Add to Wishlist.
9. Share/like on fb, twitter from a product details page.
10. Order history page should not be accessible without login.
11. Enable Recurring order functionality.(from order detail screen able to add to cart same product)
12. Forget password
13. Test Order submission, and order completion , new user registration with Mailserver on.
14. Remove "Warning:" and "Error:" from all error/warning messages. (only from frontend removed; backend is ok)
15. Admin is able to enter negative price for a product. Allow greater than 0 values.
16. Enable Who's Online functionality. (done)
17. Enable Coupon using functionality. (done)
18. Create/buy logo.
19. http://localhost/theorderapp/index.php?route=checkout/checkout&language=en-gb   (for non logged in user force to login page)
20. orders if present for that customer dont delete that customer
21. Remove IP Address from email sent to user on successful order placement (rename Telephone to Phone Number).
22. Rename Date Added to Ordered Date in the email sent to user when order is cancelled by Admin.
23. On Product Returns page, order id, name, product quantity, etc. are editable. This is wrong.
24. On Product Return page "Please complete the form below to request an RMA number." message needs to be changed.
25. On checkout, after Step1, Step3 comes, numbering needs be changed.
26. A new password was requested for <strong>Grocery Saver</strong> customer account. Correct this message.
27. Flat shipping in Delivery should not be shown when order is eligible for free shipping
28. Product return functionality not working.
29. After clicking "Add To Cart", the button is disabled and keeps showing "Loading...".
30. We deliver only in selected area of Deoria. In case of wrong address order might get cancelled.
31. save search term of customers
32. For Available coupons mention text that only one coupon can be applied per order.
33. Change the message - "Success: You have modified your shopping cart!" - this message comes when an item quantity is updated on cart page (http://localhost/theorderapp/index.php?route=checkout/cart&language=en-gb)
New message: "Shopping cart updated!"
34. When a user creates account with mobile number and then tries to login, he is not able to. It seems for login only email is being checked. (done now email/phone number both can be used with password to login)
35. get free delivery for order > 1000 (make coupon free shipping and other discount for that coupon make 0 to make only shipping charge free -- I have added proper coupon code name and setting for it) (we keep freeship coupon disabled)
36. FLAT5 - get discount 5% on checkout for order total > 2000 (show coupon available and terms on cart page)
37. Checkout Page: shipping charge not added in order total (You need to enable from here shipping to add shipping charge in total http://localhost/theorderapp/admin/index.php?route=extension/total/shipping&user_token=c5015b8a6dd6f69f45406924137dea2c )
38. How to know who cancelled the order - backend or customer did himself. (Cancelled by admin new order status added we can rename it and make it Rejected etc. and on order status change we can comment on order status change)
39. If order rejected will customer get email (be informed of reason)? (yes if u click notify user and change order status)
40. on checkout page, the "Use Coupon Code" is not obvious, you need to click on text to expand it. It should already come expanded. Show All available coupons too here.
41. After logout take to homepage
42. Display OrderId on order successful page. and link to take user to order detail page.
43. make address form minimal and easy
44. Remove "Add Comments About Your Order" from checkout page.
45. On Create Account page Continue button is still clickable when already account creation in progress
46. Admin side, Orders List show timestamp as well in Date Added and Date Modified column. Also add Added By and Modified By column.
47. Admin Side - Make Quantity, Minimum Quantity, and Price mandatory.
48. Remove Downloads, Transactions menu from "Hello {name}" menu when a user is already logged in. 
49. Add tawk.to like chat on contact us and my orders page.
50. Add (Call/Whatsapp) number in the header.
51. Payment Integration with Paytm, Google Pay, RazorPay.
52. Clicking on top right shopping cart icon does not show any pop-over.
53. Guest checkout remove.
54. Contact us page working : http://localhost/theorderapp/index.php?route=information/contact&language=en-gb
55. Setting Minimum Cart amount before a checkout is allowed.
56. SignIn with Google/Facebook (made login/register form minimal and edit account also test)
57. Enable Return order functionality. (return is working tested. but what if i want cancel whole order itself.)
58. Mail enable (I had to enable less secure app from gmail setting)
59. Add a return policy page.(in footer added; modify content as needed)
60. Not allow return option on order complete and also when a product is already in return process
61. Cancel Order option for customer. mail too sent. 
62. Enable search functionality on top right search icon, show max. 5 search results.